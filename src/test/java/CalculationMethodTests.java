import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import edu.kit.iti.formal.pse1.mandatsverteilung.calculation.CalculationMethodTest;
import edu.kit.iti.formal.pse1.mandatsverteilung.calculation.Distribution2013Test;

@RunWith(Suite.class)
/* @formatter:off */
@SuiteClasses({
    CalculationMethodTest.class,
    Distribution2013Test.class
})
/* @formatter:on */
public class CalculationMethodTests {
}
