import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
/* @formatter:off */
@SuiteClasses({
    GlobalTestCases.class,
    
    IOTests.class,
    PropagationTests.class,
    DataModelTests.class,
    CalculationMethodTests.class
})
/* @formatter:on */
public class AllTests {
}
