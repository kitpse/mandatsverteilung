import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.management.ElectionComparisonTest;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.management.SessionTest;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.ConstituencyTest;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.DataStructureFactoryTest;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.DeepCloneTest;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.FederalStateTest;

@RunWith(Suite.class)
/* @formatter:off */
@SuiteClasses({
    SessionTest.class,
    ElectionComparisonTest.class,
    
    DeepCloneTest.class,
    DataStructureFactoryTest.class,
    FederalStateTest.class,
    ConstituencyTest.class
})
/* @formatter:on */
public class DataModelTests {
}
