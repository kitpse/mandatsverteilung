package edu.kit.iti.formal.pse1.mandatsverteilung.data.model;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class FederalStateTest {
    private ElectionResult result;
    private FederalState state;
    private Constituency const1;
    private Constituency const2;
    
    @Before
    public void setUp() {
        this.result = new ElectionResult();
        this.state = new FederalState("Test Bundesland", this.result);
        this.const1 = new Constituency("Test Wahlkreis 1", this.result);
        this.const2 = new Constituency("Test Wahlkreis 2", this.result);
        this.state.addChild(this.const1);
        this.state.addChild(this.const2);
    }
    
    @After
    public void tearDown() {
        this.result = null;
        this.state = null;
        this.const1 = null;
        this.const2 = null;
    }
    
    @Test
    public void testRemoveChild() {
        this.state.removeChild(this.const1);
        
        Assert.assertEquals("Es ist nur noch ein Kind vorhanden.", 1, this.state.getChildren().size());
    }
    
    @Test
    public void testPartyDirectMandateOneConstituency() {
        Party party1 = new Party("Test Partei 1");
        Party party2 = new Party("Test Partei 2");
        this.state.addParty(party1);
        this.state.addParty(party2);
        this.const1.setFirstVotes(party1, 10000);
        this.const2.setFirstVotes(party2, 10000);
        
        Assert.assertEquals("Zwei Parteien, die jeweils einen Wahlkreis gewonnen haben, gewinnen jeweils ein Direktmandat.", 1,
                            this.state.getWonDirectMandates(party1));
        Assert.assertEquals("Zwei Parteien, die jeweils einen Wahlkreis gewonnen haben, gewinnen jeweils ein Direktmandat.", 1,
                            this.state.getWonDirectMandates(party2));
    }
    
    @Test
    public void testOnePartyDirectMandateBothConstituencies() {
        Party party = new Party("Test Partei");
        this.state.addParty(party);
        this.const1.setFirstVotes(party, 10000);
        this.const2.setFirstVotes(party, 10000);
        
        Assert.assertEquals("Bei nur einer Partei in den beiden Wahlkreisen gewinnt diese das Direktmandat.", 2, this.state.getWonDirectMandates(party));
    }
    
    @Test
    public void testLoosingPartyDirectMandateOneConstituency() {
        Party party1 = new Party("Test Partei 1");
        Party party2 = new Party("Test Partei 2");
        Party party3 = new Party("Test Partei 3");
        this.state.addParty(party1);
        this.state.addParty(party2);
        this.state.addParty(party3);
        this.const1.setFirstVotes(party1, 10000);
        this.const2.setFirstVotes(party2, 10000);
        
        Assert.assertEquals("Eine Partei, die in beiden Wahlkreisen nicht die meisten Erstimmten hat, gewinnt kein Direktmandat.", 0,
                            this.state.getWonDirectMandates(party3));
    }
    
    @Test
    public void testPartyLocked() {
        Party party = new Party("Test Partei");
        this.state.addParty(party);
        this.state.lock(party);
        
        Assert.assertTrue("Die Partei wird gelockt.", this.state.isLocked(party));
    }
    
    @Test
    public void testPartyUnlock() {
        Party party = new Party("Test Partei");
        this.state.addParty(party);
        this.state.lock(party);
        this.state.unlock(party);
        
        Assert.assertFalse("Die Partei wird gelockt.", this.state.isLocked(party));
    }
    
    @Test
    public void testNotAllPartiesLocked() {
        Party party1 = new Party("Test Partei 1");
        Party party2 = new Party("Test Partei 2");
        Party party3 = new Party("Test Partei 3");
        this.state.addParty(party1);
        this.state.addParty(party2);
        this.state.addParty(party3);
        
        this.state.lock(party1);
        this.state.lock(party2);
        
        Assert.assertFalse("Es sind nicht alle Parteien gelockt.", this.state.allPartiesLocked());
    }
    
    @Test
    public void testAllPartiesLocked() {
        Party party1 = new Party("Test Partei 1");
        Party party2 = new Party("Test Partei 2");
        Party party3 = new Party("Test Partei 3");
        this.state.addParty(party1);
        this.state.addParty(party2);
        this.state.addParty(party3);
        
        this.state.lock(party1);
        this.state.lock(party2);
        this.state.lock(party3);
        
        Assert.assertTrue("Es sind alle Parteien gelockt.", this.state.allPartiesLocked());
    }
    
    @Test
    public void removeParty() {
        Party party1 = new Party("Test Partei 1");
        
        this.state.addParty(party1);
        this.state.removeParty(party1);
        
        Assert.assertTrue("Nach dem Hinzufügen und entfernen einer Partei sind keine Parteien mehr vorhanden.", this.state.getParties().isEmpty());
    }
    
    @Test
    public void isPartyPresent() {
        Party party1 = new Party("Test Partei 1");
        
        this.state.addParty(party1);
        
        Assert.assertTrue("Partei ist vorhanden.", this.state.isPartyPresent(party1));
    }
    
    @Test
    public void isPartyNotPresent() {
        Assert.assertFalse("Partei ist nicht vorhanden.", this.state.isPartyPresent(new Party("Test")));
    }
    
    @Test
    public void testDeepClone() {
        this.const1.setCitizens(20000);
        this.const1.setEligibleVoters(16000);
        this.const1.setNumberOfVotes(13000);
        
        this.const2.setCitizens(20000);
        this.const2.setEligibleVoters(16000);
        this.const2.setNumberOfVotes(13000);
        
        Party party1 = new Party("Test Partei 1");
        Party party2 = new Party("Test Partei 2");
        Party party3 = new Party("Test Partei 3");
        this.state.addParty(party1);
        this.state.addParty(party2);
        this.state.addParty(party3);
        
        FederalState cloned = this.state.deepClone();
        
        Assert.assertEquals("Einwohner der Kopie stimmen nach Änderung nicht mehr.", cloned.getCitizens(), 40000);
        Assert.assertEquals("Wahlberechtigte der Kopie stimmen.", cloned.getEligibleVoters(), 32000);
        Assert.assertEquals("Wähler der Kopie stimmen.", cloned.getNumberOfVotes(), 26000);
        Assert.assertEquals("Es sind gleich viele Parteien vorhanden", 3, cloned.getParties().size());
    }
    
    @Test
    public void testDeepCloneChange() {
        this.const1.setCitizens(20000);
        this.const1.setEligibleVoters(16000);
        this.const1.setNumberOfVotes(13000);
        
        this.const2.setCitizens(20000);
        this.const2.setEligibleVoters(16000);
        this.const2.setNumberOfVotes(13000);
        
        FederalState cloned = this.state.deepClone();
        
        this.const1.setCitizens(10000);
        
        Assert.assertNotEquals("Einwohner der Kopie stimmen nach Änderung nicht mehr.", cloned.getCitizens(), 30000);
        Assert.assertEquals("Wahlberechtigte der Kopie stimmen.", cloned.getEligibleVoters(), 32000);
        Assert.assertEquals("Wähler der Kopie stimmen.", cloned.getNumberOfVotes(), 26000);
    }
    
    @Test
    public void testDeepCloneRemoveParty() {
        Party party1 = new Party("Test Partei 1");
        Party party2 = new Party("Test Partei 2");
        Party party3 = new Party("Test Partei 3");
        this.state.addParty(party1);
        this.state.addParty(party2);
        this.state.addParty(party3);
        
        FederalState cloned = this.state.deepClone();
        
        this.state.removeParty(party1);
        
        Assert.assertNotEquals("Es sind nicht gleich viele Parteien vorhanden", cloned.getParties().size(), 2);
    }
    
    @Test
    public void testEqualsClone() {
        Assert.assertEquals("Eine geklontes Bundesland ist zu sich selbst äquivalent.", this.state.deepClone(), this.state);
    }
    
    @Test
    public void testEquals() {
        Party party1 = new Party("Test Partei 1");
        this.state.addParty(party1);
        Assert.assertEquals("Eine geklontes Bundesland mit einer Partei ist zu sich selbst äquivalent.", this.state.deepClone(), this.state);
    }
    
    @Test
    public void testCandidateCountNewParty() {
        Party party1 = new Party("Test Partei 1");
        this.state.addParty(party1);
        
        Assert.assertEquals("Eine neu hinzugefügte Partei hat noch keine Kandiaten.", 0, this.state.getCandidateCount(party1));
    }
    
    @Test
    public void testCandidateCountWrongParty1() {
        Party party1 = new Party("Test Partei 1");
        this.state.addParty(party1);
        
        Assert.assertEquals("Eine unbekannte Partei hat keine Kandiaten.", 0, this.state.getCandidateCount(new Party("Test")));
    }
    
    @Test
    public void testCandidateCountWrongParty2() {
        Party party1 = new Party("Test Partei 1");
        this.state.addParty(party1);
        
        Assert.assertFalse("Eine unbekannte Partei hat keine Kandiaten.", this.state.hasPartyCandidateCount(new Party("Test")));
    }
    
    @Test
    public void testSetCandidateCount() {
        Party party1 = new Party("Test Partei 1");
        this.state.addParty(party1);
        this.state.changeCandidateCount(party1, 5);
        
        Assert.assertEquals("Eine Partei hat die ihr zugewiesene Anzahl an Kandidaten.", 5, this.state.getCandidateCount(party1));
    }
}
