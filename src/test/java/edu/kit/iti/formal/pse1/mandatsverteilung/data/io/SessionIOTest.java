package edu.kit.iti.formal.pse1.mandatsverteilung.data.io;

import java.io.File;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.management.ElectionComparison;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.management.Session;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.management.SessionTest.TabMangerDummy;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Election;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Federation;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Party;
import edu.kit.iti.formal.pse1.mandatsverteilung.gui.controller.Mandatsverteilung;
import edu.kit.iti.formal.pse1.mandatsverteilung.propagation.UniformDistribution;

public class SessionIOTest {
    private static String sessionPath = Session.getDefaultSessionPath();
    
    private Session session;
    private Election election2013NegSgw;
    private ElectionComparison comparison1;
    
    @BeforeClass
    public static void setUpClass() {
        new File(SessionIOTest.sessionPath).delete();
        Session.init(new TabMangerDummy());
    }
    
    @Before
    public void setUp() {
        this.session = Session.getSession();
        Election election2013_1 = Import.importInternalElection("bundestagswahl2013.csv");
        this.election2013NegSgw = Import.importInternalElection("bundestagswahl2013-NegSgw-Bayern-SPD.csv");
        
        Election election2013_2 = Import.importInternalElection("bundestagswahl2013.csv");
        election2013_2.getElectionResult().changePropagation(new UniformDistribution());
        Federation federation = election2013_2.getElectionResult().getFederation();
        Party spd = null;
        for(Party p : federation.getParties()) {
            if(p.getName().equalsIgnoreCase("SPD")) {
                spd = p;
            }
        }
        assert spd != null;
        federation.changeSecondVotes(spd, 10000000);
        
        this.session.addElection(election2013_1);
        this.session.addElection(this.election2013NegSgw);
        this.session.addElectionComparison(this.comparison1 = new ElectionComparison(election2013_1, this.election2013NegSgw, "Toller Vergleich"));
        this.session.addElection(election2013_2);
        this.session.addElectionComparison(new ElectionComparison(election2013_1, election2013_2, "Vergleich 2"));
    }
    
    @Test(timeout = 5000)
    public void exportAndReimport() {
        // some nonsens modifications to the session to make sure the import is a clean import
        this.session.removeElectionComparison(this.comparison1);
        this.session.removeElection(this.election2013NegSgw);
        this.session.addElection(Import.importInternalElection("bundestagswahl2013.csv"));
        this.session = this.session.snapshot(); // take a snapshot
        
        String string = Export.sessionToString().toString(); // export
        
        // now reset session
        while(Session.getSession().getElectionCount() > 0) {
            Session.getSession().removeElection(Session.getSession().getElections().get(0));
        }
        while(Session.getSession().getComparisonCount() > 0) {
            Session.getSession().removeElectionComparison(Session.getSession().getElectionComparisons().get(0));
        }
        try {
            Import.parseSession(string);
        }
        catch(NullPointerException | FormatException e) {
            Assert.fail(Mandatsverteilung.getNestedErrorMessages(e));
        }
        Assert.assertEquals("Die Session und die Exportierte und Reimportierte Session sind nicht gleich", this.session, Session.getSession());
    }
}
