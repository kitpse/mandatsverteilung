package edu.kit.iti.formal.pse1.mandatsverteilung.data.io;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Constituency;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.FederalState;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Federation;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Party;

/**
 * Test /T300/ from the Pflichtenheft.<br>
 * Assumes that test a the {@link Federation} level are sufficient, as the {@link Federation} is the sum of all {@link FederalState}s which are the sum of all
 * {@link Constituency} and it's very unlikely the there are false sums which sums up to a right total sum.
 * 
 * @author Felix Heim
 * @version 1
 */
@RunWith(Parameterized.class)
public class IOTestT300 {
    private static Federation federation;
    private static Set<Party> parties;
    
    private Party party;
    private int firstVotes;
    private int secondVotes;
    
    @BeforeClass
    public static void setUpClass() {
        IOTestT300.federation = Import.importInternalElection("bundestagswahl2013.csv").getElectionResult().getFederation();
        IOTestT300.parties = IOTestT300.federation.getParties();
    }
    
    @Parameters(name = "{0}: FV={1}, SV={2}")
    public static Collection<Object[]> getParameters() {
        return Arrays.asList(new Object[]{"CDU", 16233642, 14921877}, new Object[]{"SPD", 12843458, 11252215}, new Object[]{"FDP", 1028645, 2083533},
                             new Object[]{"DIE LINKE", 3585178, 3755699}, new Object[]{"GRÜNE", 3180299, 3694057}, new Object[]{"CSU", 3544079, 3243569},
                             new Object[]{"PIRATEN", 963623, 959177}, new Object[]{"NPD", 635135, 560828}, new Object[]{"Tierschutzpartei", 4437, 140366},
                             new Object[]{"REP", 27299, 91193}, new Object[]{"ÖDP", 128209, 127088}, new Object[]{"FAMILIE", 4478, 7449},
                             new Object[]{"Bündnis 21/RRP", 5324, 8578}, new Object[]{"RENTNER", 920, 25134}, new Object[]{"BP", 28430, 57395},
                             new Object[]{"PBC", 2081, 18542}, new Object[]{"BüSo", 17988, 12814}, new Object[]{"DIE VIOLETTEN", 2516, 8211},
                             new Object[]{"MLPD", 12904, 24219}, new Object[]{"Volksabstimmung", 1748, 28654}, new Object[]{"PSG", 0, 4564},
                             new Object[]{"AfD", 810915, 2056985}, new Object[]{"BIG", 2680, 17743}, new Object[]{"pro Deutschland", 4815, 73854},
                             new Object[]{"DIE RECHTE", 0, 2245}, new Object[]{"DIE FRAUEN", 0, 12148}, new Object[]{"FREIE WÄHLER", 431640, 423977},
                             new Object[]{"Nichtwähler", 0, 11349}, new Object[]{"PARTEI DER VERNUNFT", 3861, 24719}, new Object[]{"Die PARTEI", 39388, 78674},
                             new Object[]{"B", 624, 0}, new Object[]{"BGD", 1431, 0}, new Object[]{"DKP", 1699, 0}, new Object[]{"NEIN!", 290, 0});
    }
    
    public IOTestT300(String partyName, int firstVotes, int secondVotes) {
        this.party = this.getPartyByName(partyName);
        if(this.party == null) {
            Assert.fail("Die Partei \"" + partyName + "\" wurde nicht im Wahlergebnis gefunden");
        }
        this.firstVotes = firstVotes;
        this.secondVotes = secondVotes;
    }
    
    @Test
    public void checkFirstVotes() {
        Assert.assertEquals("Erststimmen wurden falsch geladen!", this.firstVotes, IOTestT300.federation.getFirstVotes(this.party));
    }
    
    @Test
    public void secondFirstVotes() {
        Assert.assertEquals("Zweitstimmen wurden falsch geladen!", this.secondVotes, IOTestT300.federation.getSecondVotes(this.party));
    }
    
    private Party getPartyByName(String name) {
        for(Party party : IOTestT300.parties) {
            if(party.getName().equalsIgnoreCase(name)) {
                return party;
            }
        }
        return null;
    }
    
    @AfterClass
    public static void tearDown() {
        IOTestT300.federation = null;
        IOTestT300.parties = null;
    }
}
