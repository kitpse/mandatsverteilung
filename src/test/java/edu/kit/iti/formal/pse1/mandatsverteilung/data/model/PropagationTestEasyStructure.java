package edu.kit.iti.formal.pse1.mandatsverteilung.data.model;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import edu.kit.iti.formal.pse1.mandatsverteilung.propagation.UniformDistribution;

public class PropagationTestEasyStructure {
    private Propagation propagation;
    private ElectionResult result;
    
    private Federation fed;
    private FederalState stateBW;
    private FederalState stateBay;
    
    private Constituency constBW1;
    private Constituency constBW2;
    private Constituency constBW3;
    private Constituency constBay1;
    private Constituency constBay2;
    private Constituency constBay3;
    
    @Rule
    public Timeout globalTimeout = new Timeout(20, TimeUnit.SECONDS);
    
    /**
     * Create a test structure with 2 federal states, each with 3 constituencies. No votes are stored yet.
     * 
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {
        this.result = new ElectionResult();
        this.fed = new Federation("Deutschland", this.result);
        
        this.stateBW = new FederalState("BW", this.result);
        this.stateBay = new FederalState("Bayern", this.result);
        
        this.constBW1 = new Constituency("BW_1", this.result);
        this.constBW2 = new Constituency("BW_2", this.result);
        this.constBW3 = new Constituency("BW_3", this.result);
        this.constBay1 = new Constituency("Bayern_1", this.result);
        this.constBay2 = new Constituency("Bayern_2", this.result);
        this.constBay3 = new Constituency("Bayern_3", this.result);
        
        this.fed.addChild(this.stateBW);
        this.fed.addChild(this.stateBay);
        
        this.stateBW.addChild(this.constBW1);
        this.stateBW.addChild(this.constBW2);
        this.stateBW.addChild(this.constBW3);
        this.stateBay.addChild(this.constBay1);
        this.stateBay.addChild(this.constBay2);
        this.stateBay.addChild(this.constBay3);
    }
    
    @After
    public void tearDown() throws Exception {
        this.result = null;
        this.fed = null;
        this.stateBW = null;
        this.stateBay = null;
        this.constBW1 = null;
        this.constBW2 = null;
        this.constBW3 = null;
        this.constBay1 = null;
        this.constBay2 = null;
        this.constBay3 = null;
    }
    
    /**
     * Checks if the specified values are the same.
     */
    private boolean ElectionAreaSameValues(ElectionArea<?> const1, ElectionArea<?> const2, boolean citizens, boolean eligibleVoters, boolean numberOfVotes,
                                           boolean validFirst, boolean validSecond) {
        if(citizens) {
            if(const1.getCitizens() != const2.getCitizens()) {
                return false;
            }
        }
        if(eligibleVoters) {
            if(const1.getEligibleVoters() != const2.getEligibleVoters()) {
                return false;
            }
        }
        if(numberOfVotes) {
            if(const1.getNumberOfVotes() != const2.getNumberOfVotes()) {
                return false;
            }
        }
        if(validFirst) {
            if(const1.getValidFirstVotes() != const2.getValidFirstVotes()) {
                return false;
            }
        }
        if(validSecond) {
            if(const1.getValidSecondVotes() != const2.getValidSecondVotes()) {
                return false;
            }
        }
        return true;
    }
    
    @Test
    public void testCitizensFederalState() {
        this.propagation = new Propagation(new UniformDistribution());
        
        this.constBay1.setCitizens(20000);
        this.constBay2.setCitizens(20000);
        this.constBay3.setCitizens(20000);
        
        this.constBay1.setEligibleVoters(10000);
        this.constBay2.setEligibleVoters(10000);
        this.constBay3.setEligibleVoters(10000);
        
        Party cdu = new Party("CDU");
        this.constBay1.addParty(cdu);
        this.constBay2.addParty(cdu);
        this.constBay3.addParty(cdu);
        
        Constituency clone1 = this.constBay1.deepClone();
        Constituency clone2 = this.constBay2.deepClone();
        Constituency clone3 = this.constBay3.deepClone();
        
        Assert.assertTrue("Die Propagierung muss erfolgreich verlaufen.", this.propagation.propagateCitizens(this.stateBay, 120000));
        Assert.assertEquals("Die Anzahl neuer Einwohner im Wahlkreis \"constBay1\" muss 40.000 betragen.", 40000, this.constBay1.getCitizens());
        Assert.assertTrue("Im Wahlkreis \"constBay1\" darf sich bis auf die Einwohner nichts ändern.",
                          this.ElectionAreaSameValues(clone1, this.constBay1, false, true, true, true, true));
        Assert.assertEquals("Die Anzahl neuer Einwohner im Wahlkreis \"constBay2\" muss 40.000 betragen.", 40000, this.constBay2.getCitizens());
        Assert.assertTrue("Im Wahlkreis \"constBay2\" darf sich bis auf die Einwohner nichts ändern.",
                          this.ElectionAreaSameValues(clone2, this.constBay2, false, true, true, true, true));
        Assert.assertEquals("Die Anzahl neuer Einwohner im Wahlkreis \"constBay3\" muss 40.000 betragen.", 40000, this.constBay3.getCitizens());
        Assert.assertTrue("Im Wahlkreis \"constBay3\" darf sich bis auf die Einwohner nichts ändern.",
                          this.ElectionAreaSameValues(clone3, this.constBay3, false, true, true, true, true));
    }
    
    @Test
    public void testCitizensFederalStateLocked() {
        this.propagation = new Propagation(new UniformDistribution());
        
        this.constBay1.setCitizens(20000);
        this.constBay2.setCitizens(20000);
        
        this.constBay1.setEligibleVoters(10000);
        this.constBay2.setEligibleVoters(10000);
        
        this.constBay3.lock();
        
        Party cdu = new Party("CDU");
        this.constBay1.addParty(cdu);
        this.constBay2.addParty(cdu);
        this.constBay3.addParty(cdu);
        
        Constituency clone1 = this.constBay1.deepClone();
        Constituency clone2 = this.constBay2.deepClone();
        Constituency clone3 = this.constBay3.deepClone();
        
        Assert.assertTrue("Die Propagierung muss erfolgreich verlaufen.", this.propagation.propagateCitizens(this.stateBay, 80000));
        Assert.assertEquals("Die Anzahl neuer Einwohner im Wahlkreis \"constBay1\" muss 40.000 betragen.", 40000, this.constBay1.getCitizens());
        Assert.assertTrue("Im Wahlkreis \"constBay1\" darf sich bis auf die Einwohner nichts ändern.",
                          this.ElectionAreaSameValues(clone1, this.constBay1, false, true, true, true, true));
        Assert.assertEquals("Die Anzahl neuer Einwohner im Wahlkreis \"constBay2\" muss 40.000 betragen.", 40000, this.constBay2.getCitizens());
        Assert.assertTrue("Im Wahlkreis \"constBay2\" darf sich bis auf die Einwohner nichts ändern.",
                          this.ElectionAreaSameValues(clone2, this.constBay2, false, true, true, true, true));
        Assert.assertTrue("Im Wahlkreis \"constBay3\" darf sich nichts ändern.",
                          this.ElectionAreaSameValues(clone3, this.constBay3, true, true, true, true, true));
    }
    
    @Test
    public void testCitizensFederalStateAllLocked() {
        this.propagation = new Propagation(new UniformDistribution());
        
        this.constBay1.lock();
        this.constBay2.lock();
        this.constBay3.lock();
        
        Party cdu = new Party("CDU");
        this.constBay1.addParty(cdu);
        this.constBay2.addParty(cdu);
        this.constBay3.addParty(cdu);
        
        Constituency clone1 = this.constBay1.deepClone();
        Constituency clone2 = this.constBay2.deepClone();
        Constituency clone3 = this.constBay3.deepClone();
        
        Assert.assertFalse("Die Propagierung muss scheitern.", this.propagation.propagateCitizens(this.stateBay, 80000));
        Assert.assertTrue("Im Wahlkreis \"constBay1\" darf sich nichts ändern.",
                          this.ElectionAreaSameValues(clone1, this.constBay1, true, true, true, true, true));
        Assert.assertTrue("Im Wahlkreis \"constBay2\" darf sich nichts ändern.",
                          this.ElectionAreaSameValues(clone2, this.constBay2, true, true, true, true, true));
        Assert.assertTrue("Im Wahlkreis \"constBay3\" darf sich nichts ändern.",
                          this.ElectionAreaSameValues(clone3, this.constBay3, true, true, true, true, true));
    }
    
    @Test
    public void testCitizensFederation() {
        this.propagation = new Propagation(new UniformDistribution());
        
        this.constBay1.setCitizens(20000);
        this.constBay2.setCitizens(20000);
        this.constBay3.setCitizens(20000);
        this.constBW1.setCitizens(20000);
        this.constBW2.setCitizens(20000);
        this.constBW3.setCitizens(20000);
        
        this.constBay1.setEligibleVoters(10000);
        this.constBay2.setEligibleVoters(10000);
        this.constBay3.setEligibleVoters(10000);
        this.constBW1.setEligibleVoters(10000);
        this.constBW2.setEligibleVoters(10000);
        this.constBW3.setEligibleVoters(10000);
        
        Party cdu = new Party("CDU");
        this.constBay1.addParty(cdu);
        this.constBay2.addParty(cdu);
        this.constBay3.addParty(cdu);
        this.constBW1.addParty(cdu);
        this.constBW2.addParty(cdu);
        this.constBW3.addParty(cdu);
        
        Constituency cloneBay1 = this.constBay1.deepClone();
        Constituency cloneBay2 = this.constBay2.deepClone();
        Constituency cloneBay3 = this.constBay3.deepClone();
        Constituency cloneBW1 = this.constBW1.deepClone();
        Constituency cloneBW2 = this.constBW2.deepClone();
        Constituency cloneBW3 = this.constBW3.deepClone();
        
        Assert.assertTrue("Die Propagierung muss erfolgreich verlaufen.", this.propagation.propagateCitizens(this.fed, 600000));
        Assert.assertEquals("Die Anzahl neuer Einwohner im Wahlkreis \"constBay1\" muss 100.000 betragen.", 100000, this.constBay1.getCitizens());
        Assert.assertTrue("Im Wahlkreis \"constBay1\" darf sich bis auf die Einwohner nichts ändern.",
                          this.ElectionAreaSameValues(cloneBay1, this.constBay1, false, true, true, true, true));
        Assert.assertEquals("Die Anzahl neuer Einwohner im Wahlkreis \"constBay2\" muss 100.000 betragen.", 100000, this.constBay2.getCitizens());
        Assert.assertTrue("Im Wahlkreis \"constBay2\" darf sich bis auf die Einwohner nichts ändern.",
                          this.ElectionAreaSameValues(cloneBay2, this.constBay2, false, true, true, true, true));
        Assert.assertEquals("Die Anzahl neuer Einwohner im Wahlkreis \"constBay3\" muss 100.000 betragen.", 100000, this.constBay3.getCitizens());
        Assert.assertTrue("Im Wahlkreis \"constBay3\" darf sich bis auf die Einwohner nichts ändern.",
                          this.ElectionAreaSameValues(cloneBay3, this.constBay3, false, true, true, true, true));
        Assert.assertEquals("Die Anzahl neuer Einwohner im Wahlkreis \"constBW1\" muss 100.000 betragen.", 100000, this.constBW1.getCitizens());
        Assert.assertTrue("Im Wahlkreis \"constBW1\" darf sich bis auf die Einwohner nichts ändern.",
                          this.ElectionAreaSameValues(cloneBW1, this.constBW1, false, true, true, true, true));
        Assert.assertEquals("Die Anzahl neuer Einwohner im Wahlkreis \"constBW2\" muss 100.000 betragen.", 100000, this.constBW2.getCitizens());
        Assert.assertTrue("Im Wahlkreis \"constBW2\" darf sich bis auf die Einwohner nichts ändern.",
                          this.ElectionAreaSameValues(cloneBW2, this.constBW2, false, true, true, true, true));
        Assert.assertEquals("Die Anzahl neuer Einwohner im Wahlkreis \"constBW3\" muss 100.000 betragen.", 100000, this.constBW3.getCitizens());
        Assert.assertTrue("Im Wahlkreis \"constBW3\" darf sich bis auf die Einwohner nichts ändern.",
                          this.ElectionAreaSameValues(cloneBW3, this.constBW3, false, true, true, true, true));
    }
    
    @Test
    public void testCitizensFederationLocked() {
        this.propagation = new Propagation(new UniformDistribution());
        
        this.constBay1.setCitizens(20000);
        this.constBay2.setCitizens(20000);
        this.constBay3.setCitizens(20000);
        
        this.constBay1.setEligibleVoters(10000);
        this.constBay2.setEligibleVoters(10000);
        this.constBay3.setEligibleVoters(10000);
        
        this.stateBW.lock();
        
        Party cdu = new Party("CDU");
        this.constBay1.addParty(cdu);
        this.constBay2.addParty(cdu);
        this.constBay3.addParty(cdu);
        
        Constituency cloneBay1 = this.constBay1.deepClone();
        Constituency cloneBay2 = this.constBay2.deepClone();
        Constituency cloneBay3 = this.constBay3.deepClone();
        Constituency cloneBW1 = this.constBW1.deepClone();
        Constituency cloneBW2 = this.constBW2.deepClone();
        Constituency cloneBW3 = this.constBW3.deepClone();
        
        Assert.assertTrue("Die Propagierung muss erfolgreich verlaufen.", this.propagation.propagateCitizens(this.fed, 600000));
        Assert.assertEquals("Die Anzahl neuer Einwohner im Wahlkreis \"constBay1\" muss 200.000 betragen.", 200000, this.constBay1.getCitizens());
        Assert.assertTrue("Im Wahlkreis \"constBay1\" darf sich bis auf die Einwohner nichts ändern.",
                          this.ElectionAreaSameValues(cloneBay1, this.constBay1, false, true, true, true, true));
        Assert.assertEquals("Die Anzahl neuer Einwohner im Wahlkreis \"constBay2\" muss 200.000 betragen.", 200000, this.constBay2.getCitizens());
        Assert.assertTrue("Im Wahlkreis \"constBay2\" darf sich bis auf die Einwohner nichts ändern.",
                          this.ElectionAreaSameValues(cloneBay2, this.constBay2, false, true, true, true, true));
        Assert.assertEquals("Die Anzahl neuer Einwohner im Wahlkreis \"constBay3\" muss 200.000 betragen.", 200000, this.constBay3.getCitizens());
        Assert.assertTrue("Im Wahlkreis \"constBay3\" darf sich bis auf die Einwohner nichts ändern.",
                          this.ElectionAreaSameValues(cloneBay3, this.constBay3, false, true, true, true, true));
        
        Assert.assertTrue("Im Wahlkreis \"cloneBW1\" darf sich nichts ändern.",
                          this.ElectionAreaSameValues(cloneBW1, this.constBW1, true, true, true, true, true));
        Assert.assertTrue("Im Wahlkreis \"cloneBW2\" darf sich nichts ändern.",
                          this.ElectionAreaSameValues(cloneBW2, this.constBW2, true, true, true, true, true));
        Assert.assertTrue("Im Wahlkreis \"cloneBW3\" darf sich nichts ändern.",
                          this.ElectionAreaSameValues(cloneBW3, this.constBW3, true, true, true, true, true));
    }
    
    @Test
    public void testCitizensFederationAllLocked() {
        this.propagation = new Propagation(new UniformDistribution());
        
        this.stateBay.lock();
        this.stateBW.lock();
        
        Constituency cloneBay1 = this.constBay1.deepClone();
        Constituency cloneBay2 = this.constBay2.deepClone();
        Constituency cloneBay3 = this.constBay3.deepClone();
        Constituency cloneBW1 = this.constBW1.deepClone();
        Constituency cloneBW2 = this.constBW2.deepClone();
        Constituency cloneBW3 = this.constBW3.deepClone();
        
        Assert.assertFalse("Die Propagierung muss scheitern.", this.propagation.propagateCitizens(this.fed, 600000));
        Assert.assertTrue("Im Wahlkreis \"constBay1\" darf sich nichts ändern.",
                          this.ElectionAreaSameValues(cloneBay1, this.constBay1, true, true, true, true, true));
        Assert.assertTrue("Im Wahlkreis \"constBay2\" darf sich nichts ändern.",
                          this.ElectionAreaSameValues(cloneBay2, this.constBay2, true, true, true, true, true));
        Assert.assertTrue("Im Wahlkreis \"constBay3\" darf sich nichts ändern.",
                          this.ElectionAreaSameValues(cloneBay3, this.constBay3, true, true, true, true, true));
        Assert.assertTrue("Im Wahlkreis \"constBW1\" darf sich nichts ändern.",
                          this.ElectionAreaSameValues(cloneBW1, this.constBW1, true, true, true, true, true));
        Assert.assertTrue("Im Wahlkreis \"constBW2\" darf sich nichts ändern.",
                          this.ElectionAreaSameValues(cloneBW2, this.constBW2, true, true, true, true, true));
        Assert.assertTrue("Im Wahlkreis \"constBW3\" darf sich nichts ändern.",
                          this.ElectionAreaSameValues(cloneBW3, this.constBW3, true, true, true, true, true));
    }
    
    @Test
    public void testEligibleVotersFederalState() {
        this.propagation = new Propagation(new UniformDistribution());
        
        this.constBay1.setCitizens(20000);
        this.constBay2.setCitizens(20000);
        this.constBay3.setCitizens(20000);
        
        this.constBay1.setEligibleVoters(10000);
        this.constBay2.setEligibleVoters(10000);
        this.constBay3.setEligibleVoters(10000);
        
        Party cdu = new Party("CDU");
        this.constBay1.addParty(cdu);
        this.constBay2.addParty(cdu);
        this.constBay3.addParty(cdu);
        
        Constituency clone1 = this.constBay1.deepClone();
        Constituency clone2 = this.constBay2.deepClone();
        Constituency clone3 = this.constBay3.deepClone();
        
        Assert.assertTrue("Die Propagierung muss erfolgreich verlaufen.", this.propagation.propagateEligibleVoters(this.stateBay, 45000));
        Assert.assertEquals("Die Anzahl neuer Wahlberechtigter im Wahlkreis \"constBay1\" muss 15.000 betragen.", 15000, this.constBay1.getEligibleVoters());
        Assert.assertTrue("Im Wahlkreis \"constBay1\" darf sich bis auf die Einwohner nichts ändern.",
                          this.ElectionAreaSameValues(clone1, this.constBay1, true, false, true, true, true));
        Assert.assertEquals("Die Anzahl neuer Wahlberechtigter im Wahlkreis \"constBay2\" muss 15.000 betragen.", 15000, this.constBay2.getEligibleVoters());
        Assert.assertTrue("Im Wahlkreis \"constBay2\" darf sich bis auf die Einwohner nichts ändern.",
                          this.ElectionAreaSameValues(clone2, this.constBay2, true, false, true, true, true));
        Assert.assertEquals("Die Anzahl neuer Wahlberechtigter im Wahlkreis \"constBay3\" muss 15.000 betragen.", 15000, this.constBay3.getEligibleVoters());
        Assert.assertTrue("Im Wahlkreis \"constBay3\" darf sich bis auf die Einwohner nichts ändern.",
                          this.ElectionAreaSameValues(clone3, this.constBay3, true, false, true, true, true));
    }
    
    @Test
    public void testEligibleVotersStateLocked() {
        this.propagation = new Propagation(new UniformDistribution());
        
        this.constBay1.setCitizens(20000);
        this.constBay2.setCitizens(20000);
        
        this.constBay1.setEligibleVoters(10000);
        this.constBay2.setEligibleVoters(10000);
        
        Party cdu = new Party("CDU");
        this.constBay1.addParty(cdu);
        this.constBay2.addParty(cdu);
        this.constBay3.addParty(cdu);
        
        this.constBay3.lock();
        
        Constituency clone1 = this.constBay1.deepClone();
        Constituency clone2 = this.constBay2.deepClone();
        Constituency clone3 = this.constBay3.deepClone();
        
        Assert.assertTrue("Die Propagierung muss erfolgreich verlaufen.", this.propagation.propagateEligibleVoters(this.stateBay, 30000));
        Assert.assertEquals("Die Anzahl neuer Wahlberechtigten im Wahlkreis \"constBay1\" muss 15.000 betragen.", 15000, this.constBay1.getEligibleVoters());
        Assert.assertTrue("Im Wahlkreis \"constBay1\" darf sich bis auf die Wahlberechtigten nichts ändern.",
                          this.ElectionAreaSameValues(clone1, this.constBay1, true, false, true, true, true));
        Assert.assertEquals("Die Anzahl neuer Wahlberechtigten im Wahlkreis \"constBay2\" muss 15.000 betragen.", 15000, this.constBay2.getEligibleVoters());
        Assert.assertTrue("Im Wahlkreis \"constBay2\" darf sich bis auf die Wahlberechtigten nichts ändern.",
                          this.ElectionAreaSameValues(clone2, this.constBay2, true, false, true, true, true));
        Assert.assertTrue("Im Wahlkreis \"constBay3\" darf sich nichts ändern.",
                          this.ElectionAreaSameValues(clone3, this.constBay3, true, true, true, true, true));
    }
    
    @Test
    public void testEligibleVotersStateAllLocked() {
        this.propagation = new Propagation(new UniformDistribution());
        
        this.constBay1.lock();
        this.constBay2.lock();
        this.constBay3.lock();
        
        Party cdu = new Party("CDU");
        this.constBay1.addParty(cdu);
        this.constBay2.addParty(cdu);
        this.constBay3.addParty(cdu);
        
        Constituency clone1 = this.constBay1.deepClone();
        Constituency clone2 = this.constBay2.deepClone();
        Constituency clone3 = this.constBay3.deepClone();
        
        Assert.assertFalse("Die Propagierung muss scheitern.", this.propagation.propagateEligibleVoters(this.stateBay, 80000));
        Assert.assertTrue("Im Wahlkreis \"constBay1\" darf sich nichts ändern.",
                          this.ElectionAreaSameValues(clone1, this.constBay1, true, true, true, true, true));
        Assert.assertTrue("Im Wahlkreis \"constBay2\" darf sich nichts ändern.",
                          this.ElectionAreaSameValues(clone2, this.constBay2, true, true, true, true, true));
        Assert.assertTrue("Im Wahlkreis \"constBay3\" darf sich nichts ändern.",
                          this.ElectionAreaSameValues(clone3, this.constBay3, true, true, true, true, true));
    }
    
    @Test
    public void testEligibleVotersFederation() {
        this.propagation = new Propagation(new UniformDistribution());
        
        this.constBay1.setCitizens(20000);
        this.constBay2.setCitizens(20000);
        this.constBay3.setCitizens(20000);
        this.constBW1.setCitizens(20000);
        this.constBW2.setCitizens(20000);
        this.constBW3.setCitizens(20000);
        
        this.constBay1.setEligibleVoters(10000);
        this.constBay2.setEligibleVoters(10000);
        this.constBay3.setEligibleVoters(10000);
        this.constBW1.setEligibleVoters(10000);
        this.constBW2.setEligibleVoters(10000);
        this.constBW3.setEligibleVoters(10000);
        
        Party cdu = new Party("CDU");
        this.constBay1.addParty(cdu);
        this.constBay2.addParty(cdu);
        this.constBay3.addParty(cdu);
        this.constBW1.addParty(cdu);
        this.constBW2.addParty(cdu);
        this.constBW3.addParty(cdu);
        
        Constituency cloneBay1 = this.constBay1.deepClone();
        Constituency cloneBay2 = this.constBay2.deepClone();
        Constituency cloneBay3 = this.constBay3.deepClone();
        Constituency cloneBW1 = this.constBW1.deepClone();
        Constituency cloneBW2 = this.constBW2.deepClone();
        Constituency cloneBW3 = this.constBW3.deepClone();
        
        Assert.assertTrue("Die Propagierung muss erfolgreich verlaufen.", this.propagation.propagateEligibleVoters(this.fed, 90000));
        Assert.assertEquals("Die Anzahl neuer Wahlberechtigten im Wahlkreis \"constBay1\" muss 15.00 betragen.", 15000, this.constBay1.getEligibleVoters());
        Assert.assertTrue("Im Wahlkreis \"constBay1\" darf sich bis auf die Wahlberechtigten nichts ändern.",
                          this.ElectionAreaSameValues(cloneBay1, this.constBay1, true, false, true, true, true));
        Assert.assertEquals("Die Anzahl neuer Wahlberechtigten im Wahlkreis \"constBay2\" muss 15.00 betragen.", 15000, this.constBay2.getEligibleVoters());
        Assert.assertTrue("Im Wahlkreis \"constBay2\" darf sich bis auf die Wahlberechtigten nichts ändern.",
                          this.ElectionAreaSameValues(cloneBay2, this.constBay2, true, false, true, true, true));
        Assert.assertEquals("Die Anzahl neuer Wahlberechtigten im Wahlkreis \"constBay3\" muss 15.00 betragen.", 15000, this.constBay3.getEligibleVoters());
        Assert.assertTrue("Im Wahlkreis \"constBay3\" darf sich bis auf die Wahlberechtigten nichts ändern.",
                          this.ElectionAreaSameValues(cloneBay3, this.constBay3, true, false, true, true, true));
        Assert.assertEquals("Die Anzahl neuer Wahlberechtigten im Wahlkreis \"constBW1\" muss 15.00 betragen.", 15000, this.constBW1.getEligibleVoters());
        Assert.assertTrue("Im Wahlkreis \"constBW1\" darf sich bis auf die Wahlberechtigten nichts ändern.",
                          this.ElectionAreaSameValues(cloneBW1, this.constBW1, true, false, true, true, true));
        Assert.assertEquals("Die Anzahl neuer Wahlberechtigten im Wahlkreis \"constBW2\" muss 15.00 betragen.", 15000, this.constBW2.getEligibleVoters());
        Assert.assertTrue("Im Wahlkreis \"constBW2\" darf sich bis auf die Wahlberechtigten nichts ändern.",
                          this.ElectionAreaSameValues(cloneBW2, this.constBW2, true, false, true, true, true));
        Assert.assertEquals("Die Anzahl neuer Wahlberechtigten im Wahlkreis \"constBW3\" muss 15.00 betragen.", 15000, this.constBW3.getEligibleVoters());
        Assert.assertTrue("Im Wahlkreis \"constBW3\" darf sich bis auf die Wahlberechtigten nichts ändern.",
                          this.ElectionAreaSameValues(cloneBW3, this.constBW3, true, false, true, true, true));
    }
    
    @Test
    public void testEligibleVotersFederationLocked() {
        this.propagation = new Propagation(new UniformDistribution());
        
        this.constBay1.setCitizens(20000);
        this.constBay2.setCitizens(20000);
        this.constBay3.setCitizens(20000);
        
        this.constBay1.setEligibleVoters(10000);
        this.constBay2.setEligibleVoters(10000);
        this.constBay3.setEligibleVoters(10000);
        
        Party cdu = new Party("CDU");
        this.constBay1.addParty(cdu);
        this.constBay2.addParty(cdu);
        this.constBay3.addParty(cdu);
        
        this.stateBW.lock();
        
        Constituency cloneBay1 = this.constBay1.deepClone();
        Constituency cloneBay2 = this.constBay2.deepClone();
        Constituency cloneBay3 = this.constBay3.deepClone();
        Constituency cloneBW1 = this.constBW1.deepClone();
        Constituency cloneBW2 = this.constBW2.deepClone();
        Constituency cloneBW3 = this.constBW3.deepClone();
        
        Assert.assertTrue("Die Propagierung muss erfolgreich verlaufen.", this.propagation.propagateEligibleVoters(this.fed, 45000));
        Assert.assertEquals("Die Anzahl neuer Wahlberechtigten im Wahlkreis \"constBay1\" muss 15.000 betragen.", 15000, this.constBay1.getEligibleVoters());
        Assert.assertTrue("Im Wahlkreis \"constBay1\" darf sich bis auf die Wahlberechtigten nichts ändern.",
                          this.ElectionAreaSameValues(cloneBay1, this.constBay1, true, false, true, true, true));
        Assert.assertEquals("Die Anzahl neuer Wahlberechtigten im Wahlkreis \"constBay2\" muss 15.000 betragen.", 15000, this.constBay2.getEligibleVoters());
        Assert.assertTrue("Im Wahlkreis \"constBay2\" darf sich bis auf die Wahlberechtigten nichts ändern.",
                          this.ElectionAreaSameValues(cloneBay2, this.constBay2, true, false, true, true, true));
        Assert.assertEquals("Die Anzahl neuer Wahlberechtigten im Wahlkreis \"constBay3\" muss 15.000 betragen.", 15000, this.constBay3.getEligibleVoters());
        Assert.assertTrue("Im Wahlkreis \"constBay3\" darf sich bis auf die Wahlberechtigten nichts ändern.",
                          this.ElectionAreaSameValues(cloneBay3, this.constBay3, true, false, true, true, true));
        
        Assert.assertTrue("Im Wahlkreis \"cloneBW1\" darf sich nichts ändern.",
                          this.ElectionAreaSameValues(cloneBW1, this.constBW1, true, true, true, true, true));
        Assert.assertTrue("Im Wahlkreis \"cloneBW2\" darf sich nichts ändern.",
                          this.ElectionAreaSameValues(cloneBW2, this.constBW2, true, true, true, true, true));
        Assert.assertTrue("Im Wahlkreis \"cloneBW3\" darf sich nichts ändern.",
                          this.ElectionAreaSameValues(cloneBW3, this.constBW3, true, true, true, true, true));
    }
    
    @Test
    public void testEligibleVotersFederationAllLocked() {
        
        this.propagation = new Propagation(new UniformDistribution());
        
        this.stateBay.lock();
        this.stateBW.lock();
        
        Constituency cloneBay1 = this.constBay1.deepClone();
        Constituency cloneBay2 = this.constBay2.deepClone();
        Constituency cloneBay3 = this.constBay3.deepClone();
        Constituency cloneBW1 = this.constBW1.deepClone();
        Constituency cloneBW2 = this.constBW2.deepClone();
        Constituency cloneBW3 = this.constBW3.deepClone();
        
        Assert.assertFalse("Die Propagierung muss scheitern.", this.propagation.propagateEligibleVoters(this.fed, 600000));
        Assert.assertTrue("Im Wahlkreis \"constBay1\" darf sich nichts ändern.",
                          this.ElectionAreaSameValues(cloneBay1, this.constBay1, true, true, true, true, true));
        Assert.assertTrue("Im Wahlkreis \"constBay2\" darf sich nichts ändern.",
                          this.ElectionAreaSameValues(cloneBay2, this.constBay2, true, true, true, true, true));
        Assert.assertTrue("Im Wahlkreis \"constBay3\" darf sich nichts ändern.",
                          this.ElectionAreaSameValues(cloneBay3, this.constBay3, true, true, true, true, true));
        Assert.assertTrue("Im Wahlkreis \"constBW1\" darf sich nichts ändern.",
                          this.ElectionAreaSameValues(cloneBW1, this.constBW1, true, true, true, true, true));
        Assert.assertTrue("Im Wahlkreis \"constBW2\" darf sich nichts ändern.",
                          this.ElectionAreaSameValues(cloneBW2, this.constBW2, true, true, true, true, true));
        Assert.assertTrue("Im Wahlkreis \"constBW3\" darf sich nichts ändern.",
                          this.ElectionAreaSameValues(cloneBW3, this.constBW3, true, true, true, true, true));
    }
    
    @Test
    public void testNumberOfVotesFederalState() {
        this.propagation = new Propagation(new UniformDistribution());
        
        this.constBay1.setCitizens(20000);
        this.constBay2.setCitizens(20000);
        this.constBay3.setCitizens(20000);
        
        this.constBay1.setEligibleVoters(20000);
        this.constBay2.setEligibleVoters(20000);
        this.constBay3.setEligibleVoters(20000);
        
        this.constBay1.setNumberOfVotes(10000);
        this.constBay2.setNumberOfVotes(10000);
        this.constBay3.setNumberOfVotes(10000);
        
        Party cdu = new Party("CDU");
        Party spd = new Party("SPD");
        Party gruene = new Party("Die Grünen");
        
        this.constBay1.addParty(cdu);
        this.constBay1.addParty(spd);
        this.constBay1.addParty(gruene);
        
        this.constBay1.setFirstVotes(cdu, 2000);
        this.constBay1.setFirstVotes(spd, 2000);
        this.constBay1.setFirstVotes(gruene, 2000);
        this.constBay1.setSecondVotes(cdu, 1000);
        this.constBay1.setSecondVotes(spd, 1000);
        this.constBay1.setSecondVotes(gruene, 1000);
        
        this.constBay2.addParty(cdu);
        this.constBay2.addParty(spd);
        this.constBay2.addParty(gruene);
        
        this.constBay2.setFirstVotes(cdu, 2000);
        this.constBay2.setFirstVotes(spd, 2000);
        this.constBay2.setFirstVotes(gruene, 2000);
        this.constBay2.setSecondVotes(cdu, 1000);
        this.constBay2.setSecondVotes(spd, 1000);
        this.constBay2.setSecondVotes(gruene, 1000);
        
        this.constBay3.addParty(cdu);
        this.constBay3.addParty(spd);
        this.constBay3.addParty(gruene);
        
        this.constBay3.setFirstVotes(cdu, 2000);
        this.constBay3.setFirstVotes(spd, 2000);
        this.constBay3.setFirstVotes(gruene, 2000);
        this.constBay3.setSecondVotes(cdu, 1000);
        this.constBay3.setSecondVotes(spd, 1000);
        this.constBay3.setSecondVotes(gruene, 1000);
        
        Constituency clone1 = this.constBay1.deepClone();
        Constituency clone2 = this.constBay2.deepClone();
        Constituency clone3 = this.constBay3.deepClone();
        
        Assert.assertTrue("Die Propagierung muss erfolgreich verlaufen.", this.propagation.propagateNumberOfVotes(this.stateBay, 60000));
        Assert.assertEquals("Die Anzahl neuer Stimmen im Wahlkreis \"constBay1\" muss 20.000 betragen.", 20000, this.constBay1.getNumberOfVotes());
        Assert.assertTrue("Im Wahlkreis \"constBay1\" darf sich bis auf die Stimmen (ingesamt, Erst- und Zweitstimmen) nichts ändern.",
                          this.ElectionAreaSameValues(clone1, this.constBay1, true, true, false, false, false));
        Assert.assertEquals("Die Anzahl neuer Stimmen im Wahlkreis \"constBay2\" muss 20.000 betragen.", 20000, this.constBay2.getNumberOfVotes());
        Assert.assertTrue("Im Wahlkreis \"constBay2\" darf sich bis auf die Stimmen (ingesamt, Erst- und Zweitstimmen) nichts ändern.",
                          this.ElectionAreaSameValues(clone2, this.constBay2, true, true, false, false, false));
        Assert.assertEquals("Die Anzahl neuer Stimmen im Wahlkreis \"constBay3\" muss 20.000 betragen.", 20000, this.constBay3.getNumberOfVotes());
        Assert.assertTrue("Im Wahlkreis \"constBay3\" darf sich bis auf die Stimmen (ingesamt, Erst- und Zweitstimmen) nichts ändern.",
                          this.ElectionAreaSameValues(clone3, this.constBay3, true, true, false, false, false));
    }
    
    @Test
    public void testValidFirstVotesConstituency() {
        this.propagation = new Propagation(new UniformDistribution());
        
        this.constBay1.setCitizens(20000);
        this.constBay1.setEligibleVoters(12000);
        this.constBay1.setNumberOfVotes(10000);
        
        Party cdu = new Party("CDU");
        Party spd = new Party("SPD");
        Party gruene = new Party("Die Grünen");
        
        this.constBay1.addParty(cdu);
        this.constBay1.addParty(spd);
        this.constBay1.addParty(gruene);
        
        this.constBay1.setFirstVotes(cdu, 2000);
        this.constBay1.setSecondVotes(cdu, 1000);
        this.constBay1.setFirstVotes(spd, 2000);
        this.constBay1.setSecondVotes(spd, 1000);
        this.constBay1.setFirstVotes(gruene, 2000);
        this.constBay1.setSecondVotes(gruene, 1000);
        
        Constituency clone = this.constBay1.deepClone();
        
        Assert.assertTrue("Die Propagierung muss erfolgreich verlaufen.", this.propagation.propagateValidFirstVotesConstituency(this.constBay1, 9000));
        Assert.assertEquals("Die Anzahl neuer gültiger Erststimmen im Wahlkreis \"constBay1\" muss 9.000 betragen.", 9000, this.constBay1.getValidFirstVotes());
        Assert.assertTrue("Bis auf die Erstimmen darf sich im Wahlkreis \"constBay1\" nichts ändern.",
                          this.ElectionAreaSameValues(clone, this.constBay1, true, true, true, false, true));
    }
    
    @Test
    public void testInvalidFirstVotesConstituency() {
        this.propagation = new Propagation(new UniformDistribution());
        
        this.constBay1.setCitizens(20000);
        this.constBay1.setEligibleVoters(12000);
        this.constBay1.setNumberOfVotes(10000);
        
        Party cdu = new Party("CDU");
        Party spd = new Party("SPD");
        Party gruene = new Party("Die Grünen");
        
        this.constBay1.addParty(cdu);
        this.constBay1.addParty(spd);
        this.constBay1.addParty(gruene);
        
        this.constBay1.setFirstVotes(cdu, 2000);
        this.constBay1.setSecondVotes(cdu, 1000);
        this.constBay1.setFirstVotes(spd, 2000);
        this.constBay1.setSecondVotes(spd, 1000);
        this.constBay1.setFirstVotes(gruene, 2000);
        this.constBay1.setSecondVotes(gruene, 1000);
        
        Constituency clone = this.constBay1.deepClone();
        
        Assert.assertTrue("Die Propagierung muss erfolgreich verlaufen.", this.propagation.propagateInvalidFirstVotesConstituency(this.constBay1, 9000));
        Assert.assertEquals("Die Anzahl neuer ungültiger Erststimmen im Wahlkreis \"constBay1\" muss 9.000 betragen.", 9000,
                            this.constBay1.getInvalidFirstVotes());
        Assert.assertTrue("Bis auf die Erstimmen darf sich im Wahlkreis \"constBay1\" nichts ändern.",
                          this.ElectionAreaSameValues(clone, this.constBay1, true, true, true, false, true));
    }
    
    @Test
    public void testValidSecondVotesConstituency() {
        this.propagation = new Propagation(new UniformDistribution());
        
        this.constBay1.setCitizens(20000);
        this.constBay1.setEligibleVoters(12000);
        this.constBay1.setNumberOfVotes(10000);
        
        Party cdu = new Party("CDU");
        Party spd = new Party("SPD");
        Party gruene = new Party("Die Grünen");
        
        this.constBay1.addParty(cdu);
        this.constBay1.addParty(spd);
        this.constBay1.addParty(gruene);
        
        this.constBay1.setFirstVotes(cdu, 2000);
        this.constBay1.setSecondVotes(cdu, 1000);
        this.constBay1.setFirstVotes(spd, 2000);
        this.constBay1.setSecondVotes(spd, 1000);
        this.constBay1.setFirstVotes(gruene, 2000);
        this.constBay1.setSecondVotes(gruene, 1000);
        
        Constituency clone = this.constBay1.deepClone();
        
        Assert.assertTrue("Die Propagierung muss erfolgreich verlaufen.", this.propagation.propagateValidSecondVotesConstituency(this.constBay1, 6000));
        Assert.assertEquals("Die Anzahl neuer gültiger Zweitstimmen im Wahlkreis \"constBay1\" muss 6.000 betragen.", 6000,
                            this.constBay1.getValidSecondVotes());
        Assert.assertTrue("Bis auf die Zweitstimmen darf sich im Wahlkreis \"constBay1\" nichts ändern.",
                          this.ElectionAreaSameValues(clone, this.constBay1, true, true, true, true, false));
    }
    
    @Test
    public void testInvalidSecondVotesConstituency() {
        this.propagation = new Propagation(new UniformDistribution());
        
        this.constBay1.setCitizens(20000);
        this.constBay1.setEligibleVoters(12000);
        this.constBay1.setNumberOfVotes(10000);
        
        Party cdu = new Party("CDU");
        Party spd = new Party("SPD");
        Party gruene = new Party("Die Grünen");
        
        this.constBay1.addParty(cdu);
        this.constBay1.addParty(spd);
        this.constBay1.addParty(gruene);
        
        this.constBay1.setFirstVotes(cdu, 2000);
        this.constBay1.setSecondVotes(cdu, 1000);
        this.constBay1.setFirstVotes(spd, 2000);
        this.constBay1.setSecondVotes(spd, 1000);
        this.constBay1.setFirstVotes(gruene, 2000);
        this.constBay1.setSecondVotes(gruene, 1000);
        
        Constituency clone = this.constBay1.deepClone();
        
        Assert.assertTrue("Die Propagierung muss erfolgreich verlaufen.", this.propagation.propagateInvalidSecondVotesConstituency(this.constBay1, 6000));
        Assert.assertEquals("Die Anzahl neuer ungültiger Zweitstimmen im Wahlkreis \"constBay1\" muss 6.000 betragen.", 6000,
                            this.constBay1.getInvalidSecondVotes());
        Assert.assertTrue("Bis auf die Zweitstimmen darf sich im Wahlkreis \"constBay1\" nichts ändern.",
                          this.ElectionAreaSameValues(clone, this.constBay1, true, true, true, true, false));
    }
}
