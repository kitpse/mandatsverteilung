package edu.kit.iti.formal.pse1.mandatsverteilung.propagation;

import java.util.Collection;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class NormalDistributionTest {
    
    /*
     * True activates printing of the results of the propagation.
     */
    private static boolean debugPrinting = false;
    
    /*
     * Error Messages
     */
    static final String failNoTolerationMessage = "unexpected Propagation Result (no tolerace test) ";
    static final String failWithTolerationMessage = "one array entry differes more than 1 from expected ";
    static final String otherAmountMessage = "the propagated amount was not the expected one ";
    
    /*
     * indicates the number of the parameterized test (incremented at each test execution)
     */
    private static int testCounter = -1;
    
    /*
     * The test values for the parameterized test
     */
    private int[] input;
    private int[] maxValues;
    private int[] expected;
    private int delta;
    private boolean tolerate1;
    
    /*
     * The used PropagationMath
     */
    private static PropagationMath distribution;
    
    /*
     * Test definition
     */
    
    @Rule
    public Timeout globalTimeout = new Timeout(3, TimeUnit.SECONDS);
    
    @BeforeClass
    public static void setUpPropagationMath() {
        NormalDistributionTest.distribution = new NormalDistribution();
    }
    
    // Contains the test cases
    @Parameterized.Parameters
    public static Collection<Object[]> Values() {
        Collection<Object[]> list = new LinkedList<Object[]>();
        
        int m = Integer.MAX_VALUE;
        
        //                                  input,                upper-bound    delta           expected     tolerate-1     test case number
        list.add(new Object[]{new int[]{1, 2, 2, 2, 2}, new int[]{m, m, m, m, m}, 0, new int[]{1, 2, 2, 2, 2}, false}); //          No  0
        
        // TODO more tests (@Alexander)
        
        //
        //
        // return list of test cases
        return list;
    }
    
    /*
     * Test execution
     */
    
    /*
     * Constructs the parameterized test instance.
     */
    public NormalDistributionTest(int[] input, int[] maxValues, int delta, int[] expected, boolean tolerate1) {
        this.input = input;
        this.maxValues = maxValues;
        this.delta = delta;
        this.expected = expected;
        this.tolerate1 = tolerate1;
    }
    
    /*
     * Checks several issues: 1. is the expected amount propagated 2. is an exception thrown 3. are the values as expected
     */
    @Test
    public void concreteValueTest() {
        NormalDistributionTest.testCounter++;
        int[] beforePropagation = this.copyArray(this.input);
        
        try {
            NormalDistributionTest.distribution.propagate(this.input, this.maxValues, this.delta);
        }
        catch(Exception e) {
            Assert.fail("An unexpected " + e.getClass().getName() + " exception occurred");
        }
        
        // print the result of the propagation for debugging if activated
        if(NormalDistributionTest.debugPrinting) {
            this.printArray(this.input);
        }
        
        // stimmt der geänderte Wert insgesamt?
        Assert.assertTrue(NormalDistributionTest.otherAmountMessage, this.delta == this.sumDifference(this.input, beforePropagation));
        
        // je nach testfall: entweder es muss übereintimmung, oder je array-Eintrag eine Abweichung von 1 erlaubt
        if(this.tolerate1) {
            Assert.assertTrue(NormalDistributionTest.failWithTolerationMessage, this.tolerate1equals(this.input, this.expected));
        }
        else {
            Assert.assertArrayEquals(NormalDistributionTest.failNoTolerationMessage, this.expected, this.input);
        }
    }
    
    /*
     * Calculates the sum of all differences of afterPropagation and beforePropagation. The Arrays must have the same length.
     */
    private int sumDifference(int[] afterPropagation, int[] beforePropagation) {
        int sumDif = 0;
        for(int i = 0; i != afterPropagation.length; i++) {
            sumDif += afterPropagation[i] - beforePropagation[i];
        }
        return sumDif;
    }
    
    /*
     * Copies an array in a loop
     */
    private int[] copyArray(int[] x) {
        int[] copy = new int[x.length];
        for(int i = 0; i != x.length; i++) {
            copy[i] = x[i];
        }
        return copy;
    }
    
    /*
     * Returns true if all array entrys of input and expected dont differ more than 1.
     */
    private boolean tolerate1equals(int[] input, int[] expected) {
        if(input.length != expected.length) {
            return false;
        }
        
        for(int i = 0; i != input.length; i++) {
            if(Math.abs(input[i] - expected[i]) > 1) {
                return false;
            }
        }
        return true;
    }
    
    /*
     * Prints the test number and the given array.
     */
    private void printArray(int[] input) {
        System.out.print("TestNr. " + NormalDistributionTest.testCounter + ":  ");
        for(int i = 0; i != this.input.length; i++) {
            System.out.print(this.input[i] + " ");
        }
        System.out.println();
    }
}