package edu.kit.iti.formal.pse1.mandatsverteilung.data.io;

import java.io.File;
import java.io.IOException;
import java.util.Random;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Bundestag;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Election;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Federation;
import edu.kit.iti.formal.pse1.mandatsverteilung.gui.controller.Mandatsverteilung;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class IOTest3X0 {
    private static final String TMP_PATH = new StringBuilder(MemoryAccess.getOsDefaultLocation()).append("tmpelection").append(new Random().nextLong())
            .append(".csv").toString(); // some random file name, which is hopefully not taken
    
    private static Election election;
    private static Federation federation;
    private static Bundestag bundestag;
    
    @BeforeClass
    public static void setUpClass() {
        IOTest3X0.election = Import.importInternalElection("bundestagswahl2013.csv");
        IOTest3X0.federation = IOTest3X0.election.getElectionResult().getFederation();
        IOTest3X0.bundestag = IOTest3X0.election.getBundestag();
    }
    
    @AfterClass
    public static void tearDownClass() {
        IOTest3X0.election = null;
        IOTest3X0.federation = null;
        IOTest3X0.bundestag = null;
        new File(IOTest3X0.TMP_PATH).delete();
    }
    
    @Test
    // part of /T310/
    public void checkEligibleVoters() {
        Assert.assertEquals("Anzahl der Wahlberechtigten wurde nicht richtig geladen", 61946900, IOTest3X0.federation.getEligibleVoters());
    }
    
    @Test
    // part of /T310/
    public void checkNumberOfVotes() {
        Assert.assertEquals("Anzahl der Wähler wurde nicht richtig geladen", 44309925, IOTest3X0.federation.getNumberOfVotes());
    }
    
    @Test
    // part of /T310/
    public void checkInvalidFirstVotes() {
        Assert.assertEquals("Anzahl der Ungültigen Erststimmen wurde nicht richtig geladen", 684883, IOTest3X0.federation.getInvalidFirstVotes());
    }
    
    @Test
    // part of /T310/
    public void checkInvalidSecondVotes() {
        Assert.assertEquals("Anzahl der Ungültigen Zweitstimmen wurde nicht richtig geladen", 583069, IOTest3X0.federation.getInvalidSecondVotes());
    }
    
    @Test
    // part of /T310/
    public void checkValidFirstVotes() {
        Assert.assertEquals("Anzahl der Gültigen Erststimmen wurde nicht richtig geladen", 43625042, IOTest3X0.federation.getValidFirstVotes());
    }
    
    @Test
    // part of /T310/
    public void checkValidSecondVotes() {
        Assert.assertEquals("Anzahl der Gültigen Zweitstimmen wurde nicht richtig geladen", 43726856, IOTest3X0.federation.getValidSecondVotes());
    }
    
    @Test
    // test /T320/
    public void exportElection2013() {
        try {
            Export.exportElection(IOTest3X0.election, IOTest3X0.TMP_PATH);
        }
        catch(IOException e) {
            Assert.fail(Mandatsverteilung.getNestedErrorMessages(e));
        }
    }
    
    @Test
    // test /T320a/
    public void reimport2013AndCheckBundestag() {
        try {
            Assert.assertEquals("Der Bundestag hat sich nach dem Reimport geändert!", IOTest3X0.bundestag, Import.importElection(IOTest3X0.TMP_PATH)
                    .getBundestag());
        }
        catch(NullPointerException | IOException | FormatException e) {
            Assert.fail(Mandatsverteilung.getNestedErrorMessages(e));
        }
    }
    
    @Test
    // general reimport test
    public void reimport2013AndCheckElection() {
        try {
            Assert.assertEquals("Die Wahl hat sich nach dem Reimport geändert!", IOTest3X0.election, Import.importElection(IOTest3X0.TMP_PATH));
        }
        catch(NullPointerException | IOException | FormatException e) {
            Assert.fail(Mandatsverteilung.getNestedErrorMessages(e));
        }
    }
}
