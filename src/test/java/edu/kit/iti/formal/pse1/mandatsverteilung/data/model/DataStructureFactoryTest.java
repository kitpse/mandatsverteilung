package edu.kit.iti.formal.pse1.mandatsverteilung.data.model;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

public class DataStructureFactoryTest {
    
    @Rule
    public Timeout globalTimeout = new Timeout(5, TimeUnit.SECONDS); // 5 seconds max per method tested
    
    @Before
    public void setUp() throws Exception {
    }
    
    @After
    public void tearDown() throws Exception {
    }
    
    /**
     * Checks if the specified values are the same.
     */
    private boolean ElectionAreaSameValues(ElectionArea<?> area1, ElectionArea<?> area2, boolean citizens, boolean eligibleVoters, boolean numberOfVotes,
                                           boolean validFirst, boolean validSecond) {
        if(citizens) {
            if(area1.getCitizens() != area2.getCitizens()) {
                return false;
            }
        }
        if(eligibleVoters) {
            if(area1.getEligibleVoters() != area2.getEligibleVoters()) {
                return false;
            }
        }
        if(numberOfVotes) {
            if(area1.getNumberOfVotes() != area2.getNumberOfVotes()) {
                return false;
            }
        }
        if(validFirst) {
            if(area1.getValidFirstVotes() != area2.getValidFirstVotes()) {
                return false;
            }
        }
        if(validSecond) {
            if(area1.getValidSecondVotes() != area2.getValidSecondVotes()) {
                return false;
            }
        }
        return true;
    }
    
    @Test
    public void testCreate_NumberFederalStates() {
        ElectionResult result = new ElectionResult();
        Federation fed = DataStructureFactory.createDataHierarchy(result);
        Assert.assertEquals("Die erzeugte Struktur muss 16 Bundesländer haben.", fed.getChildren().size(), 16);
    }
    
    @Test
    public void testCreate_NumberConstituencies() {
        ElectionResult result = new ElectionResult();
        Federation fed = DataStructureFactory.createDataHierarchy(result);
        int sum = fed.getChildren().stream().mapToInt(federalState -> federalState.getChildren().size()).sum();
        Assert.assertEquals("Die erzeugte Struktur muss 299 Wahlkreise haben.", sum, 299);
    }
    
    @Test
    public void testClone_EqualValues() {
        ElectionResult result = new ElectionResult();
        Federation fed = new Federation("Deutschland", result);
        FederalState state1 = new FederalState("BW", result);
        FederalState state2 = new FederalState("Bayern", result);
        Constituency constBW1 = new Constituency("BW_1", result);
        Constituency constBW2 = new Constituency("BW_2", result);
        Constituency constBay1 = new Constituency("Bayern_1", result);
        
        fed.addChild(state1);
        fed.addChild(state2);
        state1.addChild(constBW1);
        state1.addChild(constBW2);
        state2.addChild(constBay1);
        
        ElectionResult clone = result.deepClone();
        Federation clonedFed = clone.getFederation();
        
        Assert.assertTrue("Die Werte des geklonten Bundes sind gleich.", this.ElectionAreaSameValues(fed, clonedFed, true, true, true, true, true));
    }
    
    @Test
    public void testClone_ChangedCitizens() {
        ElectionResult result = new ElectionResult();
        Federation fed = new Federation("Deutschland", result);
        FederalState state1 = new FederalState("BW", result);
        FederalState state2 = new FederalState("Bayern", result);
        Constituency constBW1 = new Constituency("BW_1", result);
        Constituency constBW2 = new Constituency("BW_2", result);
        Constituency constBay1 = new Constituency("Bayern_1", result);
        
        fed.addChild(state1);
        fed.addChild(state2);
        state1.addChild(constBW1);
        state1.addChild(constBW2);
        state2.addChild(constBay1);
        
        constBay1.setCitizens(100000);
        
        ElectionResult clone = result.deepClone();
        Federation clonedFed = clone.getFederation();
        clonedFed.changeCitizens(4000000);
        
        Assert.assertTrue("Die Werte des geklonten Bundes sind bis auf die Bürger gleich.",
                          this.ElectionAreaSameValues(fed, clonedFed, false, true, true, true, true));
    }
    
    @Test
    public void testClone_ChangedEligibleVoters() {
        ElectionResult result = new ElectionResult();
        Federation fed = new Federation("Deutschland", result);
        FederalState state1 = new FederalState("BW", result);
        FederalState state2 = new FederalState("Bayern", result);
        Constituency constBW1 = new Constituency("BW_1", result);
        Constituency constBW2 = new Constituency("BW_2", result);
        Constituency constBay1 = new Constituency("Bayern_1", result);
        
        fed.addChild(state1);
        fed.addChild(state2);
        state1.addChild(constBW1);
        state1.addChild(constBW2);
        state2.addChild(constBay1);
        
        ElectionResult clone = result.deepClone();
        Federation clonedFed = clone.getFederation();
        
        constBay1.setEligibleVoters(60000);
        
        Assert.assertTrue("Die Werte des geklonten Bundes sind bis auf die Wahlberechtigten gleich.",
                          this.ElectionAreaSameValues(fed, clonedFed, true, false, true, true, true));
    }
    
    @Test
    public void testClone_ChangedNumberOfVotes() {
        ElectionResult result = new ElectionResult();
        Federation fed = new Federation("Deutschland", result);
        FederalState state1 = new FederalState("BW", result);
        FederalState state2 = new FederalState("Bayern", result);
        Constituency constBW1 = new Constituency("BW_1", result);
        Constituency constBW2 = new Constituency("BW_2", result);
        Constituency constBay1 = new Constituency("Bayern_1", result);
        
        fed.addChild(state1);
        fed.addChild(state2);
        state1.addChild(constBW1);
        state1.addChild(constBW2);
        state2.addChild(constBay1);
        
        ElectionResult clone = result.deepClone();
        Federation clonedFed = clone.getFederation();
        
        constBay1.setNumberOfVotes(100000);
        
        Assert.assertTrue("Die Werte des geklonten Bundes sind bis auf die Wähler gleich.",
                          this.ElectionAreaSameValues(fed, clonedFed, true, true, false, true, true));
    }
    
    @Test
    public void testClone_ChangedSecondVotesParty() {
        ElectionResult result = new ElectionResult();
        Federation fed = new Federation("Deutschland", result);
        FederalState state1 = new FederalState("BW", result);
        FederalState state2 = new FederalState("Bayern", result);
        Constituency constBW1 = new Constituency("BW_1", result);
        Constituency constBW2 = new Constituency("BW_2", result);
        Constituency constBay1 = new Constituency("Bayern_1", result);
        
        Party spd = new Party("SPD");
        constBW1.addParty(spd);
        constBW1.changeSecondVotes(spd, 10000);
        
        fed.addChild(state1);
        fed.addChild(state2);
        state1.addChild(constBW1);
        state1.addChild(constBW2);
        state2.addChild(constBay1);
        
        ElectionResult clone = result.deepClone();
        Federation clonedFed = clone.getFederation();
        
        constBW1.changeNumberOfVotes(20000);
        
        Assert.assertNotEquals("Eine Änderung einer Partei im ursprünglichen Ergebnis ändert nichts im geklonten Ergebnis.", fed.getSecondVotes(spd),
                               clonedFed.getSecondVotes(spd));
    }
}
