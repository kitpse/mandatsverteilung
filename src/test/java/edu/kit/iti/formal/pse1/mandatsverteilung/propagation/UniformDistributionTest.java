package edu.kit.iti.formal.pse1.mandatsverteilung.propagation;

import java.util.Collection;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class UniformDistributionTest {
    
    /*
     * True activates printing of the results of the propagation.
     */
    private static boolean debugPrinting = false;
    
    /*
     * Error Messages
     */
    static final String failNoTolerationMessage = "unexpected Propagation Result (no tolerace test) ";
    static final String failWithTolerationMessage = "one array entry differes more than 1 from expected ";
    static final String otherAmountMessage = "the propagated amount was not the expected one ";
    
    /*
     * indicates the number of the parameterized test (incremented at each test execution)
     */
    private static int testCounter = -1;
    
    /*
     * The test values for the parameterized test
     */
    private int[] input;
    private int[] maxValues;
    private int[] expected;
    private int delta;
    private boolean tolerate1;
    
    /*
     * The used PropagationMath
     */
    private static UniformDistribution distribution;
    
    /*
     * Test definition
     */
    
    @Rule
    public Timeout globalTimeout = new Timeout(200, TimeUnit.MILLISECONDS);
    
    @BeforeClass
    public static void setUpPropagationMath() {
        UniformDistributionTest.distribution = new UniformDistribution();
    }
    
    // Contains the test cases
    @Parameterized.Parameters
    public static Collection<Object[]> Values() {
        Collection<Object[]> list = new LinkedList<Object[]>();
        
        int maxint = Integer.MAX_VALUE; // maxint is odd by the way
        int m = maxint;
        //                                  input,                 upper-bound   delta           expected     tolerate-1     test case number
        
        list.add(new Object[]{new int[]{0, 0, 0, 0, 0}, new int[]{m, m, m, m, m}, 20, new int[]{4, 4, 4, 4, 4}, false}); //         No  0
        list.add(new Object[]{new int[]{1, 1, 1, 1, 1}, new int[]{m, m, m, m, m}, 20, new int[]{5, 5, 5, 5, 5}, false}); //         No  1
        list.add(new Object[]{new int[]{5000, 5000, 5000, 5000, 5000}, new int[]{m, m, m, m, m}, 20, new int[]{5004, 5004, 5004, 5004, 5004}, false}); // No  2
        list.add(new Object[]{new int[]{1, 2, 2, 2, 2}, new int[]{m, m, m, m, m}, 20, new int[]{5, 6, 6, 6, 6}, false}); //         No  3
        list.add(new Object[]{new int[]{1, 2, 2, 2, 2}, new int[]{m, m, m, m, m}, 19, new int[]{5, 6, 6, 6, 6}, true}); //          No  4
        list.add(new Object[]{new int[]{1, 2, 2, 2, 2}, new int[]{m, m, m, m, m}, 21, new int[]{5, 6, 6, 6, 6}, true}); //          No  5
        list.add(new Object[]{new int[]{2, 1, 2, 2, 2}, new int[]{m, m, m, m, m}, 19, new int[]{6, 5, 6, 6, 6}, true}); //          No  6
        list.add(new Object[]{new int[]{2, 1, 2, 2, 2}, new int[]{m, m, m, m, m}, 21, new int[]{6, 5, 6, 6, 6}, true}); //          No  7
        list.add(new Object[]{new int[]{2, 1, 2, 2, 2}, new int[]{m, m, m, m, m}, 20, new int[]{6, 5, 6, 6, 6}, false}); //         No  8
        list.add(new Object[]{new int[]{1, 2, 2, 2, 2}, new int[]{m, m, m, m, m}, 80, new int[]{17, 18, 18, 18, 18}, false}); //    No  9
        list.add(new Object[]{new int[]{1, 2, 2, 2, 2}, new int[]{m, m, m, m, m}, 0, new int[]{1, 2, 2, 2, 2}, false}); //          No 10
        list.add(new Object[]{new int[]{2, 2, 2, 2, 2}, new int[]{m, m, m, m, m}, -10, new int[]{0, 0, 0, 0, 0}, false}); //        No 11
        list.add(new Object[]{new int[]{1, 2, 2, 2, 2}, new int[]{m, m, m, m, m}, -9, new int[]{0, 0, 0, 0, 0}, false}); //         No 12
        list.add(new Object[]{new int[]{5000, 2, 2, 2, 2}, new int[]{m, m, m, m, m}, -10, new int[]{4998, 0, 0, 0, 0}, false}); //  No 13
        list.add(new Object[]{new int[]{5000, 2, 2, 2, 2}, new int[]{m, m, m, m, m}, -20, new int[]{4988, 0, 0, 0, 0}, false}); //  No 14
        list.add(new Object[]{new int[]{200, 201, 202, 203, 0}, new int[]{m, m, m, m, m}, -20, new int[]{195, 196, 197, 198, 0}, false}); // No 15
        list.add(new Object[]{new int[]{200, 201, 202, 0}, new int[]{m, m, m, m}, -20, new int[]{194, 195, 196, 0}, true}); //   No 16
        list.add(new Object[]{new int[]{maxint, 0, 0, maxint}, new int[]{m, m, m, m}, -maxint, new int[]{maxint / 2, 0, 0, maxint / 2}, true}); // No 17
        list.add(new Object[]{new int[]{maxint - 2, 0, 0, maxint - 2}, new int[]{m, m, m, m}, -maxint, new int[]{maxint / 2 - 2, 0, 0, maxint / 2 - 2}, true}); // No 18
        list.add(new Object[]{new int[]{maxint, 0, 0, 0}, new int[]{m, m, m, m}, -maxint, new int[]{0, 0, 0, 0}, false}); //     No 19
        list.add(new Object[]{new int[]{1573684200, 0, 0, 0}, new int[]{m, m, m, m}, -10000, new int[]{1573674200, 0, 0, 0}, false}); // No 20
        list.add(new Object[]{new int[]{1573684200, 0, 0, 0}, new int[]{m, m, m, m}, -20000000, new int[]{1553684200, 0, 0, 0}, false}); // No 21
        list.add(new Object[]{new int[]{3, 3, 3, 3, 3}, new int[]{m, m, m, m, m}, -5, new int[]{2, 2, 2, 2, 2}, false}); //         No 22
        list.add(new Object[]{new int[]{3, 3, 3, 3, 3}, new int[]{m, m, m, m, m}, -7, new int[]{2, 2, 2, 2, 2}, true}); //          No 23
        list.add(new Object[]{new int[]{0, 0, 0, 0, 0}, new int[]{m, m, m, m, m}, 2, new int[]{0, 0, 0, 0, 0}, true}); //          No 24
        list.add(new Object[]{new int[]{3, 0, 0, 0, 3}, new int[]{m, m, m, m, m}, 3, new int[]{3, 0, 0, 0, 3}, true}); //          No 25
        
        list.add(new Object[]{new int[]{3, 3, 0, 0, 3}, new int[]{3, 3, 3, 3, 3}, 6, new int[]{3, 3, 3, 3, 3}, false}); //          No 26
        list.add(new Object[]{new int[]{3, 3, 0, 0, 3}, new int[]{3, 3, 3, 3, 3}, 5, new int[]{2, 2, 2, 2, 2}, true}); //          No 27
        list.add(new Object[]{new int[]{4, 4, 4, 1}, new int[]{7, 7, 4, 2}, 5, new int[]{7, 7, 4, 2}, true}); //          No 28
        list.add(new Object[]{new int[]{0, 0, 0, 0}, new int[]{0, 0, 0, 0}, 0, new int[]{0, 0, 0, 0}, false}); //         No 29
        list.add(new Object[]{new int[]{4, 1, 1, 1}, new int[]{5, 10, 10, 10}, 7, new int[]{5, 3, 3, 3}, false}); //      No 30
        list.add(new Object[]{new int[]{8, 4, 4, 1}, new int[]{10, 20, 20, 20}, 12, new int[]{10, 7, 7, 4}, true}); //    No 31
        list.add(new Object[]{new int[]{8, 4, 4, 1}, new int[]{10, 20, 20, 20}, 14, new int[]{10, 8, 8, 5}, false}); //   No 32
        
        //
        //
        // return list of test cases
        return list;
    }
    
    /*
     * Test execution
     */
    
    /*
     * Constructs the parameterized test instance.
     */
    public UniformDistributionTest(int[] input, int[] maxValues, int delta, int[] expected, boolean tolerate1) {
        this.input = input;
        this.maxValues = maxValues;
        this.delta = delta;
        this.expected = expected;
        this.tolerate1 = tolerate1;
    }
    
    /*
     * Checks several issues: 1. is the expected amount propagated 2. is an exception thrown 3. are the values as expected
     */
    @Test
    public void concreteValueTest() {
        UniformDistributionTest.testCounter++;
        int[] beforePropagation = this.copyArray(this.input);
        
        try {
            UniformDistributionTest.distribution.propagate(this.input, this.maxValues, this.delta);
        }
        catch(Exception e) {
            Assert.fail("An unexpected " + e.getClass().getName() + " exception occurred");
        }
        
        // print the result of the propagation for debugging if activated
        if(UniformDistributionTest.debugPrinting) {
            this.printArray(this.input);
        }
        
        // stimmt der geänderte Wert insgesamt?
        Assert.assertTrue(UniformDistributionTest.otherAmountMessage, this.delta == this.sumDifference(this.input, beforePropagation));
        
        // je nach testfall: entweder es muss übereintimmung, oder je array-Eintrag eine Abweichung von 1 erlaubt
        if(this.tolerate1) {
            Assert.assertTrue(UniformDistributionTest.failWithTolerationMessage, this.tolerate1equals(this.input, this.expected));
        }
        else {
            Assert.assertArrayEquals(UniformDistributionTest.failNoTolerationMessage, this.expected, this.input);
        }
    }
    
    /*
     * Calculates the sum of all differences of afterPropagation and beforePropagation. The Arrays must have the same length.
     */
    private int sumDifference(int[] afterPropagation, int[] beforePropagation) {
        int sumDif = 0;
        for(int i = 0; i != afterPropagation.length; i++) {
            sumDif += afterPropagation[i] - beforePropagation[i];
        }
        return sumDif;
    }
    
    /*
     * Copies an array in a loop
     */
    private int[] copyArray(int[] x) {
        int[] copy = new int[x.length];
        System.arraycopy(x, 0, copy, 0, x.length);
        return copy;
    }
    
    /*
     * Returns true if all array entrys of input and expected dont differ more than 1.
     */
    private boolean tolerate1equals(int[] input, int[] expected) {
        if(input.length != expected.length) {
            return false;
        }
        
        for(int i = 0; i != input.length; i++) {
            if(Math.abs(input[i] - expected[i]) > 1) {
                return false;
            }
        }
        return true;
    }
    
    /*
     * Prints the test number and the given array.
     */
    private void printArray(int[] input) {
        System.out.print("TestNr. " + UniformDistributionTest.testCounter + ":  ");
        for(int i = 0; i != this.input.length; i++) {
            System.out.print(this.input[i] + " ");
        }
        System.out.println();
    }
}