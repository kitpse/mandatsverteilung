package edu.kit.iti.formal.pse1.mandatsverteilung.data.model.PropagationOfficialStructure;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
/* @formatter:off */
@SuiteClasses({
    PropagationCitizensProportionalTest.class,
    PropagationCitizensUniformTest.class,
    PropagationEligibleVotersProportionalTest.class,
    PropagationEligibleVotersUniformTest.class,
    PropagationFirstVotesPartyUniformTest.class,
    PropagationInvalidFirstVotesProportionalTest.class,
    PropagationInvalidFirstVotesUniformTest.class,
    PropagationInvalidSecondVotesProportionalTest.class,
    PropagationInvalidSecondVotesUniformTest.class,
    PropagationNumberOfVotesProportionalTest.class,
    PropagationNumberOfVotesUniformTest.class,
    PropagationSecondVotesPartyUniformTest.class,
    PropagationValidFirstVotesProportionalTest.class,
    PropagationValidFirstVotesUniformTest.class,
    PropagationValidSecondVotesProportionalTest.class,
    PropagationValidSecondVotesUniformTest.class
})
/* @formatter:off */
public class PropagationTestOfficialStructureAllTests {
}
