package edu.kit.iti.formal.pse1.mandatsverteilung.data.model;

import java.util.Collection;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.io.Import;

@RunWith(Parameterized.class)
public class DeepCloneTest {
    
    @SuppressWarnings("rawtypes")
    private DeepCloneable deepCloneable;
    
    @Rule
    public Timeout globalTimeout = new Timeout(3, TimeUnit.SECONDS);
    
    @SuppressWarnings("rawtypes")
    public DeepCloneTest(DeepCloneable testobject) {
        this.deepCloneable = testobject;
    }
    
    @Parameters
    public static Collection<Object> Values() {
        Collection<Object> list = new LinkedList<>();
        
        /*
         * Test with imported Election
         */
        Election election = Import.importInternalElection("bundestagswahl2013.csv");
        
        list.add(election);
        list.add(election.getSettings());
        list.add(election.getElectionResult());
        list.add(election.getElectionResult().getFederation());
        
        for(int i = 0; i != election.getElectionResult().getFederation().getChildren().size(); i++) {
            list.add(election.getElectionResult().getFederation().getChildren().get(i));
        }
        for(int i = 0; i != election.getElectionResult().getFederation().getChildren().get(0).getChildren().size(); i++) {
            list.add(election.getElectionResult().getFederation().getChildren().get(0).getChildren().get(i));
        }
        
        /*
         * Other tests
         */
        list.add(new Constituency("Test-Constituency", new ElectionResult()));
        
        return list;
        
    }
    
    @Test
    public void deepCloneTestElection() {
        Assert.assertEquals("Geklontes Objekt ist nicht gleich dem Orginalen!", this.deepCloneable, this.deepCloneable.deepClone());
    }
}
