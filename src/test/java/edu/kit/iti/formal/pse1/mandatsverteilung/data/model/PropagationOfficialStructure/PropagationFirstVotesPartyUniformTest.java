package edu.kit.iti.formal.pse1.mandatsverteilung.data.model.PropagationOfficialStructure;

import java.util.Collection;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.io.Import;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Election;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.ElectionResult;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Federation;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Party;
import edu.kit.iti.formal.pse1.mandatsverteilung.propagation.UniformDistribution;

@RunWith(Parameterized.class)
public class PropagationFirstVotesPartyUniformTest {
    private int inputNumber;
    private Boolean expectedResult;
    private String partyName;
    // the following values indicate which values have to stay the same after the propagation
    private boolean citizens;
    private boolean eligibleVoters;
    private boolean numberOfVotes;
    private boolean validFirst;
    private boolean validSecond;
    private Federation federation;
    private Federation clonedStructure;
    
    private PropagationTestHelper helper;
    
    @Rule
    public Timeout globalTimeout = new Timeout(3, TimeUnit.SECONDS);
    
    @Before
    public void setUp() throws Exception {
        Election election = Import.importInternalElection("bundestagswahl2013.csv");
        ElectionResult result = election.getElectionResult();
        result.removeChangeListener(election);
        result.changePropagation(new UniformDistribution());
        this.federation = result.getFederation();
        this.clonedStructure = this.federation.deepClone();
        this.helper = new PropagationTestHelper();
    }
    
    @After
    public void tearDown() throws Exception {
        this.federation = null;
        this.clonedStructure = null;
        this.helper = null;
    }
    
    public PropagationFirstVotesPartyUniformTest(int inputNumber, String partyName, boolean expectedResult, boolean citizens, boolean eligibleVoters,
                                                 boolean numberOfVotes, boolean validFirst, boolean validSecond) {
        this.inputNumber = inputNumber;
        this.expectedResult = expectedResult;
        this.partyName = partyName;
        this.citizens = citizens;
        this.eligibleVoters = eligibleVoters;
        this.numberOfVotes = numberOfVotes;
        this.validFirst = validFirst;
        this.validSecond = validSecond;
    }
    
    @Parameterized.Parameters
    public static Collection<Object[]> newCitizens() {
        // citizens: 61946900
        // eligible voters: 61946900
        // number of votes: 44309925            non voters: 17636975
        // valid first votes: 43625042          invalid first votes: 684883
        // valid second votes: 43726856         invalid second votes: 583069    
        // SPD first votes: 12843458            SPD second votes: 11252215
        // CSU first votes: 3544079             CSU second votes: 3243569
        Collection<Object[]> list = new LinkedList<Object[]>();
        // test for parties present in more federal states
        list.add(new Object[]{Integer.MAX_VALUE, "SPD", false, true, true, true, true, true}); // 0 set to amount to great
        list.add(new Object[]{44309926, "SPD", false, true, true, true, true, true}); // 1 set to amount to great
        list.add(new Object[]{17636975 + 684883 + 12843458 + 1, "SPD", false, true, true, true, true, true}); // 2 set to maximum possible amount + 1
        list.add(new Object[]{17636975 + 684883 + 12843458, "SPD", true, true, true, false, false, true}); // 3 set to maximum possible amount (sum number of votes in all constituencies the CDU is present)
        list.add(new Object[]{17636975 + 684883 + 12843458 - 1, "SPD", true, true, true, false, false, true}); // 4 set to maximum possible amount - 1
        list.add(new Object[]{12843459, "SPD", true, true, true, false, false, true}); // 5 one more
        list.add(new Object[]{12843458, "SPD", true, true, true, true, true, true}); // 6 no change
        list.add(new Object[]{12843457, "SPD", true, true, true, true, false, true}); // 7 one less
        list.add(new Object[]{10000, "SPD", true, true, true, true, false, true});
        list.add(new Object[]{100, "SPD", true, true, true, true, false, true});
        list.add(new Object[]{1, "SPD", true, true, true, true, false, true});
        list.add(new Object[]{0, "SPD", true, true, true, true, false, true});
        list.add(new Object[]{-1, "SPD", false, true, true, true, true, true});
        list.add(new Object[]{-21309480, "SPD", false, true, true, true, true, true});
        list.add(new Object[]{Integer.MIN_VALUE, "SPD", false, true, true, true, true, true});
        
        // test for a party only present in one federal state
        list.add(new Object[]{Integer.MAX_VALUE, "CSU", false, true, true, true, true, true}); // 15 set to amount to great
        list.add(new Object[]{44309926, "CSU", false, true, true, true, true, true}); // 16 set to amount to great
        list.add(new Object[]{2839012 + 62423 + 3544079 + 1, "CSU", false, true, true, true, true, true}); // 17 set to maximum possible amount + 1
        list.add(new Object[]{2839012 + 62423 + 3544079, "CSU", true, true, true, false, false, true}); // 18 set to maximum possible amount (sum number of votes in all constituencies the CDU is present)
        list.add(new Object[]{2839012 + 62423 + 3544079 - 1, "CSU", true, true, true, false, false, true}); // 19 set to maximum possible amount - 1
        list.add(new Object[]{3544080, "CSU", true, true, true, false, false, true}); // 20 one more
        list.add(new Object[]{3544079, "CSU", true, true, true, true, true, true}); // 21 no change
        list.add(new Object[]{3544078, "CSU", true, true, true, true, false, true}); // 22 one less
        list.add(new Object[]{10000, "CSU", true, true, true, true, false, true});
        list.add(new Object[]{100, "CSU", true, true, true, true, false, true});
        list.add(new Object[]{1, "CSU", true, true, true, true, false, true});
        list.add(new Object[]{0, "CSU", true, true, true, true, false, true});
        list.add(new Object[]{-1, "CSU", false, true, true, true, true, true});
        list.add(new Object[]{-21309480, "CSU", false, true, true, true, true, true});
        list.add(new Object[]{Integer.MIN_VALUE, "CSU", false, true, true, true, true, true});
        return list;
    }
    
    @Test
    public void testUniform_PropagateCitizensFederation() {
        
        Party party = null;
        for(Party p : this.federation.getParties()) {
            if(p.getName().equalsIgnoreCase(this.partyName)) {
                party = p;
            }
        }
        
        Assert.assertNotNull("Die angegebene Partei muss im Bundesgebiet vorhanden sein.", party);
        
        Assert.assertEquals("Beim ändern der Erststimmen der Partei " + this.partyName + " auf " + this.inputNumber + " muss die Propagierung das Ergebnis "
                            + this.expectedResult.toString() + " liefern.", this.federation.changeFirstVotes(party, this.inputNumber), this.expectedResult);
        if(this.expectedResult) {
            Assert.assertEquals("Der neue Wert muss stimmen.", this.inputNumber, this.federation.getFirstVotes(party));
        }
        this.helper.ElectionAreaSameValuesFederation(this.federation, this.clonedStructure, this.citizens, this.eligibleVoters, this.numberOfVotes,
                                                     this.validFirst, this.validSecond);
        this.helper.checkConsistency(this.federation);
    }
}
