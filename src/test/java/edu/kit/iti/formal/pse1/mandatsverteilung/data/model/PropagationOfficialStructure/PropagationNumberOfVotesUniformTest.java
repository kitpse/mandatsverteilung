package edu.kit.iti.formal.pse1.mandatsverteilung.data.model.PropagationOfficialStructure;

import java.util.Collection;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.io.Import;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Election;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.ElectionResult;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Federation;
import edu.kit.iti.formal.pse1.mandatsverteilung.propagation.UniformDistribution;

@RunWith(Parameterized.class)
public class PropagationNumberOfVotesUniformTest {
    private int inputNumber;
    private Boolean expectedResult;
    // the following values indicate which values have to stay the same after the propagation
    private boolean citizens;
    private boolean eligibleVoters;
    private boolean numberOfVotes;
    private boolean validFirst;
    private boolean validSecond;
    private Federation federation;
    private Federation clonedStructure;
    
    private PropagationTestHelper helper;
    
    @Rule
    public Timeout globalTimeout = new Timeout(3, TimeUnit.SECONDS);
    
    @Before
    public void setUp() throws Exception {
        Election election = Import.importInternalElection("bundestagswahl2013.csv");
        ElectionResult result = election.getElectionResult();
        result.removeChangeListener(election);
        result.changePropagation(new UniformDistribution());
        this.federation = result.getFederation();
        this.clonedStructure = this.federation.deepClone();
        this.helper = new PropagationTestHelper();
    }
    
    @After
    public void tearDown() throws Exception {
        this.federation = null;
        this.clonedStructure = null;
        this.helper = null;
    }
    
    public PropagationNumberOfVotesUniformTest(int inputNumber, boolean expectedResult, boolean citizens, boolean eligibleVoters, boolean numberOfVotes,
                                               boolean validFirst, boolean validSecond) {
        this.inputNumber = inputNumber;
        this.expectedResult = expectedResult;
        this.citizens = citizens;
        this.eligibleVoters = eligibleVoters;
        this.numberOfVotes = numberOfVotes;
        this.validFirst = validFirst;
        this.validSecond = validSecond;
    }
    
    @Parameterized.Parameters
    public static Collection<Object[]> newNumberOfVotes() {
        // citizens: 61946900
        // eligible voters: 61946900
        // number of votes: 44309925
        Collection<Object[]> list = new LinkedList<Object[]>();
        list.add(new Object[]{Integer.MAX_VALUE, true, false, false, false, false, false}); // No 0
        list.add(new Object[]{70000000, true, false, false, false, false, false}); // No 1
        list.add(new Object[]{61946901, true, false, false, false, false, false}); // No 2
        // the following tests increase the amount of number of votes; this can also cause the amount of citizens and eligible voters to increase, 
        // since the participation in the election can differ from constituency to constituency
        list.add(new Object[]{61946900, true, false, false, false, false, false}); // No 3
        list.add(new Object[]{61946899, true, false, false, false, false, false}); // No 4
        list.add(new Object[]{47000000, true, false, false, false, false, false}); // No 5
        list.add(new Object[]{44309926, true, false, false, false, false, false}); // No 6
        list.add(new Object[]{44309925, true, true, true, true, true, true}); // No 7
        list.add(new Object[]{44309924, true, true, true, false, false, false}); // No 8
        list.add(new Object[]{10000000, true, true, true, false, false, false}); // No 9
        list.add(new Object[]{1000000, true, true, true, false, false, false}); // No 10
        list.add(new Object[]{100000, true, true, true, false, false, false}); // No 11
        list.add(new Object[]{10000, true, true, true, false, false, false}); // No 12
        list.add(new Object[]{1000, true, true, true, false, false, false}); // No 13
        list.add(new Object[]{100, true, true, true, false, false, false}); // No 14
        list.add(new Object[]{10, true, true, true, false, false, false}); // No 15
        list.add(new Object[]{1, true, true, true, false, false, false}); // No 16
        list.add(new Object[]{0, true, true, true, false, false, false}); // No 17
        list.add(new Object[]{-1, false, true, true, true, true, true}); // No 18
        list.add(new Object[]{-34982374, false, true, true, true, true, true}); // No 19
        list.add(new Object[]{Integer.MIN_VALUE, false, true, true, true, true, true}); // No 20
        return list;
    }
    
    @Test
    public void testUniform_PropagateNumberOfVotesFederation() {
        Assert.assertEquals("Beim ändern der Wähler auf " + this.inputNumber + " muss die Propagierung das Ergebnis " + this.expectedResult.toString()
                            + " liefern.", this.federation.changeNumberOfVotes(this.inputNumber), this.expectedResult);
        if(this.expectedResult) {
            Assert.assertEquals("Der neue Wert muss stimmen.", this.federation.getNumberOfVotes(), this.inputNumber);
        }
        this.helper.ElectionAreaSameValuesFederation(this.federation, this.clonedStructure, this.citizens, this.eligibleVoters, this.numberOfVotes,
                                                     this.validFirst, this.validSecond);
        this.helper.checkConsistency(this.federation);
    }
}
