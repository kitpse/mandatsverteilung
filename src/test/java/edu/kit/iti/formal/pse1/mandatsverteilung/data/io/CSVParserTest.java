package edu.kit.iti.formal.pse1.mandatsverteilung.data.io;

import java.util.Arrays;
import java.util.Collection;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class CSVParserTest {
    private static final String testFilePath = "../test-classes/csvparsertestfile.txt";
    private static CSVParser parser;
    private int row;
    private int column;
    private String expected;
    
    public CSVParserTest(int row, int column, String expected) {
        this.row = row;
        this.column = column;
        this.expected = expected;
    }
    
    @Parameters(name = "{index}: row={0}, column={1}, expected={2}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{{0, 0, "0"}, {1, 2, "7"}, {3, 2, "12"}, {3, 0, "10"}, {2, 0, ""}, {1, 5, ""}, {3, 3, " "}, {3, 5, "13"}});
    }
    
    @BeforeClass
    public static void setUp() throws NullPointerException, FormatException {
        CSVParserTest.parser = new CSVParser(MemoryAccess.loadInternalFileOrNull(CSVParserTest.testFilePath));
    }
    
    @Test
    public void testValue() {
        Assert.assertEquals("Wert in Reihe " + this.row + ", Spalte " + this.column + " ist falsch", this.expected,
                            CSVParserTest.parser.get(this.row, this.column));
    }
    
    @AfterClass
    public static void tearDown() {
        CSVParserTest.parser = null;
    }
}
