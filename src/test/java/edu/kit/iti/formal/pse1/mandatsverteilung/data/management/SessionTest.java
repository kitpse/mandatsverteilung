package edu.kit.iti.formal.pse1.mandatsverteilung.data.management;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.io.FormatException;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.io.Import;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.io.MemoryAccess;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Election;
import edu.kit.iti.formal.pse1.mandatsverteilung.gui.controller.Mandatsverteilung;
import edu.kit.iti.formal.pse1.mandatsverteilung.gui.controller.TabManager;

public class SessionTest {
    private String name = "test";
    private static Election election1;
    private static Election election2;
    private String path;
    
    @BeforeClass
    public static void setUpClass() throws Exception {
        new File(Session.getDefaultSessionPath()).delete();
        SessionTest.election1 = Import.importInternalElection("bundestagswahl2013.csv");;
        SessionTest.election2 = Import.importInternalElection("bundestagswahl2005.csv");
    }
    
    @Before
    public void setUp() throws Exception {
        this.path = new StringBuilder(MemoryAccess.getOsDefaultLocation()).append("tmpfile").append(new Random().nextLong()).append(".csv").toString(); // some random file name, which is hopefully not taken
        Session.init(new TabMangerDummy());
        new ArrayList<>(Session.getSession().getElections()).forEach(Session.getSession()::removeElection);
        new ArrayList<>(Session.getSession().getElectionComparisons()).forEach(Session.getSession()::removeElectionComparison);
    }
    
    @After
    public void tearDown() throws Exception {
        new File(this.path).delete();
        this.path = null;
    }
    
    @AfterClass
    public static void tearDownClass() throws Exception {
        SessionTest.election1 = null;
        SessionTest.election2 = null;
    }
    
    @Test
    public void testSave() {
        try {
            Session.getSession().saveSession(this.path);
        }
        catch(IOException e) {
            Assert.fail(Mandatsverteilung.getNestedErrorMessages(e));
        }
        Assert.assertTrue(new File(this.path).exists());
    }
    
    @Test
    public void testSaveNLoad() {
        try {
            Session.getSession().saveSession(this.path);
            Assert.assertTrue(new File(this.path).exists());
            Assert.assertTrue(Session.getSession().loadSession(this.path));
        }
        catch(IOException | FormatException e) {
            Assert.fail(Mandatsverteilung.getNestedErrorMessages(e));
        }
    }
    
    @Test
    public void testElectionSave() {
        try {
            Session.getSession().saveElection(SessionTest.election1, this.path);
            Assert.assertTrue(new File(this.path).exists());
        }
        catch(IOException e) {
            Assert.fail(Mandatsverteilung.getNestedErrorMessages(e));
        }
    }
    
    @Test
    public void testElectionSaveNLoad() {
        try {
            Session.getSession().saveElection(SessionTest.election1, this.path);
            Assert.assertTrue(new File(this.path).exists());
            Election importedElection = Session.getSession().loadElection(this.path);
            Assert.assertNotNull(importedElection);
            Assert.assertTrue(SessionTest.election1.equals(importedElection));
            Assert.assertTrue(Session.getSession().getElectionCount() == 1);
            Assert.assertTrue(Session.getSession().getElementCount() == 1);
        }
        catch(IOException | NullPointerException | FormatException e) {
            Assert.fail(Mandatsverteilung.getNestedErrorMessages(e));
        }
    }
    
    @Test
    public void testSaveNLoadSessionWithElectionsAndElectionComparison() {
        
        try {
            ElectionComparison comparison1 = new ElectionComparison(SessionTest.election1, SessionTest.election2, this.name);
            Session.getSession().addElection(SessionTest.election1);
            Session.getSession().addElection(SessionTest.election2);
            Session.getSession().addElectionComparison(comparison1);
            Assert.assertTrue(Session.getSession().getElectionCount() == 2);
            Assert.assertTrue(Session.getSession().getElections().size() == 2);
            Assert.assertTrue(Session.getSession().getElectionComparisons().size() == 1);
            Assert.assertTrue(Session.getSession().getComparisonCount() == 1);
            Assert.assertTrue(Session.getSession().getElementCount() == 3);
            Session.getSession().saveSession(this.path);
            Assert.assertTrue(new File(this.path).exists());
            Assert.assertTrue("Die Session muss erfolgreich geladen werden.", Session.getSession().loadSession(this.path));
            Assert.assertTrue(Session.getSession().getElectionCount() == 2);
            Assert.assertTrue(Session.getSession().getElections().size() == 2);
            Assert.assertTrue(Session.getSession().getElectionComparisons().size() == 1);
            Assert.assertTrue(Session.getSession().getComparisonCount() == 1);
            Assert.assertTrue(Session.getSession().getElementCount() == 3);
        }
        catch(IOException | FormatException e) {
            Assert.fail(Mandatsverteilung.getNestedErrorMessages(e));
        }
    }
    
    @Test
    public void testAddNRemoveElection() {
        Session.getSession().addElection(SessionTest.election1);
        Assert.assertTrue(Session.getSession().getElections().size() == 1);
        Assert.assertTrue(Session.getSession().getElectionComparisons().size() == 0);
        Assert.assertTrue(Session.getSession().getComparisonCount() == 0);
        Assert.assertTrue(Session.getSession().getElectionCount() == 1);
        Assert.assertTrue(Session.getSession().getElementCount() == 1);
        
        Session.getSession().removeElection(SessionTest.election1);
        Assert.assertTrue(Session.getSession().getElections().size() == 0);
        Assert.assertTrue(Session.getSession().getElectionComparisons().size() == 0);
        Assert.assertTrue(Session.getSession().getComparisonCount() == 0);
        Assert.assertTrue(Session.getSession().getElectionCount() == 0);
        Assert.assertTrue(Session.getSession().getElementCount() == 0);
    }
    
    @Test
    public void testAddNRemoveComparison() {
        ElectionComparison comparison1 = new ElectionComparison(SessionTest.election1, SessionTest.election2, this.name);
        Session.getSession().addElectionComparison(comparison1);
        Assert.assertTrue(Session.getSession().getElections().size() == 0);
        Assert.assertTrue(Session.getSession().getComparisonCount() == 1);
        Assert.assertTrue(Session.getSession().getElectionComparisons().size() == 1);
        Assert.assertTrue(Session.getSession().getElectionCount() == 0);
        Assert.assertTrue(Session.getSession().getElementCount() == 1);
        
        Session.getSession().removeElectionComparison(comparison1);
        Assert.assertTrue(Session.getSession().getElections().size() == 0);
        Assert.assertTrue(Session.getSession().getElectionComparisons().size() == 0);
        Assert.assertTrue(Session.getSession().getComparisonCount() == 0);
        Assert.assertTrue(Session.getSession().getElectionCount() == 0);
        Assert.assertTrue(Session.getSession().getElementCount() == 0);
    }
    
    @Test
    public void testAddNRemoveElectionsAndComparisons() {
        ElectionComparison comparison1 = new ElectionComparison(SessionTest.election1, SessionTest.election2, this.name);
        Session.getSession().addElection(SessionTest.election1);
        Session.getSession().addElection(SessionTest.election2);
        Session.getSession().addElectionComparison(comparison1);
        Assert.assertTrue(Session.getSession().getElections().size() == 2);
        Assert.assertTrue(Session.getSession().getElectionComparisons().size() == 1);
        Assert.assertTrue(Session.getSession().getComparisonCount() == 1);
        Assert.assertTrue(Session.getSession().getElectionCount() == 2);
        Assert.assertTrue(Session.getSession().getElementCount() == 3);
        
        Session.getSession().removeElection(SessionTest.election1);
        
        Assert.assertTrue(Session.getSession().getComparisonCount() == 1);
        Assert.assertTrue(Session.getSession().getElectionCount() == 1);
        Assert.assertTrue(Session.getSession().getElections().size() == 1);
        Assert.assertTrue(Session.getSession().getElectionComparisons().size() == 1);
        Assert.assertTrue(Session.getSession().getElementCount() == 2);
        
        Session.getSession().removeElection(SessionTest.election2);
        Session.getSession().removeElectionComparison(comparison1);
        Assert.assertTrue(Session.getSession().getComparisonCount() == 0);
        Assert.assertTrue(Session.getSession().getElectionCount() == 0);
        Assert.assertTrue(Session.getSession().getElections().size() == 0);
        Assert.assertTrue(Session.getSession().getElectionComparisons().size() == 0);
        Assert.assertTrue(Session.getSession().getElementCount() == 0);
    }
    
    @Test
    public void testSetAutoSaveIntervalStandard() {
        int autosave = 300000;
        Session.getSession().setAutoSaveInterval(autosave);
        Assert.assertTrue(Session.getSession().getAutoSaveInterval() == autosave);
    }
    
    @Test(expected = NumberFormatException.class)
    public void testSetAutoSaveIntervalNegative() {
        Session.getSession().setAutoSaveInterval(-30 * 1000);
    }
    
    @Test
    public void testSetAutoSaveIntervalMax() {
        int autosave = Integer.MAX_VALUE;
        Session.getSession().setAutoSaveInterval(autosave);
        Assert.assertTrue(Session.getSession().getAutoSaveInterval() == autosave);
    }
    
    @Test(expected = NumberFormatException.class)
    public void testSetAutoSaveIntervalMin() {
        Session.getSession().setAutoSaveInterval(Integer.MIN_VALUE);
    }
    
    @Test(expected = NumberFormatException.class)
    public void testSetAutoSaveIntervalZero() {
        Session.getSession().setAutoSaveInterval(0);
    }
    
    public static class TabMangerDummy extends TabManager {
        public TabMangerDummy() {
            super(null);
        }
        
        @Override
        public void createTabComparison(ElectionComparison comparison) {
        }
        
        @Override
        public void createTabElection(Election election) {
        }
        
        @Override
        public boolean removeComparison(ElectionComparison comparison) {
            return false;
        }
        
        @Override
        public boolean removeElection(Election election) {
            return false;
        }
    }
}
