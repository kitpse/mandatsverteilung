package edu.kit.iti.formal.pse1.mandatsverteilung.calculation;

import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import edu.kit.iti.formal.pse1.mandatsverteilung.gui.controller.Mandatsverteilung;

public class CalculationMethodTest {
    
    @Rule
    public Timeout globalTimeout = new Timeout(10, TimeUnit.SECONDS); // 10 seconds max per method tested
    
    private final double DELTA = 0.0005; // delta two double are still treated as the same
    
    @Test
    public void testRound_Up() {
        Assert.assertEquals("1.5 muss auf 2 aufgerundet werden.", 2, CalculationMethod.round(1.5), this.DELTA);
    }
    
    @Test
    public void testRound_Down() {
        Assert.assertEquals("1.49 muss auf 1 abgerundet werden.", 1, CalculationMethod.round(1.49), this.DELTA);
    }
    
    @Test
    public void testSLSrealistic() {
        double[] toTest = {10000, 10000};
        double[] result = new double[2];
        double[] expectedResult = {21, 21};
        CalculationMethod.sls(toTest, result, 42);
        Assert.assertArrayEquals("Gerade Anzahl an Sitzen muss bei 2 Parteien in gleichen Teilen auf jede Partei verteilt werden.", expectedResult, result,
                                 this.DELTA);
    }
    
    @Test
    public void testSLSWinnerTakesItAll() {
        double[] toTest = {166000000, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        double[] result = new double[16];
        double[] expectedResult = {3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        CalculationMethod.sls(toTest, result, 3);
        Assert.assertArrayEquals("Wenn nur eine Partei alle Stimmen erhaelt, erhaelt sie auch alle Mandate", expectedResult, result, this.DELTA);
    }
    
    @Test(timeout = 1000)
    public void testSLS() {
        double[] toTest = {10, 10};
        double[] result = new double[2];
        double[] expectedResult = {21, 21};
        CalculationMethod.sls(toTest, result, 42);
        Assert.assertArrayEquals("Gerade Anzahl an Sitzen muss bei 2 Parteien in gleichen Teilen auf jede Partei verteilt werden.", expectedResult, result,
                                 this.DELTA);
    }
    
    @Test(timeout = 1000)
    public void zeroSLS() {
        double[] toTest = {10, 10};
        double[] result = new double[2];
        double[] expectedResult = {0, 0};
        CalculationMethod.sls(toTest, result, 0);
        Assert.assertArrayEquals("Gerade Anzahl an Sitzen muss bei 2 Parteien in gleichen Teilen auf jede Partei verteilt werden.", expectedResult, result,
                                 this.DELTA);
    }
    
    @Test(timeout = 1000)
    public void testSimiliarVotesSLS() {
        double[] toTest = {1000000, 1000001};
        double[] result = new double[2];
        double[] expectedResult = {1, 2};
        CalculationMethod.sls(toTest, result, 3);
        Assert.assertArrayEquals("Partei mit einer Stimme mehr bekommt mehr Mandate.", expectedResult, result, this.DELTA);
    }
    
    @Test(timeout = 10000000)
    public void testmagicVotesSLS() {
        double[] toTest = {93409, 78296, 223935, 33284, 582925, 188654, 120338, 272456, 248920, 56045, 330507, 311312, 186871, 467045, 282319, 288615};
        double[] result = new double[toTest.length];
        CalculationMethod.sls(toTest, result, 61);
        double sum = 0;
        int i = 0;
        for(i = 0; i < toTest.length; i++) {
            sum = sum + result[i];
        }
        Assert.assertEquals("Keine Endlosschleife.", 61, sum, this.DELTA);
    }
    
    @Test(timeout = 1000)
    public void testEqualVotesSLS() {
        double[] toTest = {1000000, 1000000};
        double[] result = new double[2];
        double sum;
        CalculationMethod.sls(toTest, result, 3);
        sum = result[0] + result[1];
        Assert.assertEquals("Partei mit einer Stimme mehr bekommt mehr Mandate.", 3, sum, this.DELTA);
    }
    
    @Test(timeout = 1000)
    public void testMultiStalemateSLS() {
        double[] toTest = {100, 100, 100, 100};
        double[] result = new double[4];
        double sum;
        CalculationMethod.sls(toTest, result, 7);
        if(Mandatsverteilung.debug) {
            System.out.println("party1=" + result[0]);
            System.out.println("party2=" + result[1]);
            System.out.println("party3=" + result[2]);
            System.out.println("party4=" + result[3]);
        }
        sum = result[0] + result[1] + result[2] + result[3];
        
        Assert.assertEquals("Partei mit einer Stimme mehr bekommt mehr Mandate.", 7, sum, this.DELTA);
    }
    
    @Test(timeout = 1000)
    public void testOtherStalemateSLS() {
        double[] toTest = {1000000, 500000};
        double[] result = new double[2];
        double sum;
        CalculationMethod.sls(toTest, result, 4);
        sum = result[0] + result[1];
        Assert.assertEquals("Keine Endlosschleife.", 4, sum, this.DELTA);
    }
    
}
