package edu.kit.iti.formal.pse1.mandatsverteilung.data.model;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ConstituencyTest {
    ElectionResult result;
    Constituency constituency;
    
    @Before
    public void setUp() {
        this.result = new ElectionResult();
        this.constituency = new Constituency("Test Wahlkreis", this.result);
    }
    
    @After
    public void tearDown() {
        this.result = null;
        this.constituency = null;
    }
    
    @Test
    public void testOnePartyDirectMandate() {
        Party party = new Party("Test Partei");
        this.constituency.addParty(party);
        this.constituency.setFirstVotes(party, 10000);
        
        Assert.assertEquals("Bei nur einer Partei gewinnt diese das Direktmandat.", 1, this.constituency.getWonDirectMandates(party));
    }
    
    @Test
    public void testMorePartiesDirectMandate() {
        Party party1 = new Party("Test Partei 1");
        Party party2 = new Party("Test Partei 2");
        Party party3 = new Party("Test Partei 3");
        this.constituency.addParty(party1);
        this.constituency.addParty(party2);
        this.constituency.addParty(party3);
        
        this.constituency.setFirstVotes(party1, 9999);
        this.constituency.setFirstVotes(party2, 10000);
        this.constituency.setFirstVotes(party3, 1000);
        
        Assert.assertEquals("Partei mit den meisten Stimmen gewinnt das Mandat.", 1, this.constituency.getWonDirectMandates(party2));
        Assert.assertEquals("Partei ohne die meisten Stimmen gewinnt das Mandat nicht.", 0, this.constituency.getWonDirectMandates(party1));
        Assert.assertEquals("Partei ohne die meisten Stimmen gewinnt das Mandat nicht.", 0, this.constituency.getWonDirectMandates(party3));
    }
    
    @Test
    public void testMorePartiesDirectMandateSameVotes() {
        Party party1 = new Party("Test Partei 1");
        Party party2 = new Party("Test Partei 2");
        Party party3 = new Party("Test Partei 3");
        this.constituency.addParty(party1);
        this.constituency.addParty(party2);
        this.constituency.addParty(party3);
        
        this.constituency.setFirstVotes(party1, 10000);
        this.constituency.setSecondVotes(party1, 1000);
        this.constituency.setFirstVotes(party2, 10000);
        this.constituency.setSecondVotes(party2, 2000);
        this.constituency.setFirstVotes(party3, 1000);
        
        Assert.assertEquals("Partei mit den meisten Stimmen gewinnt das Mandat.", 1, this.constituency.getWonDirectMandates(party2));
        Assert.assertEquals("Partei ohne die meisten Stimmen gewinnt das Mandat nicht.", 0, this.constituency.getWonDirectMandates(party1));
        Assert.assertEquals("Partei ohne die meisten Stimmen gewinnt das Mandat nicht.", 0, this.constituency.getWonDirectMandates(party3));
    }
    
    @Test
    public void testPartyLocked() {
        Party party = new Party("Test Partei");
        this.constituency.addParty(party);
        this.constituency.lock(party);
        
        Assert.assertTrue("Die Partei wird gelockt.", this.constituency.isLocked(party));
    }
    
    @Test
    public void testPartyUnlock() {
        Party party = new Party("Test Partei");
        this.constituency.addParty(party);
        this.constituency.lock(party);
        this.constituency.unlock(party);
        
        Assert.assertFalse("Die Partei wird gelockt.", this.constituency.isLocked(party));
    }
    
    @Test
    public void testNotAllPartiesLocked() {
        Party party1 = new Party("Test Partei 1");
        Party party2 = new Party("Test Partei 2");
        Party party3 = new Party("Test Partei 3");
        this.constituency.addParty(party1);
        this.constituency.addParty(party2);
        this.constituency.addParty(party3);
        
        this.constituency.lock(party1);
        this.constituency.lock(party2);
        
        Assert.assertFalse("Es sind nicht alle Parteien gelockt.", this.constituency.allPartiesLocked());
    }
    
    @Test
    public void testAllPartiesLocked() {
        Party party1 = new Party("Test Partei 1");
        Party party2 = new Party("Test Partei 2");
        Party party3 = new Party("Test Partei 3");
        this.constituency.addParty(party1);
        this.constituency.addParty(party2);
        this.constituency.addParty(party3);
        
        this.constituency.lock(party1);
        this.constituency.lock(party2);
        this.constituency.lock(party3);
        
        Assert.assertTrue("Es sind alle Parteien gelockt.", this.constituency.allPartiesLocked());
    }
    
    @Test
    public void removeParty() {
        Party party1 = new Party("Test Partei 1");
        
        this.constituency.addParty(party1);
        this.constituency.removeParty(party1);
        
        Assert.assertTrue("Nach dem Hinzufügen und entfernen einer Partei sind keine Parteien mehr vorhanden.", this.constituency.getParties().isEmpty());
    }
    
    @Test
    public void isPartyPresent() {
        Party party1 = new Party("Test Partei 1");
        
        this.constituency.addParty(party1);
        
        Assert.assertTrue("Partei ist vorhanden.", this.constituency.isPartyPresent(party1));
    }
    
    @Test
    public void isPartyNotPresent() {
        Assert.assertFalse("Partei ist nicht vorhanden.", this.constituency.isPartyPresent(new Party("Test")));
    }
    
    @Test
    public void testDeepClone() {
        this.constituency.setCitizens(20000);
        this.constituency.setEligibleVoters(16000);
        this.constituency.setNumberOfVotes(13000);
        
        Party party1 = new Party("Test Partei 1");
        Party party2 = new Party("Test Partei 2");
        Party party3 = new Party("Test Partei 3");
        this.constituency.addParty(party1);
        this.constituency.addParty(party2);
        this.constituency.addParty(party3);
        
        Constituency cloned = this.constituency.deepClone();
        
        Assert.assertEquals("Einwohner der Kopie stimmen.", cloned.getCitizens(), 20000);
        Assert.assertEquals("Wahlberechtigte der Kopie stimmen.", cloned.getEligibleVoters(), 16000);
        Assert.assertEquals("Wähler der Kopie stimmen.", cloned.getNumberOfVotes(), 13000);
        Assert.assertEquals("Es sind gleich viele Parteien vorhanden", cloned.getParties().size(), 3);
    }
    
    @Test
    public void testDeepCloneChange() {
        this.constituency.setCitizens(20000);
        this.constituency.setEligibleVoters(16000);
        this.constituency.setNumberOfVotes(13000);
        
        Party party1 = new Party("Test Partei 1");
        Party party2 = new Party("Test Partei 2");
        Party party3 = new Party("Test Partei 3");
        this.constituency.addParty(party1);
        this.constituency.addParty(party2);
        this.constituency.addParty(party3);
        
        Constituency cloned = this.constituency.deepClone();
        
        this.constituency.setCitizens(21000);
        
        Assert.assertNotEquals("Einwohner der Kopie stimmen nach Änderung nicht mehr.", cloned.getCitizens(), 21000);
        Assert.assertEquals("Wahlberechtigte der Kopie stimmen.", cloned.getEligibleVoters(), 16000);
        Assert.assertEquals("Wähler der Kopie stimmen.", cloned.getNumberOfVotes(), 13000);
        Assert.assertEquals("Es sind gleich viele Parteien vorhanden", cloned.getParties().size(), 3);
    }
    
    @Test
    public void testDeepCloneRemoveParty() {
        this.constituency.setCitizens(20000);
        this.constituency.setEligibleVoters(16000);
        this.constituency.setNumberOfVotes(13000);
        
        Party party1 = new Party("Test Partei 1");
        Party party2 = new Party("Test Partei 2");
        Party party3 = new Party("Test Partei 3");
        this.constituency.addParty(party1);
        this.constituency.addParty(party2);
        this.constituency.addParty(party3);
        
        Constituency cloned = this.constituency.deepClone();
        
        this.constituency.removeParty(party1);
        
        Assert.assertEquals("Einwohner der Kopie stimmen.", cloned.getCitizens(), 20000);
        Assert.assertEquals("Wahlberechtigte der Kopie stimmen.", cloned.getEligibleVoters(), 16000);
        Assert.assertEquals("Wähler der Kopie stimmen.", cloned.getNumberOfVotes(), 13000);
        Assert.assertNotEquals("Es sind nicht gleich viele Parteien vorhanden", cloned.getParties().size(), 2);
    }
    
    @Test
    public void testEqualsClone() {
        Assert.assertEquals("Eine geklonter Wahlkreis ist zu sich selbst äquivalent.", this.constituency.deepClone(), this.constituency);
    }
    
    @Test
    public void testEquals() {
        Party party1 = new Party("Test Partei 1");
        this.constituency.addParty(party1);
        Assert.assertEquals("Eine geklonter Wahlkreis mit einer Partei ist zu sich selbst äquivalent.", this.constituency.deepClone(), this.constituency);
    }
}
