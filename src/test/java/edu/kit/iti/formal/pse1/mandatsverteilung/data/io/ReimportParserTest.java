package edu.kit.iti.formal.pse1.mandatsverteilung.data.io;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Election;
import edu.kit.iti.formal.pse1.mandatsverteilung.gui.controller.Mandatsverteilung;

/**
 * Exports and reimports several {@link Election} objects. They then must be equal.
 * 
 * @author Felix Heim
 * @version 1
 */
@RunWith(Parameterized.class)
public class ReimportParserTest {
    private Election election;
    
    public ReimportParserTest(String internalElectionPath) {
        this.election = Import.importInternalElection(internalElectionPath);
    }
    
    @Parameters(name = "{0}")
    public static Collection<String> getParameters() {
        return Arrays.asList("bundestagswahl2013.csv", "bundestagswahl2013-NegSgw-Bayern-SPD.csv");
    }
    
    @Test
    public void exportAndReimport() {
        try {
            Assert.assertEquals("Das Reimportierte Ergebnis enspricht nicht dem Orginalen!", this.election,
                                Import.parseElection(Export.electionToString(this.election).toString()));
        }
        catch(NullPointerException | FormatException e) {
            Assert.fail(Mandatsverteilung.getNestedErrorMessages(e));
        }
    }
}
