package edu.kit.iti.formal.pse1.mandatsverteilung.data.io;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Election;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Party;

/**
 * Test the import with some randomly picked values.
 * 
 * @author Felix Heim
 * @version 1
 */
public class ImportTestRandom {
    
    private Election election;
    
    @Rule
    public Timeout globalTimeout = new Timeout(10, TimeUnit.SECONDS);
    
    @Before
    public void setUp() throws Exception {
        this.election = Import.importInternalElection("bundestagswahl2013.csv");
    }
    
    @After
    public void tearDown() throws Exception {
        this.election = null;
    }
    
    @Test
    public void testFederation_EligibleVoters() {
        int expectedAmount = 61946900;
        int storedAmount = this.election.getElectionResult().getFederation().getEligibleVoters();
        Assert.assertEquals("Die Anzahl an Wahlberechtigten im Bund muss gleich den tatsächlich Wahlberechtigten sein", expectedAmount, storedAmount);
    }
    
    @Test
    public void testFederation_NumberOfVotes() {
        int expectedAmount = 44309925;
        int storedAmount = this.election.getElectionResult().getFederation().getNumberOfVotes();
        Assert.assertEquals("Die Anzahl an abgegebenen Stimmen im Bund muss gleich den tatsächlich abgegebenen Stimmen sein", expectedAmount, storedAmount);
    }
    
    @Test
    public void testFederation_ValidFirstVotes() {
        int expectedAmount = 43625042;
        int storedAmount = this.election.getElectionResult().getFederation().getValidFirstVotes();
        Assert.assertEquals("Die Anzahl an gültigen Erststimmen im Bund muss gleich den tatsächlich abgegebenen Stimmen sein", expectedAmount, storedAmount);
    }
    
    @Test
    public void testFederation_InvalidFirstVotes() {
        int expectedAmount = 684883;
        int storedAmount = this.election.getElectionResult().getFederation().getInvalidFirstVotes();
        Assert.assertEquals("Die Anzahl an ungültigen Erststimmen im Bund muss gleich den tatsächlich abgegebenen Stimmen sein", expectedAmount, storedAmount);
    }
    
    @Test
    public void testFederation_ValidSecondVotes() {
        int expectedAmount = 43726856;
        int storedAmount = this.election.getElectionResult().getFederation().getValidSecondVotes();
        Assert.assertEquals("Die Anzahl an gültigen Zweitstimmen im Bund muss gleich den tatsächlich abgegebenen Stimmen sein", expectedAmount, storedAmount);
    }
    
    @Test
    public void testFederation_InvalidSecondVotes() {
        int expectedAmount = 583069;
        int storedAmount = this.election.getElectionResult().getFederation().getInvalidSecondVotes();
        Assert.assertEquals("Die Anzahl an ungültigen Zweitstimmen im Bund muss gleich den tatsächlich abgegebenen Stimmen sein", expectedAmount, storedAmount);
    }
    
    @Test
    public void testFederation_FirstVotesParty() {
        int expectedAmount = 16233642;
        Party cdu = null;
        for(Party party : this.election.getElectionResult().getFederation().getParties()) {
            if(party.getName().equals("CDU")) {
                cdu = party;
                break;
            }
        }
        int storedAmount = this.election.getElectionResult().getFederation().getFirstVotes(cdu);
        Assert.assertEquals("Die Anzahl an Erststimmen der CDU im Bund muss gleich den tatsächlich abgegebenen Stimmen sein", expectedAmount, storedAmount);
    }
    
    @Test
    public void testFederation_SecondVotesParty() {
        int expectedAmount = 14921877;
        Party cdu = null;
        for(Party party : this.election.getElectionResult().getFederation().getParties()) {
            if(party.getName().equals("CDU")) {
                cdu = party;
                break;
            }
        }
        int storedAmount = this.election.getElectionResult().getFederation().getSecondVotes(cdu);
        Assert.assertEquals("Die Anzahl an Zweitstimmen der CDU im Bund muss gleich den tatsächlich abgegebenen Stimmen sein", expectedAmount, storedAmount);
    }
    
    @Test
    public void testNumberFederalStates() {
        Assert.assertEquals("Die erzeugte Struktur muss 16 Bundesländer haben.", this.election.getElectionResult().getFederation().getChildren().size(), 16);
    }
    
    @Test
    public void testNumberConstituencies() {
        int sum = this.election.getElectionResult().getFederation().getChildren().stream().mapToInt(federalState -> federalState.getChildren().size()).sum();
        Assert.assertEquals("Die erzeugte Struktur muss 299 Wahlkreise haben.", sum, 299);
    }
    
}
