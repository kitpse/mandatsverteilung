package edu.kit.iti.formal.pse1.mandatsverteilung.calculation;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.io.Import;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Bundestag;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Party;

public class Distribution2013Test {
    
    private Bundestag bundestag;
    
    @Before
    public void setUp() throws Exception {
        this.bundestag = Import.importInternalElection("bundestagswahl2013.csv").getBundestag();
    }
    
    @After
    public void tearDown() {
        this.bundestag = null;
    }
    
    @Test
    public void testSeatsPartyInParliament() {
        Party spd = null;
        for(Party party : this.bundestag.getParties()) {
            if(party.getName().equals("SPD")) {
                spd = party;
                break;
            }
        }
        Assert.assertEquals("Die Anzahl der Sitze im Bundestag für die SPD muss den tatsächlichen Sitzen entsprechen", this.bundestag.getSeats(spd), 193, 0.01);
    }
    
    @Test
    public void testSeatsPartyOutOfParliament() {
        Party fdp = null;
        for(Party party : this.bundestag.getParties()) {
            if(party.getName().equals("FDP")) {
                fdp = party;
                break;
            }
        }
        Assert.assertEquals("Die Anzahl der Sitze im Bundestag für die FDP muss den tatsächlichen Sitzen entsprechen", this.bundestag.getSeats(fdp), 0, 0.01);
    }
}
