package edu.kit.iti.formal.pse1.mandatsverteilung.data.io;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import edu.kit.iti.formal.pse1.mandatsverteilung.calculation.AlternativeDistribution;
import edu.kit.iti.formal.pse1.mandatsverteilung.calculation.Distribution2013;
import edu.kit.iti.formal.pse1.mandatsverteilung.calculation.SimpleDistribution;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Settings;
import edu.kit.iti.formal.pse1.mandatsverteilung.gui.controller.Mandatsverteilung;

/**
 * Exports and reimports several {@link Settings} objects. They then must be equal.
 * 
 * @author Felix Heim
 * @version 1
 */
@RunWith(Parameterized.class)
public class SettingsIOTest {
    private Settings settings;
    
    public SettingsIOTest(Settings settings) {
        this.settings = settings;
    }
    
    @Parameters(name = "{0}")
    public static Collection<Settings> getParameters() {
        return Arrays.asList(new Settings(true, 0.03, 600, Distribution2013.getMethod()), new Settings(false, 0.05, 299, SimpleDistribution.getMethod()),
                             new Settings(false, 0.1, 999, AlternativeDistribution.getMethod()),
                             new Settings(true, 0.001, 598, SimpleDistribution.getMethod()), new Settings(),
                             new Settings(true, 0, 50, AlternativeDistribution.getMethod()), new Settings(false, 0.25, 598, Distribution2013.getMethod()));
    }
    
    @Test
    public void exportAndReimport() {
        try {
            Assert.assertEquals("Die Einstellungen wurden nicht richtig geladen!", this.settings, new SettingsParser(Export.settingsToString(this.settings)
                    .toString()).parseSettings());
        }
        catch(NullPointerException | FormatException e) {
            Assert.fail(Mandatsverteilung.getNestedErrorMessages(e));
        }
    }
}
