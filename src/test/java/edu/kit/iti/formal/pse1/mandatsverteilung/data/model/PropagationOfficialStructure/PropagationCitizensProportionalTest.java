package edu.kit.iti.formal.pse1.mandatsverteilung.data.model.PropagationOfficialStructure;

import java.util.Collection;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.io.Import;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Election;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.ElectionResult;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Federation;
import edu.kit.iti.formal.pse1.mandatsverteilung.propagation.ProportionalDistribution;

@RunWith(Parameterized.class)
public class PropagationCitizensProportionalTest {
    
    private int inputNumber;
    private Boolean expectedResult;
    // the following values indicate which values have to stay the same after the propagation
    private boolean citizens;
    private boolean eligibleVoters;
    private boolean numberOfVotes;
    private boolean validFirst;
    private boolean validSecond;
    private Federation federation;
    private Federation clonedStructure;
    
    private PropagationTestHelper helper;
    
    @Rule
    public Timeout globalTimeout = new Timeout(10, TimeUnit.SECONDS);
    
    @Before
    public void setUp() throws Exception {
        Election election = Import.importInternalElection("bundestagswahl2013.csv");
        ElectionResult result = election.getElectionResult();
        result.removeChangeListener(election);
        result.changePropagation(new ProportionalDistribution());
        this.federation = result.getFederation();
        this.clonedStructure = this.federation.deepClone();
        this.helper = new PropagationTestHelper();
    }
    
    @After
    public void tearDown() throws Exception {
        this.federation = null;
        this.clonedStructure = null;
        this.helper = null;
    }
    
    public PropagationCitizensProportionalTest(int inputNumber, boolean expectedResult, boolean citizens, boolean eligibleVoters, boolean numberOfVotes,
                                               boolean validFirst, boolean validSecond) {
        this.inputNumber = inputNumber;
        this.expectedResult = expectedResult;
        this.citizens = citizens;
        this.eligibleVoters = eligibleVoters;
        this.numberOfVotes = numberOfVotes;
        this.validFirst = validFirst;
        this.validSecond = validSecond;
    }
    
    @Parameterized.Parameters
    public static Collection<Object[]> newCitizens() {
        // citizens: 61946900
        // eligible voters: 61946900
        // number of votes: 44309925
        Collection<Object[]> list = new LinkedList<Object[]>();
        list.add(new Object[]{Integer.MAX_VALUE, true, false, true, true, true, true});
        list.add(new Object[]{70000000, true, false, true, true, true, true});
        list.add(new Object[]{61946901, true, false, true, true, true, true});
        list.add(new Object[]{61946900, true, true, true, true, true, true});
        // the following tests decrease the amount of citizens; this can also cause the number of votes to diminish, 
        // since the participation in the election can differ from constituency to constituency
        list.add(new Object[]{61946899, true, false, false, false, false, false});
        list.add(new Object[]{47000000, true, false, false, false, false, false});
        list.add(new Object[]{44309926, true, false, false, false, false, false});
        list.add(new Object[]{44309925, true, false, false, false, false, false});
        list.add(new Object[]{44309924, true, false, false, false, false, false});
        list.add(new Object[]{10000000, true, false, false, false, false, false});
        list.add(new Object[]{1000000, true, false, false, false, false, false});
        list.add(new Object[]{100000, true, false, false, false, false, false});
        list.add(new Object[]{10000, true, false, false, false, false, false});
        list.add(new Object[]{1000, true, false, false, false, false, false});
        list.add(new Object[]{100, true, false, false, false, false, false});
        list.add(new Object[]{10, true, false, false, false, false, false});
        list.add(new Object[]{1, true, false, false, false, false, false});
        list.add(new Object[]{0, true, false, false, false, false, false});
        list.add(new Object[]{-1, false, true, true, true, true, true});
        list.add(new Object[]{-34982374, false, true, true, true, true, true});
        list.add(new Object[]{Integer.MIN_VALUE, false, true, true, true, true, true});
        return list;
    }
    
    @Test
    public void testUniform_PropagateCitizensFederation() {
        Assert.assertEquals("Beim ändern der Einwohner auf " + this.inputNumber + " muss die Propagierung das Ergebnis " + this.expectedResult.toString()
                            + " liefern.", this.federation.changeCitizens(this.inputNumber), this.expectedResult);
        if(this.expectedResult) {
            Assert.assertEquals("Der neue Wert muss stimmen.", this.federation.getCitizens(), this.inputNumber);
        }
        this.helper.ElectionAreaSameValuesFederation(this.federation, this.clonedStructure, this.citizens, this.eligibleVoters, this.numberOfVotes,
                                                     this.validFirst, this.validSecond);
        this.helper.checkConsistency(this.federation);
    }
}
