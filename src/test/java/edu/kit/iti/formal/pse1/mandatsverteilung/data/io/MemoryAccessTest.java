package edu.kit.iti.formal.pse1.mandatsverteilung.data.io;

import java.io.IOException;
import java.util.List;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import edu.kit.iti.formal.pse1.mandatsverteilung.gui.controller.Mandatsverteilung;

public class MemoryAccessTest {
    private static final String testFileLoaded = "0;1;2;3;4\n5;6;7;8;9;;;\n\n10;11;12; ; ;13;\n";
    private static final String testFilePath = "../test-classes/csvparsertestfile.txt";
    private static String[] testFileArray;
    
    @BeforeClass
    public static void setUpClass() {
        String part[] = MemoryAccessTest.testFileLoaded.split("\n");
        MemoryAccessTest.testFileArray = new String[part.length + 1];
        System.arraycopy(part, 0, MemoryAccessTest.testFileArray, 0, part.length);
        MemoryAccessTest.testFileArray[part.length] = "";
    }
    
    @AfterClass
    public static void cleanUpClass() {
        MemoryAccessTest.testFileArray = null;
    }
    
    @Test(expected = NullPointerException.class)
    public void testImportInternalFileFail1() throws IOException {
        MemoryAccess.loadInternalFile(null);
    }
    
    @Test(expected = IOException.class)
    public void testImportInternalFileFail2() throws IOException {
        MemoryAccess.loadInternalFile("nonexisting");
    }
    
    @Test
    public void testImportInternalFileOrNull1() {
        Assert.assertNull(MemoryAccess.loadInternalFileOrNull("nonexisting"));
    }
    
    @Test
    public void testImportInternalFileOrNull2() {
        Assert.assertEquals("Inhalt von " + MemoryAccessTest.testFilePath + " wurde nicht richtig geladen!", MemoryAccessTest.testFileLoaded,
                            MemoryAccess.loadInternalFileOrNull(MemoryAccessTest.testFilePath));
    }
    
    @Test
    public void testImportInternalFileSuccess() {
        try {
            String file = MemoryAccess.loadInternalFile(MemoryAccessTest.testFilePath);
            Assert.assertEquals("Inhalt von " + MemoryAccessTest.testFilePath + " wurde nicht richtig geladen!", MemoryAccessTest.testFileLoaded, file);
        }
        catch(IOException e) {
            Assert.fail(Mandatsverteilung.getNestedErrorMessages(e));
        }
    }
    
    @Test(expected = NullPointerException.class)
    public void testImportInternalFileToListFail1() throws IOException {
        MemoryAccess.loadInternalFileToList(null);
    }
    
    @Test(expected = IOException.class)
    public void testImportInternalFileToListFail2() throws IOException {
        MemoryAccess.loadInternalFileToList("nonexisting");
    }
    
    @Test
    public void testImportInternalFileToListOrNull1() {
        Assert.assertNull(MemoryAccess.loadInternalFileToListOrNull("nonexisting"));
    }
    
    @Test
    public void testImportInternalFileToListOrNull2() {
        Assert.assertArrayEquals("Inhalt von " + MemoryAccessTest.testFilePath + " wurde nicht richtig geladen!", MemoryAccessTest.testFileArray, MemoryAccess
                .loadInternalFileToListOrNull(MemoryAccessTest.testFilePath).toArray());
    }
    
    @Test
    public void testImportInternalFileToListSuccess() {
        try {
            List<String> lines = MemoryAccess.loadInternalFileToList(MemoryAccessTest.testFilePath);
            Assert.assertArrayEquals("Inhalt von " + MemoryAccessTest.testFilePath + " wurde nicht richtig geladen!", MemoryAccessTest.testFileArray,
                                     lines.toArray());
        }
        catch(IOException e) {
            Assert.fail(Mandatsverteilung.getNestedErrorMessages(e));
        }
    }
}
