package edu.kit.iti.formal.pse1.mandatsverteilung.data.model.PropagationOfficialStructure;

import org.junit.Assert;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.ElectionArea;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Federation;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Party;

class PropagationTestHelper {
    String name;
    String value;
    int expected;
    int found;
    
    PropagationTestHelper() {
        this.name = "";
        this.value = "";
        this.expected = 0;
        this.found = 0;
    }
    
    /**
     * Checks if the specified values are the same for the entered area and it's equivalent and for all the area's children.
     */
    boolean ElectionAreaSameValuesFederation(ElectionArea<?> area, Federation clonedStructure, boolean citizens, boolean eligibleVoters, boolean numberOfVotes,
                                             boolean validFirst, boolean validSecond) {
        boolean s = true;
        boolean success = this.AreaSameValues(area, PropagationTestHelper.clonedEquivalent(area, clonedStructure), citizens, eligibleVoters, numberOfVotes,
                                              validFirst, validSecond);
        Assert.assertTrue(this.errorMessage(), success);
        s &= success;
        for(ElectionArea<?> child : clonedStructure.getChildren()) {
            boolean suc = this.ElectionAreaSameValuesFederalState(child, clonedStructure, citizens, eligibleVoters, numberOfVotes, validFirst, validSecond);
            Assert.assertTrue(this.errorMessage(), suc);
            s &= suc;
        }
        return s;
    }
    
    /**
     * Checks if the specified values are the same for the entered area and it's equivalent and for all the area's children.
     */
    boolean ElectionAreaSameValuesFederalState(ElectionArea<?> area, Federation clonedStructure, boolean citizens, boolean eligibleVoters,
                                               boolean numberOfVotes, boolean validFirst, boolean validSecond) {
        boolean s = true;
        boolean success = this.AreaSameValues(area, PropagationTestHelper.clonedEquivalent(area, clonedStructure), citizens, eligibleVoters, numberOfVotes,
                                              validFirst, validSecond);
        Assert.assertTrue(this.errorMessage(), success);
        s &= success;
        for(ElectionArea<?> child : clonedStructure.getChildren()) {
            boolean suc = this.ElectionAreaSameValuesConstituency(child, clonedStructure, citizens, eligibleVoters, numberOfVotes, validFirst, validSecond);
            Assert.assertTrue(this.errorMessage(), suc);
            s &= suc;
        }
        return s;
    }
    
    /**
     * Checks if the specified values are the same for the entered area and it's equivalent.
     */
    boolean ElectionAreaSameValuesConstituency(ElectionArea<?> area, Federation clonedStructure, boolean citizens, boolean eligibleVoters,
                                               boolean numberOfVotes, boolean validFirst, boolean validSecond) {
        boolean success = this.AreaSameValues(area, PropagationTestHelper.clonedEquivalent(area, clonedStructure), citizens, eligibleVoters, numberOfVotes,
                                              validFirst, validSecond);
        Assert.assertTrue(this.errorMessage(), success);
        return success;
    }
    
    /**
     * Checks if the specified values are the same.
     */
    private boolean AreaSameValues(ElectionArea<?> area1, ElectionArea<?> area2, boolean citizens, boolean eligibleVoters, boolean numberOfVotes,
                                   boolean validFirst, boolean validSecond) {
        if(citizens) {
            if(area1.getCitizens() != area2.getCitizens()) {
                this.name = area1.getName();
                this.value = "Bürger (citizens)";
                this.expected = area2.getCitizens();
                this.found = area1.getCitizens();
                return false;
            }
        }
        if(eligibleVoters) {
            if(area1.getEligibleVoters() != area2.getEligibleVoters()) {
                this.name = area1.getName();
                this.value = "Wahlberechtigten (eligible voters)";
                this.expected = area2.getEligibleVoters();
                this.found = area1.getEligibleVoters();
                return false;
            }
        }
        if(numberOfVotes) {
            if(area1.getNumberOfVotes() != area2.getNumberOfVotes()) {
                this.name = area1.getName();
                this.value = "abgegebene Stimmen (number of votes)";
                this.expected = area2.getNumberOfVotes();
                this.found = area1.getNumberOfVotes();
                return false;
            }
        }
        if(validFirst) {
            if(area1.getValidFirstVotes() != area2.getValidFirstVotes()) {
                this.name = area1.getName();
                this.value = "gültige Erststimmen (valid first votes)";
                this.expected = area2.getValidFirstVotes();
                this.found = area1.getValidFirstVotes();
                return false;
            }
        }
        if(validSecond) {
            if(area1.getValidSecondVotes() != area2.getValidSecondVotes()) {
                this.name = area1.getName();
                this.value = "gültige Zweitstimmen (valid second votes)";
                this.expected = area2.getValidSecondVotes();
                this.found = area1.getValidSecondVotes();
                return false;
            }
        }
        return true;
    }
    
    private static ElectionArea<?> clonedEquivalent(ElectionArea<?> area, Federation clonedStructure) {
        String name = area.getName();
        if(clonedStructure.getName().equals(name)) {
            return clonedStructure;
        }
        for(ElectionArea<?> child : clonedStructure.getChildren()) {
            if(child.getName().equals(name)) {
                return child;
            }
            else {
                for(ElectionArea<?> child_child : child.getChildren()) {
                    if(child_child.getName().equals(name)) {
                        return child_child;
                    }
                }
            }
        }
        assert false : "No cloned area with same name";
        return null;
    }
    
    private String errorMessage() {
        return "Im Wahlbereich <" + this.name + "> stimmt die Anzahl der <" + this.value + "> nicht. Gefunden: <" + this.found + ">; Erwartet: <"
               + this.expected + ">.";
    }
    
    void checkConsistency(Federation fed) {
        for(ElectionArea<?> child : fed.getChildren()) {
            for(ElectionArea<?> c : child.getChildren()) {
                int firstVotes = 0;
                int secondVotes = 0;
                Assert.assertTrue("Die Einwohner im Wahlkreis " + c.getName() + " müssen positiv sein.", c.getCitizens() >= 0);
                Assert.assertTrue("Die Wahlberechtigten im Wahlkreis " + c.getName() + " müssen positiv sein.", c.getEligibleVoters() >= 0);
                Assert.assertTrue("Die Wähler im Wahlkreis " + c.getName() + " müssen positiv sein.", c.getNumberOfVotes() >= 0);
                Assert.assertTrue("Die Einwohner im Wahlkreis " + c.getName() + " müssen größer gleich den Wahlberechtigten sein.",
                                  c.getCitizens() >= c.getEligibleVoters());
                Assert.assertTrue("Die Wahlberechtigten im Wahlkreis " + c.getName() + " müssen größer gleich den Wählern sein.",
                                  c.getEligibleVoters() >= c.getNumberOfVotes());
                for(Party party : c.getParties()) {
                    Assert.assertTrue("Die Erstimmen der Partei " + party.getName() + "  im Wahlkreis " + c.getName() + "müssen positiv sein.",
                                      c.getFirstVotes(party) >= 0);
                    Assert.assertTrue("Die Zweitstimmen der Partei " + party.getName() + "  im Wahlkreis " + c.getName() + "müssen positiv sein.",
                                      c.getSecondVotes(party) >= 0);
                    firstVotes += c.getFirstVotes(party);
                    secondVotes += c.getSecondVotes(party);
                }
                Assert.assertTrue("Die Wähler im Wahlkreis " + c.getName() + " müssen größer gleich den gültigen Erstimmen sein.",
                                  firstVotes <= c.getNumberOfVotes());
                Assert.assertTrue("Die Wähler im Wahlkreis " + c.getName() + " müssen größer gleich den gültigen Zweitstimmen sein.",
                                  secondVotes <= c.getNumberOfVotes());
            }
        }
    }
}
