package edu.kit.iti.formal.pse1.mandatsverteilung.data.management;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.io.Import;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Election;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Party;

public class ElectionComparisonTest {
    
    private String name;
    private static Election election1;
    private static Election election2;
    private ElectionComparison testObject;
    
    @BeforeClass
    public static void setUpClass() throws Exception {
        ElectionComparisonTest.election1 = Import.importInternalElection("bundestagswahl2013.csv");;
        ElectionComparisonTest.election2 = Import.importInternalElection("bundestagswahl2005.csv");
    }
    
    @Before
    public void setUp() throws Exception {
        this.name = "test";
        this.testObject = new ElectionComparison(ElectionComparisonTest.election1, ElectionComparisonTest.election2, this.name);
    }
    
    @After
    public void tearDown() throws Exception {
        this.name = null;
        this.testObject = null;
    }
    
    @AfterClass
    public static void tearDownClass() throws Exception {
        ElectionComparisonTest.election1 = null;
        ElectionComparisonTest.election2 = null;
    }
    
    @Test
    public void testFirstElection() {
        Assert.assertTrue(this.testObject.getFirstElection() == ElectionComparisonTest.election1);
    }
    
    @Test
    public void testSecondElection() {
        Assert.assertTrue(this.testObject.getSecondElection() == ElectionComparisonTest.election2);
    }
    
    @Test
    public void testParties() {
        Set<Party> parties1 = ElectionComparisonTest.election1.getElectionResult().getFederation().getParties();
        Set<Party> parties2 = ElectionComparisonTest.election2.getElectionResult().getFederation().getParties();
        Set<Party> parties3 = this.testObject.getParties();
        
        for(Party party : parties1) {
            Assert.assertTrue(parties3.contains(party));
        }
        
        for(Party party : parties2) {
            Assert.assertTrue(parties3.contains(party));
        }
    }
    
    @Test
    public void testNrOfParties() {
        Set<Party> parties1 = ElectionComparisonTest.election1.getElectionResult().getFederation().getParties();
        Set<Party> parties2 = ElectionComparisonTest.election2.getElectionResult().getFederation().getParties();
        Set<Party> parties3 = this.testObject.getParties();
        Set<Party> parties4 = new HashSet<>();
        Assert.assertTrue(parties1.size() + parties2.size() >= parties3.size());
        
        parties4.addAll(parties1);
        parties4.addAll(parties2);
        Assert.assertTrue(parties4.size() == parties3.size());
    }
    
    @Test
    public void testBundestag1Parties() {
        Party[] cParties = this.testObject.getBundestag1Partys();
        Party[] rParties = ElectionComparisonTest.election1.getBundestag().getParties();
        Arrays.equals(cParties, rParties);
    }
    
    @Test
    public void testBundestag2Parties() {
        Party[] cParties = this.testObject.getBundestag2Partys();
        Party[] rParties = ElectionComparisonTest.election2.getBundestag().getParties();
        Arrays.equals(cParties, rParties);
    }
    
    @Test
    public void testGetElectionResult1Partys() {
        Party[] cParties = this.testObject.getElectionResult1Partys();
        Party[] rParties = ElectionComparisonTest.election1.getElectionResult().getFederation().getParties().toArray(new Party[0]);
        Arrays.equals(cParties, rParties);
    }
    
    @Test
    public void testGetElectionResult2Partys() {
        Party[] cParties = this.testObject.getElectionResult2Partys();
        Party[] rParties = ElectionComparisonTest.election2.getElectionResult().getFederation().getParties().toArray(new Party[0]);
        Arrays.equals(cParties, rParties);
    }
    
    @Test
    public void testMapPartysForComparsion() {
        Party[] parties1 = this.testObject.getElectionResult1Partys();
        Party[] parties2 = this.testObject.getElectionResult2Partys();
        assert parties1.length > 0 && parties2.length > 0;
        this.testObject.mapPartysForComparsion(parties1[0], parties2[0]);
    }
    
    @Test
    public void testDifferenceSecondVotes() {
        Party[] parties1 = this.testObject.getElectionResult1Partys();
        Party[] parties2 = this.testObject.getElectionResult2Partys();
        assert parties1.length > 0 && parties2.length > 0;
        Party party1 = parties1[0];
        Party party2 = parties2[0];
        this.testObject.mapPartysForComparsion(party1, party2);
        Double diff = this.testObject.differenceSecondVotes(party1);
        int seats1 = ElectionComparisonTest.election1.getElectionResult().getFederation().getSecondVotes(party1);
        int seats2 = ElectionComparisonTest.election2.getElectionResult().getFederation().getSecondVotes(party2);
        Assert.assertEquals("Differenz der Zweitstimmen muss stimmen.", Double.valueOf(seats2 - seats1), diff);
    }
    
    @Test
    public void testDifferenceSecondVotesPercent() {
        Party[] parties1 = this.testObject.getElectionResult1Partys();
        Party[] parties2 = this.testObject.getElectionResult2Partys();
        assert parties1.length > 0 && parties2.length > 0;
        Party party1 = parties1[0];
        Party party2 = parties2[0];
        this.testObject.mapPartysForComparsion(party1, party2);
        Double diff = this.testObject.differenceSecondVotesPercent(party1);
        double seats1 = (double) ElectionComparisonTest.election1.getElectionResult().getFederation().getSecondVotes(party1)
                        / (double) ElectionComparisonTest.election1.getElectionResult().getFederation().getValidSecondVotes();
        double seats2 = (double) ElectionComparisonTest.election2.getElectionResult().getFederation().getSecondVotes(party2)
                        / (double) ElectionComparisonTest.election2.getElectionResult().getFederation().getValidSecondVotes();
        Assert.assertEquals("Differenz der Zweitstimmen in % muss stimmen.", Double.valueOf(seats2 - seats1), diff);
    }
    
    @Test
    public void testDifferenceBundestagSeats() {
        Party[] parties1 = ElectionComparisonTest.election1.getBundestag().getParties();
        Party[] parties2 = ElectionComparisonTest.election2.getBundestag().getParties();
        assert parties1.length > 0 && parties2.length > 0;
        Party party1 = parties1[0];
        Party party2 = parties2[0];
        this.testObject.mapPartysForComparsion(party1, party2);
        Double diff = this.testObject.differenceBundestagSeats(party1);
        int seats1 = ElectionComparisonTest.election1.getBundestag().getSeatsRounded(party1);
        int seats2 = ElectionComparisonTest.election2.getBundestag().getSeatsRounded(party2);
        Assert.assertEquals("Differenz der Bundestagssitze muss stimmen.", Double.valueOf(seats2 - seats1), diff);
    }
    
    @Test
    public void testDifferenceBundestagSeatsSymmetry() {
        Party[] parties1 = ElectionComparisonTest.election1.getBundestag().getParties();
        Party[] parties2 = ElectionComparisonTest.election2.getBundestag().getParties();
        assert parties1.length > 0 && parties2.length > 0;
        Party party1 = parties1[0];
        Party party2 = parties2[0];
        this.testObject.mapPartysForComparsion(party1, party2);
        Double diff = this.testObject.differenceBundestagSeats(party1);
        Double diff2 = this.testObject.differenceBundestagSeats(party2);
        Assert.assertEquals("Bezüglich Bundestagssitzen: Partei1 - Partei2 == - (Partei2 - Partei1)", diff, diff2);
    }
    
    @Test
    public void testDifferenceBundestagSeatsPercent() {
        Party[] parties1 = ElectionComparisonTest.election1.getBundestag().getParties();
        Party[] parties2 = ElectionComparisonTest.election2.getBundestag().getParties();
        assert parties1.length > 0 && parties2.length > 0;
        Party party1 = parties1[0];
        Party party2 = parties2[0];
        this.testObject.mapPartysForComparsion(party1, party2);
        Double diff = this.testObject.differenceBundestagSeatsPercent(party1);
        double seats1 = (double) ElectionComparisonTest.election1.getBundestag().getSeatsRounded(party1)
                        / (double) ElectionComparisonTest.election1.getBundestag().getSize();
        double seats2 = (double) ElectionComparisonTest.election2.getBundestag().getSeatsRounded(party2)
                        / (double) ElectionComparisonTest.election2.getBundestag().getSize();
        Assert.assertEquals("Differenz der Bundestagssitze in % muss stimmen.", Double.valueOf(seats2 - seats1), diff);
    }
    
    @Test
    public void testDifferenceBundestagSeatsPercentSymmetry() {
        Party[] parties1 = ElectionComparisonTest.election1.getBundestag().getParties();
        Party[] parties2 = ElectionComparisonTest.election2.getBundestag().getParties();
        assert parties1.length > 0 && parties2.length > 0;
        Party party1 = parties1[0];
        Party party2 = parties2[0];
        this.testObject.mapPartysForComparsion(party1, party2);
        Double diff = this.testObject.differenceBundestagSeatsPercent(party1);
        Double diff2 = this.testObject.differenceBundestagSeatsPercent(party2);
        Assert.assertEquals("Bezüglich Bundestagssitzen in %: Partei1 - Partei2 == - (Partei2 - Partei1)", diff, diff2);
    }
}
