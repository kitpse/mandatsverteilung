package edu.kit.iti.formal.pse1.mandatsverteilung.propagation;

import java.util.Collection;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class ProportionalDistributionTest {
    
    /*
     * True activates printing of the results of the propagation.
     */
    private static boolean debugPrinting = false;
    
    /*
     * Error Messages
     */
    static final String failNoTolerationMessage = "unexpected Propagation Result (no tolerace test) ";
    static final String failWithTolerationMessage = "one array entry differes more than 1 from expected ";
    static final String otherAmountMessage = "the propagated amount was not the expected one ";
    
    /*
     * indicates the number of the parameterized test (incremented at each test execution)
     */
    private static int testCounter = -1;
    
    /*
     * The test values for the parameterized test
     */
    private int[] input;
    private int[] maxValues;
    private int[] expected;
    private int delta;
    private boolean tolerate1;
    
    /*
     * The used PropagationMath
     */
    private static PropagationMath distribution;
    
    /*
     * Test definition
     */
    
    @Rule
    public Timeout globalTimeout = new Timeout(200, TimeUnit.MILLISECONDS);
    
    @BeforeClass
    public static void setUpPropagationMath() {
        ProportionalDistributionTest.distribution = new ProportionalDistribution();
    }
    
    // Contains the test cases
    @Parameterized.Parameters
    public static Collection<Object[]> Values() {
        Collection<Object[]> list = new LinkedList<Object[]>();
        
        int maxint = Integer.MAX_VALUE; // maxint is odd by the way
        int m = maxint;
        
        //                                  input,                upper-bound    delta           expected     tolerate-1     test case number
        list.add(new Object[]{new int[]{0, 0, 0, 0, 0}, new int[]{m, m, m, m, m}, 20, new int[]{4, 4, 4, 4, 4}, false}); //         No  0
        list.add(new Object[]{new int[]{1, 1, 1, 1, 1}, new int[]{m, m, m, m, m}, 20, new int[]{5, 5, 5, 5, 5}, false}); //         No  1
        list.add(new Object[]{new int[]{5000, 5000, 5000, 5000, 5000}, new int[]{m, m, m, m, m}, 20, new int[]{5004, 5004, 5004, 5004, 5004}, false}); // No  2
        list.add(new Object[]{new int[]{1, 2, 2, 2, 2}, new int[]{m, m, m, m, m}, 18, new int[]{3, 6, 6, 6, 6}, false}); //         No  3
        list.add(new Object[]{new int[]{1, 2, 2, 2, 2}, new int[]{m, m, m, m, m}, 17, new int[]{3, 6, 6, 6, 6}, true}); //          No  4
        list.add(new Object[]{new int[]{1, 2, 2, 2, 2}, new int[]{m, m, m, m, m}, 19, new int[]{3, 6, 6, 6, 6}, true}); //          No  5
        list.add(new Object[]{new int[]{3, 6, 6, 6, 6}, new int[]{m, m, m, m, m}, -18, new int[]{1, 2, 2, 2, 2}, false}); //        No  6
        list.add(new Object[]{new int[]{3, 6, 6, 5, 6}, new int[]{m, m, m, m, m}, -17, new int[]{1, 3, 3, 2, 3}, true}); //         No  7
        list.add(new Object[]{new int[]{3, 6, 6, 7, 6}, new int[]{m, m, m, m, m}, -19, new int[]{1, 2, 2, 3, 2}, true}); //        No  8
        list.add(new Object[]{new int[]{4, 6, 6, 6, 6}, new int[]{m, m, m, m, m}, -19, new int[]{2, 2, 2, 2, 2}, true}); //         No  9
        list.add(new Object[]{new int[]{2, 1, 2, 2, 2}, new int[]{m, m, m, m, m}, 17, new int[]{6, 3, 6, 6, 6}, true}); //          No 10
        list.add(new Object[]{new int[]{2, 1, 2, 2, 2}, new int[]{m, m, m, m, m}, 19, new int[]{6, 3, 6, 6, 6}, true}); //          No 11
        list.add(new Object[]{new int[]{2, 1, 2, 2, 2}, new int[]{m, m, m, m, m}, 18, new int[]{6, 3, 6, 6, 6}, false}); //         No 12
        list.add(new Object[]{new int[]{1, 2, 2, 2, 2}, new int[]{m, m, m, m, m}, 82, new int[]{10, 20, 20, 20, 20}, true}); //     No 13
        list.add(new Object[]{new int[]{100, 200, 200, 200, 200}, new int[]{m, m, m, m, m}, 82, new int[]{109, 218, 218, 218, 218}, true}); // No 14
        list.add(new Object[]{new int[]{1000, 2000, 2000, 2000, 2000}, new int[]{m, m, m, m, m}, 87, new int[]{1009, 2019, 2019, 2019, 2019}, true}); // No 15
        list.add(new Object[]{new int[]{1000, 2000, 2000, 2000, 2000}, new int[]{m, m, m, m, m}, 820, new int[]{1091, 2182, 2182, 2182, 2182}, true}); // No 16
        list.add(new Object[]{new int[]{1, 2, 2, 2, 2}, new int[]{m, m, m, m, m}, 0, new int[]{1, 2, 2, 2, 2}, false}); //          No 17
        list.add(new Object[]{new int[]{50000, 40000, 30000, 20000}, new int[]{m, m, m, m}, 7000, new int[]{52500, 42000, 31500, 21000}, false}); // No 18
        list.add(new Object[]{new int[]{50000, 40000, 30000, 20000}, new int[]{m, m, m, m}, 7100, new int[]{52535, 42028, 31521, 21014}, true}); //  No 19
        list.add(new Object[]{new int[]{50000, 40000, 30000, 20000}, new int[]{m, m, m, m}, -7000, new int[]{47500, 38000, 28500, 19000}, false});// No 20
        list.add(new Object[]{new int[]{50000, 40000, 30000, 20000}, new int[]{m, m, m, m}, -7100, new int[]{47465, 37972, 28479, 18986}, true}); // No 21
        list.add(new Object[]{new int[]{maxint, 0, 0, maxint}, new int[]{m, m, m, m}, -maxint, new int[]{maxint / 2, 0, 0, maxint / 2}, true}); // No 22
        list.add(new Object[]{new int[]{maxint - 2, 0, 0, maxint - 2}, new int[]{m, m, m, m}, -maxint, new int[]{maxint / 2 - 2, 0, 0, maxint / 2 - 2}, true}); // No 23
        list.add(new Object[]{new int[]{maxint, 0, 0, 0}, new int[]{m, m, m, m}, -maxint, new int[]{0, 0, 0, 0}, false}); //     No 24
        list.add(new Object[]{new int[]{1573684200, 0, 0, 0}, new int[]{m, m, m, m}, -10000, new int[]{1573674200, 0, 0, 0}, false}); // No 25
        list.add(new Object[]{new int[]{1573684200, 0, 0, 0}, new int[]{m, m, m, m}, -20000000, new int[]{1553684200, 0, 0, 0}, false}); // No 26
        
        list.add(new Object[]{new int[]{4, 4, 2, 4}, new int[]{6, 6, 8, 7}, 13, new int[]{6, 6, 8, 7}, false}); // No 27
        list.add(new Object[]{new int[]{4, 4, 2, 4}, new int[]{6, 6, 8, 7}, 12, new int[]{6, 6, 8, 7}, true}); // No 28
        list.add(new Object[]{new int[]{0, 0, 0, 0}, new int[]{0, 0, 0, 0}, 0, new int[]{0, 0, 0, 0}, false}); //         No 29
        list.add(new Object[]{new int[]{4, 1, 1, 1}, new int[]{5, 10, 10, 10}, 7, new int[]{5, 3, 3, 3}, false}); //      No 30
        list.add(new Object[]{new int[]{4, 1, 1, 2}, new int[]{5, 10, 10, 10}, 8, new int[]{5, 2, 2, 6}, true}); //       No 31
        list.add(new Object[]{new int[]{4, 1, 1, 2}, new int[]{5, 10, 10, 10}, 10, new int[]{5, 3, 3, 6}, true}); //      No 32
        list.add(new Object[]{new int[]{8, 4, 4, 1}, new int[]{10, 20, 20, 20}, 12, new int[]{10, 8, 8, 1}, true}); //    No 33
        
        //
        //
        // return list of test cases
        return list;
    }
    
    /*
     * Test execution
     */
    
    /*
     * Constructs the parameterized test instance.
     */
    public ProportionalDistributionTest(int[] input, int[] maxValues, int delta, int[] expected, boolean tolerate1) {
        this.input = input;
        this.maxValues = maxValues;
        this.delta = delta;
        this.expected = expected;
        this.tolerate1 = tolerate1;
    }
    
    /*
     * Checks several issues: 1. is the expected amount propagated 2. is an exception thrown 3. are the values as expected
     */
    @Test
    public void concreteValueTest() {
        ProportionalDistributionTest.testCounter++;
        int[] beforePropagation = this.copyArray(this.input);
        
        try {
            ProportionalDistributionTest.distribution.propagate(this.input, this.maxValues, this.delta);
        }
        catch(Exception e) {
            Assert.fail("An unexpected " + e.getClass().getName() + " exception occurred");
        }
        
        // print the result of the propagation for debugging if activated
        if(ProportionalDistributionTest.debugPrinting) {
            this.printArray(this.input);
        }
        
        // stimmt der geänderte Wert insgesamt?
        Assert.assertTrue(ProportionalDistributionTest.otherAmountMessage, this.delta == this.sumDifference(this.input, beforePropagation));
        
        // je nach testfall: entweder es muss übereintimmung, oder je array-Eintrag eine Abweichung von 1 erlaubt
        if(this.tolerate1) {
            Assert.assertTrue(ProportionalDistributionTest.failWithTolerationMessage, this.tolerate1equals(this.input, this.expected));
        }
        else {
            Assert.assertArrayEquals(ProportionalDistributionTest.failNoTolerationMessage, this.expected, this.input);
        }
    }
    
    /*
     * Calculates the sum of all differences of afterPropagation and beforePropagation. The Arrays must have the same length.
     */
    private int sumDifference(int[] afterPropagation, int[] beforePropagation) {
        int sumDif = 0;
        for(int i = 0; i != afterPropagation.length; i++) {
            sumDif += afterPropagation[i] - beforePropagation[i];
        }
        return sumDif;
    }
    
    /*
     * Copies an array in a loop
     */
    private int[] copyArray(int[] x) {
        int[] copy = new int[x.length];
        for(int i = 0; i != x.length; i++) {
            copy[i] = x[i];
        }
        return copy;
    }
    
    /*
     * Returns true if all array entrys of input and expected dont differ more than 1.
     */
    private boolean tolerate1equals(int[] input, int[] expected) {
        if(input.length != expected.length) {
            return false;
        }
        
        for(int i = 0; i != input.length; i++) {
            if(Math.abs(input[i] - expected[i]) > 1) {
                return false;
            }
        }
        return true;
    }
    
    /*
     * Prints the test number and the given array.
     */
    private void printArray(int[] input) {
        System.out.print("TestNr. " + ProportionalDistributionTest.testCounter + ":  ");
        for(int i = 0; i != this.input.length; i++) {
            System.out.print(this.input[i] + " ");
        }
        System.out.println();
    }
    
}
