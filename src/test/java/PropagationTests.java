import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.PropagationTestEasyStructure;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.PropagationOfficialStructure.PropagationTestOfficialStructureAllTests;
import edu.kit.iti.formal.pse1.mandatsverteilung.propagation.NormalDistributionTest;
import edu.kit.iti.formal.pse1.mandatsverteilung.propagation.ProportionalDistributionTest;
import edu.kit.iti.formal.pse1.mandatsverteilung.propagation.UniformDistributionTest;

@RunWith(Suite.class)
/* @formatter:off */
@SuiteClasses({
    PropagationTestEasyStructure.class,
    PropagationTestOfficialStructureAllTests.class,
    
    UniformDistributionTest.class,
    ProportionalDistributionTest.class,
    NormalDistributionTest.class
})
/* @formatter:on */
public class PropagationTests {
}
