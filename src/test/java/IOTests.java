import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.io.CSVParserTest;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.io.IOTest3X0;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.io.IOTestT300;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.io.ImportTestRandom;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.io.MemoryAccessTest;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.io.ReimportParserTest;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.io.SessionIOTest;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.io.SettingsIOTest;

@RunWith(Suite.class)
/* @formatter:off */
@SuiteClasses({
    MemoryAccessTest.class,
    
    IOTest3X0.class,
    IOTestT300.class,
    
    SessionIOTest.class,
    SettingsIOTest.class,
    
    ReimportParserTest.class,
    CSVParserTest.class,
    
    ImportTestRandom.class,
})
/* @formatter:on */
public class IOTests {
}
