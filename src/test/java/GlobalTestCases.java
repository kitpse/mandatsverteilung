import org.junit.Assert;
import org.junit.Test;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.io.Import;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Election;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.ElectionArea;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Federation;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Party;

public class GlobalTestCases {
    
    /**
     * Change votes for the SPD.
     */
    @Test
    public void testT110() {
        Election election = Import.importInternalElection("bundestagswahl2013.csv");
        Federation fed = election.getElectionResult().getFederation();
        
        Party spd = null;
        for(Party party : fed.getParties()) {
            if(party.getName().equalsIgnoreCase("spd")) {
                spd = party;
            }
        }
        int oldVotes = fed.getValidSecondVotes();
        int oldInvalidVotes = fed.getInvalidSecondVotes();
        fed.changeSecondVotes(spd, fed.getSecondVotes(spd) + 2000);
        Assert.assertEquals("Die Anzahl der neuen Stimmen muss stimmen.", fed.getValidSecondVotes(), oldVotes + 2000);
        Assert.assertEquals("Die Nichtwähler verringern sich um 2000.", oldInvalidVotes - 2000, fed.getInvalidSecondVotes());
    }
    
    /**
     * Add a new party. Election result has to be different.
     */
    @Test
    public void testT130() {
        Election election = Import.importInternalElection("bundestagswahl2013.csv");
        Federation fed = election.getElectionResult().getFederation();
        
        int size = election.getBundestag().getSize();
        
        ElectionArea<?> state = null;
        for(ElectionArea<?> area : fed.getChildren()) {
            if(area.getName().equalsIgnoreCase("Bayern")) {
                state = area;
            }
        }
        Party newParty = new Party("neue Partei");
        state.addParty(newParty);
        state.changeNumberOfVotes(state.getNumberOfVotes() + 80000000);
        state.changeSecondVotes(newParty, 8000000);
        
        election.getElectionResult().onChange();
        
        Assert.assertNotEquals(size, election.getBundestag().getSize());
    }
    
    /**
     * Change some data in Hamburg. New result has to be correct.
     */
    @Test
    public void testT200a() {
        Election election = Import.importInternalElection("bundestagswahl2013.csv");
        Federation fed = election.getElectionResult().getFederation();
        
        ElectionArea<?> state = null;
        for(ElectionArea<?> area : fed.getChildren()) {
            if(area.getName().equalsIgnoreCase("Hamburg")) {
                state = area;
            }
        }
        Party party = null;
        for(Party par : state.getParties()) {
            if(par.getName().equalsIgnoreCase("SPD")) {
                party = par;
            }
        }
        state.changeFirstVotes(party, state.getFirstVotes(party) + 10000);
        
        election.getElectionResult().onChange();
        
        Assert.assertEquals(election.getBundestag().getSize(), 631);
        
        for(Party par : election.getBundestag().getParties()) {
            if(par.getName().equalsIgnoreCase("Die Linke")) {
                Assert.assertEquals(election.getBundestag().getSeatsRounded(par), 64);
            }
            if(par.getName().equalsIgnoreCase("Grüne")) {
                Assert.assertEquals(election.getBundestag().getSeatsRounded(par), 63);
            }
            if(par.getName().equalsIgnoreCase("SPD")) {
                Assert.assertEquals(election.getBundestag().getSeatsRounded(par), 193);
            }
            if(par.getName().equalsIgnoreCase("CSU")) {
                Assert.assertEquals(election.getBundestag().getSeatsRounded(par), 56);
            }
            if(par.getName().equalsIgnoreCase("CDU")) {
                Assert.assertEquals(election.getBundestag().getSeatsRounded(par), 255);
            }
        }
    }
    
    /**
     * Change some data in Hamburg. New result has to be correct.
     */
    @Test
    public void testT200b() {
        Election election = Import.importInternalElection("bundestagswahl2013.csv");
        Federation fed = election.getElectionResult().getFederation();
        
        ElectionArea<?> state = null;
        for(ElectionArea<?> area : fed.getChildren()) {
            if(area.getName().equalsIgnoreCase("Hamburg")) {
                state = area;
            }
        }
        
        state.changeValidSecondVotes((int) (state.getValidSecondVotes() * 1.05));
        
        election.getElectionResult().onChange();
        
        Assert.assertEquals(election.getBundestag().getSize(), 631);
        
        for(Party par : election.getBundestag().getParties()) {
            if(par.getName().equalsIgnoreCase("Die Linke")) {
                Assert.assertEquals(election.getBundestag().getSeatsRounded(par), 64);
            }
            if(par.getName().equalsIgnoreCase("Grüne")) {
                Assert.assertEquals(election.getBundestag().getSeatsRounded(par), 63);
            }
            if(par.getName().equalsIgnoreCase("SPD")) {
                Assert.assertEquals(election.getBundestag().getSeatsRounded(par), 193);
            }
            if(par.getName().equalsIgnoreCase("CSU")) {
                Assert.assertEquals(election.getBundestag().getSeatsRounded(par), 56);
            }
            if(par.getName().equalsIgnoreCase("CDU")) {
                Assert.assertEquals(election.getBundestag().getSeatsRounded(par), 255);
            }
        }
    }
    
    /**
     * Change some data in the constituency "Bremen I". The amount of second votes has to change for the federation.
     */
    @Test
    public void testT230a() {
        Election election = Import.importInternalElection("bundestagswahl2013.csv");
        Federation fed = election.getElectionResult().getFederation();
        
        int oldAmount = fed.getValidSecondVotes();
        
        ElectionArea<?> constit = null;
        for(ElectionArea<?> area : fed.getChildren()) {
            if(area.getName().equalsIgnoreCase("Bremen")) {
                for(ElectionArea<?> a : area.getChildren()) {
                    if(a.getName().equalsIgnoreCase("Bremen I")) {
                        constit = area;
                    }
                }
            }
        }
        
        /**
         * Raise election participation by 0.5 percent.
         */
        constit.changeValidSecondVotes((int) (constit.getValidSecondVotes() * 1.005));
        
        election.getElectionResult().onChange();
        
        Assert.assertNotEquals(oldAmount, fed.getValidSecondVotes());
    }
    
    /**
     * Change some data in the constituency "Bremen I". The amount of second votes has to change for the federation.
     */
    @Test
    public void testT230b() {
        Election election = Import.importInternalElection("bundestagswahl2013.csv");
        Federation fed = election.getElectionResult().getFederation();
        
        int oldAmount = fed.getEligibleVoters();
        
        ElectionArea<?> constit = null;
        for(ElectionArea<?> area : fed.getChildren()) {
            if(area.getName().equalsIgnoreCase("Bremen")) {
                for(ElectionArea<?> a : area.getChildren()) {
                    if(a.getName().equalsIgnoreCase("Bremen I")) {
                        constit = area;
                    }
                }
            }
        }
        
        /**
         * Raise eligible voters by 50000.
         */
        constit.changeEligibleVoters(constit.getEligibleVoters() + 50000);
        
        election.getElectionResult().onChange();
        
        Assert.assertNotEquals(oldAmount, fed.getEligibleVoters());
    }
}
