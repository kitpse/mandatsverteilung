/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.RenderingHints;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * This class is a Diagram which renders a List of {@link DisplayableValues} as a PieChart with its description to the side.<br>
 * The PieChart can be either the common PieChart or only the upper half with a hole in the middle (also known as the typically Bundestag-Diagram).
 * 
 * @author Felix Heim
 * @version 2
 */
abstract class PieChart extends Diagram {
    private static final long serialVersionUID = 1L;
    
    // The size of the PieChart (width and height)
    private static final int DIMENSION = 300;
    private static final int DIMENSION_THIRD = PieChart.DIMENSION / 3;
    
    // true -> Bundestag like Char, false -> normal Pie Chart
    private boolean halfRingChart;
    
    /** the sum of all values (used to calculate the angle of the values) */
    private int totalSum;
    
    /** a panel holding all the descriptions */
    private JPanel symbologyPanel = new JPanel();
    
    /**
     * @param halfRingChart
     *            whether only the upper half of the chart should be rendered with an hole in middle (also known as the typically Bundestag-Diagram).
     */
    protected PieChart(boolean halfRingChart) {
        super(new FlowLayout());
        this.halfRingChart = halfRingChart;
        this.add(new ChartPanel()); // the panel with the actual chart
    }
    
    @Override
    protected void setValues(List<DisplayableValues> values) {
        this.values = values;
        this.totalSum = (int) this.values.stream().mapToDouble(pair -> pair.value).sum();
        
        // first remove the old pane
        this.remove(this.symbologyPanel);
        
        // then recreate the symbology pane for the new values
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        c.anchor = GridBagConstraints.WEST;
        this.symbologyPanel = new JPanel(new GridBagLayout());
        for(DisplayableValues value : values) {
            JLabel label = new JLabel(value.description); // the text to display
            label.setForeground(value.color); // the text gets displayed in the Color of the Party
            this.symbologyPanel.add(label, c);
            c.gridy++;
            if(c.gridy == Math.ceil(this.values.size() / 2.0) || c.gridy == 10 && this.halfRingChart || c.gridy == 20) { // check if we need to start a new column
                c.gridx++;
                c.gridy = 0;
            }
        }
        this.add(this.symbologyPanel); // add the new pane
    }
    
    private class ChartPanel extends JPanel {
        private static final long serialVersionUID = 1L;
        
        @Override
        public Dimension getPreferredSize() {
            return new Dimension(PieChart.DIMENSION + 25, PieChart.this.halfRingChart ? 150 : 300); // the size of the Chart is fixed
        }
        
        @Override
        protected void paintComponent(Graphics g) { // override the paint method to render the chart instead of the usual components
            ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON); // use antialiasing
            
            int last = 180; // the last angle, we start at the left side instead of the right one, so wee need to rotate by the half at the beginning
            
            for(DisplayableValues pair : PieChart.this.values) {
                g.setColor(pair.color);
                int angle = (int) Math.round(pair.value / PieChart.this.totalSum * (PieChart.this.halfRingChart ? -180 : -360));
                g.fillArc(0, 0, PieChart.DIMENSION, PieChart.DIMENSION, last, angle);
                last += angle;
            }
            
            g.setColor(Color.WHITE);
            g.fillArc(0, 0, PieChart.DIMENSION, PieChart.DIMENSION, last, PieChart.this.halfRingChart ? -last : -180 - last); // since rounding is not precise the rest of circle gets filled white
            if(PieChart.this.halfRingChart) { // on a halfRingChart wee need to cut out a circle in the middle....
                g.setColor(this.getBackground());
                g.fillArc(PieChart.DIMENSION_THIRD, PieChart.DIMENSION_THIRD, PieChart.DIMENSION_THIRD, PieChart.DIMENSION_THIRD, 180, -180);
                g.setColor(Color.BLACK);
                g.drawString("Sitze: " + PieChart.this.totalSum, PieChart.DIMENSION_THIRD + 10, PieChart.DIMENSION / 2 - 2); //... and display the sum which is the number of seats in the Bundestag
            }
            
            g.dispose();
        }
    }
}