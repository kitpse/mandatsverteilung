/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.data.model;

import java.util.HashSet;
import java.util.Set;

import edu.kit.iti.formal.pse1.mandatsverteilung.propagation.PropagationMath;
import edu.kit.iti.formal.pse1.mandatsverteilung.propagation.ProportionalDistribution;

/**
 * This class represents an election result for an election. It contains all votes for the citizens in the federation, federal states and constituencies.
 * 
 * @author Nils Wilka
 * @version 1.0
 */
public class ElectionResult implements DeepCloneable<ElectionResult>, ChangeListener {
    
    /**
     * The federation for this election result.
     */
    private Federation federation;
    
    /**
     * The propagation class for this election result.
     */
    private Propagation propagation;
    
    /**
     * The listeners on changes of this election result.
     */
    private Set<ChangeListener> listeners = new HashSet<>();
    
    /**
     * Creates a new election result with standard values.
     */
    public ElectionResult() {
        this.federation = DataStructureFactory.createDataHierarchy(this);
        this.federation.addChangeListener(this);
        this.propagation = new Propagation(new ProportionalDistribution());
    }
    
    /**
     * Creates a new election result with given {@link Propagation}. This method must only be used for cloning an election result. The cloning process must make
     * sure to create a well defined object with well defined associated objects.
     * 
     * @param propagation
     *            The {@link Propagation} to use.
     */
    private ElectionResult(Propagation propagation) {
        this.federation = null;
        this.propagation = propagation;
    }
    
    /**
     * Gets the federation of this election result. It has the vote results of this election and a number of states which have a number of constituencies. They
     * can be accessed through {@code getChildren()} and contain vote results for the specific election area, too.
     * 
     * @return The federation containing the vote results of this election.
     */
    public Federation getFederation() {
        return this.federation;
    }
    
    /**
     * Changes the method of propagating votes to the different election areas if a vote gets changed.
     * 
     * @param propagationMethod
     *            The method of propagating votes to the different election areas if a vote gets changed.
     */
    public void changePropagation(PropagationMath propagationMethod) {
        this.propagation.setPropagation(propagationMethod);
        this.notifyListeners();
    }
    
    /**
     * Gets the method of propagating votes to the different election areas if a vote gets changed.
     * 
     * @return The method of propagating votes to the different election areas if a vote gets changed.
     */
    public PropagationMath getPropagationMath() {
        return this.propagation.getPropagation();
    }
    
    /**
     * Gets the class that changes the votes. It contains the method of propagating votes to the different election areas if a vote gets changed.
     * 
     * @return The class for changing votes if they need to be propagated.
     */
    public Propagation getPropagation() {
        return this.propagation;
    }
    
    @Override
    public ElectionResult deepClone() {
        ElectionResult clone = new ElectionResult(this.propagation.deepClone());
        clone.federation = DataStructureFactory.cloneStructure(clone, this);
        clone.federation.addChangeListener(clone);
        return clone;
    }
    
    @Override
    public void onChange() {
        this.notifyListeners();
    }
    
    /**
     * Adds the given listener to the set of listeners of this election result.
     * 
     * @param listener
     *            The listener to add.
     */
    public void addChangeListener(ChangeListener listener) {
        this.listeners.add(listener);
    }
    
    /**
     * Removes the given listener from the set of listeners of this Election if it is present.
     * 
     * @param listener
     *            The listener to remove.
     */
    public boolean removeChangeListener(ChangeListener listener) {
        return this.listeners.remove(listener);
    }
    
    /**
     * Notifies all listeners of this {@code ElectionResult}.
     */
    private void notifyListeners() {
        this.listeners.forEach(ChangeListener::onChange);
    }
    
    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof ElectionResult)) {
            return false;
        }
        ElectionResult comp = (ElectionResult) obj;
        return comp.federation.equals(this.federation) && comp.getPropagationMath().equals(this.getPropagationMath());
    }
}
