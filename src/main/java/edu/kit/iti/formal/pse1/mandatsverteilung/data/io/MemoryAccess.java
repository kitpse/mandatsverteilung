/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.data.io;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.jar.JarFile;

import org.apache.tika.parser.txt.CharsetDetector;

import edu.kit.iti.formal.pse1.mandatsverteilung.gui.GuiHelper;
import edu.kit.iti.formal.pse1.mandatsverteilung.gui.controller.Mandatsverteilung;

/**
 * The {@link MemoryAccess} class is a utility class to handle writing and reading to/from the file system.
 * 
 * @author Felix Heim
 * @version 1.0
 */
public final class MemoryAccess {
    static {
        // this checks, which Operating System we are on an sets the OS_DEFAULT_LOCATION
        String os = System.getProperty("os.name");
        String programName = "Mandatsverteilung" + File.separator;
        if(os.startsWith("Win")) {
            // Windows
            OS_DEFAULT_LOCATION = System.getenv("APPDATA") + File.separator + programName;
        }
        else if(os.startsWith("Mac")) {
            // Mac OS
            OS_DEFAULT_LOCATION = System.getProperty("user.home") + "/Library/Application Support/" + programName;
        }
        else if(os.startsWith("Linux")) {
            // Linux Kernel
            OS_DEFAULT_LOCATION = System.getProperty("user.home") + "/." + programName;
        }
        else {
            // on every other os we just use the directory of the jar
            OS_DEFAULT_LOCATION = System.getProperty("user.dir") + File.separator + "." + programName;
        }
        
        File defaultFolder = new File(MemoryAccess.OS_DEFAULT_LOCATION);
        if(defaultFolder.exists()) {
            Mandatsverteilung.log("Found folder " + MemoryAccess.OS_DEFAULT_LOCATION + " to load and store Application data");
            if(defaultFolder.isFile()) {
                GuiHelper.showError("Achtung!\nDer Ordner zum Speichern von Datein (" + MemoryAccess.OS_DEFAULT_LOCATION + ") scheint eine Datei zu sein!");
            }
        }
        else {
            Mandatsverteilung.log("Creating Folder " + MemoryAccess.OS_DEFAULT_LOCATION + " to store Application data");
            if(!defaultFolder.mkdirs()) {
                GuiHelper.showError("Achtung!\nEs gab ein Problem beim erstellen des Ornders: " + MemoryAccess.OS_DEFAULT_LOCATION);
            }
        }
        
        // now check if we are in a jar file or in a folder
        boolean inJar = false;
        JarFile jar = null;
        File parent = null;
        try {
            parent = new File(MemoryAccess.class.getProtectionDomain().getCodeSource().getLocation().toURI());
            File memoryAccess = new File(parent, MemoryAccess.class.getName().replace('.', File.separatorChar) + ".class");
            inJar = !memoryAccess.exists();
            if(inJar) {
                jar = new JarFile(parent);
            }
        }
        catch(URISyntaxException | IOException e) {
            assert false : "Internal Error while looking for the path to the program";
        }
        if(inJar) {
            JAR = jar;
            Mandatsverteilung.log("The program is run from the following JarFile: " + jar.getName());
        }
        else {
            JAR = null;
            Mandatsverteilung.log("The program is run from the following directory: " + parent.getAbsolutePath());
        }
        PARENT = parent;
    }
    
    /**
     * The operating system dependent default location to save application data.
     */
    private static final String OS_DEFAULT_LOCATION;
    
    private static final File PARENT;
    private static final JarFile JAR;
    
    /**
     * Utility classes don't have instances.
     */
    private MemoryAccess() {
    }
    
    /**
     * Loads data from the file system from the given file path. If the last line is empty, it will be omitted.
     * 
     * @param path
     *            The path of the file to load.
     * @return The loaded data.
     * @throws IOException
     *             Thrown if an error occurred while reading file.
     * @throws FileNotFoundException
     *             Thrown if file specified by given path could not be found.
     */
    public static String loadExternalFile(String path) throws FileNotFoundException, IOException {
        return MemoryAccess.readInputStream(new FileInputStream(new File(path)));
    }
    
    /**
     * Loads data from the file system from the given file path. If the last line is empty, it will be omitted.
     * 
     * @param path
     *            The path of the file to load.
     * @return The loaded data.
     * @throws IOException
     *             Thrown if an error occurred while reading file.
     * @throws FileNotFoundException
     *             Thrown if file specified by given path could not be found.
     */
    public static List<String> loadExternalFileToList(String path) throws FileNotFoundException, IOException {
        return MemoryAccess.readInputStreamToList(new FileInputStream(new File(path)));
    }
    
    /**
     * Loads data from the file system from the given file path. If the last line is empty, it will be omitted.
     * 
     * @param path
     *            The path of the file to load.
     * @return The loaded data or null if an error occurred.
     */
    public static String loadExternalFileOrNull(String path) {
        try {
            return MemoryAccess.loadExternalFile(path);
        }
        catch(IOException e) {
            return null;
        }
    }
    
    /**
     * Loads data from the file system from the given file path. If the last line is empty, it will be omitted.
     * 
     * @param path
     *            The path of the file to load.
     * @return The loaded data in a list or null if an error occurred.
     */
    public static List<String> loadExternalFileToListOrNull(String path) {
        try {
            return MemoryAccess.loadExternalFileToList(path);
        }
        catch(IOException e) {
            return null;
        }
    }
    
    /**
     * Loads data from the file system from the given file path. If the last line is empty, it will be omitted.
     * 
     * @param name
     *            The name of the file to load in the program folder.
     * @return The loaded data.
     * @throws IOException
     *             Thrown if an error occurred while reading file.
     * @throws FileNotFoundException
     *             Thrown if file specified by given path could not be found.
     */
    public static String loadInternalFile(String name) throws FileNotFoundException, IOException {
        return MemoryAccess.readInputStream(MemoryAccess.JAR == null ? new FileInputStream(new File(MemoryAccess.PARENT, name)) : MemoryAccess.JAR
                .getInputStream(MemoryAccess.JAR.getEntry(name)));
    }
    
    /**
     * Loads data from the file system from the given file path. If the last line is empty, it will be omitted.
     * 
     * @param name
     *            The name of the file to load in the program folder.
     * @return The loaded data in a list.
     * @throws IOException
     *             Thrown if an error occurred while reading file.
     * @throws FileNotFoundException
     *             Thrown if file specified by given path could not be found.
     */
    public static List<String> loadInternalFileToList(String name) throws FileNotFoundException, IOException {
        return MemoryAccess.readInputStreamToList(MemoryAccess.JAR == null ? new FileInputStream(new File(MemoryAccess.PARENT, name)) : MemoryAccess.JAR
                .getInputStream(MemoryAccess.JAR.getEntry(name)));
    }
    
    public static InputStream getInternalInputStream(String name) throws FileNotFoundException, IOException {
        return MemoryAccess.JAR == null ? new FileInputStream(new File(MemoryAccess.PARENT, name)) : MemoryAccess.JAR.getInputStream(MemoryAccess.JAR
                .getEntry(name));
    }
    
    /**
     * Loads data from the file system from the given file path. If the last line is empty, it will be omitted.
     * 
     * @param name
     *            The name of the file to load in the program folder.
     * @return The loaded data or null if and error occurred.
     */
    public static String loadInternalFileOrNull(String name) {
        try {
            return MemoryAccess.loadInternalFile(name);
        }
        catch(IOException e) {
            return null;
        }
    }
    
    /**
     * Loads data from the file system from the given file path. If the last line is empty, it will be omitted.
     * 
     * @param name
     *            The name of the file to load in the program folder.
     * @return The loaded data in a list or null if and error occurred.
     */
    public static List<String> loadInternalFileToListOrNull(String name) {
        try {
            return MemoryAccess.loadInternalFileToList(name);
        }
        catch(IOException e) {
            return null;
        }
    }
    
    private static List<String> readInputStreamToList(InputStream in) throws IOException {
        List<String> lines = new LinkedList<>();
        if(!(in instanceof BufferedInputStream)) {
            in = new BufferedInputStream(in);
        }
        BufferedReader reader = new BufferedReader(new InputStreamReader(in, Charset.forName(new CharsetDetector().setText(in).detect().getName())));
        String line;
        while((line = reader.readLine()) != null) {
            lines.add(line);
        }
        reader.close();
        return lines;
    }
    
    private static String readInputStream(InputStream in) throws IOException {
        Iterator<String> lines = MemoryAccess.readInputStreamToList(in).iterator();
        if(!lines.hasNext()) {
            return null;
        }
        StringBuilder builder = new StringBuilder(lines.next());
        while(lines.hasNext()) {
            builder.append('\n').append(lines.next());
        }
        return builder.toString();
    }
    
    /**
     * Stores the given data in the given file on the file system.
     * 
     * @param filePath
     *            The file to write the data to.
     * @param data
     *            The data to write into the file.
     * @throws IOException
     *             if an error occurs
     */
    public static void storeData(String filePath, String data) throws IOException {
        FileOutputStream out = new FileOutputStream(filePath);
        out.write(data.getBytes());
        out.close();
    }
    
    /**
     * Gets the OS dependent default location to save application data.
     * 
     * @return The OS dependent default location to save application data.
     */
    public static String getOsDefaultLocation() {
        return MemoryAccess.OS_DEFAULT_LOCATION;
    }
}
