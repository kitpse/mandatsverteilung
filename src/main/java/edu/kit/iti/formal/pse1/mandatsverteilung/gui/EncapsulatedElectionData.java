/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.gui;

import java.util.function.Function;

import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.ElectionArea;

/**
 * The {@link EncapsulatedElectionData} shows the collapsed version of {@link DataPanel}s for each child of the given {@link ElectionArea}
 * 
 * @author Felix Heim
 * @version 1
 */
class EncapsulatedElectionData extends Tab {
    private JComponent contentPane;
    
    /**
     * @param area
     *            the area for which the {@link EncapsulatedElectionData} should be get created
     * @param useScrollbar
     *            whether this pane should be in a {@link JScrollPane}
     * @param electionDataCreator
     *            the function to create the election data pane
     */
    EncapsulatedElectionData(ElectionArea<?> area, boolean useScrollbar, Function<ElectionArea<?>, Tab> electionDataCreator) {
        final JPanel contentPane = new JPanel();
        contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
        this.contentPane = useScrollbar ? GuiHelper.createScrollPane(contentPane) : contentPane;
        // adds a data panel for each child
        area.getChildren().forEach(childArea -> contentPane.add(new DataPanel(childArea, electionDataCreator)));
    }
    
    @Override
    public JComponent getContentPane() {
        return this.contentPane;
    }
}
