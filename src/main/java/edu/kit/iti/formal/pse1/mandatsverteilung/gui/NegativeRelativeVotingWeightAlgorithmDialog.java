/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.gui;

import java.awt.BorderLayout;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.WindowConstants;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Election;
import edu.kit.iti.formal.pse1.mandatsverteilung.gui.controller.Mandatsverteilung;
import edu.kit.iti.formal.pse1.mandatsverteilung.simulation.NegativeRelativeVotingWeight;
import edu.kit.iti.formal.pse1.mandatsverteilung.simulation.NegativeRelativeVotingWeightElement;

/**
 * This Dialog shows the process of a running {@link NegativeRelativeVotingWeight} Algorithm. It shows the time passed since it started and has the option to
 * abort it. <br>
 * When the algorithm has finished it loads the results into the GUI.
 * 
 * @author Felix Heim
 * @version 1
 */
public class NegativeRelativeVotingWeightAlgorithmDialog extends JDialog {
    private static final long serialVersionUID = 1L;
    
    /** the time in millis when the algorithm started */
    private long startTime = System.currentTimeMillis();
    
    /** a {@link JLabel} showing the time in minutes and seconds since the algorithm started */
    private JLabel timePassed = new JLabel("Verstrichene Zeit: 00:00 min");
    
    /**
     * @param election
     *            the {@link Election} in which to search for negative relative voting weight
     */
    private NegativeRelativeVotingWeightAlgorithmDialog(Election election) {
        // set the title
        super(Mandatsverteilung.getMainWindow(), "Algorithmus l\u00E4uft");
        // create a new algorithm instance
        NegativeRelativeVotingWeight algo = new NegativeRelativeVotingWeight();
        // use the abort button to close the window
        this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        this.setLayout(new BorderLayout());
        // create the Thread running the actual algorithm 
        new Thread(
                   () -> {
                       /* log that we started */
                       Mandatsverteilung.log("Started Negative Relative Voting Weight Algorithm for " + election.getName());
                       /* run the algorithm an save the result */
                       List<NegativeRelativeVotingWeightElement> elements = algo.findNegativeRelativeVotingWeights(election);
                       /* show the result in the GUI */
                       Mandatsverteilung.getMainWindow().addTabAlgorithmResult(new TabNegRelVotingWeightResult(election.getName() + " - Neg.Rel. Stimmgewicht",
                                                                                                               elements));
                       
                       /* calculate the time needed */
                       long timePassed = (System.currentTimeMillis() - this.startTime) / 1000;
                       long minutes = timePassed / 60;
                       long seconds = timePassed % 60;
                       /* and log the time needed */
                       Mandatsverteilung.log(new StringBuilder("Finished Negative Relative Voting Weight Algorithm for ").append(election.getName())
                               .append(" in ").append(minutes < 0 ? "0" : "").append(minutes).append(seconds < 10 ? ":0" : ":").append(seconds).append(" min")
                               .toString());
                       /* finally close this dialog */
                       this.dispose();
                   }).start();
        // first line: show the name of the algorithm and the election
        this.add(new JLabel("<html><body>Algorithmus: Auffinden von negativem<br>relativem Stimmgewicht<br>Datensatz: " + election.getName() + "</body></html>"),
                 BorderLayout.PAGE_START);
        // second line: the time passed
        this.add(this.timePassed, BorderLayout.CENTER);
        // third line: the abort button
        JButton buttonStop = new JButton("Stoppen");
        buttonStop.addActionListener(e -> algo.abortSimulationAlgo());
        this.add(buttonStop, BorderLayout.PAGE_END);
        // pack the dialog and then show it
        this.pack();
        this.setLocationRelativeTo(this.getOwner());
        this.setResizable(false);
        this.setVisible(true);
        // finally the thread to update the time passed
        new Thread(() -> {
            while(this.isVisible()) {
                long timePassed = (System.currentTimeMillis() - this.startTime) / 1000;
                long minutes = timePassed / 60;
                long seconds = timePassed % 60;
                /* updates the label with new time, since the start time */
                this.timePassed.setText(new StringBuilder(28).append("Verstrichene Zeit: ").append(minutes < 0 ? "0" : "").append(minutes)
                        .append(seconds < 10 ? ":0" : ":").append(seconds).append(" min").toString());
                try {
                    /* update only once every second */
                    Thread.sleep(1000);
                }
                catch(InterruptedException e) {
                }
            }
        }).start();
    }
    
    /**
     * starts the {@link NegativeRelativeVotingWeight} Algorithm for the given {@link Election}.<br>
     * First it shows a dialog with the time passed and the option to abort it, then it loads the the results into a new tab in the GUI.
     * 
     * @param election
     *            the {@link Election} in which to search for negative relative voting weight
     */
    static void show(Election election) {
        new NegativeRelativeVotingWeightAlgorithmDialog(election.deepClone()); // don't modify the one in the GUI
    }
}
