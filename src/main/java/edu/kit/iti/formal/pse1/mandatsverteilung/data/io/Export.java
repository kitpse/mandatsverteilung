/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.data.io;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.management.ElectionComparison;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.management.Session;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Constituency;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Election;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.ElectionArea;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.ElectionResult;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.FederalState;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Federation;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Party;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Settings;
import edu.kit.iti.formal.pse1.mandatsverteilung.propagation.PropagationMath;

/**
 * Class for reading data of elections (and associated classes) and the session and then creating their string representation and exporting them into .csv
 * files.
 * 
 * @author Nils Wilka
 * @version 1.6
 */
public class Export {
    
    /**
     * True if currently exporting, else false;
     */
    private static boolean busy = false;
    
    /**
     * The used separation symbol.
     */
    private static final String SEPARATOR = ";";
    
    /**
     * The line break.
     */
    private static final String EOL = "\n";
    
    /* Approximated sizes */
    private static final int ELECTION_COMPARISON_SIZE = Export.ELECTION_SIZE * 2;
    private static final int ELECTION_SIZE = Export.ELECTION_RESULT_SIZE + Export.ELECTION_SETTINGS_SIZE;
    private static final int ELECTION_RESULT_SIZE = Export.ELECTION_BODY_SIZE + Export.ELECTION_HEAD_SIZE;
    private static final int ELECTION_BODY_SIZE = (1 + Election.NUMBER_STATES + Election.NUMBER_CONSTITUENCIES) * Export.CHARS_PER_LINE;
    private static final int ELECTION_HEAD_SIZE = 5 * Export.CHARS_PER_LINE;
    private static final int ELECTION_SETTINGS_SIZE = Export.CHARS_PER_LINE;
    private static final int ELECTION_PROPAGATION_SIZE = Export.CHARS_PER_LINE;
    private static final int CHARS_PER_LINE = 400;
    
    /**
     * Private to avoid instantiation.
     */
    private Export() {
    }
    
    /**
     * Checks if this class is currently exporting.
     * 
     * @return True if this class is currently exporting, else false.
     */
    public static boolean isBusy() {
        return Export.busy;
    }
    
    /**
     * Reads the data of the current Session and exports it into a .csv file (located by {@code location}) with its information, including all elections and
     * election comparisons. Each election or election comparison is surrounded by brackets. Check if class is exporting with method {@code isBusy()}.
     * 
     * @param location
     *            The location to export the session to.
     * @throws IOException
     *             Thrown if an error occurs while writing file.
     */
    public static synchronized void exportSession(final String location) throws IOException {
        assert location != null;
        Export.busy = true;
        MemoryAccess.storeData(location, Export.sessionToString().toString());
        Export.busy = false;
    }
    
    /**
     * Reads the data of the given election and exports it into a .csv file (located by {@code location}) with its information, including all election results
     * and the settings. Check if class is exporting with method {@code isBusy()}.
     * 
     * @param election
     *            The given election to read and export.
     * @param location
     *            The location to export the election to.
     * @throws IOException
     *             Thrown if an error occurs while writing file.
     */
    public static synchronized void exportElection(final Election election, final String location) throws IOException {
        assert election != null;
        assert location != null;
        Export.busy = true;
        MemoryAccess.storeData(location, Export.electionToString(election).toString());
        Export.busy = false;
    }
    
    /**
     * Creates the string representation of the {@code Session}. In the first line is the identifier for the session, then its attributes (currently only the
     * auto save interval). The elections are appended after that and at last the election comparisons (each surrounded by brackets).
     * 
     * @return The string representation of the {@code Session}.
     */
    static StringBuilder sessionToString() {
        Session singleton = Session.getSession();
        StringBuilder string = new StringBuilder(singleton.getElectionCount() * Export.ELECTION_SIZE + singleton.getComparisonCount()
                                                 * Export.ELECTION_COMPARISON_SIZE);
        string.append(Format.SESSION_ID).append(Export.SEPARATOR).append(singleton.getAutoSaveInterval()).append(Export.EOL);
        
        for(Election election : singleton.getElections()) {
            string.append(Export.Format.SECTION_START).append(Export.EOL);
            string.append(Export.electionToString(election));
            string.append(Export.Format.SECTION_END).append(Export.EOL);
        }
        
        for(ElectionComparison comparison : singleton.getElectionComparisons()) {
            string.append(Export.Format.SECTION_START).append(Export.EOL);
            string.append(Export.electionComparisonToString(comparison));
            string.append(Export.Format.SECTION_END).append(Export.EOL);
        }
        return string;
    }
    
    /**
     * Creates the string representation for the given {@code ElectionComparison}.
     * 
     * @param comparison
     *            The {@code ElectionComparison} to create the string representation for.
     * @return The string representation of the {@code ElectionComparison}.
     */
    static StringBuilder electionComparisonToString(final ElectionComparison comparison) {
        StringBuilder string = new StringBuilder(Export.ELECTION_COMPARISON_SIZE);
        Export.appendValue(Format.ELECTION_COMPARISON_ID, string);
        string.append(comparison.getName()).append(Export.EOL);
        string.append(Export.electionToString(comparison.getFirstElection()));
        string.append(Export.Format.STOP).append(Export.EOL);
        string.append(Export.electionToString(comparison.getSecondElection()));
        return string;
    }
    
    /**
     * Creates the string representation of the {@code Election}.
     * 
     * @param election
     *            The {@code Election} to create the string representation for.
     * @return The string representation of the {@code Election}.
     */
    static StringBuilder electionToString(final Election election) {
        StringBuilder string = new StringBuilder(Export.ELECTION_SIZE);
        // line 1 head
        Export.appendValue(election.getName(), string);
        string.append(Export.propagationToString(election)).append(Export.EOL);
        // line 2 settings
        string.append(Export.settingsToString(election.getSettings()));
        // lines 2 - 5 election result head lines / 6 - x election result body
        string.append(Export.electionResultToString(election.getElectionResult()));
        return string;
    }
    
    /**
     * Creates the string representation for the {@code PropagationMath} of the given {@link Election}.
     * 
     * @param election
     *            The election, that has the {@code PropagationMath} to create the string representation for.
     * @return The string representation of the {@code PropagationMath}.
     */
    static StringBuilder propagationToString(Election election) {
        StringBuilder string = new StringBuilder(Export.ELECTION_PROPAGATION_SIZE);
        PropagationMath math = election.getElectionResult().getPropagationMath();
        string.append(math.getClass().getSimpleName()).append(Export.SEPARATOR);
        
        if(math.getParameters().isPresent()) {
            for(Object o : math.getParameters().get()) {
                string.append(o.toString()).append(Export.SEPARATOR);
            }
        }
        return string;
    }
    
    /**
     * Creates the string representation of the {@code Settings}.
     * 
     * @param election
     *            The {@code Settings} to create the string representation for.
     * @return The string representation of the {@code Settings}.
     */
    static StringBuilder settingsToString(final Settings settings) {
        StringBuilder string = new StringBuilder(Export.ELECTION_SETTINGS_SIZE);
        Export.appendValue(Format.ELECTION_SETTINGS_ID, string);
        Export.appendValue(Format.ELECTION_SETTINGS_SIZE, string);
        Export.appendValue(String.valueOf(settings.getParliamentSize()), string);
        Export.appendValue(Format.ELECTION_SETTINGS_HURDLE, string);
        Export.appendValue(String.valueOf(settings.getPercentHurdle()), string);
        Export.appendValue(Format.ELECTION_SETTINGS_CALCULATION_METHOD, string);
        Export.appendValue(settings.getCalculationMethod().getClass().getSimpleName(), string);
        Export.appendValue(Format.ELECTION_SETTINGS_NON_VOTER_PARTY, string);
        Export.appendValue(String.valueOf(settings.useNonVoterParty()), string);
        return string.append(Export.EOL);
    }
    
    /**
     * Creates the string representation for the given {@code ElectionResult}.
     * 
     * @param election
     *            The {@code ElectionResult} to create the string representation for.
     * @return The string representation of the {@code ElectionResult}.
     */
    static StringBuilder electionResultToString(final ElectionResult electionResult) {
        StringBuilder string = new StringBuilder(Export.ELECTION_RESULT_SIZE);
        Federation federation = electionResult.getFederation();
        final List<Party> parties = Export.getPartyList(federation); // To set an order.
        string.append(Export.createLine1(parties));
        string.append(Export.createLine2(parties.size()));
        string.append(Export.createLine3());
        string.append(Export.createBody(parties, federation));
        return string;
    }
    
    /**
     * Creates string representation for every {@code ElectionArea} in one line and appends them all for.
     * 
     * @see electionResultToString
     * 
     * @param parties
     *            The parties to get the values for.
     * @param federation
     *            The root {@code ElectionArea}.
     * @return The string representation for every {@code ElectionArea}.
     */
    private static StringBuilder createBody(final List<Party> parties, Federation federation) {
        StringBuilder body = new StringBuilder(Export.ELECTION_BODY_SIZE);
        body.append(Export.electionAreaDataToString(federation, Format.FEDERATION_ID, Format.FEDERATION_BELONGSTO_ID, parties));
        body.append(Export.EOL);
        
        int constituencyID = Format.CONSTITUENCY_FIRST_ID;
        int stateID = Format.STATE_FIRST_ID;
        for(FederalState state : federation.getChildren()) {
            body.append(Export.federalStateDataToString(state, stateID, String.valueOf(Format.FEDERATION_ID), parties));
            body.append(Export.EOL);
            
            for(Constituency constituency : state.getChildren()) {
                body.append(Export.electionAreaDataToString(constituency, constituencyID++, String.valueOf(stateID), parties));
                body.append(Export.EOL);
            }
            stateID++;
        }
        return body;
    }
    
    /**
     * Creates the 3. line, which is an empty line.
     * 
     * @see electionResultToString
     * 
     * @return The 3. line, a line break for the {@code ElectionResult}.
     */
    private static String createLine3() {
        return Export.EOL;
    }
    
    /**
     * Creates the 2. line, which appends empty cells and then the subline for each party.
     * 
     * @see electionResultToString
     * 
     * @param times
     *            The number of times to append the second line for the parties.
     * @return The 2. line for the {@code ElectionResult}.
     */
    private static StringBuilder createLine2(final int times) {
        StringBuilder subline = new StringBuilder();
        subline.append(Export.createSubLine(1, Format.SECOND_LINE)); // for invalid and valid votes
        subline.append(Export.createSubLine(times, Format.PARTY_SECOND_LINE));
        return subline.append(Export.EOL);
    }
    
    /**
     * Creates a line by repeatedly appending the given array.
     * 
     * @param times
     *            The number of times to repeat concatenation of given array.
     * @param head
     *            The values to append.
     * @return The created {@code StringBuilder} containing the given values.
     */
    private static StringBuilder createSubLine(final int times, final String[] head) {
        StringBuilder subline = new StringBuilder();
        for(int i = 0; i < times; i++) {
            for(String element : head) {
                subline.append(element).append(Export.SEPARATOR);
            }
        }
        return subline;
    }
    
    /**
     * Creates the 1. line, which appends {@code Format.FIRST_LINE} and then the string representation of each party.
     * 
     * @see electionResultToString
     * 
     * @param parties
     *            The parties to create the string representation for.
     * @return The 1. line for the {@code ElectionResult}.
     */
    private static StringBuilder createLine1(List<Party> parties) {
        StringBuilder line3 = new StringBuilder(Format.FIRST_LINE.length * 10 * (parties.size() + 1));
        for(String headElement : Format.FIRST_LINE) {
            line3.append(headElement).append(Export.SEPARATOR);
        }
        line3.append(Export.createParties(parties));
        line3.append(Export.EOL);
        return line3;
    }
    
    /**
     * Creates the string representation for each given {@code Party}.
     * 
     * @param parties
     *            The list of parties to create the string representation for.
     * @return The string representation for each given {@code Party}.
     */
    private static StringBuilder createParties(final List<Party> parties) {
        StringBuilder string = new StringBuilder(parties.size() * 10);
        for(Party party : parties) {
            String color = Integer.toString(party.getColor().getRGB(), 16);
            string.append(party.getName()).append(Export.SEPARATOR);
            string.append(party.getPoliticalOrientation()).append(Export.SEPARATOR);
            string.append(color).append(Export.SEPARATOR);
        }
        return string;
    }
    
    /**
     * Creates a list of parties to set an order.
     * 
     * @param federation
     *            The {@code ElectionArea} to get the parties from
     * @return A list of parties.
     */
    private static List<Party> getPartyList(Federation federation) {
        Set<Party> partiesSet = federation.getParties();
        List<Party> parties = new ArrayList<>(partiesSet.size());
        parties.addAll(partiesSet);
        return parties;
    }
    
    /**
     * Creates the string representation for the given {@code ElectionArea} and appends the party information at the end.
     * 
     * @param area
     *            The {@code ElectionArea} to create the string representation for.
     * @param id
     *            The ID for the {@code area}.
     * @param belongsTo
     *            The "belongs to"-ID for the {@code area}.
     * @param parties
     *            The parties to get and append information for.
     * @return The string representation of the {@code ElectionArea}.
     */
    private static StringBuilder electionAreaDataToString(final ElectionArea<?> area, final int id, final String belongsTo, List<Party> parties) {
        return new StringBuilder(Export.createElectionAreaData(area, id, belongsTo)).append(Export.createPartyInformation(area, parties));
    }
    
    /**
     * Creates the string representation for the given {@code FederalState} and appends the party information at the end.
     * 
     * @param state
     *            The {@code FederalState} to create the string representation for.
     * @param id
     *            The ID for the {@code state}.
     * @param belongsTo
     *            The "belongs to"-ID for the {@code state}.
     * @param parties
     *            The parties to get and append information for.
     * @return The string representation of the {@code FederalState}.
     */
    private static StringBuilder federalStateDataToString(final FederalState state, final int id, final String belongsTo, List<Party> parties) {
        return new StringBuilder().append(Export.createElectionAreaData(state, id, belongsTo)).append(Export.createPartyInformation(state, parties));
    }
    
    /**
     * Creates the string representation for the given {@code ElectionArea}, except parties.
     * 
     * @param area
     *            The {@code ElectionArea} to create the string representation for.
     * @param id
     *            The ID for the {@code area}.
     * @param belongsTo
     *            The "belongs to"-ID for the {@code area}.
     * @return The string representation of the {@code ElectionArea}.
     */
    private static StringBuilder createElectionAreaData(final ElectionArea<?> area, final int id, final String belongsTo) {
        StringBuilder string = new StringBuilder();
        Export.appendValue(String.valueOf(id), string);
        Export.appendValue(area.getName(), string);
        Export.appendValue(String.valueOf(belongsTo), string);
        Export.appendValue(String.valueOf(area.getCitizens()), string);
        Export.appendValue(String.valueOf(area.getNumberOfVotes()), string);
        Export.appendValue(String.valueOf(area.getEligibleVoters()), string);
        Export.appendValue(String.valueOf(area.getValidFirstVotes()), string);
        Export.appendValue(String.valueOf(area.getValidSecondVotes()), string);
        Export.appendValue(String.valueOf(area.getInvalidFirstVotes()), string);
        Export.appendValue(String.valueOf(area.getInvalidSecondVotes()), string);
        return string;
    }
    
    /**
     * Creates the string representation for each given {@code Party}.
     * 
     * @param state
     *            The {@code FederalState} to create the string representation for.
     * @param parties
     *            The parties to get and append information for.
     * @return The string representation of the {@code FederalState}.
     */
    private static StringBuilder createPartyInformation(final FederalState state, List<Party> parties) {
        StringBuilder string = new StringBuilder();
        for(Party party : parties) {
            string.append(state.getFirstVotes(party)).append(Export.SEPARATOR);
            string.append(state.getSecondVotes(party)).append(Export.SEPARATOR);
            string.append(state.getCandidateCount(party)).append(Export.SEPARATOR);
        }
        return string;
    }
    
    /**
     * Creates the string representation for each given {@code Party}.
     * 
     * @param area
     *            The {@code ElectionArea} to create the string representation for.
     * @param parties
     *            The parties to get and append information for.
     * @return The string representation of the {@code ElectionArea}.
     */
    private static StringBuilder createPartyInformation(final ElectionArea<?> area, List<Party> parties) {
        StringBuilder string = new StringBuilder();
        for(Party party : parties) {
            string.append(area.getFirstVotes(party)).append(Export.SEPARATOR);
            string.append(area.getSecondVotes(party)).append(Export.SEPARATOR);
            string.append("").append(Export.SEPARATOR);
        }
        return string;
    }
    
    /**
     * Appends a value to the given {@code StringBuilder} and appends a {@code Export.SEPARATOR} at the end.
     * 
     * @param value
     *            The value to append.
     * @param string
     *            The {@code StringBuilder} to append to.
     */
    private static void appendValue(final String value, StringBuilder string) {
        string.append(value).append(Export.SEPARATOR);
    }
    
    /**
     * This class encapsulates the static format strings and therefore specifies the format of the data (import and export).
     * 
     * @author Nils Wilka
     * @version 1.0
     */
    static class Format {
        
        /* Session */
        static final String[] SESSION_FIRSTLINE_REGEX = new String[]{Format.SESSION_ID, "[0-9]+"};
        
        /* Election Comparison */
        static final String[] ELECTION_COMPARISON_FIRSTLINE_REGEX = new String[]{Format.ELECTION_COMPARISON_ID, ".+"};
        
        /* IDS */
        static final String SESSION_ID = "Sitzung";
        static final String ELECTION_ID = "Wahl";
        static final String ELECTION_COMPARISON_ID = "Wahlvergleich";
        static final String ELECTION_SETTINGS_ID = "Einstellungen";
        static final String SECTION_START = "(";
        static final String SECTION_END = ")";
        static final String STOP = "-";
        static final int FEDERATION_ID = 0;
        static final String FEDERATION_BELONGSTO_ID = "";
        static final int STATE_FIRST_ID = 1;
        static final int STATE_LAST_ID = 16;
        static final int CONSTITUENCY_FIRST_ID = 17;
        
        /* Election */
        static final String[] ELECTION_FIRST_LINE_REGEX = new String[]{".+", "[0-9]+"};
        
        /* Settings */
        static final String[] ELECTION_SETTINGS_REGEX = {Format.ELECTION_SETTINGS_ID, Format.ELECTION_SETTINGS_SIZE, "[-]{0,1}[0-9]+",
                                                         Format.ELECTION_SETTINGS_HURDLE, "(\\d+\\.\\d+)", Format.ELECTION_SETTINGS_CALCULATION_METHOD, ".+",
                                                         Format.ELECTION_SETTINGS_NON_VOTER_PARTY, "true|false"};
        static final String ELECTION_SETTINGS_SIZE = "Groesse";
        static final String ELECTION_SETTINGS_HURDLE = "Prozenthuerde";
        static final String ELECTION_SETTINGS_CALCULATION_METHOD = "Sitzverteilungsberechnung";
        static final String ELECTION_SETTINGS_NON_VOTER_PARTY = "Partei der Nichtwaehler";
        
        /* Head */
        static final String[] FIRST_LINE = new String[]{"Nr", "Gebiet", "gehoert zu", "Buerger", "Waehler", "Wahlberechtigte", "Gueltige", "", "Ungueltige", ""};
        static final String[] SECOND_LINE = {"", "", "", "", "", "", Format.FIRST_VOTES, Format.SECOND_VOTES, Format.FIRST_VOTES, Format.SECOND_VOTES};
        
        /* Party */
        static final String[] PARTY_FIRST_LINE_REGEX = new String[]{".+", ".+", ".{0}"};
        static final String[] PARTY_SECOND_LINE = new String[]{Format.FIRST_VOTES, Format.SECOND_VOTES, Format.LANDESLISTE};
        
        static final String FIRST_VOTES = "Erststimmen";
        static final String SECOND_VOTES = "Zweitstimmen";
        static final String LANDESLISTE = "Landesliste";
    }
}
