/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.simulation;

import java.util.LinkedList;
import java.util.List;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Election;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.FederalState;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Party;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Propagation;
import edu.kit.iti.formal.pse1.mandatsverteilung.gui.controller.Mandatsverteilung;
import edu.kit.iti.formal.pse1.mandatsverteilung.propagation.PropagationMath;
import edu.kit.iti.formal.pse1.mandatsverteilung.propagation.ProportionalDistribution;

/**
 * This class implements the algorithm for finding occurrences of negative relative voting weights.
 * 
 * @author Alexander Grünen
 *
 */
public class NegativeRelativeVotingWeight {
    private boolean abortAlgo = false;
    
    /**
     * This method implements the algorithm for finding occurrences of negative relative voting weights. It uses a workaround since simply changing the second
     * votes for a party in a state oder constituency is no longer an option. That workaround slows the algorithm down considerably. It takes approx. 2 hours.
     * 
     * @param election
     * @return A list of occurrences of negative voting weights represented by @class NegativeRelativeVotingWeightElem.
     */
    public LinkedList<NegativeRelativeVotingWeightElement> findNegativeRelativeVotingWeights(Election election) {
        
        // set propagation to proportional propagation
        LinkedList<NegativeRelativeVotingWeightElement> list = new LinkedList<NegativeRelativeVotingWeightElement>();
        List<PropagationMath> methods = Propagation.getMethods(); // a list of all valid propagation methods
        PropagationMath propagationMethod = methods.get(0);
        election.getElectionResult().getPropagation().setPropagation(propagationMethod);
        if(!(propagationMethod instanceof ProportionalDistribution)) {
            throw new IllegalStateException("Setting propagation to proportional distribution failed!");
        }
        
        int orgVotes;
        int oldSize = election.getBundestag().getSize();
        int currentVotes;
        double orgFraction;
        double altFraction;
        
        // change = delta between two tested election outcomes for one party in one state
        // smaller change = better accuracy = more time to complete
        final int change = election.getElectionResult().getFederation().getValidSecondVotes() / election.getBundestag().getSize() / 1001 + 1;
        
        if(Mandatsverteilung.debug) {
            System.out.println("change=" + change);
        }
        
        for(FederalState state : election.getElectionResult().getFederation().getChildren()) {
            
            if(this.abortAlgo) {
                return list;
            }
            for(Party party : election.getBundestag().getParties()) {
                
                if(this.abortAlgo) {
                    return list;
                }
                
                orgVotes = state.getSecondVotes(party);
                orgFraction = election.getBundestag().getSeats(party) / oldSize;
                altFraction = orgFraction;
                currentVotes = state.getSecondVotes(party);
                
                if(orgVotes == 0) {
                    continue;
                }
                // search for more votes less representation:
                while(altFraction <= orgFraction) {
                    if(this.abortAlgo) {
                        return list;
                    }
                    currentVotes += change;
                    if(Mandatsverteilung.debug) {
                        System.out.println("state=" + state.getName() + " Party:" + party.getName() + " Votes: " + currentVotes);
                    }
                    state.changeSecondVotes(party, currentVotes);
                    altFraction = election.getBundestag().getSeats(party) / election.getBundestag().getSize();
                    
                    if(altFraction < orgFraction) {
                        if(Mandatsverteilung.debug) {
                            System.out.println("Occurence found!");
                        }
                        NegativeRelativeVotingWeightElement newEntry = new NegativeRelativeVotingWeightElement();
                        newEntry.setParty(party);
                        newEntry.setState(state);
                        newEntry.setVotesOld(orgVotes);
                        newEntry.setVotesNew(state.getSecondVotes(party) + 1);
                        newEntry.setBundestagSizeOld(oldSize);
                        newEntry.setBundestagSizeNew(election.getBundestag().getSize());
                        newEntry.setSeatsNew((int) election.getBundestag().getSeats(party));
                        state.changeSecondVotes(party, orgVotes);
                        if(orgVotes != state.getSecondVotes(party)) {
                            throw new IllegalStateException("Party not correctly set back to its original value!");
                        }
                        newEntry.setSeatsOld((int) election.getBundestag().getSeats(party));
                        list.add(newEntry);
                        break;
                    }
                }
                
                if(this.abortAlgo) {
                    return list;
                }
                
                state.changeSecondVotes(party, orgVotes);
                
                if(orgVotes != state.getSecondVotes(party)) {
                    System.out.println("original=" + orgVotes);
                    System.out.println("new=" + state.getSecondVotes(party));
                    throw new IllegalStateException("Party not correctly set back to its original value!");
                }
                
                // search for less votes more representation:
                
                altFraction = orgFraction;
                currentVotes = state.getSecondVotes(party);
                
                while(altFraction >= orgFraction) {
                    if(this.abortAlgo) {
                        return list;
                    }
                    
                    currentVotes -= change;
                    if(currentVotes <= 0) {
                        break;
                    }
                    if(Mandatsverteilung.debug) {
                        System.out.println("state=" + state.getName() + " Party:" + party.getName() + " Votes: " + currentVotes);
                    }
                    state.changeSecondVotes(party, currentVotes);
                    altFraction = election.getBundestag().getSeats(party) / election.getBundestag().getSize();
                    
                    if(altFraction > orgFraction) {
                        if(Mandatsverteilung.debug) {
                            System.out.println("Occurence found!");
                        }
                        NegativeRelativeVotingWeightElement newEntry = new NegativeRelativeVotingWeightElement();
                        newEntry.setParty(party);
                        newEntry.setState(state);
                        newEntry.setVotesOld(orgVotes);
                        newEntry.setVotesNew(state.getSecondVotes(party) - 1);
                        newEntry.setBundestagSizeOld(oldSize);
                        newEntry.setBundestagSizeNew(election.getBundestag().getSize());
                        newEntry.setSeatsNew((int) election.getBundestag().getSeats(party));
                        state.changeSecondVotes(party, orgVotes);
                        if(orgVotes != state.getSecondVotes(party)) {
                            throw new IllegalStateException("Party not correctly set back to its original value!");
                        }
                        newEntry.setSeatsOld((int) election.getBundestag().getSeats(party));
                        list.add(newEntry);
                        break;
                    }
                }
                
                if(this.abortAlgo) {
                    return list;
                }
                
                state.changeSecondVotes(party, orgVotes);
                
                if(orgVotes != state.getSecondVotes(party)) {
                    System.out.println("original=" + orgVotes);
                    System.out.println("new=" + state.getSecondVotes(party));
                    throw new IllegalStateException("Party not correctly set back to its original value!");
                }
                
            }
        }
        return list;
    }
    
    public void abortSimulationAlgo() {
        this.abortAlgo = true;
    }
}
