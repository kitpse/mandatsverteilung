/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.data.model;

import java.awt.Color;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.io.MemoryAccess;

/**
 * Represents a party.
 * 
 * @author unknown
 * @version 1.0
 */
public class Party implements DeepCloneable<Party> {
    
    /**
     * The default color values.
     */
    private static final Map<String, Color> DEFAULT_COLOR_MAP = new HashMap<>();
    
    /**
     * The default political orientation values.
     */
    private static final Map<String, Double> DEFAULT_ORIENTATION_MAP = new HashMap<>();
    
    /**
     * The name of the party.
     */
    private String name;
    
    /**
     * The color of the party to display in the GUI.
     */
    private Color color;
    
    /**
     * The political orientation of this party from -100 (total left) to 100 (total right).
     */
    private Double politicalOrientation;
    
    /**
     * Creates a new party.
     * 
     * @param name
     *            The name of the party.
     */
    public Party(String name) {
        this(name, Party.DEFAULT_COLOR_MAP.get(name.toUpperCase()), Party.DEFAULT_ORIENTATION_MAP.get(name.toUpperCase()));
    }
    
    /**
     * Creates a new party.
     * 
     * @param name
     *            The name of the party.
     * @param color
     *            The {@link Color} of the party.
     * @param politicalOrientation
     *            The political orientation of the party.
     */
    public Party(String name, Color color, Double politicalOrientation) {
        this.name = name.equals("Die Linke.") ? "DIE LINKE" : name;
        this.politicalOrientation = politicalOrientation == null ? 0 : politicalOrientation;
        this.color = color == null ? new Color((int) (Math.random() * Integer.MAX_VALUE)) : color;
    }
    
    /**
     * Gets the name of the party.
     * 
     * @return The name of the party.
     */
    public String getName() {
        return this.name;
    }
    
    /**
     * Sets the name of the party.
     * 
     * @param name
     *            The name of the party to set.
     */
    public void setName(String name) {
        this.name = name;
    }
    
    /**
     * Gets the political orientation of the party.
     * 
     * @return The political orientation of the party.
     */
    public Double getPoliticalOrientation() {
        return this.politicalOrientation;
    }
    
    /**
     * Sets the political orientation of the party.
     * 
     * @param politicalOrientation
     *            The political orientation of the party.
     */
    public void setPoliticalOrientation(Double politicalOrientation) {
        this.politicalOrientation = politicalOrientation;
    }
    
    /**
     * Gets the {@link Color} of the party.
     * 
     * @return The {@link Color} of the party.
     */
    public Color getColor() {
        return this.color;
    }
    
    /**
     * Sets the {@link Color} of the party.
     * 
     * @param color
     *            The {@link Color} of the party.
     */
    public void setColor(Color color) {
        this.color = color;
    }
    
    @Override
    public boolean equals(Object other) {
        if(!(other instanceof Party)) {
            return false;
        }
        return this.name.equalsIgnoreCase(((Party) other).name);
    }
    
    static {
        try {
            for(String line : MemoryAccess.loadInternalFileToList("partyToColorMap.txt")) {
                String[] parts = line.split(":");
                String name = parts[0].toUpperCase();
                Color color;
                if(parts[1].startsWith("#")) {
                    color = new Color(Integer.parseInt(parts[1].substring(1), 16));
                }
                else {
                    String[] colorParts = parts[1].split(",");
                    color = new Color(Integer.parseInt(colorParts[0]), Integer.parseInt(colorParts[1]), Integer.parseInt(colorParts[2]));
                }
                Party.DEFAULT_COLOR_MAP.put(name, color);
            }
        }
        catch(IOException e) {
        }
        try {
            for(String line : MemoryAccess.loadInternalFileToList("partyToPoliticalOrientationMap.txt")) {
                String[] parts = line.split(":");
                String name = parts[0].toUpperCase();
                Double value = Double.parseDouble(parts[1]);
                Party.DEFAULT_ORIENTATION_MAP.put(name, value);
            }
        }
        catch(IOException e) {
        }
    }
    
    @Override
    public int hashCode() {
        return this.name.toUpperCase().hashCode();
    }
    
    @Override
    public Party deepClone() {
        return new Party(this.name, this.color, this.getPoliticalOrientation());
    }
    
    @Override
    public String toString() {
        return this.name;
    }
}
