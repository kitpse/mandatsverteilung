/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.data.model;

/**
 * Interface for classes that can be cloned not only by reference, but deep.
 * 
 * @author Ben Wilhelm
 *
 * @param <T>
 *            The type of the cloned object. Should usually be the same type as the cloned object.
 */
public interface DeepCloneable<T> {
    /**
     * Returns a deep copy of this object. Associated Objects must implement the DeepCloneable Interface. For copying these associated objects, the deepClone()
     * method is used.
     * 
     * Listeners are NOT copied at all. The copy has no listeners.
     * 
     * @return a deep copy of this object.
     */
    public T deepClone();
}
