/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.simulation;

import java.util.LinkedList;
import java.util.List;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.FederalState;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Party;
import edu.kit.iti.formal.pse1.mandatsverteilung.gui.GuiHelper;

/**
 * This class stores the data of one entry in a list of negative voting weight occurrences.
 * 
 * @author Alexander Grünen
 */
public class NegativeRelativeVotingWeightElement {
    
    private Party party;
    private FederalState state;
    private int votesOld;
    private int votesNew;
    private int bundestagSizeOld;
    private int bundestagSizeNew;
    private int seatsOld;
    private int seatsNew;
    
    public NegativeRelativeVotingWeightElement() {
    }
    
    public Party getParty() {
        return this.party;
    }
    
    public FederalState getState() {
        return this.state;
    }
    
    public int getVotesOld() {
        return this.votesOld;
    }
    
    public int getVotesNew() {
        return this.votesNew;
    }
    
    public int getBundestagSizeOld() {
        return this.bundestagSizeOld;
    }
    
    public int getBundestagSizeNew() {
        return this.bundestagSizeNew;
    }
    
    public int getSeatsOld() {
        return this.seatsOld;
    }
    
    public int getSeatsNew() {
        return this.seatsNew;
    }
    
    public double getOldFraction() {
        return (double) this.seatsOld / this.bundestagSizeOld;
    }
    
    public double getNewFraction() {
        return (double) this.seatsNew / this.bundestagSizeNew;
    }
    
    public List<String> toParamStrings() {
        List<String> params = new LinkedList<>();
        params.add(this.getParty().getName());
        params.add(this.getState().getName());
        params.add(Integer.toString(this.getVotesOld()));
        params.add(Integer.toString(this.getVotesNew()));
        params.add(this.getSeatsOld() + "/" + this.getBundestagSizeOld());
        params.add(this.getSeatsNew() + "/" + this.getBundestagSizeNew());
        params.add(GuiHelper.doubleToPercentString(this.getOldFraction()) + "%");
        params.add(GuiHelper.doubleToPercentString(this.getNewFraction()) + "%");
        return params;
    }
    
    public void setParty(Party party) {
        this.party = party;
    }
    
    public void setState(FederalState state) {
        this.state = state;
    }
    
    public void setVotesOld(int votesOld) {
        this.votesOld = votesOld;
    }
    
    public void setVotesNew(int votesNew) {
        this.votesNew = votesNew;
    }
    
    public void setBundestagSizeOld(int bundestagSizeOld) {
        this.bundestagSizeOld = bundestagSizeOld;
    }
    
    public void setBundestagSizeNew(int bundestagSizeNew) {
        this.bundestagSizeNew = bundestagSizeNew;
    }
    
    public void setSeatsOld(int seatsOld) {
        this.seatsOld = seatsOld;
    }
    
    public void setSeatsNew(int seatsNew) {
        this.seatsNew = seatsNew;
    }
}
