/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.data.io;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Class for easier parsing of .csv files. A string representing a file, which will be split on line break, or the lines of a file directly, can be handed over
 * to this class. All cells will then be accessible from specified start to end line. The lines are split by {@code Import.SEPARATOR}.
 * 
 * @author Nils Wilka
 * @version 1.1
 */
class CSVParser {
    /**
     * This list contains all columns of the file.
     */
    private final List<List<String>> columns = new LinkedList<>();
    
    /**
     * The number of lines.
     */
    private int rowLength = 0;
    
    /**
     * Splits the given lines at every character {@code Import.SEPARATOR} and makes every cell accessible like a matrix.
     * 
     * @param lines
     *            The lines to be parsed.
     * @throws FormatException
     *             Not thrown.
     * @throws NullPointerException
     *             Thrown if lines are null.
     */
    public CSVParser(final String[] lines) throws FormatException, NullPointerException {
        this(lines, 0);
    }
    
    /**
     * Splits the given lines from start to end of the array at every character {@code Import.SEPARATOR} and makes every cell accessible like a matrix.
     * 
     * @param lines
     *            The lines to be parsed.
     * @param start
     *            The first line to parse starting with 0 or how many lines at the start get skipped.
     * @throws FormatException
     *             Thrown if not enough lines given for specified parsing area, more formally if {@code start > lines.length}.
     * @throws NullPointerException
     *             Thrown if lines are null.
     */
    public CSVParser(final String[] lines, int start) throws FormatException, NullPointerException {
        this(lines, start, lines.length);
    }
    
    /**
     * Splits the given lines from start to end at every character {@code Import.SEPARATOR} and makes every cell accessible like a matrix.
     * 
     * @param lines
     *            The lines to be parsed.
     * @param start
     *            The first line to parse starting with 0 or how many lines at the start get skipped.
     * @param end
     *            The last line not to parse anymore.
     * @throws FormatException
     *             Thrown if not enough lines given for specified parsing area, more formally if {@code lines.length < end - start}.
     * @throws NullPointerException
     *             Thrown if lines are null.
     */
    public CSVParser(final String[] lines, int start, int end) throws FormatException, NullPointerException {
        this.rowLength = end - start;
        if(end < start) {
            throw new FormatException("Expected end \"" + end + "\" to be greater than start \"" + lines.length + "\".");
        }
        else if(lines.length < this.getRowLength()) {
            throw new FormatException("Expected a row length of a minimum of \"" + this.getRowLength() + "\", but got \"" + lines.length + "\".");
        }
        
        for(int row = start; row < end; row++) {
            String[] line = lines[row].split(Import.SEPARATOR);
            
            int column = 0;
            for(; column < line.length; column++) {
                List<String> list;
                
                if(this.getColumnLength() - 1 < column) {
                    list = new ArrayList<>(this.getRowLength());
                    for(int i = start; i < row; i++) {
                        // fill in missed rows.
                        list.add("");
                    }
                    this.columns.add(list);
                }
                else {
                    list = this.columns.get(column);
                }
                list.add(line[column]);
            }
            // if line is too short
            for(; column < this.getColumnLength(); column++) {
                this.columns.get(column).add("");
            }
        }
    }
    
    /**
     * Splits the given data at line breaks and splits the lines from start to end at every character {@code Import.SEPARATOR} and makes every cell accessible
     * like a matrix.
     * 
     * @param data
     *            The lines to be split at line breaks and then parsed.
     * @throws FormatException
     *             Not thrown.
     * @throws NullPointerException
     *             Thrown if data is null.
     */
    public CSVParser(final String data) throws FormatException, NullPointerException {
        this(data.split("\n"));
    }
    
    /**
     * Splits the given data at line breaks splits the given lines from start to end of the array at every character {@code Import.SEPARATOR} and makes every
     * cell accessible like a matrix.
     * 
     * @param data
     *            The lines to be split at line breaks and then parsed.
     * @param start
     *            The first line to parse starting with 0 or how many lines at the start get skipped.
     * @throws FormatException
     *             Thrown if not enough lines given for specified parsing area, more formally if {@code start > lines.length}.
     * @throws NullPointerException
     *             Thrown if data is null.
     */
    public CSVParser(final String data, int start) throws FormatException, NullPointerException {
        this(data.split("\n"), start);
    }
    
    /**
     * Splits the given data at line breaks and splits the lines from start to end at every character {@code Import.SEPARATOR} and makes every cell accessible
     * like a matrix.
     * 
     * @param data
     *            The lines to be split at line breaks and then parsed.
     * @param start
     *            The first line to parse starting with 0 or how many lines at the start get skipped.
     * @param end
     *            The last line not to parse anymore.
     * @throws FormatException
     *             Thrown if not enough lines given for specified parsing area, more formally if {@code lines.length < end - start}.
     * @throws NullPointerException
     *             Thrown if data is null.
     */
    public CSVParser(final String data, int start, int end) throws FormatException, NullPointerException {
        this(data.split("\n"), start, end);
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder(10 * this.getColumnLength() * this.getRowLength());
        for(int i = 0; i < this.getRowLength(); i++) {
            Iterator<String> rowIterator = this.rowIterator(i);
            for(int j = 0; rowIterator.hasNext(); j++) {
                String element = rowIterator.next();
                if(j == 0) {
                    if(element.equals("") && this.isEmpty(i, j)) {
                        break;
                    }
                    else {
                        sb.append(element).append(";");
                    }
                }
                else {
                    if(element.equals("") && this.isEmpty(i, j)) {
                        if(sb.length() > 0) {
                            // remove last ;
                            sb.deleteCharAt(sb.length() - 1);
                        }
                        break;
                    }
                    sb.append(element).append(";");
                }
            }
            sb.append("\n");
        }
        return sb.toString();
    }
    
    /**
     * Checks if the row, selected by the given row, is empty.
     * 
     * @param row
     *            The row to check
     * @param start
     *            The column to start at.
     * @return true if row from specified start to end is empty, else false.
     */
    private boolean isEmpty(int row, int start) {
        Iterator<String> iterator = this.rowIterator(row, start);
        boolean empty = true;
        
        while(iterator.hasNext()) {
            if(!iterator.next().equals("")) {
                empty = false;
                break;
            }
        }
        return empty;
    }
    
    /**
     * Gets the number of lines/rows in this parser.
     * 
     * @return The number of lines/rows in this parser.
     */
    public int getRowLength() {
        return this.rowLength;
    }
    
    /**
     * Gets the number of columns in this parser.
     * 
     * @return The number of columns in this parser.
     */
    public int getColumnLength() {
        return this.columns.size();
    }
    
    /**
     * Gets the content of the cell in specified row and specified column.
     * 
     * @param row
     *            The row (starting with 0) to get the content from.
     * @param column
     *            The column (starting with 0) to get the content from.
     * @return The content of the cell in specified row and specified column.
     * @throws IndexOutOfBoundsException
     *             If at least one index is wrong.
     */
    public String get(int row, int column) throws IndexOutOfBoundsException {
        if(column < 0 || row < 0) {
            throw new IndexOutOfBoundsException("Expected (row/column) to be greater than 0, but got (" + row + "/" + column + ").");
            
        }
        else if(column >= this.getColumnLength() || row >= this.getRowLength()) {
            throw new IndexOutOfBoundsException("Expected (row/column) to be smaller than (" + this.getRowLength() + "/" + this.getColumnLength()
                                                + "), but got (" + row + "/" + column + ").");
        }
        return this.columns.get(column).get(row);
    }
    
    /**
     * Gets an iterator, that iterates over a column specified by the column number.
     * 
     * @param column
     *            The number of the column to iterate over.
     * @return An iterator, that iterates over a column specified by the column number.
     * @throws IndexOutOfBoundsException
     *             If at least one index is wrong.
     */
    public Iterator<String> columnIterator(int column) throws IndexOutOfBoundsException {
        return this.columnIterator(column, 0);
    }
    
    /**
     * Gets an iterator, that iterates over a column specified by the column number starting at specified row.
     * 
     * @param column
     *            The number of the column to iterate over.
     * @param start
     *            The number of the row to start at.
     * @return An iterator, that iterates over a column specified by the column number.
     * @throws IndexOutOfBoundsException
     *             If at least one index is wrong.
     */
    public Iterator<String> columnIterator(int column, int start) throws IndexOutOfBoundsException {
        if(column < 0 || column > this.getColumnLength() || start < 0 || start > this.getRowLength()) {
            throw new IndexOutOfBoundsException();
        }
        List<String> columnList = this.columns.get(column);
        if(start == 0) {
            return new lineIterator(columnList);
        }
        else {
            List<String> list = new ArrayList<>(this.getRowLength() - start);
            for(int i = start; i < this.getRowLength(); i++) {
                list.add(columnList.get(i));
            }
            return new lineIterator(list);
        }
    }
    
    /**
     * Gets an iterator, that iterates over a row specified by the row number.
     * 
     * @param row
     *            The number of the row to iterate over.
     * @return An iterator, that iterates over a row specified by the row number.
     * @throws IndexOutOfBoundsException
     *             If at least one index is wrong.
     */
    public Iterator<String> rowIterator(int row) throws IndexOutOfBoundsException {
        return this.rowIterator(row, 0);
    }
    
    /**
     * Gets an iterator, that iterates over a row specified by the row number starting at specified column.
     * 
     * @param row
     *            The number of the row to iterate over.
     * @param start
     *            The number of the column to start at.
     * @return An iterator, that iterates over a row specified by the row number.
     * @throws IndexOutOfBoundsException
     *             If at least one index is wrong.
     */
    public Iterator<String> rowIterator(int row, int start) throws IndexOutOfBoundsException {
        if(row < 0 || row > this.getRowLength() || start < 0 || start > this.getColumnLength()) {
            throw new IndexOutOfBoundsException();
        }
        List<String> list = new ArrayList<>(this.getColumnLength() - start);
        for(int i = start; i < this.getColumnLength(); i++) {
            list.add(this.columns.get(i).get(row));
        }
        return new lineIterator(list);
    }
    
    /**
     * Iterator to iterate over specified line (either row or column).
     * 
     * @author Nils Wilka
     * @version 1.0
     */
    private class lineIterator implements Iterator<String> {
        
        /**
         * The line to iterate over.
         */
        final List<String> line;
        
        /**
         * Marks next element to get.
         */
        int index = 0;
        
        /**
         * Creates a new line iterator with given line to iterate over.
         * 
         * @param line
         *            The line to iterate over.
         * @throws NullPointerException
         *             Thrown if line is null.
         */
        public lineIterator(final List<String> line) throws NullPointerException {
            if(line == null) {
                throw new NullPointerException();
            }
            else {
                this.line = line;
            }
        }
        
        @Override
        public boolean hasNext() {
            return this.index < this.line.size();
        }
        
        @Override
        public String next() throws NoSuchElementException {
            if(this.hasNext()) {
                return this.line.get(this.index++);
            }
            else {
                throw new NoSuchElementException("No more elements.");
            }
        }
    }
}
