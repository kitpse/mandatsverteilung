/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.data.io;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Constituency;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Election;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.ElectionArea;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.ElectionResult;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.FederalState;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Party;

/**
 * Class for parsing and setting information of {@link ElectionResult}s, which have been exported by the program before.
 * 
 * @author Nils Wilka
 * @version 1.0
 */
class ReimportParser extends ElectionResultParser {
    
    /**
     * The column containing the ID for the {@link ElectionArea}.
     */
    private static final int POS_ID = 0;
    
    /**
     * The column containing the name for the {@link ElectionArea}.
     */
    private static final int POS_NAME = 1;
    
    /**
     * The column containing the "belongs to"-ID for the {@link ElectionArea}.
     */
    private static final int POS_BELONGSTO = 2;
    
    /**
     * The column containing the number of citizens for the {@link ElectionArea}.
     */
    private static final int POS_CITIZENS = 3;
    
    /**
     * The column containing the number of voters for the {@link ElectionArea}.
     */
    private static final int POS_VOTERS = 4;
    
    /**
     * The column containing the eligible voters for the {@link ElectionArea}.
     */
    private static final int POS_ELIGIBLE_VOTERS = 5;
    
    /**
     * The column containing the first {@link Party} for the {@link ElectionArea}.
     */
    private static final int POS_FIRST_PARTY = 10;
    
    /**
     * The expected head of an {@link ElectionResult}.
     */
    private static final String HEAD = "Nr;Gebiet;gehoert zu;Buerger;Waehler;Wahlberechtigte;Gueltige;;Ungueltige;;\n"
                                       + ";;;;;;Erststimmen;Zweitstimmen;Erststimmen;Zweitstimmen;\n" + ";;;;;;;;;;\n";
    
    /**
     * The expected head for the parties ({@link Party}).
     */
    private static final String PARTY_HEAD_REGEX = ".+;.+;.+;\n" + "Erststimmen;Zweitstimmen;Landesliste;\n" + ";;;";
    
    /**
     * Used to parse {@code HEAD}.
     */
    private static CSVParser HEAD_FORMAT;
    
    /**
     * Used to parse {@code PARTY_HEAD_REGEX}.
     */
    private static CSVParser PARTY_HEAD_FORMAT;
    
    /**
     * The number of columns to next party.
     */
    private static final int STEP = 3;
    
    /**
     * The {@link ElectionResult} to be set and returned.
     */
    private ElectionResult result;
    
    /**
     * The parties ({@link Party}) for the {@link ElectionResult}.
     */
    private Optional<List<Party>> parties;
    
    /**
     * Maps the IDs of the file to their line index.
     */
    private Map<Integer, Integer> idToLineIndex;
    
    /**
     * The row where an exception occurred.
     */
    private Optional<Integer> currentRow;
    
    /**
     * The column where an exception occurred.
     */
    private Optional<Integer> currentColumn;
    
    static {
        try {
            ReimportParser.HEAD_FORMAT = new CSVParser(ReimportParser.HEAD);
            ReimportParser.PARTY_HEAD_FORMAT = new CSVParser(ReimportParser.PARTY_HEAD_REGEX);
        }
        catch(FormatException e) {
            // never happens. 
            e.printStackTrace();
        }
    }
    
    /**
     * Creates a new ReimportParser which is used to parse given data from specified start line to end of the file and return an {@link ElectionResult}.
     * 
     * @param lines
     *            The lines to parse.
     * @param start
     *            The first row to read.
     * @throws NullPointerException
     *             Thrown if lines are null.
     * @throws FormatException
     *             Thrown if given lines do not fulfill correct format. E.g. if not enough lines are given.
     */
    public ReimportParser(final String[] lines, int start) throws NullPointerException, FormatException {
        super(lines, start);
    }
    
    /**
     * Creates a new ReimportParser which is used to parse given data from first line to end of the file and return an {@link ElectionResult}.
     * 
     * @param lines
     *            The lines to parse.
     * @throws NullPointerException
     *             Thrown if lines are null.
     * @throws FormatException
     *             Thrown if given lines do not fulfill correct format. E.g. if not enough lines are given.
     */
    public ReimportParser(final String[] lines) throws NullPointerException, FormatException {
        super(lines);
    }
    
    /**
     * Creates a new ReimportParser which is used to parse given data from first line to end of the file and return an {@link ElectionResult}.
     * 
     * @param data
     *            The data to split at line breaks and to parse.
     * @throws NullPointerException
     *             Thrown if data is null.
     * @throws FormatException
     *             Thrown if given data does not fulfill correct format. E.g. if not enough lines are contained.
     */
    public ReimportParser(final String data) throws NullPointerException, FormatException {
        super(data);
    }
    
    /**
     * Creates a new ReimportParser which is used to parse given data from specified start line to end of the file and return an {@link ElectionResult}.
     * 
     * @param data
     *            The data to split at line breaks and to parse.
     * @param start
     *            The first row to read.
     * @throws NullPointerException
     *             Thrown if data is null.
     * @throws FormatException
     *             Thrown if given data does not fulfill correct format. E.g. if not enough lines are contained.
     */
    public ReimportParser(final String data, int start) throws NullPointerException, FormatException {
        super(data, start);
    }
    
    @Override
    public ElectionResult parseElectionResult() throws FormatException {
        this.parseFile(false);
        return this.result;
    }
    
    /**
     * Parses the file of the {@link ElectionResult}.
     * 
     * @param eligibleVotersOnly
     *            Determines if only the eligible voters shall be parsed.
     * @throws FormatException
     *             Thrown if format of data is unexpected.
     */
    private void parseFile(final boolean eligibleVotersOnly) throws FormatException {
        this.initialize();
        assert this.result.getFederation().getChildren().size() == Election.NUMBER_STATES;
        this.parseColumnID();
        this.parseHeadColumns();
        if(!eligibleVotersOnly) {
            this.parsePartyHeadColumns();
            this.parseParties();
        }
        this.parseBodyValues(eligibleVotersOnly);
    }
    
    /**
     * Sets all data structures.
     */
    private void initialize() {
        this.result = new ElectionResult();
        this.idToLineIndex = new HashMap<>();
        this.parties = Optional.empty();
        this.currentRow = Optional.empty();
        this.currentColumn = Optional.empty();
        this.removeConstituencies();
    }
    
    /**
     * Removes the constituencies of every {@link FederalState}.
     */
    private void removeConstituencies() {
        this.result.getFederation().getChildren().forEach(state -> new ArrayList<>(state.getChildren()).forEach(state::removeChild));
    }
    
    /**
     * Parses the ID column and maps IDs to line indices.
     * 
     * @throws FormatException
     *             Thrown if format of data is unexpected.
     */
    private void parseColumnID() throws FormatException {
        Iterator<String> iterator = this.parser.columnIterator(ReimportParser.POS_ID, 3);
        
        for(int lineIndex = ElectionResultParser.POS_BODY_START; iterator.hasNext(); lineIndex++) {
            String stringID = iterator.next();
            
            try {
                this.checkValidID(stringID);
            }
            catch(FormatException fe) {
                this.currentRow = Optional.of(lineIndex);
                this.currentColumn = Optional.of(ReimportParser.POS_ID);
                throw new FormatException(this.buildPosition(), fe);
            }
            Integer identifier = Integer.parseInt(stringID);
            
            if(this.idToLineIndex.containsKey(identifier)) {
                this.currentRow = Optional.of(lineIndex);
                this.currentColumn = Optional.of(ReimportParser.POS_ID);
                throw new FormatException("Expected to find only one election area with ID of \"" + Export.Format.FEDERATION_ID + "\", but got it twice."
                                          + this.buildPosition());
            }
            else {
                this.idToLineIndex.put(identifier, lineIndex);
            }
        }
        Integer[] ids = this.idToLineIndex.keySet().toArray(new Integer[0]);
        Arrays.sort(ids);
        for(int i = Export.Format.FEDERATION_ID; i < ids.length; i++) {
            if(!ids[i].equals(i)) {
                this.currentRow = Optional.of(this.idToLineIndex.get(ids[i]));
                this.currentColumn = Optional.of(ReimportParser.POS_ID);
                throw new FormatException("Expected to find ID \"" + i + "\", but got \"" + ids[i] + "\"." + this.buildPosition());
            }
        }
    }
    
    /**
     * Parses the head columns.
     * 
     * @throws FormatException
     *             Thrown if format of data is unexpected.
     */
    private void parseHeadColumns() throws FormatException {
        for(int i = 0; i < ReimportParser.HEAD_FORMAT.getRowLength(); i++) {
            
            for(int j = 0; j < ReimportParser.HEAD_FORMAT.getColumnLength(); j++) {
                String expected = ReimportParser.HEAD_FORMAT.get(i, j);
                String current = this.parser.get(i, j);
                if(!current.matches(expected)) {
                    throw new FormatException("Expected to find \"" + expected + "\", but got \"" + current + "\".", i, j);
                }
            }
        }
    }
    
    /**
     * Parses the {@link Party} head columns.
     * 
     * @throws FormatException
     *             Thrown if format of data is unexpected.
     */
    private void parsePartyHeadColumns() throws FormatException {
        if(this.parser.getColumnLength() > this.minNumberOfColumns()) {
            
            if((this.parser.getColumnLength() - this.minNumberOfColumns()) % ReimportParser.STEP == 0) {
                
                // for all parties
                for(int k = ReimportParser.POS_FIRST_PARTY; k < this.parser.getColumnLength(); k += ReimportParser.STEP) {
                    
                    // parse head "matrix"
                    for(int i = 0; i < ReimportParser.PARTY_HEAD_FORMAT.getRowLength(); i++) {
                        
                        for(int j = 0; j < ReimportParser.PARTY_HEAD_FORMAT.getColumnLength(); j++) {
                            String expected = ReimportParser.PARTY_HEAD_FORMAT.get(i, j);
                            String current = this.parser.get(i, j + k);
                            if(!current.matches(expected)) {
                                throw new FormatException("Expected to find \"" + expected + "\", but got \"" + current + "\"", i, j + k);
                            }
                        }
                    }
                }
            }
            else {
                this.currentRow = Optional.of(ElectionResultParser.POS_HEAD_INFOLINE);
                throw new FormatException("Expected \"" + ReimportParser.STEP + "\" columns per Party, but got \""
                                          + (this.parser.getColumnLength() - this.minNumberOfColumns()) + "\" lines." + this.buildPosition());
            }
        }
    }
    
    /**
     * Parses the parties ({@link Party}).
     * 
     * @throws FormatException
     *             Thrown if format of data is unexpected.
     */
    private void parseParties() throws FormatException {
        if(this.parser.getColumnLength() > this.minNumberOfColumns()) {
            this.parties = Optional.of(new ArrayList<>((this.parser.getColumnLength() - this.minNumberOfColumns()) / ReimportParser.STEP));
            
            for(int columnIndex = ReimportParser.POS_FIRST_PARTY; columnIndex < this.parser.getColumnLength(); columnIndex += ReimportParser.STEP) {
                Iterator<String> iterator = this.parser.rowIterator(ElectionResultParser.POS_HEAD_INFOLINE, columnIndex);
                String name = iterator.next();
                String orientationString = iterator.next();
                String colorString = iterator.next();
                Color color;
                Double orientation;
                
                if(colorString.equals("")) {
                    color = null;
                }
                else {
                    try {
                        color = this.parseColor(colorString);
                    }
                    catch(FormatException fe) {
                        this.currentRow = Optional.of(ElectionResultParser.POS_HEAD_INFOLINE);
                        this.currentColumn = Optional.of(columnIndex + 2);
                        throw new FormatException(this.buildPosition(), fe);
                    }
                }
                try {
                    orientation = this.checkDouble(orientationString);;
                }
                catch(FormatException fe) {
                    this.currentRow = Optional.of(ElectionResultParser.POS_HEAD_INFOLINE);
                    this.currentColumn = Optional.of(columnIndex + 1);
                    throw new FormatException(" As political orientation for party." + this.buildPosition(), fe);
                }
                this.parties.get().add(new Party(name, color, orientation));
            }
        }
    }
    
    /**
     * Parses the color for a party ({@link Color}).
     * 
     * @param string
     *            The string containing the color and to be parsed.
     * @throws FormatException
     *             Thrown if format of data is unexpected.
     */
    private Color parseColor(String string) throws FormatException {
        try {
            return new Color(Integer.parseInt(string, 16));
        }
        catch(NumberFormatException nfe) {
            throw new FormatException("Expected hex color for party, but got \"" + string + "\".");
        }
    }
    
    /**
     * Parses the body values.
     * 
     * @param eligibleVotersOnly
     *            Determines if only the eligible voters shall be parsed.
     * @throws FormatException
     *             Thrown if format of data is unexpected.
     */
    private void parseBodyValues(final boolean eligibleVotersOnly) throws FormatException {
        int lineIndex = this.idToLineIndex.get(Export.Format.FEDERATION_ID);
        String name = this.parser.get(lineIndex, ReimportParser.POS_NAME);
        this.result.getFederation().setName(name);
        String belongsTo = this.parser.get(lineIndex, ReimportParser.POS_BELONGSTO);
        
        if(!belongsTo.equals(String.valueOf(Export.Format.FEDERATION_BELONGSTO_ID))) {
            this.currentRow = Optional.of(lineIndex);
            this.currentColumn = Optional.of(ReimportParser.POS_BELONGSTO);
            throw new FormatException("Expected to find \"" + Export.Format.FEDERATION_BELONGSTO_ID + "\" as \"belongs to\"-ID for federation, but got \""
                                      + belongsTo + "\"." + this.buildPosition());
        }
        List<FederalState> states = this.result.getFederation().getChildren();
        for(int i = Export.Format.STATE_FIRST_ID; i <= Export.Format.STATE_LAST_ID; i++) {
            // state head
            lineIndex = this.idToLineIndex.get(i);
            name = this.parser.get(lineIndex, ReimportParser.POS_NAME);
            states.get(i - Export.Format.STATE_FIRST_ID).setName(name);
            belongsTo = this.parser.get(lineIndex, ReimportParser.POS_BELONGSTO);
            
            if(!belongsTo.equals(String.valueOf(Export.Format.FEDERATION_ID))) {
                this.currentRow = Optional.of(lineIndex);
                this.currentColumn = Optional.of(ReimportParser.POS_BELONGSTO);
                throw new FormatException("Expected to find \"" + Export.Format.FEDERATION_ID + "\" as \"belongs to\"-ID for federal state, but got \""
                                          + belongsTo + "\"." + this.buildPosition());
            }
        }
        for(int i = Export.Format.CONSTITUENCY_FIRST_ID; i < this.idToLineIndex.size(); i++) {
            // constituencies
            lineIndex = this.idToLineIndex.get(i);
            name = this.parser.get(lineIndex, ReimportParser.POS_NAME);
            belongsTo = this.parser.get(lineIndex, ReimportParser.POS_BELONGSTO);
            int belongsToNumber = this.checkInteger(belongsTo);
            
            if(!(belongsToNumber >= Export.Format.STATE_FIRST_ID && belongsToNumber <= Export.Format.STATE_LAST_ID)) {
                this.currentRow = Optional.of(lineIndex);
                this.currentColumn = Optional.of(ReimportParser.POS_BELONGSTO);
                throw new FormatException("Expected to find value between \"" + Export.Format.STATE_FIRST_ID + "\" and \"" + Export.Format.STATE_LAST_ID
                                          + "\" as \"belongs to\"-ID for constituency, but got \"" + belongsTo + "\"." + this.buildPosition());
            }
            Constituency constituency = new Constituency(name, this.result);
            // add constituency to state
            states.get(belongsToNumber - Export.Format.STATE_FIRST_ID).addChild(constituency);
            this.setConstituencyValues(constituency, lineIndex, eligibleVotersOnly);
            if(!eligibleVotersOnly) {
                this.parsePartiesForConstituency(constituency, lineIndex);
            }
        }
        
        for(int i = Export.Format.STATE_FIRST_ID; i <= Export.Format.STATE_LAST_ID; i++) {
            // state values (after children were set).
            lineIndex = this.idToLineIndex.get(i);
            
            if(!eligibleVotersOnly) {
                this.parseCandidateCount(states.get(i - Export.Format.STATE_FIRST_ID), lineIndex);
            }
        }
    }
    
    /**
     * Parses the body values.
     * 
     * @param row
     *            The row of the {@code constituency}.
     * @param constituency
     *            The {@link Constituency} to set the values ffor.
     * @param eligibleVotersOnly
     *            Determines if only the eligible voters shall be parsed.
     * @throws FormatException
     *             Thrown if format of data is unexpected.
     */
    private void setConstituencyValues(final Constituency constituency, int row, final boolean eligibleVotersOnly) throws FormatException {
        int number = this.checkInteger(this.parser.get(row, ReimportParser.POS_ELIGIBLE_VOTERS));
        if(!constituency.setEligibleVoters(number)) {
            throw new FormatException("Expected valid number for eligible voters, but got \"" + number + "\".");
        }
        
        if(!eligibleVotersOnly) {
            number = this.checkInteger(this.parser.get(row, ReimportParser.POS_VOTERS));
            if(!constituency.setNumberOfVotes(number)) {
                throw new FormatException("Expected valid number for voters, but got \"" + number + "\".");
            }
            number = this.checkInteger(this.parser.get(row, ReimportParser.POS_CITIZENS));
            if(!constituency.setCitizens(number)) {
                throw new FormatException("Expected valid number for citizens, but got \"" + number + "\".");
            }
        }
    }
    
    /**
     * Checks if a given string matches "[-]{0,1}[0-9]+" and parses it.
     * 
     * @param current
     *            The string to check for.
     * @return The string converted to integer.
     * @throws FormatException
     *             Thrown if string does not match "[-]{0,1}[0-9]+".
     */
    private int checkInteger(String current) throws FormatException {
        if(!current.matches("[-]{0,1}[0-9]+")) {
            throw new FormatException("Expected integer, but got \"" + current + "\".");
        }
        else {
            return Integer.parseInt(current);
        }
    }
    
    /**
     * Checks if a given string matches "[-]{0,1}(\\d+\\.\\d+)" and parses it.
     * 
     * @param current
     *            The string to check for.
     * @return The string converted to double.
     * @throws FormatException
     *             Thrown if string does not match "[-]{0,1}(\\d+\\.\\d+)".
     */
    private double checkDouble(String current) throws FormatException {
        if(!current.matches("[-]{0,1}(\\d+\\.\\d+)")) {
            throw new FormatException("Expected float, but got \"" + current + "\".");
        }
        else {
            return Double.parseDouble(current);
        }
    }
    
    /**
     * Parses the parties for the {@link Constituency}.
     * 
     * @param lineIndex
     *            The row of the {@code constituency}.
     * @param constituency
     *            The {@link Constituency} to parse the parties for.
     * @throws FormatException
     *             Thrown if format is wrong.
     */
    private void parsePartiesForConstituency(final Constituency constituency, int lineIndex) throws FormatException {
        for(int k = ReimportParser.POS_FIRST_PARTY, index = 0; k < this.parser.getColumnLength(); k += ReimportParser.STEP, index++) {
            try {
                this.setConstituencyVotes(constituency, this.parties.get().get(index), lineIndex, k);
            }
            catch(FormatException fe) {
                this.currentRow = Optional.of(lineIndex);
                this.currentColumn = Optional.of(k);
                throw new FormatException(" For constituency." + this.buildPosition(), fe);
            }
        }
    }
    
    /**
     * Sets the {@link Constituency} votes for given {@link Party}.
     * 
     * @param constituency
     *            The {@link Constituency} to set the votes for.
     * @param party
     *            The {@link Party} to set the votes for.
     * @param row
     *            The row of the {@code constituency}.
     * @param column
     *            The column of the {@code constituency}.
     * @throws FormatException
     *             Thrown if format is wrong.
     */
    private void setConstituencyVotes(final Constituency constituency, final Party party, int row, int column) throws FormatException {
        // assert 'column' is correct column number to 'party'
        int firstVote = this.checkInteger(this.parser.get(row, column));
        int secondVote = this.checkInteger(this.parser.get(row, column + 1));
        
        String current = this.parser.get(row, column + 2);
        if(!current.equals("")) {
            this.currentRow = Optional.of(row);
            this.currentColumn = Optional.of(column + 2);
            throw new FormatException("Expected empty cell for constituency, but got \"" + current + "\".");
        }
        
        if(firstVote != 0 || secondVote != 0) {
            constituency.addParty(party);
            if(firstVote != 0) {
                if(!constituency.setFirstVotes(party, firstVote)) {
                    throw new FormatException("Expected valid first votes for party \"" + party.getName() + "\", but got \"" + firstVote + "\".");
                }
            }
            if(secondVote != 0) {
                if(!constituency.setSecondVotes(party, secondVote)) {
                    throw new FormatException("Expected valid second votes for party \"" + party.getName() + "\", but got \"" + secondVote + "\".");
                }
            }
        }
    }
    
    private void parseCandidateCount(final FederalState state, int lineIndex) throws FormatException {
        for(int k = ReimportParser.POS_FIRST_PARTY, index = 0; k < this.parser.getColumnLength(); k += ReimportParser.STEP, index++) {
            try {
                int number = this.checkInteger(this.parser.get(lineIndex, k + 2));
                
                if(!state.setCandidateCount(this.parties.get().get(index), number)) {
                    throw new FormatException("Expected valid candidate count for federal state\"" + state.getName() + "\", but got \"" + number + "\".");
                }
            }
            catch(FormatException fe) {
                this.currentRow = Optional.of(lineIndex);
                this.currentColumn = Optional.of(k);
                throw new FormatException(this.buildPosition(), fe);
            }
        }
    }
    
    /**
     * Checks if given {@code id} is valid.
     * 
     * @param id
     *            The ID to check for.
     * @throws FormatException
     *             Thrown if {@code id} does not match {@link ElectionResultParser.ID_REGEX}
     */
    private void checkValidID(String id) throws FormatException {
        if(!id.matches(ElectionResultParser.ID_REGEX)) {
            throw new FormatException("Expected ID of format \"" + ElectionResultParser.ID_REGEX + "\", but got \"" + id + "\".");
        }
    }
    
    /**
     * Depending on which value is set, build a string representation of error location.
     * 
     * @return String representation of error location.
     */
    private String buildPosition() {
        String string = "";
        
        if(this.currentRow.isPresent() && this.currentColumn.isPresent()) {
            string = " At position (" + this.currentRow.get() + "," + this.currentColumn.get() + ")";
        }
        else if(this.currentRow.isPresent()) {
            string = " In row " + this.currentRow.get();
        }
        else if(this.currentColumn.isPresent()) {
            string = " In column " + this.currentColumn.get();
            
        }
        return string;
    }
    
    @Override
    public ElectionResult parseEligibleVoters() throws FormatException {
        this.parseFile(true);
        return this.result;
    }
}
