/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.gui;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

/**
 * A {@code KeyPressedListener} is the same as an {@link KeyListener}, but with an default implementation of {@link #keyReleased(KeyEvent)} and
 * {@link #keyTyped(KeyEvent)}. Therefore {@link #keyPressed(KeyEvent)} is the only unimplemented method which makes this class into an
 * {@link FunctionalInterface} which means it can be used in lambdas.
 * 
 * @author Felix Heim
 * @version 1
 */
@FunctionalInterface
public interface KeyPressedListener extends KeyListener {
    @Override
    default void keyReleased(KeyEvent e) {
    }
    
    @Override
    default void keyTyped(KeyEvent e) {
    }
}
