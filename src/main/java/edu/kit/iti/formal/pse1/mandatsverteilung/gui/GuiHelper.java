/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.gui;

import java.awt.Component;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.util.Optional;
import java.util.function.Consumer;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import edu.kit.iti.formal.pse1.mandatsverteilung.gui.controller.Mandatsverteilung;

/**
 * The {@code GuiHelper} has a collection of useful methods to work and create gui elements.
 * 
 * @author Felix Heim
 * @version 9
 */
public class GuiHelper {
    private GuiHelper() {
    }
    
    /**
     * Creates a {@link JScrollPane} on top of the given {@link Component}. The {@link JScrollPane} will never show a horizontal scroll bar and always a
     * vertical scroll bar.<br>
     * Also the scrolling speed is in increased to be more comfortable.
     * 
     * @param view
     *            the pane to be displayed in {@link JScrollPane}
     * @return the newly created {@link JScrollPane}
     */
    public static JScrollPane createScrollPane(Component view) {
        JScrollPane scrollPane = new JScrollPane(view, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.getVerticalScrollBar().setUnitIncrement(10);
        return scrollPane;
    }
    
    /**
     * Converts a double value into a {@link String} representing a percent value with two decimal places.
     * 
     * @param value
     *            the value to be converted to the {@link String}
     * @return the formatted {@link String} (e.g. 0.99845 converts to 99.85)
     */
    public static String doubleToPercentString(double value) {
        return Double.toString(Math.round(value * 10000) / 100.0);
    }
    
    /**
     * Converts a {@link String} which represents a percent {@link String} into double value if possible.
     * 
     * @param percentString
     *            the {@link String} to be converted
     * @return the {@link Optional} double representing the percent (100% converts to 1)
     */
    public static Optional<Double> percentStringToDouble(String percentString) {
        String trimmed = percentString.trim();
        if(trimmed.endsWith("%")) {
            trimmed = trimmed.substring(0, trimmed.length() - 1).trim();
        }
        try {
            return Optional.of(Double.parseDouble(trimmed) / 100.0);
        }
        catch(NumberFormatException e) {
            return Optional.empty();
        }
    }
    
    /**
     * Shows an error to the user using a {@link JOptionPane}.
     * 
     * @param message
     *            the error message to be displayed to the user
     */
    public static void showError(String message) {
        JOptionPane.showMessageDialog(Mandatsverteilung.getMainWindow(), message, "Fehler!", JOptionPane.ERROR_MESSAGE);
    }
    
    /**
     * Shows an error to the user using a {@link JOptionPane}.
     * 
     * @param message
     *            the info message to be displayed to the user
     * @param title
     *            the title to show in the info dialog
     */
    public static void showInfo(String message, String title) {
        JOptionPane.showMessageDialog(Mandatsverteilung.getMainWindow(), message, title, JOptionPane.INFORMATION_MESSAGE);
    }
    
    /**
     * checks if the given {@link Optional} double value is between 0 and 100%
     * 
     * @param value
     *            the {@link Optional} value to check
     * @return if the value is in range of 0 and 100%
     */
    public static boolean validatePercent(Optional<Double> value) {
        return value.isPresent() && value.get() >= 0 && value.get() <= 1;
    }
    
    /**
     * Adds an validator to an {@link JTextField}
     * 
     * @param parent
     *            the parent to move the focus to when validating
     * @param textField
     *            the {@link JTextField} to which the validator should be added
     * @param validator
     *            the actual method validating the {@link JTextField}
     */
    public static void addValidator(JComponent parent, JTextField textField, Consumer<JTextField> validator) {
        GuiHelper.addKeyPressedListener(textField, e -> {
            if(e.getKeyCode() == KeyEvent.VK_ENTER) {
                parent.requestFocusInWindow();
            }
        });
        GuiHelper.addFocusLostListener(textField, e -> validator.accept(textField));
    }
    
    /**
     * adds a {@link KeyPressedListener} as a {@link KeyListener} to the given {@link JTextField}
     * 
     * @param textField
     *            the {@link JTextField} the listener should be added to
     * @param listener
     *            the {@link KeyPressedListener} added as a {@link KeyListener}
     */
    public static void addKeyPressedListener(JTextField textField, KeyPressedListener listener) {
        textField.addKeyListener(listener);
    }
    
    /**
     * adds a {@link FocusLostListener} as a {@link FocusListener} to the given {@link JTextField}
     * 
     * @param textField
     *            the {@link JTextField} the listener should be added to
     * @param listener
     *            the {@link FocusLostListener} added as a {@link FocusListener}
     */
    public static void addFocusLostListener(JTextField textField, FocusLostListener listener) {
        textField.addFocusListener(listener);
    }
    
    /**
     * adds a {@link MouseClickedListener} as a {@link MouseListener} to the given {@link JList}
     * 
     * @param list
     *            the {@link JList} the listener should be added to
     * @param listener
     *            the {@link MouseClickedListener} added as a {@link MouseListener}
     */
    public static void addMouseClickedListener(JList<?> list, MouseClickedListener listener) {
        list.addMouseListener(listener);
    }
    
    /**
     * adds a {@link MouseClickedListener} as a {@link MouseListener} to the given {@link Jla}
     * 
     * @param label
     *            the {@link JLabel} the listener should be added to
     * @param listener
     *            the {@link MouseClickedListener} added as a {@link MouseListener}
     */
    public static void addMouseClickedListener(JLabel label, MouseClickedListener listener) {
        label.addMouseListener(listener);
    }
}
