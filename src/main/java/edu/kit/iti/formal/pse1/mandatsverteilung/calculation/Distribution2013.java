/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.calculation;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Bundestag;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Federation;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Settings;

/**
 * This class provides the calculation of the Bundestag according to the electoral law from 2013.
 *
 * @author Alexander Grünen
 *
 */
public class Distribution2013 extends CalculationMethod {
    private static final Distribution2013 INSTANCE = new Distribution2013();
    
    private Distribution2013() {
    }
    
    @Override
    public Bundestag calculate(Federation federation, Settings settings) {
        Bundestag bundestag = new Bundestag(settings.getParliamentSize());
        this.findElectedParties(federation, bundestag, settings);
        this.seatsPerState(federation, bundestag, settings);
        this.calculateStateGroupSizes(federation, bundestag);
        this.addOverhang(federation, bundestag);
        this.supersizeBundestag(federation, bundestag, settings);
        this.distributePartyMandatesToStates(federation, bundestag);
        this.addFractionlessWinners(federation, bundestag, settings);
        this.enableAbsoluteWinner(federation, bundestag);
        return bundestag;
    }
    
    @Override
    public String getDescription() {
        return "Berechnungsverfahren nach dem Wahlgesetz vom 3. Mai 2013";
    }
    
    public static CalculationMethod getMethod() {
        return Distribution2013.INSTANCE;
    }
}
