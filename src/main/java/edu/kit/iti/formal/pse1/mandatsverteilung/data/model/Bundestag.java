/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.data.model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * Class that stores information for a Bundestag of the federal republic of Germany.
 * 
 * The setters should only be called at the object creation, after calling {@code lockWrite} method it is not allowed to access the setters any more. The
 * setters of the Bundestag do not notify the listeners about changes! Listeners of this Bundestag are only notified when the Bundestag is replaced in the
 * Election (using {@code applyListeners}).
 *
 * @author Nils Wilka
 * @version 1.3
 */
public class Bundestag {
    
    /**
     * Error message if Bundestag would be changed after its lock is active.
     */
    private final static String LOCK_MESSAGE = "Lock write is active.";
    
    /**
     * Error message if Bundestag would be changed with an invalid argument.
     */
    private final static String INVALID_ARG = "Invalid argument.";
    
    /**
     * This set stores which parties are represented in the Bundestag.
     */
    private Set<Party> inBundestag = new HashSet<>();
    
    /**
     * This map saves the overhang mandates for a party.
     */
    private Map<Party, Double> overhangMandates = new HashMap<>();
    
    /**
     * This map saves the compensatory mandates for a {@link Party}.
     */
    private Map<Party, Double> compensatoryMandates = new HashMap<>();
    
    /**
     * This map saves the second compensatory mandates (the compensatory mandates for compensatory mandates) for a {@link Party}.
     */
    private Map<Party, Double> secondCompensatoryMandates = new HashMap<>();
    
    /**
     * This map saves the direct mandates for a {@link Party}.
     */
    private Map<ElectionArea<?>, Map<Party, Integer>> directMandates = new HashMap<>();
    
    /**
     * This map saves the number of seats for a {@link Party}.
     */
    private Map<Party, Double> seats = new HashMap<>();
    
    /**
     * This attribute saves for a {@link Party} in a {@link FederalState} its seats per state.
     */
    private Map<FederalState, Map<Party, Double>> seatsPerState = new HashMap<>();
    
    /**
     * This map saves the number of voters per member of parliament for a {@link Party}.
     */
    private Map<Party, Double> numbersOfVotersPerMP = new HashMap<>();
    
    /**
     * This map saves the number of seats for a {@link FederalState}.
     */
    private Map<FederalState, Double> numberOfSeats = new HashMap<>();
    
    /**
     * If Bundestag has been locked.
     */
    private boolean isLocked = false;
    
    /**
     * The size of the Bundestag.
     */
    private int size;
    
    /**
     * The listeners of this Bundestag.
     */
    private List<BundestagListener> listeners = new LinkedList<>();
    
    /**
     * Creates an empty class Bundestag without any information. Use the set methods to fill it with information. Then call {@code lockWrite()} to prevent any
     * further modifications, this can not be revoked.
     * 
     * @param size
     *            The size of this Bundestag.
     */
    public Bundestag(int size) {
        this.size = size;
    }
    
    /**
     * Gets for the specified {@code ElectionArea} and the specified {@link Party} in this {@code ElectionArea}, how many direct mandates it has.
     * 
     * @param area
     *            The {@code ElectionArea} to check in.
     * @param party
     *            The {@link Party} to check for.
     * @return The number of direct mandates for the specified {@link Party} in specified {@code ElectionArea}.
     */
    public int getDirectMandates(ElectionArea<?> area, Party party) {
        return Optional.ofNullable(this.directMandates.containsKey(area) ? this.directMandates.get(area).get(party) : null).orElse(0);
    }
    
    /**
     * Gets for the specified {@link Party}, how many parliament seats it has.
     * 
     * @param party
     *            The {@link Party} to check for.
     * @return The number of parliament seats for the specified {@link Party}.
     */
    public double getSeats(Party party) {
        return Optional.ofNullable(this.seats.get(party)).orElse(0.0);
    }
    
    /**
     * Gets the seats of the given {@link Party} in the given state.
     * 
     * @param state
     *            The state to get the seats from.
     * @param party
     *            The {@link Party} to get the seats for.
     * @return The number of seats of the given {@link Party} in the given state.
     */
    public double getSeats(FederalState state, Party party) {
        return Optional.ofNullable(this.seatsPerState.containsKey(state) ? this.seatsPerState.get(state).get(party) : null).orElse(0.0);
    }
    
    /**
     * Gets for the specified {@link Party}, how many parliament seats it has. Rounded value, for exact value use {@code getSeats(Party party)}.
     * 
     * @param party
     *            The {@link Party} to check for.
     * @return The number of parliament seats for the specified {@link Party}.
     */
    public int getSeatsRounded(Party party) {
        return (int) this.getSeats(party);
    }
    
    /**
     * Gets for the specified {@link Party}, how many overhang mandates it has.
     * 
     * @param party
     *            The {@link Party} to check for.
     * @return The number of overhang mandates for the specified {@link Party}.
     */
    public double getOverhangMandates(Party party) {
        return Optional.ofNullable(this.overhangMandates.get(party)).orElse(0.0);
    }
    
    /**
     * Gets for the specified {@link Party}, how many compensatory mandates it has.
     * 
     * @param party
     *            The {@link Party} to check for.
     * @return The number of compensatory mandates for the specified {@link Party}.
     */
    public double getCompensatoryMandates(Party party) {
        return Optional.ofNullable(this.compensatoryMandates.get(party)).orElse(0.0);
    }
    
    /**
     * Gets for the specified {@link Party}, how many second compensatory mandates it has.
     * 
     * @param party
     *            The {@link Party} to check for.
     * @return The number of second compensatory mandates for the specified {@link Party}.
     */
    public double getSecondCompensatoryMandates(Party party) {
        return Optional.ofNullable(this.secondCompensatoryMandates.get(party)).orElse(0.0);
    }
    
    /**
     * Checks for the specified {@link Party}, if it is in the Bundestag.
     * 
     * @param party
     *            The {@link Party} to check for.
     * @return If the specified {@link Party} is in the Bundestag.
     */
    public boolean isPartyInParliament(Party party) {
        return this.inBundestag.contains(party);
    }
    
    /**
     * Gets the number of voters per MP for specified {@link Party}.
     * 
     * @param party
     *            The {@link Party} to check for.
     * @return The number of voters per MP for specified {@link Party}.
     */
    public double getNumberOfVotersPerMP(Party party) {
        return Optional.ofNullable(this.numbersOfVotersPerMP.get(party)).orElse(0.0);
    }
    
    public double getNumberOfSeats(FederalState state) {
        return Optional.ofNullable(this.numberOfSeats.get(state)).orElse(0.0);
    }
    
    /**
     * Gets the parties in the Bundestag in array representation.
     * 
     * @return The parties in the Bundestag in array representation.
     */
    public Party[] getParties() {
        return this.inBundestag.toArray(new Party[0]);
    }
    
    /**
     * Gets size of the Bundestag.
     * 
     * @return The size of the Bundestag.
     */
    public int getSize() {
        return this.size;
    }
    
    /**
     * Locks the Bundestag, which means no set (or other modification) method can be used anymore.
     */
    public void lockWrite() {
        this.isLocked = true;
    }
    
    /**
     * Adds the listeners of the given Bundestag as listeners to this Bundestag. All listeners will then be informed about the change.
     * 
     * @param oldBundestag
     *            The old Bundestag to get the listeners from.
     */
    public void applyListeners(Bundestag oldBundestag) {
        this.listeners.addAll(oldBundestag.listeners);
        this.listeners.stream().forEach(l -> l.onChange(this));
    }
    
    /**
     * Registers a Bundestag listener to be informed about changes of this object.
     * 
     * @param listener
     *            The listener that listens on changes of the Bundestag.
     */
    public void addListener(BundestagListener listener) {
        assert listener != null : Bundestag.INVALID_ARG;
        this.listeners.add(listener);
    }
    
    /**
     * Removes the given listener from the set of listeners of this object.
     * 
     * @param listener
     *            the listener to remove.
     */
    public void removeListener(BundestagListener listener) {
        this.listeners.remove(listener);
    }
    
    /**
     * Adds the specified {@link Party} to the Bundestag.
     * 
     * @param party
     *            The specified {@link Party} to be added to the Bundestag.
     */
    public void addParty(Party party) {
        assert !this.isLocked : Bundestag.LOCK_MESSAGE;
        assert party != null : Bundestag.INVALID_ARG;
        this.inBundestag.add(party);
    }
    
    /**
     * Sets for the specified {@code ElectionArea} and the specified {@link Party} in this {@code ElectionArea}, how many parliament seats it has.
     * 
     * @param party
     *            The {@link Party} to set the seats for.
     * @param amount
     *            The seats.
     */
    public void setSeats(Party party, double amount) {
        assert !this.isLocked : Bundestag.LOCK_MESSAGE;
        assert party != null && amount >= 0 : Bundestag.INVALID_ARG;
        this.seats.put(party, amount);
    }
    
    /**
     * Sets for the given {@link FederalState} and the specified {@link Party}, how many parliament seats it has.
     * 
     * @param party
     *            The {@link Party} to set the group size for.
     * @param amount
     *            The group size of the given {@link Party} in the given state.
     */
    public void setSeats(FederalState state, Party party, double amount) {
        assert !this.isLocked : Bundestag.LOCK_MESSAGE;
        assert state != null && party != null && amount >= 0 : Bundestag.INVALID_ARG;
        Optional<Map<Party, Double>> opt = Optional.ofNullable(this.seatsPerState.get(state));
        Map<Party, Double> map = opt.orElse(new HashMap<>());
        this.seatsPerState.put(state, map);
        map.put(party, amount);
    }
    
    /**
     * Sets the number of overhang mandates for the specified {@link Party}.
     * 
     * @param party
     *            The {@link Party} to set the overhang mandates for.
     * @param amount
     *            The number of overhang mandates for the specified {@link Party}.
     */
    public void setOverhangMandates(Party party, double amount) {
        assert !this.isLocked : Bundestag.LOCK_MESSAGE;
        assert party != null && amount >= 0 : Bundestag.INVALID_ARG;
        this.overhangMandates.put(party, amount);
    }
    
    /**
     * Sets the number of compensatory mandates for the specified {@link Party}.
     * 
     * @param party
     *            The {@link Party} to set the compensatory mandates for.
     * @param amount
     *            The number of compensatory mandates for the specified {@link Party}.
     */
    public void setCompensatoryMandates(Party party, double amount) {
        assert !this.isLocked : Bundestag.LOCK_MESSAGE;
        assert party != null && amount >= 0 : Bundestag.INVALID_ARG;
        this.compensatoryMandates.put(party, amount);
    }
    
    /**
     * Sets the number of second compensatory mandates for the specified {@link Party}.
     * 
     * @param party
     *            The {@link Party} to set the second compensatory mandates for.
     * @param amount
     *            The number of second compensatory mandates for the specified {@link Party}.
     */
    public void setSecondCompensatoryMandates(Party party, double amount) {
        assert !this.isLocked : Bundestag.LOCK_MESSAGE;
        assert party != null && amount >= 0 : Bundestag.INVALID_ARG;
        this.secondCompensatoryMandates.put(party, amount);
    }
    
    /**
     * Sets the number of direct mandates mandates for the specified {@link Party}.
     * 
     * @param area
     *            The {@code ElectionArea} to set the direct mandates in.
     * @param party
     *            The {@link Party} to set the direct mandates mandates for.
     * @param amount
     *            The number of direct mandates mandates for the specified {@link Party}.
     */
    public void setDirectMandates(ElectionArea<?> area, Party party, Integer amount) {
        assert !this.isLocked : Bundestag.LOCK_MESSAGE;
        assert area != null && party != null && amount != null && amount >= 0 : Bundestag.INVALID_ARG;
        Optional<Map<Party, Integer>> opt = Optional.ofNullable(this.directMandates.get(area));
        Map<Party, Integer> map = opt.orElse(new HashMap<>());
        this.directMandates.put(area, map);
        map.put(party, amount);
    }
    
    /**
     * Sets the given amount as the number of voters per MP for the specified {@link Party}.
     * 
     * @param party
     *            The {@link Party} the number of voters per MP is to be set.
     * @param amount
     *            The number of voters per MP for the specified {@link Party}.
     */
    public void setNumberOfVotersPerMP(Party party, double amount) {
        assert !this.isLocked : Bundestag.LOCK_MESSAGE;
        assert party != null && amount >= 0 : Bundestag.INVALID_ARG;
        this.numbersOfVotersPerMP.put(party, amount);
    }
    
    /**
     * Sets the number of seats for given {@link FederalState}.
     * 
     * @param state
     *            The {@link FederalState} to set the seats for.
     * @param value
     *            The number of seats to set.
     */
    public void setNumberOfSeats(FederalState state, double value) {
        assert state != null && value >= 0 : Bundestag.INVALID_ARG;
        this.numberOfSeats.put(state, value);
    }
    
    /**
     * Sets the size of the Bundestag.
     * 
     * @param size
     *            The size of the Bundestag.
     */
    public void setSize(int size) {
        assert !this.isLocked : Bundestag.LOCK_MESSAGE;
        assert size >= 0 : Bundestag.INVALID_ARG;
        this.size = size;
    }
    
    @Override
    public boolean equals(Object e) {
        if(e == this) {
            return true;
        }
        else if(e instanceof Bundestag) {
            Bundestag other = (Bundestag) e;
            if(other.isLocked != this.isLocked) {
                return false;
            }
            else if(!other.compensatoryMandates.equals(this.compensatoryMandates)) {
                return false;
            }
            else if(!other.directMandates.equals(this.directMandates)) {
                return false;
            }
            else if(!other.inBundestag.equals(this.inBundestag)) {
                return false;
            }
            else if(!other.numberOfSeats.equals(this.numberOfSeats)) {
                return false;
            }
            else if(!other.numbersOfVotersPerMP.equals(this.numbersOfVotersPerMP)) {
                return false;
            }
            else if(!other.overhangMandates.equals(this.overhangMandates)) {
                return false;
            }
            else if(!other.seats.equals(this.seats)) {
                return false;
            }
            else if(!other.seatsPerState.equals(this.seatsPerState)) {
                return false;
            }
            else if(!other.secondCompensatoryMandates.equals(this.secondCompensatoryMandates)) {
                return false;
            }
            else if(other.size != this.size) {
                return false;
            }
            else {
                return true;
            }
        }
        else {
            return false;
        }
    }
}
