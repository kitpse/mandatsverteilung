/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.data.model;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * A federal state stores how many votes which party got. Changes will be propagated to the constituencies, which are the children of this federal state.
 * Additionally stores for each party the size of the list of party candidates for election at the level of a federal state.
 * 
 * @author unknown, Nils Wilka
 * @version 1.0
 */
public class FederalState extends ElectionArea<Constituency> {
    private Map<Party, Integer> candidateCount = new HashMap<>();
    
    /**
     * Creates a new federation with given {@code name} and {@code ElectionResult}.
     * 
     * @param name
     *            The name of the federation.
     * @param electionResult
     *            The {@code ElectionResult} the created federation belongs to.
     */
    public FederalState(String name, ElectionResult electionResult) {
        super(name, electionResult);
    }
    
    @Override
    public boolean addChild(Constituency c) {
        return super.addChild(c);
    }
    
    @Override
    public boolean removeChild(Constituency c) {
        return super.removeChild(c);
    }
    
    /**
     * Gets the size of the list of party candidates for election at the level of a federal state for the given {@code Party}.
     * 
     * @param party
     *            The {@code Party} to get the size of the list of party candidates for election at the level of a federal state for.
     * @return The size of the list of party candidates for election at the level of a federal state, or if the party is not contained 0.
     */
    public int getCandidateCount(final Party party) {
        return Optional.ofNullable(this.candidateCount.get(party)).orElse(0);
    }
    
    /**
     * Changes the size of the list of party candidates for election at the level of a federal state for the given {@code Party} with the given value.
     * 
     * @param party
     *            The {@code Party} to set the size of the list of party candidates for election at the level of a federal state for.
     * @param count
     *            The new candidate count value to be set.
     * @return True if candidate count was set and if count > 0, false if party is unknown to federal state and candidate count could not be set or count <= 0.
     */
    public boolean changeCandidateCount(Party party, int count) {
        boolean exec = false;
        if(this.isPartyPresent(party) && count > 0) {
            this.candidateCount.put(party, count);
            exec = true;
            this.notifyListeners();
        }
        return exec;
    }
    
    /**
     * Accepts 0 as valid input (returns true in that case), but does not change anything. Use {@code changeCandidateCount} for checking that. Sets the size of
     * the list of party candidates for election at the level of a federal state for the given {@code Party} with the given value.
     * 
     * @param party
     *            The {@code Party} to set the size of the list of party candidates for election at the level of a federal state for.
     * @param count
     *            The new candidate count value to be set.
     * @return True if candidate count was set and if count >= 0 (but does not change anything for count == 0), false if candidate count < 0.
     */
    public boolean setCandidateCount(Party party, int count) {
        boolean exec = false;
        if(count == 0) {
            exec = true;
        }
        else {
            exec = this.changeCandidateCount(party, count);
        }
        return exec;
    }
    
    /**
     * Checks if the party has a candidate count in this federal state.
     * 
     * @param party
     *            The {@code Party} to check for.
     * @return True if the party has a candidate count in this federal state, else false.
     */
    public boolean hasPartyCandidateCount(Party party) {
        return Optional.ofNullable(this.candidateCount.get(party)).isPresent();
    }
    
    @Override
    public FederalState deepClone() {
        FederalState copy = new FederalState(this.getName(), this.getElectionResult());
        List<Party> copiedParties = this.copyParties();
        for(Constituency constituency : this.getChildren()) {
            copy.addChild(constituency.deepClone());
        }
        for(Party party : this.getParties()) {
            Party searchedParty = null;
            for(Party partyCopy : copiedParties) {
                if(party.equals(partyCopy)) {
                    searchedParty = partyCopy;
                    break;
                }
            }
            if(this.hasPartyCandidateCount(party)) {
                copy.setCandidateCount(searchedParty, this.getCandidateCount(party));
            }
        }
        if(this.isLocked()) {
            copy.lock();
        }
        return copy;
    }
    
    @Override
    public boolean equals(Object obj) {
        if(!super.equals(obj)) {
            return false;
        }
        if(!(obj instanceof FederalState)) {
            return false;
        }
        FederalState comp = (FederalState) obj;
        return comp.candidateCount.equals(this.candidateCount);
    }
    
    @Override
    public int hashCode() {
        return super.hashCode() + this.candidateCount.hashCode();
    }
}
