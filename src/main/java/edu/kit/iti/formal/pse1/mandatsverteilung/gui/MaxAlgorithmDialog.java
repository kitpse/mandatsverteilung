/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.util.Optional;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.management.Session;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Bundestag;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Election;
import edu.kit.iti.formal.pse1.mandatsverteilung.gui.controller.Mandatsverteilung;
import edu.kit.iti.formal.pse1.mandatsverteilung.simulation.Max;

/**
 * This Dialog shows the process of a running {@link Max} Algorithm. It shows the time passed since it started and has the option to abort it. <br>
 * When the algorithm has finished it loads the results into the GUI.
 * 
 * @author Felix Heim
 * @version 1
 */
public class MaxAlgorithmDialog extends JDialog {
    private static final long serialVersionUID = 1L;
    /** the instance of the configuration dialog */
    private static final MaxAlgorithmDialog INSTANCE = new MaxAlgorithmDialog();
    
    /** the {@link Election} in which to try to maximize the {@link Bundestag} */
    private Election election;
    /** the field where to enter the allowed delta for the first votes when running the algorithm */
    private JTextField fvdeltaField = new JTextField(6);
    /** the field where to enter the allowed delta for the second votes when running the algorithm */
    private JTextField svdeltaField = new JTextField(6);
    
    /** the main content pane of the dialog */
    private JPanel contentPane = new JPanel();
    /** the first line showing for which {@link Election} the algo gets configured */
    private JLabel title = new JLabel("Maximierungsalgorithmus f\u00FCr die Wahl \"\"");
    
    private MaxAlgorithmDialog() {
        // set the title
        super(Mandatsverteilung.getMainWindow(), "Maximierungsalgorithmus konfigurieren");
        this.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        this.contentPane.setLayout(new BoxLayout(this.contentPane, BoxLayout.Y_AXIS));
        // add a 10 pixel gap between the title and the next text
        this.title.setBorder(BorderFactory.createEmptyBorder(1, 1, 10, 1));
        this.title.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.contentPane.add(this.title);
        
        // label describing what to enter
        JLabel description = new JLabel("Erlaubte Abweichung vom gegebenen Ergebnis f\u00FCr eine Partei in einem Wahlkreis:");
        description.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.contentPane.add(description);
        
        // panel where to configure the allowed delta for the first votes
        JPanel fvdeltaPanel = new JPanel();
        fvdeltaPanel.add(new JLabel("Erststimmen: "));
        this.fvdeltaField.setText(GuiHelper.doubleToPercentString(0.0) + " %");
        fvdeltaPanel.add(this.fvdeltaField);
        this.contentPane.add(fvdeltaPanel);
        
        // panel where to configure the allowed delta for the second votes
        JPanel svdeltaPanel = new JPanel();
        svdeltaPanel.add(new JLabel("Zweitstimmen: "));
        this.svdeltaField.setText(GuiHelper.doubleToPercentString(0.0) + " %");
        svdeltaPanel.add(this.svdeltaField);
        this.contentPane.add(svdeltaPanel);
        
        // the buttons
        JPanel buttonPanel = new JPanel();
        JButton buttonAbort = new JButton("Abbrechen");
        // abort -> only hide the dialog again
        buttonAbort.addActionListener(e -> this.setVisible(false));
        buttonPanel.add(buttonAbort);
        JButton buttonStart = new JButton("Start");
        // start -> read the values, validate them, and if they're valid, then run the algorithm and hide the dialog
        buttonStart.addActionListener(e -> {
            Optional<Double> fvdelta = GuiHelper.percentStringToDouble(this.fvdeltaField.getText());
            Optional<Double> svdelta = GuiHelper.percentStringToDouble(this.svdeltaField.getText());
            if(fvdelta.isPresent() && svdelta.isPresent()) {
                /* creates a new dialog to show the process */
                new AlgorithmRunningDialog(this.election, fvdelta.get(), svdelta.get());
                this.setVisible(false);
            }
            else {
                GuiHelper.showError("Ung\u00FCltige Prozentzahl angegeben");
            }
        });
        buttonPanel.add(buttonStart);
        buttonPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.contentPane.add(buttonPanel);
        this.add(this.contentPane);
        this.pack();
        this.setResizable(false);
    }
    
    /**
     * starts the {@link Max} Algorithm for the given {@link Election}.<br>
     * First it shows a dialog with the time passed and the option to abort it, then it loads the the results into a new tab in the GUI.
     * 
     * @param election
     *            the {@link Election} in which to try to maximize the {@link Bundestag}
     */
    static void show(Election election) {
        MaxAlgorithmDialog.INSTANCE.election = election;
        MaxAlgorithmDialog.INSTANCE.title.setText("Maximierungsalgorithmus f\u00FCr die Wahl \"" + election.getName() + "\""); // update the title
        
        // fill with default values
        MaxAlgorithmDialog.INSTANCE.fvdeltaField.setText("10.00 %");
        MaxAlgorithmDialog.INSTANCE.svdeltaField.setText("10.00 %");
        
        // recenter and show the dialog
        MaxAlgorithmDialog.INSTANCE.setLocationRelativeTo(Mandatsverteilung.getMainWindow());
        MaxAlgorithmDialog.INSTANCE.setVisible(true);
    }
    
    private class AlgorithmRunningDialog extends JDialog {
        private static final long serialVersionUID = 1L;
        
        /** the time in millis when the algorithm started */
        private long startTime = System.currentTimeMillis();
        
        /** a {@link JLabel} showing the time in minutes and seconds since the algorithm started */
        private JLabel timePassed = new JLabel("Verstrichene Zeit: 00:00 min");
        
        /**
         * @param originalElection
         *            the {@link Election} in which to try to maximize the {@link Bundestag}
         * @param fvdelta
         *            the allowed delta of first votes from the original first votes
         * @param svdelta
         *            the allowed delta of second votes from the original second votes
         */
        public AlgorithmRunningDialog(Election originalElection, double fvdelta, double svdelta) {
            // set the title
            super(Mandatsverteilung.getMainWindow(), "Algorithmus l\u00E4uft");
            // create a new algorithm instance
            Max algo = new Max();
            // use the abort button to close the window
            this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
            this.setLayout(new BorderLayout());
            // create the Thread running the actual algorithm 
            new Thread(() -> {
                /* log that we started */
                Mandatsverteilung.log("Started Max Algorithm for " + originalElection.getName());
                /* run the algorithm an save the result */
                Election newElection = algo.maximizeParliament(originalElection, fvdelta, svdelta);
                /* adapt the name of the new election */
                newElection.setName(originalElection.getName() + " - Maximiert");
                /* show the result in the GUI */
                Session.getSession().addElection(newElection);
                
                /* calculate the time needed */
                long timePassed = (System.currentTimeMillis() - this.startTime) / 1000;
                long minutes = timePassed / 60;
                long seconds = timePassed % 60;
                /* and log the time needed */
                Mandatsverteilung.log(new StringBuilder("Finished Max Algorithm for ").append(originalElection.getName()).append(" in ")
                        .append(minutes < 0 ? "0" : "").append(minutes).append(seconds < 10 ? ":0" : ":").append(seconds).append(" min").toString());
                /* finally close this dialog */
                this.dispose();
            }).start();
            // first line: show the name of the algorithm and the election
            this.add(new JLabel("<html><body>Algorithmus: Maximierung des Bundestags<br>Datensatz: " + originalElection.getName() + "</body></html>"),
                     BorderLayout.PAGE_START);
            // second line: the time passed
            this.add(this.timePassed, BorderLayout.CENTER);
            // third line: the abort button
            JButton buttonStop = new JButton("Stoppen");
            buttonStop.addActionListener(e -> algo.abortSimulationAlgo());
            this.add(buttonStop, BorderLayout.PAGE_END);
            // pack the dialog and then show it
            this.pack();
            this.setLocationRelativeTo(this.getOwner());
            this.setResizable(false);
            this.setVisible(true);
            // finally the thread to update the time passed
            new Thread(() -> {
                while(this.isVisible()) {
                    long timePassed = (System.currentTimeMillis() - this.startTime) / 1000;
                    long minutes = timePassed / 60;
                    long seconds = timePassed % 60;
                    /* updates the label with new time, since the start time */
                    this.timePassed.setText(new StringBuilder(28).append("Verstrichene Zeit: ").append(minutes < 0 ? "0" : "").append(minutes)
                            .append(seconds < 10 ? ":0" : ":").append(seconds).append(" min").toString());
                    try {
                        /* update only once every second */
                        Thread.sleep(1000);
                    }
                    catch(InterruptedException e) {
                    }
                }
            }).start();
        }
    }
}
