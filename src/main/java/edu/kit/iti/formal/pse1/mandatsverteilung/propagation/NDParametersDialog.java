/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.propagation;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import edu.kit.iti.formal.pse1.mandatsverteilung.gui.GuiHelper;
import edu.kit.iti.formal.pse1.mandatsverteilung.gui.controller.Mandatsverteilung;

/**
 * A Dialog where all the values for a {@link NormalDistribution} can be set.
 * 
 * @author Felix Heim
 * @version 1
 */
public class NDParametersDialog extends JDialog {
    private static final long serialVersionUID = 1L;
    
    /** the first row, the first row is special because the dominance is deactivated as long as there aren't more rows */
    private Row firstRow;
    
    /** all rows */
    private List<Row> rows = new LinkedList<>();
    
    private GridBagConstraints c = new GridBagConstraints();
    
    public NDParametersDialog(NormalDistribution propagation) {
        super(Mandatsverteilung.getMainWindow());
        this.setLayout(new GridBagLayout());
        
        JButton buttonAddRow = new JButton("Weitere Normalverteilung hinzuf\u00FCgen");
        JButton buttonFinish = new JButton("Fertig");
        buttonAddRow.addActionListener(e -> {
            /* to add a row we need to insert the row before the buttons... */
            this.firstRow.dominance.setEnabled(true);
            
            /* ... so we first remove the buttons ... */
            this.remove(buttonAddRow);
            this.remove(buttonFinish);
            this.c.gridx = 0;
            
            /* ... then add a placeholder ... */
            this.add(NDParametersDialog.createEmptryPanel(), this.c);
            this.c.gridy++;
            this.c.gridwidth = 1;
            
            /* .. then add the new row ... */
            this.rows.add(new Row(this.rows.size()));
            this.c.gridwidth = 2;
            this.c.gridx = 0;
            
            /* ... and finally readd the buttons */
            this.add(buttonAddRow, this.c);
            this.c.gridy++;
            this.add(buttonFinish, this.c);
            this.pack();
        });
        buttonFinish.addActionListener(e -> {
            try {
                propagation.setParameters(this.rows.stream()
                        // try to map the rows to a NDParameter with all the fields of the row parsed as a double
                        .map(row -> {
                                 return propagation.new NDParameter(Double.parseDouble(row.expectation.getText()), Double.parseDouble(row.standardDeviation
                                         .getText()), Double.parseDouble(row.dominance.getText()));
                             }).collect(Collectors.toList()));
                this.dispose();
            }
            catch(IllegalArgumentException iae) {
                /* if an error occurs (e.g. a false number) we show the error to the user */
                GuiHelper.showError("Fehler bei der Eingabe der Parameter: " + iae.getMessage());
            }
        });
        
        this.c.gridy = 0;
        this.c.gridx = 0;
        this.c.fill = GridBagConstraints.HORIZONTAL;
        
        // add the first row
        this.firstRow = new Row(0);
        this.rows.add(this.firstRow);
        this.firstRow.dominance.setEnabled(false);
        
        // add the buttons
        this.c.gridx = 0;
        this.c.gridwidth = 2;
        this.add(buttonAddRow, this.c);
        this.c.gridy++;
        this.add(buttonFinish, this.c);
        
        // show the dialog with the right dimensions
        this.pack();
        this.setLocationRelativeTo(this.getOwner());
        this.setResizable(false);
        this.setVisible(true);
    }
    
    /**
     * This creates a {@link JPanel} containing nothing but an empty border.<br>
     * It is used to create a placeholder.
     * 
     * @return the empty {@link JPanel}
     */
    private static JPanel createEmptryPanel() {
        JPanel panel = new JPanel();
        panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        return panel;
    }
    
    /**
     * Defines a "Row" containing an input field for the expectation, the standard deviation and the dominance.
     * 
     * @author Felix Heim
     * @version 2
     */
    private class Row {
        private JTextField expectation = new JTextField("1");
        private JTextField standardDeviation = new JTextField("0.1");
        private JTextField dominance = new JTextField("1");
        
        private Row(int index) {
            this.addTextField("Erwartungswert (\u03BC_" + index + "):", this.expectation);
            this.addTextField("Standardabweichung (\u03C3):", this.standardDeviation);
            this.addTextField("Dominanz (\u03C3):", this.dominance);
        }
        
        private void addTextField(String name, JTextField textField) {
            // add the label left-aligned
            NDParametersDialog.this.c.gridx = 0;
            NDParametersDialog.this.c.anchor = GridBagConstraints.WEST;
            NDParametersDialog.this.add(new JLabel(name), NDParametersDialog.this.c);
            
            // add the text field right-aligned
            NDParametersDialog.this.c.gridx = 1;
            NDParametersDialog.this.c.anchor = GridBagConstraints.EAST;
            NDParametersDialog.this.add(textField, NDParametersDialog.this.c);
            
            // move to the next cell
            NDParametersDialog.this.c.gridy++;
        }
    }
}
