/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.gui;

import java.awt.Event;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.management.ElectionComparison;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.management.Session;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Election;

/**
 * The {@link MenuEdit} contains {@link JMenuItem}s to compare elections and to start the algorithms.
 * 
 * @author Felix Heim
 * @version 3
 */
class MenuEdit extends JMenu {
    private static final long serialVersionUID = 1L;
    
    private MainWindow mainWindow;
    
    /** the {@link JMenu} containing a list of loaded {@link Election}s */
    private JMenu compareWith = new JMenu("Vergleichen mit:");
    
    /** */
    private Map<Election, JMenuItem> electionToMenuItemMap = new HashMap<>();
    
    MenuEdit(MainWindow mainWindow) {
        super("Bearbeiten");
        this.mainWindow = mainWindow;
        this.add(this.compareWith); // add the compare with menu
        this.add(new JSeparator());// add a seperator line
        this.addRenameTab();
        this.addShiftTabLeft();
        this.addShiftTabRight();
        this.add(new JSeparator());// add a seperator line
        this.addMaxAlgorithm();
        this.addNegativeRelativeAlgorithm();
    }
    
    /**
     * adds the given {@link Election} to the {@link #compareWith} menu
     * 
     * @param election
     *            the {@link Election} to add
     */
    void addElection(Election election) {
        JMenuItem item = new JMenuItem(election.getName()); // create with the name being the name of the election
        item.addActionListener(e -> {
            Optional<TabElection> optionalSelected = this.mainWindow.getSelectedTabElection(); // check the selected tab of the gui
            if(optionalSelected.isPresent()) { // if it is an TabElection, go on
                Election other = optionalSelected.get().getElection(); // get the other election
                if(other != election) { // if the elections aren't the same, go on
                    // enter the name for the comparison via a input dialog of JOptionPane
                    String name = JOptionPane.showInputDialog(this.mainWindow, "Name des Vergleichs:", other.getName() + " vs. " + election.getName());
                    if(name != null) { // if the name input wasn't aborted
                        Session.getSession().addElectionComparison(new ElectionComparison(other, election, name));
                    }
                }
            }
        });
        this.electionToMenuItemMap.put(election, item); // map the election to its menu
        this.compareWith.add(item); // add to the menu
    }
    
    /**
     * removes the given {@link Election} from the {@link #compareWith} menu
     * 
     * @param election
     *            the {@link Election} to remove
     */
    void removeElection(Election election) {
        this.compareWith.remove(this.electionToMenuItemMap.get(election));
    }
    
    private void addRenameTab() {
        JMenuItem renameTab = new JMenuItem("Tab umbenennen");
        renameTab.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask() | Event.ALT_MASK));
        renameTab.addActionListener(e -> this.mainWindow.setSelectedTabName(JOptionPane.showInputDialog(this.mainWindow, "Neuer Name für den aktuellen Tab:",
                                                                                                        this.mainWindow.getSelectedTabName())));
        this.add(renameTab);
    }
    
    private void addShiftTabLeft() {
        JMenuItem shiftLeft = new JMenuItem("Tab nach links rücken");
        shiftLeft.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask() | Event.ALT_MASK));
        shiftLeft.addActionListener(e -> this.mainWindow.shiftTabLeft());
        this.add(shiftLeft);
    }
    
    private void addShiftTabRight() {
        JMenuItem shiftRight = new JMenuItem("Tab nach rechts rücken");
        shiftRight.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask() | Event.ALT_MASK));
        shiftRight.addActionListener(e -> this.mainWindow.shiftTabRight());
        this.add(shiftRight);
        
    }
    
    private void addMaxAlgorithm() {
        JMenuItem maxAlgorithm = new JMenuItem("Maximiere den Bundestag");
        maxAlgorithm.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_M, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask() | Event.ALT_MASK)); // add a shortcut
        maxAlgorithm.addActionListener(e -> this.mainWindow.getSelectedTabElection() // on click: if the selected tab is a TabEelcetion, then show the configuration for the Max Algorithm
                .ifPresent(tabElection -> MaxAlgorithmDialog.show(tabElection.getElection())));
        this.add(maxAlgorithm);
        
    }
    
    private void addNegativeRelativeAlgorithm() {
        JMenuItem negativeWeightAlgorithm = new JMenuItem("Finde Negatives Relatives Stimmgewicht");
        negativeWeightAlgorithm.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask() | Event.ALT_MASK)); // add a shortcut
        negativeWeightAlgorithm.addActionListener(e -> this.mainWindow.getSelectedTabElection() // on click: if the selected tab is a TabEelcetion, then run the Neg. Rel. Voting Weight Algorithm
                .ifPresent(tabElection -> NegativeRelativeVotingWeightAlgorithmDialog.show(tabElection.getElection())));
        this.add(negativeWeightAlgorithm);
    }
}
