/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.data.model;

import java.util.ArrayList;
import java.util.List;

import edu.kit.iti.formal.pse1.mandatsverteilung.propagation.NormalDistribution;
import edu.kit.iti.formal.pse1.mandatsverteilung.propagation.PropagationMath;
import edu.kit.iti.formal.pse1.mandatsverteilung.propagation.ProportionalDistribution;
import edu.kit.iti.formal.pse1.mandatsverteilung.propagation.UniformDistribution;

/**
 * Class that provides functionality to propagate changes from a higher level to a lower level (e.g. from federation to its federal states). Can use different
 * strategies (propagation math).
 * 
 * @author unknown
 * @version 1.0
 */
public class Propagation implements DeepCloneable<Propagation> {
    private static final List<PropagationMath> METHODS = new ArrayList<>(3);
    
    /**
     * The propagation math.
     */
    private PropagationMath math;
    
    /**
     * Creates a new propagation with given {@link PropagationMath}.
     * 
     * @param propagation
     *            The {@link PropagationMath} to use.
     */
    Propagation(PropagationMath propagation) {
        this.math = propagation;
    }
    
    /**
     * Gets the {@link PropagationMath}.
     * 
     * @return The used {@link PropagationMath}.
     */
    public PropagationMath getPropagation() {
        return this.math;
    }
    
    /**
     * Sets the {@link PropagationMath}.
     * 
     * @param propagation
     *            The {@link PropagationMath} to use.
     */
    public void setPropagation(PropagationMath propagation) {
        this.math = propagation;
    }
    
    /**
     * Gets an array list of all unlocked children.
     * 
     * @param area
     *            The {@link ElectionArea} ({@link FederalState} or {@link Federation}) to get the unlocked children for (an ElectionArea is considered
     *            unlocked, if at least one constituency is unlocked).
     * @return A list of unlocked children.
     */
    private ArrayList<ElectionArea<?>> unlockedChildren(ElectionArea<?> area) {
        ArrayList<ElectionArea<?>> unlocked = new ArrayList<>();
        
        if(!area.isLocked()) {
            for(ElectionArea<?> child : area.getChildren()) {
                if(PropagationHelper.isOneUnlocked(child)) {
                    unlocked.add(child);
                }
            }
        }
        return unlocked;
    }
    
    /**
     * Gets an array list of all unlocked children where at least one constituency is unlocked and contains the given party.
     * 
     * @param area
     *            The {@link ElectionArea} ({@link FederalState} or {@link Federation}) to get the unlocked children for (an ElectionArea is considered
     *            unlocked, if at least one constituency is unlocked).
     * @param party
     *            the party to search for
     * @return A list of unlocked children.
     */
    private ArrayList<ElectionArea<?>> unlockedChildren(ElectionArea<?> area, Party party) {
        ArrayList<ElectionArea<?>> unlocked = new ArrayList<>();
        
        if(!area.isLocked()) {
            for(ElectionArea<?> child : area.getChildren()) {
                if(PropagationHelper.isOneUnlocked(area, party)) {
                    unlocked.add(child);
                }
            }
        }
        return unlocked;
    }
    
    /**
     * Propagate the delta into the votes array respecting the constaints given by the maxVotes array.
     * 
     * @param votes
     * @param maxVotes
     * @param delta
     */
    private void propagate(int votes[], int maxVotes[], int delta) {
        this.math.propagate(votes, maxVotes, delta);
    }
    
    private void propagateWithoutConstraint(int votes[], int delta) {
        int[] maxVotes = new int[votes.length];
        for(int i = 0; i < votes.length; i++) {
            maxVotes[i] = Integer.MAX_VALUE;
        }
        this.math.propagate(votes, maxVotes, delta);
    }
    
    /**
     * Propagate the amount citizens with the set propagation method over all constituencies under this election area. If the new amount is lower than the
     * amount of eligible voters or the number of votes, they will be set to this amount.
     * 
     * @param area
     *            The area to change the votes for.
     * @param amount
     *            The new amount
     * @return True on success, else false (no changes will happen in this case).
     */
    boolean propagateCitizens(ElectionArea<?> area, int amount) {
        if(amount < 0) {
            return false;
        }
        
        ArrayList<ElectionArea<?>> unlocked = this.unlockedChildren(area);
        if(unlocked.size() == 0) {
            // all children locked
            return false;
        }
        int[] votes = unlocked.stream().mapToInt(a -> PropagationHelper.unlockedCitizens(a)).toArray();
        
        this.propagateWithoutConstraint(votes, amount - PropagationHelper.unlockedCitizens(area));
        
        for(int i = 0; i < votes.length; i++) {
            unlocked.get(i).changeCitizens(votes[i]);
        }
        return true;
    }
    
    /**
     * Propagate the amount eligible voters with the set propagation method over all constituencies under this election area. If the new amount is lower than
     * the amount of cast votes, they will be set to this amount (this will cause an adaptation of the cast votes over all constituencies with the set
     * propagation method). Is the new amount greater than the citizens, they will be set to this amount.
     * 
     * @param area
     *            The area to change the votes for.
     * @param amount
     *            The new amount
     * @return True on success, else false (no changes will happen in this case).
     */
    boolean propagateEligibleVoters(ElectionArea<?> area, int amount) {
        if(amount < 0) {
            return false;
        }
        
        ArrayList<ElectionArea<?>> unlocked = this.unlockedChildren(area);
        if(unlocked.size() == 0) {
            // all children locked
            return false;
        }
        int[] votes = unlocked.stream().mapToInt(a -> PropagationHelper.unlockedEligibleVoters(a)).toArray();
        
        this.propagateWithoutConstraint(votes, amount - PropagationHelper.unlockedEligibleVoters(area));
        
        for(int i = 0; i < votes.length; i++) {
            unlocked.get(i).changeEligibleVoters(votes[i]);
        }
        return true;
    }
    
    /**
     * Propagate the amount of votes (valid and invalid) with the set propagation method over all constituencies under this election area. The changed votes
     * will raise the amount of eligible voters and / or citizens if necessary (the ratio between valid and invalid stays the same). If the votes are decreased,
     * from all unlocked parties (ratio stays the same).
     * 
     * @param area
     *            The area to change the votes for.
     * @param amount
     *            The new amount
     * @return True on success, else false (no changes will happen in this case).
     */
    boolean propagateNumberOfVotes(ElectionArea<?> area, int amount) {
        if(amount < 0) {
            return false;
        }
        
        ArrayList<ElectionArea<?>> unlocked = this.unlockedChildren(area);
        if(unlocked.size() == 0) {
            // all children locked
            return false;
        }
        int[] votes = unlocked.stream().mapToInt(a -> PropagationHelper.unlockedNumberOfVotes(a)).toArray();
        
        this.propagateWithoutConstraint(votes, amount - PropagationHelper.unlockedNumberOfVotes(area));
        
        for(int i = 0; i < votes.length; i++) {
            unlocked.get(i).changeNumberOfVotes(votes[i]);
        }
        return true;
    }
    
    /**
     * Change the amount of valid first votes in this area and propagate the changes with the set propagation method over all constituencies under this election
     * area. Tries to take the votes from / give the votes to the invalid votes first, error if the amount is greater than the number of cast votes.
     * 
     * @param area
     *            The area to change the votes for.
     * @param amount
     *            The new amount
     * @return True on success, else false (no changes will happen in this case).
     */
    boolean propagateValidFirstVotes(ElectionArea<?> area, int amount) {
        if(amount < 0 || PropagationHelper.unlockedNumberOfVotes(area) < amount) {
            return false;
        }
        
        ArrayList<ElectionArea<?>> unlocked = this.unlockedChildren(area);
        if(unlocked.size() == 0) {
            // all children locked
            return false;
        }
        int[] votes = unlocked.stream().mapToInt(a -> PropagationHelper.unlockedValidFirstVotes(a)).toArray();
        int[] maxVotes = unlocked.stream().mapToInt(a -> PropagationHelper.unlockedNumberOfVotes(a)).toArray();
        
        this.propagate(votes, maxVotes, amount - PropagationHelper.unlockedValidFirstVotes(area));
        
        for(int i = 0; i < votes.length; i++) {
            unlocked.get(i).changeValidFirstVotes(votes[i]);
        }
        return true;
    }
    
    /**
     * Change the amount of valid second votes in this area and propagate the changes with the set propagation method over all constituencies under this
     * election area. Tries to take the votes from / give the votes to the invalid votes first, error if the amount is greater than the number of cast votes.
     * 
     * @param area
     *            The area to change the votes for.
     * @param amount
     *            The new amount
     * @return True on success, else false (no changes will happen in this case).
     */
    boolean propagateValidSecondVotes(ElectionArea<?> area, int amount) {
        if(amount < 0 || PropagationHelper.unlockedNumberOfVotes(area) < amount) {
            return false;
        }
        
        ArrayList<ElectionArea<?>> unlocked = this.unlockedChildren(area);
        if(unlocked.size() == 0) {
            // all children locked
            return false;
        }
        int[] votes = unlocked.stream().mapToInt(a -> PropagationHelper.unlockedValidSecondVotes(a)).toArray();
        int[] maxVotes = unlocked.stream().mapToInt(a -> PropagationHelper.unlockedNumberOfVotes(a)).toArray();
        
        this.propagate(votes, maxVotes, amount - PropagationHelper.unlockedValidSecondVotes(area));
        
        for(int i = 0; i < votes.length; i++) {
            unlocked.get(i).changeValidSecondVotes(votes[i]);
        }
        return true;
    }
    
    /**
     * Change the amount of invalid first votes in this area and propagate the changes with the set propagation method over all constituencies under this
     * election area. Tries to take the votes from / give the votes to the valid votes first, error if the amount is greater than the number of cast votes.
     * 
     * @param area
     *            The area to change the votes for.
     * @param amount
     *            The new amount
     * @return True on success, else false (no changes will happen in this case).
     */
    boolean propagateInvalidFirstVotes(ElectionArea<?> area, int amount) {
        if(amount < 0 || PropagationHelper.unlockedNumberOfVotes(area) < amount) {
            return false;
        }
        
        ArrayList<ElectionArea<?>> unlocked = this.unlockedChildren(area);
        if(unlocked.size() == 0) {
            // all children locked
            return false;
        }
        int[] votes = unlocked.stream().mapToInt(a -> PropagationHelper.unlockedInvalidFirstVotes(a)).toArray();
        int[] maxVotes = unlocked.stream().mapToInt(a -> PropagationHelper.unlockedNumberOfVotes(a)).toArray();
        
        this.propagate(votes, maxVotes, amount - PropagationHelper.unlockedInvalidFirstVotes(area));
        
        for(int i = 0; i < votes.length; i++) {
            unlocked.get(i).changeInvalidFirstVotes(votes[i]);
        }
        return true;
    }
    
    /**
     * Change the amount of invalid second votes in this area and propagate the changes with the set propagation method over all constituencies under this
     * election area. Tries to take the votes from / give the votes to the valid votes first, error if the amount is greater than the number of cast votes.
     * 
     * @param area
     *            The area to change the votes for.
     * @param amount
     *            The new amount
     * @return True on success, else false (no changes will happen in this case).
     */
    boolean propagateInvalidSecondVotes(ElectionArea<?> area, int amount) {
        if(amount < 0 || PropagationHelper.unlockedNumberOfVotes(area) < amount) {
            return false;
        }
        
        ArrayList<ElectionArea<?>> unlocked = this.unlockedChildren(area);
        if(unlocked.size() == 0) {
            // all children locked
            return false;
        }
        int[] votes = unlocked.stream().mapToInt(a -> PropagationHelper.unlockedInvalidSecondVotes(a)).toArray();
        int[] maxVotes = unlocked.stream().mapToInt(a -> PropagationHelper.unlockedNumberOfVotes(a)).toArray();
        
        this.propagate(votes, maxVotes, amount - PropagationHelper.unlockedInvalidSecondVotes(area));
        
        for(int i = 0; i < votes.length; i++) {
            unlocked.get(i).changeInvalidSecondVotes(votes[i]);
        }
        return true;
    }
    
    /**
     * Change the amount of first votes in this area for a party and propagate the changes with the set propagation method over parties present in the
     * constituencies under this election area. Tries to take the votes from / give the votes to the valid votes first, error if the new amount is greater than
     * the number of cast votes.
     * 
     * @param party
     *            The party to change the votes for.
     * @param area
     *            The area to change the votes for.
     * @param amount
     *            The new amount
     * @return True on success, else false (no changes will happen in this case).
     */
    boolean propagateFirstVotes(ElectionArea<?> area, Party party, int amount) {
        if(Integer.MAX_VALUE - (PropagationHelper.unlockedValidFirstVotes(area, party) - PropagationHelper.unlockedFirstVotes(area, party)) < amount) {
            // an integer overflow will occur
            return false;
        }
        int newAmount = PropagationHelper.unlockedValidFirstVotes(area, party) - PropagationHelper.unlockedFirstVotes(area, party) + amount;
        
        if(amount < 0 || PropagationHelper.unlockedEligibleVoters(area, party) < newAmount) {
            return false;
        }
        
        ArrayList<ElectionArea<?>> unlockedPresent = this.unlockedChildren(area, party);
        if(unlockedPresent.size() == 0) {
            // all children locked
            return false;
        }
        int[] votes = unlockedPresent.stream().mapToInt(a -> PropagationHelper.unlockedFirstVotes(a, party)).toArray();
        
        int[] maxVotes = new int[unlockedPresent.size()];
        for(int i = 0; i < maxVotes.length; i++) {
            ElectionArea<?> a = unlockedPresent.get(i);
            maxVotes[i] = PropagationHelper.unlockedEligibleVoters(a, party) - PropagationHelper.unlockedNumberOfVotes(a, party)
                          + PropagationHelper.unlockedInvalidFirstVotes(a, party) + PropagationHelper.unlockedFirstVotes(a, party);
        }
        
        this.propagate(votes, maxVotes, amount - PropagationHelper.unlockedFirstVotes(area, party));
        
        for(int i = 0; i < votes.length; i++) {
            unlockedPresent.get(i).changeFirstVotes(party, votes[i]);
        }
        return true;
    }
    
    /**
     * Change the amount of second votes in this area for a party and propagate the changes with the set propagation method over parties present in the
     * constituencies under this election area. Tries to take the votes from / give the votes to the valid votes first, error if the new amount is greater than
     * the number of cast votes.
     * 
     * @param party
     *            The party to change the votes for.
     * @param area
     *            The area to change the votes for.
     * @param amount
     *            The new amount
     * @return True on success, else false (no changes will happen in this case).
     */
    boolean propagateSecondVotes(ElectionArea<?> area, Party party, int amount) {
        if(Integer.MAX_VALUE - (PropagationHelper.unlockedValidSecondVotes(area, party) - PropagationHelper.unlockedSecondVotes(area, party)) < amount) {
            // an integer overflow will occur
            return false;
        }
        int newAmount = PropagationHelper.unlockedValidSecondVotes(area, party) - PropagationHelper.unlockedSecondVotes(area, party) + amount;
        
        if(amount < 0 || PropagationHelper.unlockedEligibleVoters(area, party) < newAmount) {
            return false;
        }
        
        ArrayList<ElectionArea<?>> unlockedPresent = this.unlockedChildren(area, party);
        if(unlockedPresent.size() == 0) {
            // all children locked
            return false;
        }
        int[] votes = unlockedPresent.stream().mapToInt(a -> PropagationHelper.unlockedSecondVotes(a, party)).toArray();
        
        int[] maxVotes = new int[unlockedPresent.size()];
        for(int i = 0; i < maxVotes.length; i++) {
            ElectionArea<?> a = unlockedPresent.get(i);
            maxVotes[i] = PropagationHelper.unlockedEligibleVoters(a, party) - PropagationHelper.unlockedNumberOfVotes(a, party)
                          + PropagationHelper.unlockedInvalidSecondVotes(a, party) + PropagationHelper.unlockedSecondVotes(a, party);
        }
        
        this.propagate(votes, maxVotes, amount - PropagationHelper.unlockedSecondVotes(area, party));
        
        for(int i = 0; i < votes.length; i++) {
            unlockedPresent.get(i).changeSecondVotes(party, votes[i]);
        }
        return true;
    }
    
    /**
     * Propagate the amount citizens with the set propagation method. If the new amount is lower than the amount of eligible voters or the number of votes, they
     * will be set to this amount (this will cause an adaptation of the cast votes over all unlocked parties with the set propagation method).
     * 
     * @param area
     *            The area to change the votes for.
     * @param amount
     *            The new amount
     * @return True on success, else false (no changes will happen in this case).
     */
    boolean propagateCitizensConstituency(Constituency area, int amount) {
        if(amount < 0 || area.isLocked()) {
            return false;
        }
        
        area.setCitizens(amount);
        if(amount < area.getEligibleVoters()) {
            area.setEligibleVoters(amount);
            if(amount < area.getNumberOfVotes()) {
                this.setNumberOfVotesProportional(area, amount);
            }
        }
        return true;
    }
    
    /**
     * Sets the number of votes in the given area to the given amount. Also, the ratio between first votes and second votes will be adapted, so it stays the
     * same.
     * 
     * @param area
     *            the area to change the number of votes
     * @param amount
     *            the new amount
     */
    private void setNumberOfVotesProportional(Constituency area, int amount) {
        int oldNumber = area.getNumberOfVotes();
        if(oldNumber == 0) {
            this.setValidFirstVotes(area, (int) Math.round((double) amount / 2));
            this.setValidSecondVotes(area, (int) Math.round((double) amount / 2));
        }
        else {
            assert oldNumber > 0 : "Die Anzahl an Wählern muss positiv sein.";
            this.setValidSecondVotes(area, (int) Math.round((double) area.getValidSecondVotes() / oldNumber * amount));
            this.setValidFirstVotes(area, (int) Math.round((double) area.getValidFirstVotes() / oldNumber * amount));
        }
        area.setNumberOfVotes(amount);
    }
    
    /**
     * Change the amount eligible voters in the constituency. If the new amount is lower than the amount of cast votes, they will be set to this amount (this
     * will cause an adaptation of the cast votes over all unlocked parties with the set propagation method). Is the new amount greater than the citizens, they
     * will be set to this amount.
     * 
     * @param area
     *            The area to change the votes for.
     * @param amount
     *            The new amount
     * @return True on success, else false (no changes will happen in this case).
     */
    boolean propagateEligibleVotersConstituency(Constituency area, int amount) {
        if(amount < 0 || area.isLocked()) {
            return false;
        }
        assert (amount > area.getCitizens() & amount < area.getNumberOfVotes()) == false : "Inconsistent data.";
        
        if(amount > area.getCitizens()) {
            area.setCitizens(amount);
        }
        if(amount < area.getNumberOfVotes()) {
            this.setNumberOfVotesProportional(area, amount);
        }
        area.setEligibleVoters(amount);
        return true;
    }
    
    /**
     * Propagate the amount of votes (valid and invalid) with the set propagation method. The changed votes will raise the amount of eligible voters and / or
     * citizens if necessary (the ratio between valid and invalid stays the same). If the votes are decreased, all unlocked parties will loose votes, but the
     * ratio between valid and invalid votes will stays the same.
     * 
     * @param area
     *            The area to change the votes for.
     * @param amount
     *            The new amount
     * @return True on success, else false (no changes will happen in this case).
     */
    boolean propagateNumberOfVotesConstituency(Constituency area, int amount) {
        if(amount < 0 || area.isLocked()) {
            return false;
        }
        
        if(amount > area.getEligibleVoters()) {
            area.setEligibleVoters(amount);
            if(amount > area.getCitizens()) {
                area.setCitizens(amount);
            }
        }
        this.setNumberOfVotesProportional(area, amount);
        return true;
    }
    
    /**
     * Change the amount of valid first votes in this area and propagate the changes with the set propagation method over all unlocked parties. Tries to take
     * the votes from / give the votes to the invalid votes first, error if the amount is greater than the number of cast votes.
     * 
     * @param area
     *            The area to change the votes for.
     * @param amount
     *            The new amount
     * @return True on success, else false (no changes will happen in this case).
     */
    boolean propagateValidFirstVotesConstituency(Constituency area, int amount) {
        if(amount < 0 || amount > area.getNumberOfVotes() || area.isLocked()) {
            return false;
        }
        return this.setValidFirstVotes(area, amount);
    }
    
    private boolean setValidFirstVotes(Constituency area, int amount) {
        ArrayList<Party> parties = new ArrayList<Party>();
        parties.addAll(area.getParties());
        if(parties.isEmpty()) {
            // no parties to propagate
            return false;
        }
        
        int[] votes = new int[parties.size()];
        for(int i = 0; i < votes.length; i++) {
            votes[i] = area.getFirstVotes(parties.get(i));
        }
        
        this.propagateWithoutConstraint(votes, amount - area.getValidFirstVotes());
        
        for(int i = 0; i < votes.length; i++) {
            area.setFirstVotes(parties.get(i), votes[i]);
        }
        return true;
    }
    
    /**
     * Change the amount of valid second votes in this area and propagate the changes with the set propagation method over all unlocked parties. Tries to take
     * the votes from / give the votes to the invalid votes first, error if the amount is greater than the number of cast votes.
     * 
     * @param area
     *            The area to change the votes for.
     * @param amount
     *            The new amount
     * @return True on success, else false (no changes will happen in this case).
     */
    boolean propagateValidSecondVotesConstituency(Constituency area, int amount) {
        if(amount < 0 || amount > area.getNumberOfVotes() || area.isLocked()) {
            return false;
        }
        return this.setValidSecondVotes(area, amount);
    }
    
    private boolean setValidSecondVotes(Constituency area, int amount) {
        ArrayList<Party> parties = new ArrayList<Party>();
        parties.addAll(area.getParties());
        if(parties.isEmpty()) {
            // no parties to propagate
            return false;
        }
        
        int[] votes = new int[parties.size()];
        for(int i = 0; i < votes.length; i++) {
            votes[i] = area.getSecondVotes(parties.get(i));
        }
        
        this.propagateWithoutConstraint(votes, amount - area.getValidSecondVotes());
        
        for(int i = 0; i < votes.length; i++) {
            area.setSecondVotes(parties.get(i), votes[i]);
        }
        return true;
    }
    
    /**
     * Change the amount of invalid first votes in this area and propagate the changes with the set propagation method over all unlocked parties. Tries to take
     * the votes from / give the votes to the valid votes first, error if the amount is greater than the number of cast votes.
     * 
     * @param area
     *            The area to change the votes for.
     * @param amount
     *            The new amount
     * @return True on success, else false (no changes will happen in this case).
     */
    boolean propagateInvalidFirstVotesConstituency(Constituency area, int amount) {
        if(amount < 0 || amount > area.getNumberOfVotes() || area.isLocked()) {
            return false;
        }
        return this.setValidFirstVotes(area, area.getNumberOfVotes() - amount);
    }
    
    /**
     * Change the amount of invalid second votes in this area and propagate the changes with the set propagation method over all unlocked parties. Tries to take
     * the votes from / give the votes to the valid votes first, error if the amount is greater than the number of cast votes.
     *
     * @param area
     *            The area to change the votes for.
     * @param amount
     *            The new amount
     * @return True on success, else false (no changes will happen in this case).
     */
    boolean propagateInvalidSecondVotesConstituency(Constituency area, int amount) {
        if(amount < 0 || amount > area.getNumberOfVotes() || area.isLocked()) {
            return false;
        }
        return this.setValidSecondVotes(area, area.getNumberOfVotes() - amount);
    }
    
    /**
     * Change the amount of first votes in this area for a party. Tries to take the votes from / give the votes to the invalid votes first, error if the new
     * amount of cast votes is greater than the number of old cast votes.
     * 
     * @param party
     *            The party to change the votes for.
     * @param area
     *            The area to change the votes for.
     * @param amount
     *            The new amount
     * @return True on success, else false (no changes will happen in this case).
     */
    boolean propagateFirstVotesConstituency(Constituency area, Party party, int amount) {
        if(!area.isPartyPresent(party)) {
            return false;
        }
        if(Integer.MAX_VALUE - (area.getValidFirstVotes() - area.getFirstVotes(party)) < amount) {
            // an integer overflow will occur
            return false;
        }
        int new_first_votes = area.getValidFirstVotes() - area.getFirstVotes(party) + amount;
        if(amount < 0 || new_first_votes > area.getEligibleVoters() || area.isLocked()) {
            return false;
        }
        
        if(new_first_votes > area.getNumberOfVotes()) {
            area.setNumberOfVotes(new_first_votes);
        }
        area.setFirstVotes(party, amount);
        
        return true;
    }
    
    /**
     * Change the amount of second votes in this area for a party. Tries to take the votes from / give the votes to the invalid second first, error if the new
     * amount of cast votes is greater than the number of old cast votes.
     * 
     * @param party
     *            The party to change the votes for.
     * @param area
     *            The area to change the votes for.
     * @param amount
     *            The new amount
     * @return True on success, else false (no changes will happen in this case).
     */
    boolean propagateSecondVotesConstituency(Constituency area, Party party, int amount) {
        if(!area.isPartyPresent(party)) {
            return false;
        }
        
        if(Integer.MAX_VALUE - (area.getValidSecondVotes() - area.getSecondVotes(party)) < amount) {
            // an integer overflow will occur
            return false;
        }
        int new_second_votes = area.getValidSecondVotes() - area.getSecondVotes(party) + amount;
        if(amount < 0 || new_second_votes > area.getEligibleVoters() || area.isLocked()) {
            return false;
        }
        
        if(new_second_votes > area.getNumberOfVotes()) {
            area.setNumberOfVotes(new_second_votes);
        }
        area.setSecondVotes(party, amount);
        return true;
    }
    
    @Override
    public Propagation deepClone() {
        return new Propagation(this.math);
    }
    
    /**
     * Returns available concrete implementations of {@link PropagationMath}. The returned objects either do not need any configuration or default
     * configurations are set.
     * 
     * @return A list of objects that implement the {@link PropagationMath} interface.
     */
    public static List<PropagationMath> getMethods() {
        return Propagation.METHODS;
    }
    
    static {
        Propagation.METHODS.add(new ProportionalDistribution());
        Propagation.METHODS.add(new UniformDistribution());
        Propagation.METHODS.add(new NormalDistribution());
    }
    
}
