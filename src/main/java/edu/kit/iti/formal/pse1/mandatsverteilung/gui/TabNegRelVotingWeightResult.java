/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.gui;

import java.util.List;

import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Election;
import edu.kit.iti.formal.pse1.mandatsverteilung.simulation.NegativeRelativeVotingWeight;
import edu.kit.iti.formal.pse1.mandatsverteilung.simulation.NegativeRelativeVotingWeightElement;

/**
 * The {@link TabNegRelVotingWeightResult} is a Tab which holds a Table with the results of a {@link NegativeRelativeVotingWeight} algorithm as a table.
 * 
 * @author Felix Heim
 * @version 1
 */
public class TabNegRelVotingWeightResult extends Tab {
    /** the descriptions of the table */
    private static final String[] DESCRIPTIONS = {"Partei", "Bundesland", "Stimmen", "ge\u00E4nderte Stimmen", "Sitze/Gesamtsitze",
                                                  "ge\u00E4nderte Sitze/Gesamtsitze", "rel. Fraktionsgr\u00F6\u00DFe",
                                                  "ge\u00E4nderte rel. Fraktionsgr\u00F6\u00DFe"};
    
    private String name;
    
    /** the content pane of this tab */
    private JScrollPane overlay;
    
    /**
     * @param name
     *            the name of the result (normally the name of the {@link Election} who started the algorithm)
     * @param elements
     *            the results of the algorithm to display
     */
    public TabNegRelVotingWeightResult(String name, List<NegativeRelativeVotingWeightElement> elements) {
        this.name = name;
        
        // converts NegativeRelativeVotingWeightElements to two dimensional array, where each line contains the values of on element
        Object[][] data = elements.stream().map(element -> element.toParamStrings().toArray()).toArray(Object[][]::new);
        
        // sets the "contentPane" to a new JScrollPane containing a Table with the Elements using the default AbstractModel from JTable, but non-editable cells
        JTable table = new JTable(new AbstractTableModel() {
            private static final long serialVersionUID = 1L;
            
            @Override
            public String getColumnName(int column) {
                return TabNegRelVotingWeightResult.DESCRIPTIONS[column];
            }
            
            @Override
            public int getRowCount() {
                return data.length;
            }
            
            @Override
            public int getColumnCount() {
                return TabNegRelVotingWeightResult.DESCRIPTIONS.length;
            }
            
            @Override
            public Object getValueAt(int row, int col) {
                return data[row][col];
            }
            
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
            
            @Override
            public void setValueAt(Object value, int row, int col) {
                throw new UnsupportedOperationException("Values can't be modified");
            }
        });
        this.overlay = GuiHelper.createScrollPane(table);
    }
    
    @Override
    public JComponent getContentPane() {
        return this.overlay;
    }
    
    /** @return the name to be displayed in the Tab */
    public String getName() {
        return this.name;
    }
}
