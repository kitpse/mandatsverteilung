/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.data.model;

import java.util.LinkedList;
import java.util.List;

/**
 * Class for creation and deep cloning ({@link DeepCloneable}) of a well-defined {@link ElectionArea}-hierarchy consisting of one {@link Federation} as root, 16
 * {@link FederalState}s and 299 constituencies ({@link Constituency}).
 * 
 * @author Nils Wilka
 * @version 1.2
 */
class DataStructureFactory {
    
    private static String[] stateNames = {"Schleswig-Holstein", "Hamburg", "Niedersachsen", "Bremen", "Nordrhein-Westfalen", "Hessen", "Rheinland-Pfalz",
                                          "Baden-Wuerttemberg", "Bayern", "Saarland", "Berlin", "Brandenburg", "Mecklenburg-Vorpommern", "Sachsen",
                                          "Sachsen-Anhalt", "Thueringen"};
    
    private static String[][] constituencyNames = new String[Election.NUMBER_STATES][];
    
    static {
        /* Effective 2013 */
        
        // Schleswig-Holstein
        DataStructureFactory.constituencyNames[0] = new String[]{"Flensburg - Schleswig (001)", "Nordfriesland - Dithmarschen Nord (002)",
                                                                 "Steinburg - Dithmarschen Sued (003)", "Rendsburg-Eckernfoerde (004)", "Kiel (005)",
                                                                 "Ploen - Neumuenster (006)", "Pinneberg (007)", "Segeberg - Stormarn-Mitte (008)",
                                                                 "Ostholstein - Stormarn-Nord (009)", "Herzogtum Lauenburg - Stormarn-Sued (010)",
                                                                 "Luebeck (011)"};
        // Hamburg
        DataStructureFactory.constituencyNames[1] = new String[]{"Hamburg-Mitte (018)", "Hamburg-Altona (019)", "Hamburg-Eimsbuettel (020)",
                                                                 "Hamburg-Nord (021)", "Hamburg-Wandsbek (022)", "Hamburg-Bergedorf - Harburg (023)"};
        // Niedersachsen
        DataStructureFactory.constituencyNames[2] = new String[]{"Aurich - Emden (024)", "Unterems (025)", "Friesland - Wilhelmshaven - Wittmund (026)",
                                                                 "Oldenburg - Ammerland (027)", "Delmenhorst - Wesermarsch - Oldenburg-Land (028)",
                                                                 "Cuxhaven - Stade II (029)", "Stade I - Rotenburg II (030)", "Mittelems (031)",
                                                                 "Cloppenburg - Vechta (032)", "Diepholz - Nienburg I (033)", "Osterholz - Verden (034)",
                                                                 "Rotenburg I - Heidekreis (035)", "Harburg (036)", "Luechow-Dannenberg - Lueneburg (037)",
                                                                 "Osnabrueck-Land (038)", "Stadt Osnabrueck (039)", "Nienburg II - Schaumburg (040)",
                                                                 "Stadt Hannover I (041)", "Stadt Hannover II (042)", "Hannover-Land I (043)",
                                                                 "Celle - Uelzen (044)", "Gifhorn - Peine (045)", "Hameln-Pyrmont - Holzminden (046)",
                                                                 "Hannover-Land II (047)", "Hildesheim (048)", "Salzgitter - Wolfenbuettel (049)",
                                                                 "Braunschweig (050)", "Helmstedt - Wolfsburg (051)", "Goslar - Northeim - Osterode (052)",
                                                                 "Goettingen (053)"};
        // Bremen
        DataStructureFactory.constituencyNames[3] = new String[]{"Bremen I (054)", "Bremen II - Bremerhaven (055)"};
        
        // Nordrhein-Westfalen
        DataStructureFactory.constituencyNames[4] = new String[]{"Aachen I (087)", "Aachen II (088)", "Heinsberg (089)", "Dueren (090)",
                                                                 "Rhein-Erft-Kreis I (091)", "Euskirchen - Rhein-Erft-Kreis II (092)", "Koeln I (093)",
                                                                 "Koeln II (094)", "Koeln III (095)", "Bonn (096)", "Rhein-Sieg-Kreis I (097)",
                                                                 "Rhein-Sieg-Kreis II (098)", "Oberbergischer Kreis (099)", "Rheinisch-Bergischer Kreis (100)",
                                                                 "Leverkusen - Koeln IV (101)", "Wuppertal I (102)",
                                                                 "Solingen - Remscheid - Wuppertal II (103)", "Mettmann I (104)", "Mettmann II (105)",
                                                                 "Duesseldorf I (106)", "Duesseldorf II (107)", "Neuss I (108)", "Moenchengladbach (109)",
                                                                 "Krefeld I - Neuss II (110)", "Viersen (111)", "Kleve (112)", "Wesel I (113)",
                                                                 "Krefeld II - Wesel II (114)", "Duisburg I (115)", "Duisburg II (116)",
                                                                 "Oberhausen - Wesel III (117)", "Muelheim - Essen I (118)", "Essen II (119)",
                                                                 "Essen III (120)", "Recklinghausen I (121)", "Recklinghausen II (122)", "Gelsenkirchen (123)",
                                                                 "Steinfurt I - Borken I (124)", "Bottrop - Recklinghausen III (125)", "Borken II (126)",
                                                                 "Coesfeld - Steinfurt II (127)", "Steinfurt III (128)", "Muenster (129)", "Warendorf (130)",
                                                                 "Guetersloh I (131)", "Bielefeld - Guetersloh II (132)",
                                                                 "Herford - Minden-Luebbecke II (133)", "Minden-Luebbecke I (134)", "Lippe I (135)",
                                                                 "Hoexter - Lippe II (136)", "Paderborn - Guetersloh III (137)",
                                                                 "Hagen - Ennepe-Ruhr-Kreis I (138)", "Ennepe-Ruhr-Kreis II (139)", "Bochum I (140)",
                                                                 "Herne - Bochum II (141)", "Dortmund I (142)", "Dortmund II (143)", "Unna I (144)",
                                                                 "Hamm - Unna II (145)", "Soest (146)", "Hochsauerlandkreis (147)",
                                                                 "Siegen-Wittgenstein (148)", "Olpe - Maerkischer Kreis I (149)", "Maerkischer Kreis II (150)"};
        // Hessen
        DataStructureFactory.constituencyNames[5] = new String[]{"Waldeck (167)", "Kassel (168)", "Werra-Meissner - Hersfeld-Rotenburg (169)",
                                                                 "Schwalm-Eder (170)", "Marburg (171)", "Lahn-Dill (172)", "Giessen (173)", "Fulda (174)",
                                                                 "Main-Kinzig - Wetterau II - Schotten (175)", "Hochtaunus (176)", "Wetterau I (177)",
                                                                 "Rheingau-Taunus - Limburg (178)", "Wiesbaden (179)", "Hanau (180)", "Main-Taunus (181)",
                                                                 "Frankfurt am Main I (182)", "Frankfurt am Main II (183)", "Gross-Gerau (184)",
                                                                 "Offenbach (185)", "Darmstadt (186)", "Odenwald (187)", "Bergstrasse (188)"};
        
        // Rheinland-Pfalz
        DataStructureFactory.constituencyNames[6] = new String[]{"Neuwied (198)", "Ahrweiler (199)", "Koblenz (200)", "Mosel/Rhein-Hunsrueck (201)",
                                                                 "Kreuznach (202)", "Bitburg (203)", "Trier (204)", "Montabaur (205)", "Mainz (206)",
                                                                 "Worms (207)", "Ludwigshafen/Frankenthal (208)", "Neustadt - Speyer (209)",
                                                                 "Kaiserslautern (210)", "Pirmasens (211)", "Suedpfalz (212)"};
        // Baden-Württemberg
        DataStructureFactory.constituencyNames[7] = new String[]{"Stuttgart I (258)", "Stuttgart II (259)", "Boeblingen (260)", "Esslingen (261)",
                                                                 "Nuertingen (262)", "Goeppingen (263)", "Waiblingen (264)", "Ludwigsburg (265)",
                                                                 "Neckar-Zaber (266)", "Heilbronn (267)", "Schwaebisch Hall - Hohenlohe (268)",
                                                                 "Backnang - Schwaebisch Gmuend (269)", "Aalen - Heidenheim (270)", "Karlsruhe-Stadt (271)",
                                                                 "Karlsruhe-Land (272)", "Rastatt (273)", "Heidelberg (274)", "Mannheim (275)",
                                                                 "Odenwald - Tauber (276)", "Rhein-Neckar (277)", "Bruchsal - Schwetzingen (278)",
                                                                 "Pforzheim (279)", "Calw (280)", "Freiburg (281)", "Loerrach - Muellheim (282)",
                                                                 "Emmendingen - Lahr (283)", "Offenburg (284)", "Rottweil - Tuttlingen (285)",
                                                                 "Schwarzwald-Baar (286)", "Konstanz (287)", "Waldshut (288)", "Reutlingen (289)",
                                                                 "Tuebingen (290)", "Ulm (291)", "Biberach (292)", "Bodensee (293)", "Ravensburg (294)",
                                                                 "Zollernalb - Sigmaringen (295)"};
        // Bayern
        DataStructureFactory.constituencyNames[8] = new String[]{"Altoetting (213)", "Erding - Ebersberg (214)", "Freising (215)", "Fuerstenfeldbruck (216)",
                                                                 "Ingolstadt (217)", "Muenchen-Nord (218)", "Muenchen-Ost (219)", "Muenchen-Sued (220)",
                                                                 "Muenchen-West/Mitte (221)", "Muenchen-Land (222)", "Rosenheim (223)", "Starnberg (224)",
                                                                 "Traunstein (225)", "Weilheim (226)", "Deggendorf (227)", "Landshut (228)", "Passau (229)",
                                                                 "Rottal-Inn (230)", "Straubing (231)", "Amberg (232)", "Regensburg (233)", "Schwandorf (234)",
                                                                 "Weiden (235)", "Bamberg (236)", "Bayreuth (237)", "Coburg (238)", "Hof (239)",
                                                                 "Kulmbach (240)", "Ansbach (241)", "Erlangen (242)", "Fuerth (243)", "Nuernberg-Nord (244)",
                                                                 "Nuernberg-Sued (245)", "Roth (246)", "Aschaffenburg (247)", "Bad Kissingen (248)",
                                                                 "Main-Spessart (249)", "Schweinfurt (250)", "Wuerzburg (251)", "Augsburg-Stadt (252)",
                                                                 "Augsburg-Land (253)", "Donau-Ries (254)", "Neu-Ulm (255)", "Oberallgaeu (256)",
                                                                 "Ostallgaeu (257)"};
        // Saarland
        DataStructureFactory.constituencyNames[9] = new String[]{"Saarbruecken (296)", "Saarlouis (297)", "St. Wendel (298)", "Homburg (299)"};
        
        // Berlin
        DataStructureFactory.constituencyNames[10] = new String[]{"Berlin-Mitte (075)", "Berlin-Pankow (076)", "Berlin-Reinickendorf (077)",
                                                                  "Berlin-Spandau - Charlottenburg Nord (078)", "Berlin-Steglitz - Zehlendorf (079)",
                                                                  "Berlin-Charlottenburg - Wilmersdorf (080)", "Berlin-Tempelhof - Schoeneberg (081)",
                                                                  "Berlin-Neukoelln (082)", "Berlin-Friedrichshain-Kreuzberg - Prenzlauer Berg Ost (083)",
                                                                  "Berlin-Treptow - Koepenick (084)", "Berlin-Marzahn - Hellersdorf (085)",
                                                                  "Berlin-Lichtenberg (086)"};
        // Brandenburg
        DataStructureFactory.constituencyNames[11] = new String[]{"Prignitz - Ostprignitz-Ruppin - Havelland I (056)", "Uckermark - Barnim I (057)",
                                                                  "Oberhavel - Havelland II (058)", "Maerkisch-Oderland - Barnim II (059)",
                                                                  "Brandenburg an der Havel - Potsdam-Mittelmark I - Havelland III - Teltow-Flaeming I (060)",
                                                                  "Potsdam - Potsdam-Mittelmark II - Teltow-Flaeming II (061)",
                                                                  "Dahme-Spreewald - Teltow-Flaeming III - Oberspreewald-Lausitz I (062)",
                                                                  "Frankfurt (Oder) - Oder-Spree (063)", "Cottbus - Spree-Neisse (064)",
                                                                  "Elbe-Elster - Oberspreewald-Lausitz II (065)"};
        // Mecklenburg-Vorpommern
        DataStructureFactory.constituencyNames[12] = new String[]{"Schwerin - Ludwigslust-Parchim I - Nordwestmecklenburg I (012)",
                                                                  "Ludwigslust-Parchim II - Nordwestmecklenburg II - Landkreis Rostock I (013)",
                                                                  "Rostock - Landkreis Rostock II (014)", "Vorpommern-Ruegen - Vorpommern-Greifswald I (015)",
                                                                  "Mecklenburgische Seenplatte I - Vorpommern-Greifswald II (016)",
                                                                  "Mecklenburgische Seenplatte II - Landkreis Rostock III (017)"};
        // Sachsen
        DataStructureFactory.constituencyNames[13] = new String[]{"Nordsachsen (151)", "Leipzig I (152)", "Leipzig II (153)", "Leipzig-Land (154)",
                                                                  "Meissen (155)", "Bautzen I (156)", "Goerlitz (157)",
                                                                  "Saechsische Schweiz - Osterzgebirge (158)", "Dresden I (159)",
                                                                  "Dresden II - Bautzen II (160)", "Mittelsachsen (161)", "Chemnitz (162)",
                                                                  "Chemnitzer Umland - Erzgebirgskreis II (163)", "Erzgebirgskreis I (164)", "Zwickau (165)",
                                                                  "Vogtlandkreis (166)"};
        // Sachsen-Anhalt
        DataStructureFactory.constituencyNames[14] = new String[]{"Altmark (066)", "Boerde - Jerichower Land (067)", "Harz (068)", "Magdeburg (069)",
                                                                  "Dessau - Wittenberg (070)", "Anhalt (071)", "Halle (072)", "Burgenland - Saalekreis (073)",
                                                                  "Mansfeld (074)"};
        
        // Thüringen
        DataStructureFactory.constituencyNames[15] = new String[]{"Eichsfeld - Nordhausen - Unstrut-Hainich-Kreis I (189)",
                                                                  "Eisenach - Wartburgkreis - Unstrut-Hainich-Kreis II (190)",
                                                                  "Kyffhaeuserkreis - Soemmerda - Weimarer Land I (191)", "Gotha - Ilm-Kreis (192)",
                                                                  "Erfurt - Weimar - Weimarer Land II (193)", "Gera - Jena - Saale-Holzland-Kreis (194)",
                                                                  "Greiz - Altenburger Land (195)", "Sonneberg - Saalfeld-Rudolstadt - Saale-Orla-Kreis (196)",
                                                                  "Suhl - Schmalkalden-Meiningen - Hildburghausen (197)"};
    };
    
    /**
     * Private to avoid instantiation.
     */
    private DataStructureFactory() {
    }
    
    /**
     * Creates a new {@link ElectionArea}-hierarchy consisting of one {@link Federation} as root, 16 {@link FederalState}s and 299 constituencies (
     * {@link Constituency}).
     * 
     * @param result
     *            The {@link ElectionResult} to create the {@link ElectionArea}-hierarchy for.
     * @return The root of the {@link ElectionArea}-hierarchy.
     */
    static Federation createDataHierarchy(ElectionResult result) {
        // assert there are Election.NUMBER_CONSTITUENCIES many names in constituencyNames.
        // assert there are Election.NUMBER_STATES many names in stateNames.
        Federation root = DataStructureFactory.createFederation(result);
        List<FederalState> states = DataStructureFactory.createFederalStates(result);
        states.stream().forEach(root::addChild); // Add states as children to federation.
        List<Constituency> constituencies = DataStructureFactory.createConstituencies(result); // Create constituencies.
        int current = 0; // Add some constituencies to every state as children.
        for(int i = 0; i < DataStructureFactory.constituencyNames.length; i++) {
            // assert federal states and constituencies have been created in correct order.
            for(int j = 0; j < DataStructureFactory.constituencyNames[i].length; j++, current++) {
                states.get(i).addChild(constituencies.get(current));
            }
        }
        return root;
    }
    
    /**
     * Deep clones a given {@link ElectionArea}-hierarchy and all associated Objects. Listeners are not copied.
     * 
     * @param clone
     *            The {@link ElectionResult} clone.
     * @param result
     *            The {@link ElectionResult} to deep clone the {@link ElectionArea}-hierarchy from.
     * @return The deep cloned {@link ElectionArea}-hierarchy.
     */
    static Federation cloneStructure(ElectionResult clone, ElectionResult result) {
        Federation federation = result.getFederation();
        Federation federationCopy = new Federation(federation.getName(), clone);
        List<Party> copiedParties = federation.copyParties();
        
        for(FederalState state : federation.getChildren()) {
            FederalState stateCopy = new FederalState(state.getName(), clone);
            
            for(Constituency constituency : state.getChildren()) {
                Constituency constituencyCopy = new Constituency(constituency.getName(), clone);
                constituencyCopy.setCitizens(constituency.getCitizens());
                constituencyCopy.setEligibleVoters(constituency.getEligibleVoters());
                constituencyCopy.setNumberOfVotes(constituency.getNumberOfVotes());
                
                for(Party party : constituency.getParties()) {
                    Party searchedParty = null;
                    for(Party partyCopy : copiedParties) {
                        if(party.equals(partyCopy)) {
                            searchedParty = partyCopy;
                            break;
                        }
                    }
                    constituencyCopy.addParty(searchedParty);
                    constituencyCopy.changeFirstVotes(searchedParty, constituency.getFirstVotes(party));
                    constituencyCopy.changeSecondVotes(searchedParty, constituency.getSecondVotes(party));
                    if(constituency.isLocked(party)) {
                        constituencyCopy.lock(searchedParty);
                    }
                }
                stateCopy.addChild(constituencyCopy);
                if(constituency.isLocked()) {
                    constituencyCopy.lock();
                }
            }
            for(Party party : state.getParties()) {
                Party searchedParty = null;
                for(Party partyCopy : copiedParties) {
                    if(party.equals(partyCopy)) {
                        searchedParty = partyCopy;
                        break;
                    }
                }
                if(state.hasPartyCandidateCount(party)) {
                    stateCopy.changeCandidateCount(searchedParty, state.getCandidateCount(party));
                }
            }
            federationCopy.addChild(stateCopy);
            if(state.isLocked()) {
                stateCopy.lock();
            }
        }
        if(federation.isLocked()) {
            federationCopy.lock();
        }
        return federationCopy;
    }
    
    /**
     * Creates a new {@link Federation} with a standard name.
     * 
     * @param result
     *            The {@link ElectionResult} the {@link Federation} belongs to.
     * @return A new {@link Federation} with a standard name.
     */
    private static Federation createFederation(ElectionResult result) {
        return new Federation("Bundesgebiet", result);
    }
    
    /**
     * Creates {@link Election.NUMBER_STATES} many {@link FederalState}s.
     * 
     * @param result
     *            The {@link ElectionResult} the {@link FederalState}s belong to.
     * @return {@link Election.NUMBER_STATES} many {@link FederalState}s.
     */
    private static List<FederalState> createFederalStates(ElectionResult result) {
        List<FederalState> list = new LinkedList<>();
        
        for(String name : DataStructureFactory.stateNames) {
            // The order is important.
            list.add(new FederalState(name, result));
        }
        return list;
    }
    
    /**
     * Creates {@link Election.NUMBER_STATES} many constituencies ({@link Constituency}).
     * 
     * @param result
     *            The {@link ElectionResult} the constituencies ({@link Constituency}) belong to.
     * @return {@link Election.NUMBER_STATES} many constituencies ({@link Constituency}) as list.
     */
    private static List<Constituency> createConstituencies(ElectionResult result) {
        List<Constituency> list = new LinkedList<>();
        
        for(String[] state : DataStructureFactory.constituencyNames) {
            // The order is important.
            for(String constituency : state) {
                list.add(new Constituency(constituency, result));
            }
        }
        return list;
    }
}
