/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.data.model;

/**
 * Interface for classes that want to be notified about the recalculation of the {@link Bundestag}.
 * 
 * @author Nils Wilka
 * @version 1.0
 */
public interface BundestagListener {
    
    /**
     * This method will be called if the observed {@link Bundestag} was recalculated.
     * 
     * @param bundestag
     *            The new created {@link Bundestag} to be listened on in future.
     */
    public void onChange(Bundestag bundestag);
}
