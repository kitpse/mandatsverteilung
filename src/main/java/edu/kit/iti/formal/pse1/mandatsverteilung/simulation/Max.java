/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.simulation;

import java.util.ArrayList;
import java.util.List;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Constituency;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Election;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.FederalState;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Party;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Propagation;
import edu.kit.iti.formal.pse1.mandatsverteilung.gui.controller.Mandatsverteilung;
import edu.kit.iti.formal.pse1.mandatsverteilung.propagation.PropagationMath;
import edu.kit.iti.formal.pse1.mandatsverteilung.propagation.ProportionalDistribution;

/**
 * This class contains the algorithm for finding to a given election result a similar one that results in a large number of seats in the parliament. *
 * 
 * @author Alexander Grünen
 *
 */
public class Max {
    private boolean abortAlgo = false;
    
    /**
     * This algorithm investigates how large the parliament could have become if the outcome of the election had been just slightly different. Its parameters
     * allow for adjusting how much the fictitious election result maximizing the parliament may differ from the actual outcome. Passing fvdelta = 0.2 and means
     * that for every constituency and every party, the outcome of the election may differ by that value, i.e. a party at each constituency may have 10 percent
     * less or more first votes. Passing svdelta = 0.05 means that for every state and every part, the outcome of the election may differ by 5 percent *
     * 
     * @param originalElection
     *            the original election result
     * @param fvdelta
     *            first votes delta: the maximal value any first vote result (of a party in a constituency) can be changed
     * @param svdelta
     *            second votes delta: the maximal value any second vote result (of a party in a constituency) can be changed
     * @return
     */
    public Election maximizeParliament(Election originalElection, double fvdelta, double svdelta) {
        /*
         * Since using a simulated annealing meta-heuristic has proven ineffective due to the discrete nature of target function, a hill climber algorithm with
         * a variable neighborhood has been implemented.
         */
        
        // set propagation to proportional propagation
        List<PropagationMath> methods = Propagation.getMethods(); // a list of all valid propagation methods
        PropagationMath propagationMethod = methods.get(0);
        originalElection.getElectionResult().getPropagation().setPropagation(propagationMethod);
        if(!(propagationMethod instanceof ProportionalDistribution)) {
            throw new IllegalStateException("Setting propagation to proportional distribution failed!");
        }
        
        Election bestElection = new Election(); // will be returned
        bestElection = originalElection.deepClone();
        Election altElection = new Election(); // alternative Election that is considered to replace the best election
        altElection = originalElection.deepClone();
        
        double r; // random probability
        boolean improvement = true; // improvement during local search at the end
        
        Party firstPlace; // original winner of a direct mandate
        Party secondPlace; // original second place in the battle for a direct mandate
        int firstPlaceVotes;
        int secondPlaceVotes;
        int votes;
        int i, n;
        int currentSize;
        int index;
        currentSize = bestElection.getBundestag().getSize();
        System.out.println("current size:" + currentSize);
        
        int altSize;
        
        int noc = originalElection.getSettings().getParliamentSize() / 2; // number of constituencies
        ArrayList<Constituency> orgWKs = new ArrayList<Constituency>();
        
        ArrayList<Party> nonSmallParties = new ArrayList<Party>();
        
        // create a list of flippable constituencies
        ArrayList<Integer> flippableWKIndices = new ArrayList<Integer>();
        ArrayList<Party> flippableWinners = new ArrayList<Party>();
        ArrayList<Party> flippableLosers = new ArrayList<Party>();
        ArrayList<Integer> stalemates = new ArrayList<Integer>(); // if winner and loser met in the middle
        ArrayList<Integer> orgVotesWinners = new ArrayList<Integer>();
        ArrayList<Integer> orgVotesLosers = new ArrayList<Integer>();
        
        // create array of all constituencies
        for(FederalState state : originalElection.getElectionResult().getFederation().getChildren()) {
            for(Constituency wk : state.getChildren()) {
                orgWKs.add(wk);
            }
        }
        
        // find out if a constituency is flippable
        for(int j = 0; j < noc; j++) {
            firstPlaceVotes = 0;
            secondPlaceVotes = 0;
            firstPlace = null;
            secondPlace = null;
            for(Party party : originalElection.getBundestag().getParties()) {
                votes = orgWKs.get(j).getFirstVotes(party);
                if(votes > firstPlaceVotes) {
                    secondPlace = firstPlace;
                    secondPlaceVotes = firstPlaceVotes;
                    firstPlace = party;
                    firstPlaceVotes = votes;
                }
                else if(votes > secondPlaceVotes) {
                    secondPlace = party;
                    secondPlaceVotes = votes;
                }
            }
            if(secondPlaceVotes * (1 + fvdelta) > firstPlaceVotes * (1 - fvdelta)) {
                flippableWKIndices.add(j);
                flippableWinners.add(firstPlace);
                flippableLosers.add(secondPlace);
                stalemates.add(firstPlaceVotes - (firstPlaceVotes - secondPlaceVotes) / 2);
                orgVotesWinners.add(firstPlaceVotes);
                orgVotesLosers.add(secondPlaceVotes);
            }
        }
        
        System.out.println("flippable constituencies: " + flippableWKIndices.size());
        
        // create array of all parties 
        for(Party party : originalElection.getElectionResult().getFederation().getParties()) {
            if((double) originalElection.getElectionResult().getFederation().getSecondVotes(party)
               / originalElection.getElectionResult().getFederation().getValidSecondVotes() > 0.01) {
                nonSmallParties.add(party);
            }
        }
        
        int steps; // number of times the inner loop has been executed\
        int level; // low level: intensify search; high level: diversify search
        
        steps = 0;
        improvement = false;
        while(steps < 1151) {
            if(improvement) {
                steps = 0;
                improvement = false;
            }
            level = (steps >> 7) + 1;
            
            if(Mandatsverteilung.debug) {
                System.out.println("size:" + currentSize + " steps:" + steps + " level:" + level);
            }
            
            altElection = bestElection.deepClone();
            
            // change a random number of direct mandates
            n = level;
            if(fvdelta > 0 && flippableWKIndices.size() > 0) {
                ArrayList<Constituency> altWKs = new ArrayList<Constituency>();
                for(FederalState state : altElection.getElectionResult().getFederation().getChildren()) {
                    for(Constituency wk : state.getChildren()) {
                        altWKs.add(wk);
                    }
                }
                for(int j = 0; j < n; j++) {
                    // create array of all constituencies 
                    
                    index = (int) (Math.random() * flippableWKIndices.size());
                    i = flippableWKIndices.get(index);
                    if(altWKs.get(i).getFirstVotes(flippableWinners.get(index)) > altWKs.get(i).getFirstVotes(flippableLosers.get(index))) {
                        altWKs.get(i).changeFirstVotes(flippableWinners.get(index), stalemates.get(index));
                        altWKs.get(i).changeFirstVotes(flippableLosers.get(index), stalemates.get(index) + 10);
                    }
                    else { // restore original values
                        altWKs.get(i).changeFirstVotes(flippableWinners.get(index), orgVotesWinners.get(index));
                        altWKs.get(i).changeFirstVotes(flippableLosers.get(index), orgVotesLosers.get(index));
                    }
                }
            }
            // change second votes in a number of states
            
            for(int j = 0; j < n; j++) {
                i = (int) (Math.random() * altElection.getElectionResult().getFederation().getChildren().size());
                FederalState altState = altElection.getElectionResult().getFederation().getChildren().get(i);
                FederalState orgState = originalElection.getElectionResult().getFederation().getChildren().get(i);
                i = (int) (Math.random() * nonSmallParties.size());
                r = Math.random();
                if(r < 0.33333) {
                    altState.changeSecondVotes(nonSmallParties.get(i),
                                               orgState.getSecondVotes(nonSmallParties.get(i))
                                                       - (int) Math.round(orgState.getSecondVotes(nonSmallParties.get(i)) * svdelta));
                }
                else if(r < 0.66666) {
                    altState.changeSecondVotes(nonSmallParties.get(i),
                                               orgState.getSecondVotes(nonSmallParties.get(i))
                                                       + (int) Math.round(orgState.getSecondVotes(nonSmallParties.get(i)) * svdelta));
                }
                else {
                    altState.changeSecondVotes(nonSmallParties.get(i), orgState.getSecondVotes(nonSmallParties.get(i)));
                }
            }
            altSize = altElection.getBundestag().getSize();
            
            if(altSize > currentSize) { // make change permanent
                improvement = true;
                currentSize = altSize;
                bestElection = altElection.deepClone();
                System.out.println("current size:" + currentSize);
            }
            if(this.abortAlgo) {
                return bestElection;
            }
            steps++;
        }
        
        // local search: change winner of constituencies if it results in a larger parliament
        if(fvdelta > 0) {
            do {
                improvement = false;
                for(index = 0; index < flippableWKIndices.size(); index++) {
                    altElection = bestElection.deepClone();
                    ArrayList<Constituency> altWKs = new ArrayList<Constituency>();
                    for(FederalState state : altElection.getElectionResult().getFederation().getChildren()) {
                        for(Constituency wk : state.getChildren()) {
                            altWKs.add(wk);
                        }
                    }
                    // create array of all constituencies 
                    i = flippableWKIndices.get(index);
                    if(altWKs.get(i).getFirstVotes(flippableWinners.get(index)) > altWKs.get(i).getFirstVotes(flippableLosers.get(index))) {
                        altWKs.get(i).changeFirstVotes(flippableWinners.get(index), stalemates.get(index));
                        altWKs.get(i).changeFirstVotes(flippableLosers.get(index), stalemates.get(index) + 1);
                    }
                    else { // restore original values
                        altWKs.get(i).changeFirstVotes(flippableWinners.get(index), orgVotesWinners.get(index));
                        altWKs.get(i).changeFirstVotes(flippableLosers.get(index), orgVotesLosers.get(index));
                    }
                    
                    altSize = altElection.getBundestag().getSize();
                    
                    if(altSize > currentSize) { // make change permanent
                        improvement = true;
                        currentSize = altSize;
                        bestElection = altElection.deepClone();
                    }
                }
                if(this.abortAlgo) {
                    return bestElection;
                }
            } while(improvement);
        }
        return bestElection;
    }
    
    public void abortSimulationAlgo() {
        this.abortAlgo = true;
    }
}
