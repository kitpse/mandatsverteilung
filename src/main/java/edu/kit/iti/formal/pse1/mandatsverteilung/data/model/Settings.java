/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.data.model;

import java.util.ArrayList;
import java.util.List;

import edu.kit.iti.formal.pse1.mandatsverteilung.calculation.AlternativeDistribution;
import edu.kit.iti.formal.pse1.mandatsverteilung.calculation.CalculationMethod;
import edu.kit.iti.formal.pse1.mandatsverteilung.calculation.Distribution2013;
import edu.kit.iti.formal.pse1.mandatsverteilung.calculation.SimpleDistribution;

/**
 * Stores informations about how the Bundestag of a Election should be calculated.
 * 
 * @author Ben Wilhelm
 * @author Felix Heim
 * @version 2
 */
public class Settings implements DeepCloneable<Settings> {
    private static final List<CalculationMethod> METHODS = new ArrayList<>(3);
    
    private boolean useNonVoterParty;
    private double percentageHurdle;
    private int sizeBundestag;
    private CalculationMethod calculationMethod;
    
    private List<ChangeListener> listeners = new ArrayList<>();
    
    /**
     * Creates a new Settings object with default settings.
     */
    public Settings() {
        this(false, 0.05, 598, Distribution2013.getMethod());
    }
    
    /**
     * Creates a new Settings object with the given values.
     * 
     * @param useNonVoterParty
     * @param percentageHurdle
     * @param sizeBundestag
     * @param calc
     * @throws IllegalArgumentException
     *             For {@code calc == null || !checkSizeBundestag(sizeBundestag) || !checkPercentageHurdle(percentageHurdle)}
     */
    public Settings(boolean useNonVoterParty, double percentageHurdle, int sizeBundestag, CalculationMethod calc) throws IllegalArgumentException {
        this.useNonVoterParty = useNonVoterParty;
        
        if(Settings.checkPercentageHurdle(percentageHurdle)) {
            this.percentageHurdle = percentageHurdle;
        }
        else {
            throw new IllegalArgumentException();
        }
        
        if(Settings.checkSizeBundestag(sizeBundestag)) {
            this.sizeBundestag = sizeBundestag;
        }
        else {
            throw new IllegalArgumentException();
        }
        if(calc != null) {
            this.calculationMethod = calc;
        }
        else {
            throw new IllegalArgumentException();
        }
    }
    
    /**
     * Checks for valid input.
     * 
     * @return True ff input is valid (>= 0 && <= 1.0), else false.
     */
    public static boolean checkPercentageHurdle(double percentageHurdle) {
        return percentageHurdle >= 0 && percentageHurdle <= 1.0;
    }
    
    /**
     * Checks for valid input.
     * 
     * @return True if input is valid (>= 1), else false.
     */
    public static boolean checkSizeBundestag(int sizeBundestag) {
        return sizeBundestag >= 1;
    }
    
    /**
     * Gets the description for the right input.
     * 
     * @return The description for the right input.
     */
    public static String getRangeHurdleDescription() {
        return "Prozenth\u00FCrde muss zwischen 0% und 100%! sein";
    }
    
    /**
     * Gets the description for the right input.
     * 
     * @return The description for the right input.
     */
    public static String getRangeSizeDescription() {
        return "Bundestaggrö\u00DFe muss grö\u00DFer als 0 sein!";
    }
    
    /**
     * Returns if a non Voter party is activated.
     * 
     * @return true, if non voter party is activated, else false.
     */
    public boolean useNonVoterParty() {
        return this.useNonVoterParty;
    }
    
    /**
     * Returns the current percent hurdle.
     * 
     * @return the percent hurdle.
     */
    public double getPercentHurdle() {
        return this.percentageHurdle;
    }
    
    /**
     * Returns the nominal size of the parliament.
     * 
     * @return the nominal size of the parliament.
     */
    public int getParliamentSize() {
        return this.sizeBundestag;
    }
    
    /**
     * Returns the current CalculationMethod.
     * 
     * @return the CalculationMethod.
     */
    public CalculationMethod getCalculationMethod() {
        return this.calculationMethod;
    }
    
    /**
     * Defines weather a non voter party is activated or not.
     * 
     * @param useNonVoterParty
     *            true if non voter party is activated, else false.
     */
    public void setUseNonVoterParty(boolean useNonVoterParty) {
        this.useNonVoterParty = useNonVoterParty;
        this.notifyListeners();
    }
    
    /**
     * Sets the percent hurdle to a given value.
     * 
     * @param percent
     *            the new percent hurdle.
     */
    public void setPercentHurdle(double percent) {
        this.percentageHurdle = percent;
        this.notifyListeners();
    }
    
    /**
     * Sets the nominal size of the parliament.
     * 
     * @param seats
     *            the nominal size of the parliament.
     */
    public void setParliamentSize(int seats) {
        this.sizeBundestag = seats;
        this.notifyListeners();
    }
    
    /**
     * Sets the CalculationMethod.
     * 
     * @param calculationMethod
     */
    public void setCalculationMethod(CalculationMethod calculationMethod) {
        this.calculationMethod = calculationMethod;
        this.notifyListeners();
    }
    
    @Override
    public Settings deepClone() {
        return new Settings(this.useNonVoterParty, this.percentageHurdle, this.sizeBundestag, this.calculationMethod);
    }
    
    /**
     * Adds the given listener to the set of listeners.
     * 
     * @param listener
     *            the listener to add.
     */
    public void addListener(ChangeListener listener) {
        this.listeners.add(listener);
    }
    
    /**
     * Removes the given listener from the set of listeners.
     * 
     * @param listener
     *            the listener to remove.
     */
    public void removeListener(ChangeListener listener) {
        this.listeners.remove(listener);
    }
    
    /**
     * Notifies all listeners of this Settings.
     */
    private void notifyListeners() {
        this.listeners.forEach(ChangeListener::onChange);
    }
    
    public static List<CalculationMethod> getMethods() {
        return Settings.METHODS;
    }
    
    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Settings)) {
            return false;
        }
        Settings comp = (Settings) obj;
        return comp.useNonVoterParty == this.useNonVoterParty && comp.calculationMethod.equals(this.calculationMethod)
               && comp.percentageHurdle == this.percentageHurdle && comp.sizeBundestag == this.sizeBundestag;
    }
    
    @Override
    public String toString() {
        return new StringBuilder("settings: useNonVoterParty=").append(this.useNonVoterParty).append(", percentageHurdle=").append(this.percentageHurdle)
                .append(", sizeBundestag=").append(this.sizeBundestag).append(", calculationMethod=").append(this.calculationMethod.getClass().getSimpleName())
                .toString();
    }
    
    static {
        Settings.METHODS.add(Distribution2013.getMethod());
        Settings.METHODS.add(SimpleDistribution.getMethod());
        Settings.METHODS.add(AlternativeDistribution.getMethod());
    }
    
}
