/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.font.FontRenderContext;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This class is a Diagram which renders a List of {@link DisplayableValues} as a BarChart with its description to the top and the percent it is representing
 * below. It can only display positive values.
 * 
 * @author Felix Heim
 * @version 2
 */
abstract class SimpleBarChart extends Diagram {
    private static final long serialVersionUID = 1L;
    
    /** the minimum width of a bar */
    private static final int MINIMUM_WIDTH = 25;
    
    /** the height if the BARS */
    private static final int BAR_HEIGHT = 200;
    
    /** the amount of values to display */
    private int valueCount;
    
    /** the minimum ideal width of the chart */
    private int requiredWidth;
    
    /** the amount of spaces (bars + spaces in between) required */
    private int requiredSpaces;
    
    /** the highest value */
    private int maximumValue;
    
    /** the scale to multiply the value with, so the are displayed with the right height */
    private double scale;
    
    /** the sum of all values */
    private double sum;
    
    SimpleBarChart() {
    }
    
    @Override
    protected void setValues(List<DisplayableValues> values) {
        this.maximumValue = (int) values.stream().mapToDouble(vals -> vals.value).max().orElse(0.0);
        this.scale = (double) SimpleBarChart.BAR_HEIGHT / this.maximumValue;
        this.values = values.stream()
        // filter out values where the bar would be smaller than one pixel
                .filter(vals -> vals.value * this.scale >= 1).collect(Collectors.toList());
        
        this.valueCount = this.values.size();
        if(this.valueCount != values.size()) {
            DisplayableValues last = this.values.remove(this.valueCount - 1);
            double restSum = values.stream().filter(vals -> vals.value * this.scale < 1).mapToDouble(vals -> vals.value).sum() + last.value;
            this.values.add(this.valueCount - 1, new DisplayableValues("Sonstige", restSum, Color.GRAY));
        }
        this.sum = values.stream().mapToDouble(vals -> vals.value).sum();
        
        this.requiredSpaces = 2 * this.valueCount - 1;
        this.requiredWidth = this.requiredSpaces * SimpleBarChart.MINIMUM_WIDTH;
        
    }
    
    @Override
    public Dimension getPreferredSize() {
        return new Dimension(Math.max(775, super.getPreferredSize().width), SimpleBarChart.BAR_HEIGHT + 40); // define a minimum width and set a fix height
    }
    
    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON); // use antialiasing
        // helper to calculate the width of a String to center it
        FontRenderContext frc = g2d.getFontRenderContext();
        Font font = g2d.getFont();
        
        int totalWidth = this.getSize().width;
        int totalHeight = SimpleBarChart.BAR_HEIGHT + 15; // basically the x-axis
        int barWidth;
        int spaceWidth;
        if(totalWidth < this.requiredWidth) { // make sure the bar width doesn't undergoes a minimum width 
            barWidth = SimpleBarChart.MINIMUM_WIDTH;
            spaceWidth = Math.max(0, (totalWidth - barWidth * this.valueCount) / (this.valueCount - 1));
        }
        else {
            barWidth = spaceWidth = totalWidth / this.requiredSpaces;
        }
        
        double barWidthHalf = barWidth / 2.0;
        int x = 0;
        int i = 0;
        for(DisplayableValues values : this.values) {
            int value = (int) (values.value * this.scale);
            g.setColor(values.color);
            g.fillRect(x, totalHeight - value, barWidth, value); // render the bar
            double descriptionWidth = font.getStringBounds(values.description, frc).getWidth(); // the width of the description
            String percentString = GuiHelper.doubleToPercentString(values.value / this.sum) + "%"; // the percent to be displayed above the Bar
            double percentWidth = font.getStringBounds(percentString, frc).getWidth(); // the width of the String representing the percent
            g.drawString(values.description, (int) (x == 0 ? x // the first element gets left aligned 
                    : i == this.valueCount - 1 ? x - descriptionWidth + barWidth // the last element gets right aligned 
                    : x + barWidthHalf - descriptionWidth / 2) // every other item gets centered
                         , i++ % 2 * 12 + totalHeight + 12);
            g.drawString(percentString, (int) (x == 0 ? x // the first element gets left aligned
                    : i == this.valueCount ? x - percentWidth + barWidth // the last element gets right aligned
                    : x + barWidthHalf - percentWidth / 2) // every other item gets centered
                         , totalHeight - value - 2);
            x += barWidth + spaceWidth; // move forward
        }
        g.dispose();
    }
}
