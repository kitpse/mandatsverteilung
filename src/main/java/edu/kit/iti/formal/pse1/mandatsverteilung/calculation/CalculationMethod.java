/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.calculation;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Bundestag;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Constituency;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.FederalState;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Federation;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Party;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Settings;
import edu.kit.iti.formal.pse1.mandatsverteilung.gui.controller.Mandatsverteilung;

/**
 * This class contains a public method for the Sainte-Lague/Schepers procedure and a lot of protected methods used by the different electoral laws.
 * 
 * @author Alexander Grünen
 *
 */
public abstract class CalculationMethod {
    
    /**
     * Returns a description of the calculation method.
     * 
     * @return a short description text.
     */
    public abstract String getDescription();
    
    /**
     * Calculates the Bundestag applying the given settings.
     * 
     * @param federation
     *            the Federation of the election Result to calculate the Bundestag for.
     * @param settings
     *            the settings for the calculation.
     * @return the Bundestag calculated from the given data.
     */
    public abstract Bundestag calculate(Federation federation, Settings settings);
    
    /**
     * Standard Rounding
     * 
     * @param f
     *            a double to be rounded
     * @return rounded double
     */
    protected static double round(double f) {
        if(f - Math.floor(f) < 0.5) {
            return Math.floor(f);
        }
        else {
            return Math.ceil(f);
        }
    }
    
    /**
     * seats = round(votes / divisor)
     * 
     * @param votes
     *            array containing doubles representing votes / voters / citizens
     * @param seats
     *            array that stores the result of the distribution of seats
     * @param divisor
     *            common divisor
     */
    private static void distributeSeats(double[] votes, double[] seats, double divisor) {
        for(int i = 0; i < seats.length; i++) {
            seats[i] = CalculationMethod.round(votes[i] / divisor);
        }
    }
    
    /**
     * This method implements the Sainte-Lague/Schepers procedure. It reads from votes[] and divisor and writes into seats[]. It uses doubles for performance
     * reasons. The amount of votes for each position in the array has to be greater than the number N to distribute.
     * 
     * This function is public because is is needed by the propagation classes.
     * 
     * @param votes
     *            array, that usually contains the second votes
     * @param seats
     *            array, that usually shall contain the corresponding number of seats
     * @param N
     *            the number of seats to be distributed
     */
    public static void sls(double[] votes, double[] seats, double N) {
        if(N < 0) {
            throw new IllegalArgumentException("Elements (seats) to be distributed must not be negative");
        }
        if(seats.length > 100) {
            throw new IllegalArgumentException("sls received more than 100 parties/states");
        }
        for(int j = 0; j < votes.length; j++) {
            if(votes[j] < 0) {
                throw new IllegalArgumentException("There must be no negative number of votes / voters / citizens");
            }
        }
        double divisor; // the common divisor
        double seatsTaken = 0; // The number of seats currently distributed
        double hare = 0;
        double min;
        double max;
        double r = -1;
        int loopCounter = 0;
        int gift;
        int[] indices = new int[votes.length];
        for(int j = 0; j < indices.length; j++) {
            indices[j] = j;
        }
        
        // set divisor to Hare-Quota
        for(int j = 0; j < votes.length; j++) {
            hare = hare + votes[j];
        }
        hare = hare / N;
        
        CalculationMethod.distributeSeats(votes, seats, hare);
        
        for(int j = 0; j < seats.length; j++) {
            seatsTaken = seatsTaken + seats[j];
        }
        
        if(!(seatsTaken == N)) {
            
            // try most likely other solution for divisor
            if(seatsTaken < N) {
                divisor = hare - hare / N;
            }
            else {
                divisor = hare + hare / N;
            }
            CalculationMethod.distributeSeats(votes, seats, divisor);
            seatsTaken = 0;
            for(int j = 0; j < seats.length; j++) {
                seatsTaken = seatsTaken + seats[j];
            }
            
            // try other likely other solution for divisor
            if(!(seatsTaken == N)) {
                if(seatsTaken < N) {
                    divisor = hare - hare / (2 * N);
                }
                else {
                    divisor = hare + hare / (2 * N);
                }
                CalculationMethod.distributeSeats(votes, seats, divisor);
                seatsTaken = 0;
                for(int j = 0; j < seats.length; j++) {
                    seatsTaken = seatsTaken + seats[j];
                }
                // if else fails, use nested intervals
                // shuffle the array using Fisher–Yates algorithm
                int tmp;
                for(int j = votes.length - 1; j > 0; j--) {
                    int a = (int) (Math.random() * (j + 1));
                    tmp = indices[a];
                    indices[a] = indices[j];
                    indices[j] = tmp;
                }
                gift = 0;
                while(!(seatsTaken == N)) {
                    divisor = hare;
                    min = 0;
                    max = 2 * hare;
                    while(!(seatsTaken == N)) {
                        
                        if(Mandatsverteilung.debug && loopCounter > 0 && loopCounter % 1000000 == 0) {
                            System.out.println("slsLoop=" + loopCounter);
                            System.out.println("N=" + N);
                            System.out.println("seats=" + N);
                            System.out.println("seatsTaken=" + seatsTaken);
                            System.out.println("divisor=" + divisor);
                            for(int j = 0; j < seats.length; j++) {
                                System.out.println("votes=" + votes[j]);
                            }
                            System.out.println("size=" + votes.length);
                        }
                        if(loopCounter > 40000000) {
                            if(Mandatsverteilung.debug) {
                                System.out.println("slsLoop=" + loopCounter);
                                System.out.println("N=" + N);
                                System.out.println("seats=" + N);
                                System.out.println("seatsTaken=" + seatsTaken);
                                System.out.println("divisor=" + divisor);
                                for(int j = 0; j < seats.length; j++) {
                                    System.out.println("votes=" + votes[j]);
                                }
                                System.out.println("size=" + votes.length);
                            }
                            throw new IllegalStateException("Schepers infinity loop detected");
                        }
                        
                        loopCounter++;
                        
                        if(Math.nextAfter(max, Double.NEGATIVE_INFINITY) == min || max == min) {
                            break;
                        }
                        else {
                            
                            if(seatsTaken < N) {
                                max = divisor;
                            }
                            else {
                                min = divisor;
                            }
                            
                            divisor = min + (max - min) * 0.5;
                        }
                        CalculationMethod.distributeSeats(votes, seats, divisor);
                        seatsTaken = 0;
                        for(int j = 0; j < seats.length; j++) {
                            seatsTaken = seatsTaken + seats[j];
                        }
                    }
                    if(!(seatsTaken == N)) { // try to break stalemate by giving a random party one vote
                        if(gift >= indices.length) {
                            votes[indices[gift - 1]]--;
                            gift = 0;
                            // shuffle the array again
                            for(int j = votes.length - 1; j > 0; j--) {
                                int a = (int) (Math.random() * (j + 1));
                                tmp = indices[a];
                                indices[a] = indices[j];
                                indices[j] = tmp;
                            }
                            if(Math.random() < 0.5) {
                                votes[indices[votes.length - 1]]++;
                            }
                            else {
                                votes[indices[votes.length - 1]]--;
                            }
                        }
                        if(gift > 0 && r != -1) {
                            if(r < 0.5) {
                                votes[indices[gift - 1]]--;
                            }
                            else {
                                votes[indices[gift]]++;
                            }
                        }
                        r = Math.random();
                        if(r < 0.5) {
                            votes[indices[gift]]++;
                        }
                        else {
                            votes[indices[gift]]--;
                        }
                        gift++;
                    }
                }
            }
        }
    }
    
    /**
     * Add Non Voter Party.
     * 
     * @param federation
     *            contains all raw data of an election
     * @param bundestag
     *            current state of the Bundestag being calculated
     */
    protected void addNonVoterParty(Federation federation, Bundestag bundestag) {
        Party nvp = new Party("NWP");
        federation.addParty(nvp);
        bundestag.addParty(nvp);
        for(FederalState state : federation.getChildren()) {
            for(Constituency wk : state.getChildren()) {
                wk.setSecondVotes(nvp, wk.getEligibleVoters() - wk.getNumberOfVotes());
                wk.setFirstVotes(nvp, wk.getEligibleVoters() - wk.getNumberOfVotes());
                wk.setNumberOfVotes(wk.getNumberOfVotes() + wk.getSecondVotes(nvp));
            }
        }
    }
    
    /**
     * Find the parties that will have seats in the parliament
     * 
     * @param federation
     *            contains all raw data of an election
     * @param bundestag
     *            current state of the Bundestag being calculated
     * @param settings
     *            settings like nominal size, percent hurdle, electoral law and propagation method
     */
    protected void findElectedParties(Federation federation, Bundestag bundestag, Settings settings) {
        for(Party party : federation.getParties()) {
            if((double) federation.getSecondVotes(party) / federation.getValidSecondVotes() >= settings.getPercentHurdle()
               && federation.getSecondVotes(party) > 0) {
                bundestag.addParty(party);
            }
            else {
                int mandates = federation.getWonDirectMandates(party);
                switch(mandates) {
                    case 0:
                        break;
                    case 1:
                        bundestag.setSize(bundestag.getSize() - 1);
                        break;
                    case 2:
                        bundestag.setSize(bundestag.getSize() - 2);
                        break;
                    default:
                        if(federation.getSecondVotes(party) > 0) {
                            bundestag.addParty(party);
                        }
                        else {
                            bundestag.setSize(bundestag.getSize() - mandates);
                        }
                        break;
                }
            }
        }
    }
    
    /**
     * Calculate how many seats each party gets within the alternative/simple distribution
     * 
     * @param federation
     *            contains all raw data of an election
     * @param bundestag
     *            current state of the Bundestag being calculated
     */
    protected void calculateSeatsPerParty(Federation federation, Bundestag bundestag) {
        int i = 0;
        double[] votes = new double[bundestag.getParties().length];
        for(Party party : bundestag.getParties()) {
            votes[i] = federation.getSecondVotes(party);
            i++;
        }
        
        double[] seatsPerParty = new double[votes.length];
        CalculationMethod.sls(votes, seatsPerParty, bundestag.getSize());
        
        i = 0;
        for(Party party : bundestag.getParties()) {
            bundestag.setSeats(party, seatsPerParty[i]);
            i++;
        }
    }
    
    /**
     * Distribute party seats to the state groups with the alternative/simple distribution.
     * 
     * @param federation
     *            contains all raw data of an election
     * @param bundestag
     *            current state of the Bundestag being calculated
     */
    protected void simpleDistributionToGroups(Federation federation, Bundestag bundestag) {
        int i;
        double[] votes = new double[federation.getChildren().size()];
        double[] partySeatsPerState = new double[votes.length];
        for(Party party : bundestag.getParties()) {
            i = 0;
            for(FederalState state : federation.getChildren()) {
                votes[i] = state.getSecondVotes(party);
                i++;
            }
            CalculationMethod.sls(votes, partySeatsPerState, bundestag.getSeats(party));
            i = 0;
            for(FederalState state : federation.getChildren()) {
                bundestag.setSeats(state, party, (int) partySeatsPerState[i]);
                i++;
            }
        }
    }
    
    /**
     * Change the size of the fraction so that all direct mandates may be realized while balancing out overhang mandates between the state groups of one party.
     * Used only by the alternative distribution.
     * 
     * @param federation
     *            contains all raw data of an election
     * @param bundestag
     *            current state of the Bundestag being calculated. Ensure here, that each state group has the correct final number of seats
     * @param settings
     *            settings like nominal size, percent hurdle, electoral law and propagation method
     */
    protected void distributeSeatsToStateGroups(Federation federation, Bundestag bundestag, Settings settings) {
        int blockedSeats; // seats not taking part in sls in order to force a higher divisor
        int i;
        boolean fits; // can all overhang mandates be served
        double surplus;
        double[] votes = new double[federation.getChildren().size()];
        double[] partySeatsPerState = new double[votes.length];
        for(Party party : bundestag.getParties()) {
            blockedSeats = 0;
            fits = false;
            while(!fits) {
                fits = true;
                i = 0;
                for(FederalState state : federation.getChildren()) {
                    votes[i] = state.getSecondVotes(party);
                    i++;
                }
                CalculationMethod.sls(votes, partySeatsPerState, bundestag.getSeats(party) - blockedSeats);
                i = 0;
                surplus = 0;
                for(FederalState state : federation.getChildren()) {
                    surplus = bundestag.getSeats(state, party) - partySeatsPerState[i];
                    i++;
                }
                if(surplus < 0) {
                    fits = false;
                    blockedSeats = blockedSeats + 1;
                }
                
            }
            i = 0;
            for(FederalState state : federation.getChildren()) {
                if(bundestag.getSeats(state, party) < partySeatsPerState[i]) {
                    bundestag.setSeats(state, party, (int) partySeatsPerState[i]);
                }
                i++;
            }
        }
    }
    
    /**
     * Distribute the seats of the Bundestag (nominal size) to the federal states. Results will be discarded later.
     * 
     * @param federation
     *            contains all raw data of an election
     * @param bundestag
     *            current state of the Bundestag being calculated
     * @param settings
     *            settings like nominal size, percent hurdle, electoral law and propagation method
     */
    protected void seatsPerState(Federation federation, Bundestag bundestag, Settings settings) {
        double[] citizens = new double[federation.getChildren().size()];
        double[] seatsPerState = new double[citizens.length];
        int i;
        i = 0;
        for(FederalState state : federation.getChildren()) {
            citizens[i] = state.getCitizens();
            i++;
        }
        CalculationMethod.sls(citizens, seatsPerState, settings.getParliamentSize());
        i = 0;
        for(FederalState state : federation.getChildren()) {
            bundestag.setNumberOfSeats(state, seatsPerState[i]); // seats belonging to a state, not a party
            i++;
        }
        for(Party party : federation.getParties()) {
            if((double) federation.getSecondVotes(party) / federation.getValidSecondVotes() < settings.getPercentHurdle()
               && federation.getWonDirectMandates(party) > 0 && federation.getWonDirectMandates(party) < 3) {
                bundestag.setSize(bundestag.getSize() - federation.getWonDirectMandates(party));
                for(FederalState state : federation.getChildren()) {
                    bundestag.setNumberOfSeats(state, bundestag.getNumberOfSeats(state) - state.getWonDirectMandates(party));
                }
            }
        }
    }
    
    /**
     * Calculate the size of the state group based on the number of second votes in the particular state. Results will be changed later.
     * 
     * @param federation
     *            contains all raw data of an election
     * @param bundestag
     *            current state of the Bundestag being calculated
     */
    protected void calculateStateGroupSizes(Federation federation, Bundestag bundestag) {
        double[] votes = new double[bundestag.getParties().length];
        double[] seats = new double[votes.length];
        int i;
        for(FederalState state : federation.getChildren()) {
            i = 0;
            for(Party party : bundestag.getParties()) {
                votes[i] = state.getSecondVotes(party);
                i++;
            }
            CalculationMethod.sls(votes, seats, bundestag.getNumberOfSeats(state));
            i = 0;
            for(Party party : bundestag.getParties()) {
                bundestag.setSeats(state, party, (int) seats[i]);
                i++;
            }
        }
    }
    
    /**
     * Change the size of the state groups so that all direct mandates can be realized. Sizes will be changed by following methods.
     * 
     * @param federation
     *            contains all raw data of an election
     * @param bundestag
     *            current state of the Bundestag being calculated
     */
    protected void addOverhang(Federation federation, Bundestag bundestag) {
        int wonMandates;
        double seats;
        for(FederalState state : federation.getChildren()) {
            for(Party party : bundestag.getParties()) {
                wonMandates = state.getWonDirectMandates(party);
                if(bundestag.getSeats(state, party) < wonMandates) {
                    bundestag.setSeats(state, party, wonMandates);
                }
            }
        }
        for(Party party : bundestag.getParties()) {
            seats = 0;
            for(FederalState state : federation.getChildren()) {
                seats = seats + bundestag.getSeats(state, party);
            }
            bundestag.setSeats(party, seats);
        }
    }
    
    /**
     * Enlarge the Bundestag until all parties meet their required number of seats while using sls (Schepers). This yields (except for special cases) the final
     * results for the seats per party.
     * 
     * @param federation
     *            contains all raw data of an election
     * @param bundestag
     *            current state of the Bundestag being calculated
     * @param settings
     *            settings like nominal size, percent hurdle, electoral law and propagation method
     */
    protected void supersizeBundestag(Federation federation, Bundestag bundestag, Settings settings) {
        double[] votes = new double[federation.getParties().size()];
        double[] seats = new double[votes.length];
        boolean fits;
        int i;
        fits = false;
        while(!fits) {
            i = 0;
            for(Party party : bundestag.getParties()) {
                votes[i] = federation.getSecondVotes(party);
                i++;
            }
            CalculationMethod.sls(votes, seats, bundestag.getSize());
            fits = true;
            i = 0;
            
            for(Party party : bundestag.getParties()) {
                if(seats[i] < bundestag.getSeats(party)) {
                    fits = false;
                    bundestag.setSize(bundestag.getSize() + 1);
                    break;
                }
                i++;
            }
        }
        i = 0;
        for(Party party : bundestag.getParties()) {
            bundestag.setSeats(party, seats[i]);
            i++;
        }
    }
    
    /**
     * Distribute party mandates to states This yields (except for special cases) the final results for the seats per party and state.
     * 
     * @param federation
     *            contains all raw data of an election
     * @param bundestag
     *            current state of the Bundestag being calculated. Ensure here, that each state group has the correct final number of seats
     */
    protected void distributePartyMandatesToStates(Federation federation, Bundestag bundestag) {
        double[] seats = new double[federation.getChildren().size()];
        double[] votes = new double[seats.length];
        boolean fits = false;
        int blockedSeats; // seats not taking part in sls in order to force a higher divisor
        int i;
        int surplus; // Group Size - Seats reserved for that Group
        for(Party party : bundestag.getParties()) {
            fits = false;
            blockedSeats = 0;
            i = 0;
            for(FederalState state : federation.getChildren()) {
                votes[i] = state.getSecondVotes(party);
                i++;
            }
            while(!fits) {
                fits = true;
                CalculationMethod.sls(votes, seats, bundestag.getSeats(party) - blockedSeats);
                i = 0;
                for(FederalState state : federation.getChildren()) {
                    surplus = (int) (bundestag.getSeats(state, party) - seats[i]);
                    i++;
                    if(surplus < 0) {
                        fits = false;
                        blockedSeats = blockedSeats + 1;
                    }
                }
            }
            i = 0;
            for(FederalState state : federation.getChildren()) {
                if(bundestag.getSeats(state, party) < seats[i]) {
                    bundestag.setSeats(state, party, (int) seats[i]);
                }
                else {
                    bundestag.setSeats(state, party, bundestag.getSeats(state, party));
                }
                i++;
            }
        }
    }
    
    /**
     * If a party gets more than half of the second votes casted in the Federation, it gains additional seats until it has more than half the seats in the
     * parliament as well.
     * 
     * @param federation
     *            contains all raw data of an election
     * @param bundestag
     *            current state of the Bundestag being calculated
     */
    protected void enableAbsoluteWinner(Federation federation, Bundestag bundestag) {
        double[] seats = new double[federation.getChildren().size()];
        double[] votes = new double[seats.length];
        int i;
        int blockedSeats = 0;
        Party winner = null;
        int votesWinner = 0;
        int votesCurrent;
        boolean enlarged = false;
        boolean fits = false;
        int surplus;
        
        for(Party party : bundestag.getParties()) {
            votesCurrent = federation.getSecondVotes(party);
            if(votesCurrent > votesWinner) {
                winner = party;
                votesWinner = votesCurrent;
            }
        }
        if(votesWinner > federation.getValidSecondVotes() * 0.5) {
            while(bundestag.getSeats(winner) <= bundestag.getSize() * 0.5) {
                bundestag.setSeats(winner, bundestag.getSeats(winner) + 1);
                bundestag.setSize(bundestag.getSize() + 1);
                enlarged = true;
            }
            if(enlarged) {
                i = 0;
                for(FederalState state : federation.getChildren()) {
                    votes[i] = state.getSecondVotes(winner);
                    i++;
                }
                
            }
            while(!fits) {
                fits = true;
                CalculationMethod.sls(votes, seats, bundestag.getSeats(winner) - blockedSeats);
                i = 0;
                for(FederalState state : federation.getChildren()) {
                    surplus = (int) (bundestag.getSeats(state, winner) - seats[i]);
                    i++;
                    if(surplus < 0) {
                        fits = false;
                        blockedSeats = blockedSeats + 1;
                    }
                }
            }
            i = 0;
            for(FederalState state : federation.getChildren()) {
                if(bundestag.getSeats(state, winner) < seats[i]) {
                    bundestag.setSeats(state, winner, (int) seats[i]);
                }
                else {
                    bundestag.setSeats(state, winner, bundestag.getSeats(state, winner));
                }
                i++;
            }
        }
    }
    
    /**
     * This function should adds winners of direct mandates that do not belong to a fraction in the parliament.
     * 
     * @param federation
     *            contains all raw data of an election
     * @param bundestag
     *            current state of the Bundestag being calculated
     * @param settings
     *            settings like nominal size, percent hurdle, electoral law and propagation method
     */
    protected void addFractionlessWinners(Federation federation, Bundestag bundestag, Settings settings) {
        for(Party party : federation.getParties()) {
            if((double) federation.getSecondVotes(party) / federation.getValidSecondVotes() < settings.getPercentHurdle()) {
                int mandates = federation.getWonDirectMandates(party);
                switch(mandates) {
                    case 0:
                        break;
                    case 1:
                        bundestag.setSize(bundestag.getSize() + 1);
                        bundestag.addParty(party);
                        bundestag.setSeats(party, 1);
                        break;
                    case 2:
                        bundestag.setSize(bundestag.getSize() + 2);
                        bundestag.addParty(party);
                        bundestag.setSeats(party, 2);
                        break;
                    default:
                        if(federation.getSecondVotes(party) == 0) {
                            bundestag.addParty(party);
                            bundestag.setSeats(party, mandates);
                        }
                        break;
                }
            }
        }
    }
    
    /**
     * ListExhaustion is not implemented, but the method exists.
     * 
     * @param bundestag
     *            current state of the Bundestag being calculated
     * @param federation
     *            contains all raw data of an election
     */
    protected void listExhaustion(Bundestag bundestag, Federation federation) {
        for(FederalState state : federation.getChildren()) {
            for(Party party : bundestag.getParties()) {
                if(state.getCandidateCount(party) < bundestag.getSeats(state, party) - state.getWonDirectMandates(party)) {
                    double delta = bundestag.getSeats(state, party);
                    bundestag.setSeats(state, party, state.getCandidateCount(party) + state.getWonDirectMandates(party));
                    delta = delta - bundestag.getSeats(state, party);
                    bundestag.setSeats(party, bundestag.getSeats(party) - delta);
                }
            }
        }
    }
}
