/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.data.model;

public class PropagationHelper {
    
    /**
     * Checks if at least one constituency of the given area is unlocked.
     * 
     * @param area
     *            the area to check for
     * @return true if so; false otherwise
     */
    public static boolean isOneUnlocked(ElectionArea<?> area) {
        if(!area.isLocked()) {
            if(area.isConstituency()) {
                // area is constituency
                return true;
            }
            else {
                for(ElectionArea<?> child : area.getChildren()) {
                    if(!child.isLocked()) {
                        if(child.isConstituency()) {
                            // area is federal state            
                            return true;
                        }
                        else {
                            // area is federation
                            for(ElectionArea<?> c : child.getChildren()) {
                                if(!c.isLocked()) {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }
    
    /**
     * Checks if at least one constituency where the given party exists of the given area is unlocked.
     * 
     * @param area
     *            the area to check for
     * @param party
     *            the party so search for
     * @return true if so; false otherwise
     */
    public static boolean isOneUnlocked(ElectionArea<?> area, Party party) {
        if(!area.isLocked()) {
            if(area.isConstituency() && area.isPartyPresent(party)) {
                // area is constituency
                return true;
            }
            else {
                for(ElectionArea<?> child : area.getChildren()) {
                    if(!child.isLocked()) {
                        if(child.isConstituency() && child.isPartyPresent(party)) {
                            // area is federal state            
                            return true;
                        }
                        else {
                            // area is federation
                            for(ElectionArea<?> c : child.getChildren()) {
                                if(!c.isLocked() && c.isPartyPresent(party)) {
                                    return true;
                                }
                            }
                        }
                    }
                }
            }
        }
        return false;
    }
    
    /**
     * Calculates the amount of citizens of all underlaying areas that are not locked.
     * 
     * @param area
     *            the area to calculate the amount for
     * @return amount of citizens not locked
     */
    public static int unlockedCitizens(ElectionArea<?> area) {
        int num = 0;
        if(!area.isLocked()) {
            if(area.isConstituency()) {
                // area is constituency
                num += area.getCitizens();
            }
            else {
                for(ElectionArea<?> child : area.getChildren()) {
                    if(!child.isLocked()) {
                        if(child.isConstituency()) {
                            // area is federal state            
                            num += child.getCitizens();
                        }
                        else {
                            // area is federation
                            for(ElectionArea<?> c : child.getChildren()) {
                                if(!c.isLocked()) {
                                    num += c.getCitizens();
                                }
                            }
                        }
                    }
                }
            }
        }
        return num;
    }
    
    /**
     * Calculates the amount of eligible voters of all underlaying areas that are not locked.
     * 
     * @param area
     *            the area to calculate the amount for
     * @return amount of eligible voters not locked
     */
    public static int unlockedEligibleVoters(ElectionArea<?> area) {
        int num = 0;
        if(!area.isLocked()) {
            if(area.isConstituency()) {
                // area is constituency       
                num += area.getEligibleVoters();
            }
            else {
                for(ElectionArea<?> child : area.getChildren()) {
                    if(!child.isLocked()) {
                        if(child.isConstituency()) {
                            // area is federal state            
                            num += child.getEligibleVoters();
                        }
                        else {
                            // area is federation
                            for(ElectionArea<?> c : child.getChildren()) {
                                if(!c.isLocked()) {
                                    num += c.getEligibleVoters();
                                }
                            }
                        }
                    }
                }
            }
        }
        return num;
    }
    
    /**
     * Calculates the amount of eligible voters of all underlaying areas that are not locked and contain the given party.
     * 
     * @param area
     *            the area to calculate the amount for
     * @param party
     *            the party to look for
     * @return amount of number of votes not locked
     */
    public static int unlockedEligibleVoters(ElectionArea<?> area, Party party) {
        int num = 0;
        if(!area.isLocked()) {
            if(area.isConstituency() && area.isPartyPresent(party)) {
                // area is constituency
                
                num += area.getEligibleVoters();
            }
            else {
                for(ElectionArea<?> child : area.getChildren()) {
                    if(!child.isLocked()) {
                        if(child.isConstituency() && child.isPartyPresent(party)) {
                            // area is federal state            
                            num += child.getEligibleVoters();
                        }
                        else {
                            // area is federation
                            for(ElectionArea<?> c : child.getChildren()) {
                                if(!c.isLocked() && c.isPartyPresent(party)) {
                                    num += c.getEligibleVoters();
                                }
                            }
                        }
                    }
                }
            }
        }
        return num;
    }
    
    /**
     * Calculates the amount of number of votes of all underlaying areas that are not locked.
     * 
     * @param area
     *            the area to calculate the amount for
     * @return amount of number of votes not locked
     */
    public static int unlockedNumberOfVotes(ElectionArea<?> area) {
        int num = 0;
        if(!area.isLocked()) {
            if(area.isConstituency()) {
                // area is constituency
                
                num += area.getNumberOfVotes();
            }
            else {
                for(ElectionArea<?> child : area.getChildren()) {
                    if(!child.isLocked()) {
                        if(child.isConstituency()) {
                            // area is federal state            
                            num += child.getNumberOfVotes();
                        }
                        else {
                            // area is federation
                            for(ElectionArea<?> c : child.getChildren()) {
                                if(!c.isLocked()) {
                                    num += c.getNumberOfVotes();
                                }
                            }
                        }
                    }
                }
            }
        }
        return num;
    }
    
    /**
     * Calculates the amount of number of votes of all underlaying areas that are not locked and contain the given party.
     * 
     * @param area
     *            the area to calculate the amount for
     * @param party
     *            the party to look for
     * @return amount of number of votes not locked
     */
    public static int unlockedNumberOfVotes(ElectionArea<?> area, Party party) {
        int num = 0;
        if(!area.isLocked()) {
            if(area.isConstituency() && area.isPartyPresent(party)) {
                // area is constituency
                
                num += area.getNumberOfVotes();
            }
            else {
                for(ElectionArea<?> child : area.getChildren()) {
                    if(!child.isLocked()) {
                        if(child.isConstituency() && child.isPartyPresent(party)) {
                            // area is federal state            
                            num += child.getNumberOfVotes();
                        }
                        else {
                            // area is federation
                            for(ElectionArea<?> c : child.getChildren()) {
                                if(!c.isLocked() && c.isPartyPresent(party)) {
                                    num += c.getNumberOfVotes();
                                }
                            }
                        }
                    }
                }
            }
        }
        return num;
    }
    
    /**
     * Calculates the amount of valid first votes of all underlaying areas that are not locked.
     * 
     * @param area
     *            the area to calculate the amount for
     * @return amount of valid first votes not locked
     */
    public static int unlockedValidFirstVotes(ElectionArea<?> area) {
        int num = 0;
        if(!area.isLocked()) {
            if(area.isConstituency()) {
                // area is constituency              
                num += area.getValidFirstVotes();
            }
            else {
                for(ElectionArea<?> child : area.getChildren()) {
                    if(!child.isLocked()) {
                        if(child.isConstituency()) {
                            // area is federal state            
                            num += child.getValidFirstVotes();
                        }
                        else {
                            // area is federation
                            for(ElectionArea<?> c : child.getChildren()) {
                                if(!c.isLocked()) {
                                    num += c.getValidFirstVotes();
                                }
                            }
                        }
                    }
                }
            }
        }
        return num;
    }
    
    /**
     * Calculates the amount of valid first votes of all underlaying areas that are not locked and contain the given party.
     * 
     * @param area
     *            the area to calculate the amount for
     * @param party
     *            the party to look for
     * @return amount of number of votes not locked
     */
    public static int unlockedValidFirstVotes(ElectionArea<?> area, Party party) {
        int num = 0;
        if(!area.isLocked()) {
            if(area.isConstituency() && area.isPartyPresent(party)) {
                // area is constituency
                
                num += area.getValidFirstVotes();
            }
            else {
                for(ElectionArea<?> child : area.getChildren()) {
                    if(!child.isLocked()) {
                        if(child.isConstituency() && child.isPartyPresent(party)) {
                            // area is federal state            
                            num += child.getValidFirstVotes();
                        }
                        else {
                            // area is federation
                            for(ElectionArea<?> c : child.getChildren()) {
                                if(!c.isLocked() && c.isPartyPresent(party)) {
                                    num += c.getValidFirstVotes();
                                }
                            }
                        }
                    }
                }
            }
        }
        return num;
    }
    
    /**
     * Calculates the amount of valid second votes of all underlaying areas that are not locked.
     * 
     * @param area
     *            the area to calculate the amount for
     * @return amount of valid second votes not locked
     */
    public static int unlockedValidSecondVotes(ElectionArea<?> area) {
        int num = 0;
        if(!area.isLocked()) {
            if(area.isConstituency()) {
                // area is constituency               
                num += area.getValidSecondVotes();
            }
            else {
                for(ElectionArea<?> child : area.getChildren()) {
                    if(!child.isLocked()) {
                        if(child.isConstituency()) {
                            // area is federal state            
                            num += child.getValidSecondVotes();
                        }
                        else {
                            // area is federation
                            for(ElectionArea<?> c : child.getChildren()) {
                                if(!c.isLocked()) {
                                    num += c.getValidSecondVotes();
                                }
                            }
                        }
                    }
                }
            }
        }
        
        return num;
    }
    
    /**
     * Calculates the amount of valid second votes of all underlaying areas that are not locked and contain the given party.
     * 
     * @param area
     *            the area to calculate the amount for
     * @param party
     *            the party to look for
     * @return amount of number of votes not locked
     */
    public static int unlockedValidSecondVotes(ElectionArea<?> area, Party party) {
        int num = 0;
        if(!area.isLocked()) {
            if(area.isConstituency() && area.isPartyPresent(party)) {
                // area is constituency
                
                num += area.getValidSecondVotes();
            }
            else {
                for(ElectionArea<?> child : area.getChildren()) {
                    if(!child.isLocked()) {
                        if(child.isConstituency() && child.isPartyPresent(party)) {
                            // area is federal state            
                            num += child.getValidSecondVotes();
                        }
                        else {
                            // area is federation
                            for(ElectionArea<?> c : child.getChildren()) {
                                if(!c.isLocked() && c.isPartyPresent(party)) {
                                    num += c.getValidSecondVotes();
                                }
                            }
                        }
                    }
                }
            }
        }
        return num;
    }
    
    /**
     * Calculates the amount of invalid first votes of all underlaying areas that are not locked.
     * 
     * @param area
     *            the area to calculate the amount for
     * @return amount of invalid first votes not locked
     */
    public static int unlockedInvalidFirstVotes(ElectionArea<?> area) {
        int num = 0;
        if(!area.isLocked()) {
            if(area.isConstituency()) {
                // area is constituency               
                num += area.getInvalidFirstVotes();
            }
            else {
                for(ElectionArea<?> child : area.getChildren()) {
                    if(!child.isLocked()) {
                        if(child.isConstituency()) {
                            // area is federal state            
                            num += child.getInvalidFirstVotes();
                        }
                        else {
                            // area is federation
                            for(ElectionArea<?> c : child.getChildren()) {
                                if(!c.isLocked()) {
                                    num += c.getInvalidFirstVotes();
                                }
                            }
                        }
                    }
                }
            }
        }
        return num;
    }
    
    /**
     * Calculates the amount of invalid first votes of all underlaying areas that are not locked and contain the given party.
     * 
     * @param area
     *            the area to calculate the amount for
     * @param party
     *            the party to look for
     * @return amount of number of votes not locked
     */
    public static int unlockedInvalidFirstVotes(ElectionArea<?> area, Party party) {
        int num = 0;
        if(!area.isLocked()) {
            if(area.isConstituency() && area.isPartyPresent(party)) {
                // area is constituency
                
                num += area.getInvalidFirstVotes();
            }
            else {
                for(ElectionArea<?> child : area.getChildren()) {
                    if(!child.isLocked()) {
                        if(child.isConstituency() && child.isPartyPresent(party)) {
                            // area is federal state            
                            num += child.getInvalidFirstVotes();
                        }
                        else {
                            // area is federation
                            for(ElectionArea<?> c : child.getChildren()) {
                                if(!c.isLocked() && c.isPartyPresent(party)) {
                                    num += c.getInvalidFirstVotes();
                                }
                            }
                        }
                    }
                }
            }
        }
        return num;
    }
    
    /**
     * Calculates the amount of invalid second votes of all underlaying areas that are not locked.
     * 
     * @param area
     *            the area to calculate the amount for
     * @return amount of invalid second votes not locked
     */
    public static int unlockedInvalidSecondVotes(ElectionArea<?> area) {
        int num = 0;
        if(!area.isLocked()) {
            if(area.isConstituency()) {
                // area is constituency                
                num += area.getInvalidSecondVotes();
            }
            else {
                for(ElectionArea<?> child : area.getChildren()) {
                    if(!child.isLocked()) {
                        if(child.isConstituency()) {
                            // area is federal state            
                            num += child.getInvalidSecondVotes();
                        }
                        else {
                            // area is federation
                            for(ElectionArea<?> c : child.getChildren()) {
                                if(!c.isLocked()) {
                                    num += c.getInvalidSecondVotes();
                                }
                            }
                        }
                    }
                }
            }
        }
        
        return num;
    }
    
    /**
     * Calculates the amount of invalid second votes of all underlaying areas that are not locked and contain the given party.
     * 
     * @param area
     *            the area to calculate the amount for
     * @param party
     *            the party to look for
     * @return amount of number of votes not locked
     */
    public static int unlockedInvalidSecondVotes(ElectionArea<?> area, Party party) {
        int num = 0;
        if(!area.isLocked()) {
            if(area.isConstituency() && area.isPartyPresent(party)) {
                // area is constituency
                
                num += area.getInvalidSecondVotes();
            }
            else {
                for(ElectionArea<?> child : area.getChildren()) {
                    if(!child.isLocked()) {
                        if(child.isConstituency() && child.isPartyPresent(party)) {
                            // area is federal state            
                            num += child.getInvalidSecondVotes();
                        }
                        else {
                            // area is federation
                            for(ElectionArea<?> c : child.getChildren()) {
                                if(!c.isLocked() && c.isPartyPresent(party)) {
                                    num += c.getInvalidSecondVotes();
                                }
                            }
                        }
                    }
                }
            }
        }
        return num;
    }
    
    /**
     * Calculates the amount of first votes of a party of all underlaying areas that are not locked.
     * 
     * @param area
     *            the area to calculate the amount for
     * @param party
     *            the party to calculate the amount for
     * @return amount of first votes or a party not locked
     */
    public static int unlockedFirstVotes(ElectionArea<?> area, Party party) {
        int num = 0;
        if(!area.isLocked()) {
            if(area.isConstituency() && area.isPartyPresent(party)) {
                // area is constituency             
                num += area.getFirstVotes(party);
            }
            else {
                for(ElectionArea<?> child : area.getChildren()) {
                    if(!child.isLocked()) {
                        if(child.isConstituency() && child.isPartyPresent(party)) {
                            // area is federal state            
                            num += child.getFirstVotes(party);
                        }
                        else {
                            // area is federation
                            for(ElectionArea<?> c : child.getChildren()) {
                                if(!c.isLocked() && c.isPartyPresent(party)) {
                                    num += c.getFirstVotes(party);
                                }
                            }
                        }
                    }
                }
            }
        }
        return num;
    }
    
    /**
     * Calculates the amount of second votes of a party of all underlaying areas that are not locked.
     * 
     * @param area
     *            the area to calculate the amount for
     * @param party
     *            the party to calculate the amount for
     * @return amount of second votes for a party not locked
     */
    public static int unlockedSecondVotes(ElectionArea<?> area, Party party) {
        int num = 0;
        if(!area.isLocked()) {
            if(area.isConstituency() && area.isPartyPresent(party)) {
                // area is constituency        
                num += area.getSecondVotes(party);
            }
            else {
                for(ElectionArea<?> child : area.getChildren()) {
                    if(!child.isLocked()) {
                        if(child.isConstituency() && child.isPartyPresent(party)) {
                            // area is federal state            
                            num += child.getSecondVotes(party);
                        }
                        else {
                            // area is federation
                            for(ElectionArea<?> c : child.getChildren()) {
                                if(!c.isLocked() && c.isPartyPresent(party)) {
                                    num += c.getSecondVotes(party);
                                }
                            }
                        }
                    }
                }
            }
        }
        return num;
    }
}
