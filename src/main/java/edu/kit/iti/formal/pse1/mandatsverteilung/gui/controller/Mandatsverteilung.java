/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.gui.controller;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.time.LocalTime;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.io.MemoryAccess;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.management.Session;
import edu.kit.iti.formal.pse1.mandatsverteilung.gui.GuiHelper;
import edu.kit.iti.formal.pse1.mandatsverteilung.gui.MainWindow;

/**
 * Class that contains the main method starting the program "Mandatsverteilung fuer den deutschen Bundestag".
 * 
 * @author Felix Heim
 * @version 3
 */
public final class Mandatsverteilung {
    /** whether we are running on a mac system */
    private static final boolean IS_ON_MAC = System.getProperty("os.name").startsWith("Mac");
    private static MainWindow mainWindow;
    
    public static boolean debug = false;
    
    public static void main(String[] args) {
        if(Mandatsverteilung.IS_ON_MAC) {
            System.setProperty("apple.laf.useScreenMenuBar", "true"); // on macs the the menubar can be displayed in the native menu bar
            String iconPath = new File(MemoryAccess.getOsDefaultLocation(), "icon.png").getAbsolutePath();
            try {
                Files.copy(MemoryAccess.getInternalInputStream("icon.png"), FileSystems.getDefault().getPath(iconPath), StandardCopyOption.REPLACE_EXISTING);
                Class<?> appClass = Class.forName("com.apple.eawt.Application");
                appClass.getMethod("setDockIconImage", Image.class).invoke(appClass.getMethod("getApplication").invoke(null),
                                                                           new ImageIcon(iconPath).getImage());
            }
            catch(IOException | ClassNotFoundException | NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                Mandatsverteilung.debugStackTraces(e);
            }
        }
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch(ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
        }
        Mandatsverteilung.mainWindow = Mandatsverteilung.mainWindow == null ? new MainWindow() : Mandatsverteilung.mainWindow; // create a new MainWindow
        try {
            BufferedImage image = ImageIO.read(MemoryAccess.getInternalInputStream("icon.png"));
            Mandatsverteilung.mainWindow.setIconImage(new ImageIcon(image).getImage());
        }
        catch(IOException e) {
            Mandatsverteilung.logError("Error loading the icon for the GUI: " + Mandatsverteilung.getNestedErrorMessages(e));
            Mandatsverteilung.debugStackTraces(e);
        }
        if(args != null && args.length > 0) {
            for(String arg : args) {
                switch(arg) {
                    case "-reset":
                        new File(Session.getDefaultSessionPath()).delete();
                        break;
                    case "-debug":
                        Mandatsverteilung.debug = true;
                        break;
                    default:
                        break;
                }
            }
        }
        Session.init(new TabManager(Mandatsverteilung.mainWindow)); // initialize the Session with a new TabManager for the mainWindow
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                Session.getSession().saveSession(Session.getSession().getSessionPath());
            }
            catch(IOException e) {
                Mandatsverteilung.debugStackTraces(e);
                GuiHelper.showError("Sitzung konnte nicht gespeichert werden!\n" + Mandatsverteilung.getNestedErrorMessages(e));
            }
        }));
    }
    
    /** @return whether we are running on a mac system */
    public static boolean isOnMac() {
        return Mandatsverteilung.IS_ON_MAC;
    }
    
    /**
     * Logs a message with the current time to the default output stream.
     * 
     * @param log
     *            the message to print out
     */
    public static void log(String log) {
        final LocalTime now = LocalTime.now();
        final int hour = now.getHour();
        final int minute = now.getMinute();
        final int second = now.getSecond();
        System.out.println(new StringBuilder(17 + log.length()).append(hour < 10 ? "[0" : "[").append(hour).append(minute < 10 ? ":0" : ":").append(minute)
                .append(second < 10 ? ":0" : ":").append(second).append(" INFO]: ").append(log).toString());
    }
    
    /**
     * Logs a message with the current time to the default error stream.
     * 
     * @param log
     *            the message to print out
     */
    public static void logError(String log) {
        final LocalTime now = LocalTime.now();
        final int hour = now.getHour();
        final int minute = now.getMinute();
        final int second = now.getSecond();
        System.err.println(new StringBuilder(17 + log.length()).append(hour < 10 ? "[0" : "[").append(hour).append(minute < 10 ? ":0" : ":").append(minute)
                .append(second < 10 ? ":0" : ":").append(second).append(" INFO]: ").append(log).toString());
    }
    
    public static String getNestedErrorMessages(Throwable t) {
        if(t == null) {
            return "";
        }
        StringBuilder builder = new StringBuilder(t.getClass().getSimpleName()).append(": ").append(t.getMessage());
        while((t = t.getCause()) != null) {
            builder.append(", ").append(t.getClass().getSimpleName()).append(": ").append(t.getMessage());
        }
        return builder.toString();
    }
    
    public static void debugStackTraces(Throwable t) {
        if(!Mandatsverteilung.debug || t == null) {
            return;
        }
        do {
            t.printStackTrace();
        } while((t = t.getCause()) != null);
    }
    
    /** @return the {@link MainWindow} currently used by the program */
    public static MainWindow getMainWindow() {
        return Mandatsverteilung.mainWindow;
    }
}
