/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.gui.controller;

import java.util.HashMap;
import java.util.Map;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.management.ElectionComparison;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Election;
import edu.kit.iti.formal.pse1.mandatsverteilung.gui.MainWindow;
import edu.kit.iti.formal.pse1.mandatsverteilung.gui.Tab;
import edu.kit.iti.formal.pse1.mandatsverteilung.gui.TabComparison;
import edu.kit.iti.formal.pse1.mandatsverteilung.gui.TabElection;

/**
 * A class to manage the Tabs in the {@link MainWindow}. It is capable of add and removing {@link Election}s and {@link ElectionComparison}s to the GUI.
 * 
 * @author Felix Heim
 * @version 1
 */
public class TabManager {
    /** a map holding which {@link Election} belongs to which {@link TabElection} */
    private final Map<Election, TabElection> electionMap = new HashMap<>();
    
    /** a map holding which {@link ElectionComparison} belongs to which {@link TabComparison} */
    private final Map<ElectionComparison, TabComparison> comparisonMap = new HashMap<>();
    
    /** the {@link MainWindow} where the {@link Tab}s are displayed */
    private final MainWindow mainWindow;
    
    public TabManager(MainWindow mainWindow) {
        this.mainWindow = mainWindow;
    }
    
    /**
     * adds a new {@link TabElection} to the {@link MainWindow}
     * 
     * @param election
     *            the {@link Election} to display in the {@link TabElection}
     */
    public void createTabElection(Election election) {
        TabElection tabElection = new TabElection(election);
        this.mainWindow.addTabElection(tabElection);
        this.electionMap.put(election, tabElection);
    }
    
    /**
     * removes the given {@link Election} from the {@link MainWindow}
     * 
     * @param election
     *            the {@link Election} to be removed
     */
    public boolean removeElection(Election election) {
        TabElection tabElection = this.electionMap.remove(election);
        if(tabElection == null) {
            return false;
        }
        this.mainWindow.removeTabElection(tabElection);
        return true;
    }
    
    /**
     * adds a new {@link TabComparison} to the {@link MainWindow}
     * 
     * @param comparison
     *            the {@link ElectionComparison} to display in the {@link TabComparison}
     */
    public void createTabComparison(ElectionComparison comparison) {
        TabComparison tabComparison = new TabComparison(comparison);
        this.mainWindow.addTabComparison(tabComparison);
        this.comparisonMap.put(comparison, tabComparison);
    }
    
    /**
     * removes the given {@link ElectionComparison} from the {@link MainWindow}
     * 
     * @param comparison
     *            the {@link ElectionComparison} to be removed
     */
    public boolean removeComparison(ElectionComparison comparison) {
        TabComparison tabComparison = this.comparisonMap.remove(comparison);
        if(tabComparison == null) {
            return false;
        }
        this.mainWindow.removeTabComparison(tabComparison);
        return true;
    }
}
