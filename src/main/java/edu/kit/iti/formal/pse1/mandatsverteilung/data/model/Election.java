/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.data.model;

import java.util.HashSet;
import java.util.Set;

import edu.kit.iti.formal.pse1.mandatsverteilung.calculation.CalculationMethod;

/**
 * Stores all information for a certain Election.
 * 
 * @author Ben Wilhelm
 *
 */
public class Election implements ChangeListener, DeepCloneable<Election> {
    
    public final static int NUMBER_STATES = 16;
    public final static int NUMBER_CONSTITUENCIES = 299;
    
    private ElectionResult electionResult;
    private Bundestag bundestag;
    private Settings settings;
    
    private String name;
    
    private Set<ChangeListener> listeners = new HashSet<>();
    
    /**
     * Constructs a new Election object with a new default Settings object and a new default ElectionResult object. The name of the new Election is
     * "default Election".
     */
    public Election() {
        this(new Settings(), new ElectionResult(), "default Election");
    }
    
    /**
     * Constructs a new Election object with a new default ElectionResult object but with the given Settings object. It does not copy the Settings.
     * 
     * @param settings
     *            the Settings to use for the new Election.
     * @param name
     *            the name of the new Election.
     */
    public Election(Settings settings, String name) {
        this(settings, new ElectionResult(), name);
    }
    
    /**
     * Constructs a new Election object and inserts the given Settings object and ElectionResult object. It does not copy the Settings or the ElectionResult!
     * 
     * @param settings
     *            the settings used for the new Election.
     * @param electionResult
     *            the electionResult for the new Election.
     * @param name
     *            the name of the new Election.
     */
    public Election(Settings settings, ElectionResult electionResult, String name) {
        electionResult.addChangeListener(this);
        this.electionResult = electionResult;
        settings.addListener(this);
        this.settings = settings;
        this.name = name;
    }
    
    /**
     * Defines the name for this Election.
     * 
     * @param name
     *            the new name.
     */
    public void setName(String name) {
        this.name = name;
        this.notifyListeners();
    }
    
    /**
     * Returns the name of this Election.
     * 
     * @return the name of this Election.
     */
    public String getName() {
        return this.name;
    }
    
    /**
     * Returns the ElectionResult of this Election.
     * 
     * @return the ElectionResult.
     */
    public ElectionResult getElectionResult() {
        return this.electionResult;
    }
    
    /**
     * Returns the Bundestag of this Election. The Bundestag object will be locked for writing access.
     * 
     * @return the Bundestag.
     */
    public Bundestag getBundestag() {
        if(this.bundestag == null) {
            this.calculateBundestag();
        }
        return this.bundestag;
    }
    
    /**
     * Returns the Settings of this Election. You can change the settings for this election at the Settings object.
     * 
     * @return the Settings.
     */
    public Settings getSettings() {
        return this.settings;
    }
    
    /**
     * Returns a deep copy of this Election. The name of the copy is the same as this one.
     */
    @Override
    public Election deepClone() {
        return new Election(this.settings.deepClone(), this.electionResult.deepClone(), this.name);
    }
    
    /**
     * This election reacts in this method with recalculating the Bundestag and notifying its ElectionComparisons.
     */
    @Override
    public void onChange() {
        this.calculateBundestag();
        this.notifyListeners();
    }
    
    /**
     * Adds the given listener to the set of listeners of this Election.
     * 
     * @param listener
     *            the listener to add.
     */
    public void addListener(ChangeListener listener) {
        this.listeners.add(listener);
    }
    
    /**
     * Removes the given listener from the set of listeners of this Election.
     * 
     * @param listener
     *            the listener to remove.
     */
    public void removeListener(ChangeListener listener) {
        this.listeners.remove(listener);
    }
    
    private void notifyListeners() {
        this.listeners.forEach(ChangeListener::onChange);
    }
    
    /**
     * Recalculates the Bundestag of this Election.
     */
    private void calculateBundestag() {
        Federation federation = this.electionResult.getFederation();
        CalculationMethod currentCalculationMethod = this.settings.getCalculationMethod();
        Bundestag oldBundestag = this.bundestag;
        
        this.bundestag = currentCalculationMethod.calculate(federation, this.settings);
        if(oldBundestag != null) {
            this.bundestag.applyListeners(oldBundestag);
        }
    }
    
    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Election)) {
            return false;
        }
        Election comp = (Election) obj;
        return comp.getBundestag().equals(this.getBundestag()) && comp.electionResult.equals(this.electionResult) && comp.name.equals(this.name)
               && comp.settings.equals(this.settings);
    }
}
