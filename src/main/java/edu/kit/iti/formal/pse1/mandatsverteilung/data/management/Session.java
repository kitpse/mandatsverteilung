/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.data.management;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.io.Export;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.io.FormatException;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.io.Import;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.io.MemoryAccess;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Election;
import edu.kit.iti.formal.pse1.mandatsverteilung.gui.GuiHelper;
import edu.kit.iti.formal.pse1.mandatsverteilung.gui.controller.Mandatsverteilung;
import edu.kit.iti.formal.pse1.mandatsverteilung.gui.controller.TabManager;

/**
 * This class bundles up all Elections and ElectionComparisons that are opened in the program. Allows store the program state (individual Elections or the
 * Session itself).
 *
 */
public final class Session {
    /** the default path to the {@link Session} file on disk */
    private static final String DEFAULT_SESSION_PATH = MemoryAccess.getOsDefaultLocation() + "defaultSession.txt";
    
    /** the singleton instance of {@code Session} */
    private static final Session singleton = new Session();
    
    /** if the singleton already has been initialized */
    private boolean initialized;
    
    /** the interval in which the Session gets autosaved */
    private int autoSaveInterval = 5 * 60 * 1000; // standard is every 5 minutes
    
    /** the Thread which handles the auto-saving of the Session */
    private Thread autoSaveThread;
    
    /** if the auto-save-Thread is currently sleeping (doing nothing) */
    private boolean autoSaveThreadSleeping;
    
    /** the path to the {@link Session} file on disk */
    private String sessionPath = Session.DEFAULT_SESSION_PATH;
    
    /** a {@link List} of all {@link Election}s the {@link Session} watches */
    private List<Election> elections = new ArrayList<>();
    
    /** a {@link List} of all {@link ElectionComparison}s the {@link Session} watches */
    private List<ElectionComparison> comparisons = new ArrayList<>();
    
    /** the link to the Gui to add/remove Tabs */
    private TabManager tabManager;
    
    /** to assure we only have a single instance */
    private Session() {
    }
    
    /** @return the singleton instance of the {@code Session} */
    public static Session getSession() {
        if(!Session.singleton.initialized) {
            throw new IllegalStateException("The Session has not been initialized");
        }
        return Session.singleton;
    }
    
    /**
     * @param election
     *            the {@link Election} to add
     */
    public void addElection(Election election) {
        Objects.requireNonNull(election);
        this.elections.add(election);
        this.tabManager.createTabElection(election);
    }
    
    /**
     * @param electionComparison
     *            the {@link ElectionComparison} to add
     */
    public void addElectionComparison(ElectionComparison electionComparison) {
        Objects.requireNonNull(electionComparison);
        this.comparisons.add(electionComparison);
        this.tabManager.createTabComparison(electionComparison);
    }
    
    /**
     * @param election
     *            the {@link Election} to remove
     */
    public boolean removeElection(Election election) {
        this.elections.remove(election);
        return this.tabManager.removeElection(election);
    }
    
    /**
     * @param electionComparison
     *            the {@link ElectionComparison} to remove
     */
    public boolean removeElectionComparison(ElectionComparison electionComparison) {
        this.comparisons.remove(electionComparison);
        return this.tabManager.removeComparison(electionComparison);
    }
    
    /** @return the accumulated count of {@link Election}s and {@link ElectionComparison}s the {@code Session} watches */
    public int getElementCount() {
        return this.getElectionCount() + this.getComparisonCount();
    }
    
    /** @return the count of {@link Election}s the {@code Session} watches */
    public int getElectionCount() {
        return this.elections.size();
    }
    
    /** @return the count of {@link ElectionComparison}s the {@code Session} watches */
    public int getComparisonCount() {
        return this.comparisons.size();
    }
    
    /**
     * saves an {@link Election} to the disk
     * 
     * @param election
     *            the {@link Election} to save
     * @param filePath
     *            the name of the File in which to save the {@link Election}
     * @throws IOException
     *             if an error occurs
     */
    public void saveElection(Election election, String filePath) throws IOException {
        Export.exportElection(election, filePath);
    }
    
    /**
     * saves the {@code Session} to the disk
     * 
     * @param filePath
     *            the name of the File in which to save the {@code Session}
     * @throws IOException
     *             if an error occurs
     */
    public synchronized void saveSession(String filePath) throws IOException {
        Export.exportSession(filePath);
    }
    
    /**
     * loads an Election from the disk
     * 
     * @param filePath
     *            the path of the file where the {@link Election} is saved
     * @return the loaded {@link Election}
     * @throws FormatException
     *             if the file format is wrong
     * @throws IOException
     *             if an io error occurs
     * @throws FileNotFoundException
     *             if the file could not be found
     * @throws NullPointerException
     *             if {@link NullPointerException} occurs anywhere
     */
    public Election loadElection(String filePath) throws NullPointerException, FileNotFoundException, IOException, FormatException {
        Election election = Import.importElection(filePath);
        this.addElection(election);
        return election;
    }
    
    /**
     * loads a Session from the disk
     * 
     * @param filePath
     *            the path of the file where the {@code Session} is saved
     * @return if the loading was successful
     * @throws FormatException
     *             if the session file wasn't the right format
     * @throws IOException
     *             if an io error occured
     * @throws FileNotFoundException
     *             if the file could not be found
     */
    public boolean loadSession(String filePath) throws FileNotFoundException, IOException, FormatException {
        return Import.importSession(filePath);
    }
    
    /** @return the path in which the {@code Session} is autosaved */
    public String getSessionPath() {
        return this.sessionPath;
    }
    
    /**
     * @param filePath
     *            the path in which the {@code Session} should be autosaved
     */
    public void setSessionPath(String filePath) {
        this.sessionPath = filePath;
    }
    
    /** @return the interval in which the {@code Session} gets autosaved */
    public int getAutoSaveInterval() {
        return this.autoSaveInterval;
    }
    
    /**
     * @param time
     *            the time interval (in milliseconds) in which the {@code Session} should be autosaved (must be at least ten second)
     * @throws NumberFormatException
     */
    public void setAutoSaveInterval(int time) throws NumberFormatException {
        if(!this.checkAutoSaveInterval(time)) { // if the interval is way to small throw an exception
            throw new NumberFormatException("Intervall kleiner als 10 Sekunden!");
        }
        synchronized(Session.class) {
            int oldTime = this.autoSaveInterval;
            this.autoSaveInterval = time;
            if(oldTime != time) {
                if(this.autoSaveThreadSleeping) {
                    this.autoSaveThread.interrupt();
                }
            }
        }
    }
    
    /**
     * Checks if the time (given in milliseconds) is at least 10 seconds.
     * 
     * @param time
     *            the time to check in milliseconds
     * @return true if the {@code time} >= 10000, else false.
     */
    public boolean checkAutoSaveInterval(int time) {
        return time >= 10 * 1000;
    }
    
    public List<Election> getElections() {
        return this.elections;
    }
    
    public List<ElectionComparison> getElectionComparisons() {
        return this.comparisons;
    }
    
    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Session)) {
            return false;
        }
        Session other = (Session) obj;
        if(other.elections.size() != this.elections.size() || other.comparisons.size() != this.comparisons.size()) {
            return false;
        }
        for(Election election : this.elections) {
            if(!other.elections.contains(election)) {
                return false;
            }
        }
        for(ElectionComparison comparison : this.comparisons) {
            if(!other.comparisons.contains(comparison)) {
                return false;
            }
        }
        
        return true;
    }
    
    public Session snapshot() {
        Session snapshot = new Session();
        snapshot.initialized = this.initialized;
        snapshot.autoSaveInterval = this.autoSaveInterval;
        snapshot.sessionPath = this.sessionPath;
        snapshot.elections.addAll(this.elections);
        snapshot.comparisons.addAll(this.comparisons);
        snapshot.tabManager = null; // immutable, throws a NullPointerException if someone tries to modify it
        return snapshot;
    }
    
    /**
     * initializes the the Sessions singleton with the given {@link TabManager}
     * 
     * @param tabManger
     *            the {@link TabManager} of the linked GUI
     */
    public static void init(TabManager tabManger) {
        if(!Session.singleton.initialized) {
            Session.singleton.initialized = true;
            Session.singleton.tabManager = tabManger;
            Session.singleton.autoSaveThread = new Thread(Session.singleton.new AutoSaver());
            Session.singleton.autoSaveThread.start();
            try {
                if(Session.singleton.loadSession(Session.singleton.sessionPath)) {
                    Mandatsverteilung.log("Successfully loaded the Session from" + Session.singleton.sessionPath);
                }
                else {
                    throw new IOException("Unknow Error while loading the Session");
                }
            }
            catch(IOException | FormatException e) {
                Session.singleton.addElection(Import.importInternalElection("bundestagswahl2013.csv"));
                Mandatsverteilung.logError("The Session could not be loaded: " + Mandatsverteilung.getNestedErrorMessages(e));
                Mandatsverteilung.debugStackTraces(e);
            }
        }
    }
    
    public static String getDefaultSessionPath() {
        return Session.DEFAULT_SESSION_PATH;
    }
    
    /**
     * The AutoSaver runs a loop in his own {@link Thread} which regularly saves the session
     * 
     * @author Felix Heim
     * @version 1
     */
    private final class AutoSaver implements Runnable {
        @Override
        public void run() {
            while(true) {
                try {
                    synchronized(Session.singleton) {
                        Session.this.autoSaveThreadSleeping = true;
                    }
                    Thread.sleep(Session.this.autoSaveInterval);
                    synchronized(Session.singleton) {
                        Session.this.autoSaveThreadSleeping = false;
                        try {
                            Export.exportSession(Session.this.sessionPath);
                        }
                        catch(IOException e) {
                            Mandatsverteilung.debugStackTraces(e);
                            GuiHelper.showError("Fehler beim Speichern der Sitzung: " + Mandatsverteilung.getNestedErrorMessages(e));
                        }
                    }
                }
                catch(InterruptedException e) {
                }
            }
        }
    }
}
