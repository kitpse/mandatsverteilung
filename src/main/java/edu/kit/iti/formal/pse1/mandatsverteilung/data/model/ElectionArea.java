/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.data.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.ToIntFunction;

/**
 * Abstract class for a election area e.g. Federation, FederalState, Constituency. Sores how many votes which party got in this election area. Changes on higher
 * level will be propagated to the lower levels.
 * 
 * @author Axel Trefzer
 * @author Felix Heim
 * @author Ben Wilhelm - notifications
 * @version 0.2
 * @param <C>
 *            the type of the child of the {@code ElectionArea}
 */
public abstract class ElectionArea<C extends ElectionArea<?>> implements ChangeListener, DeepCloneable<ElectionArea<C>> {
    
    /** name of the ElectionArea. */
    private String name;
    
    /** Describes, whether the area is effected by changes of votes. */
    private boolean isLocked;
    
    private ArrayList<C> children = new ArrayList<>();
    
    /** the ElectionResult every element knows */
    private ElectionResult electionResult;
    
    private ArrayList<ChangeListener> changeListeners = new ArrayList<>();
    
    /**
     * Masks notifications from children to this class while values are propagated to the children.
     */
    private boolean propagationActive = false;
    
    /**
     * @param name
     *            the name of this {@link ElectionArea}
     * @param electionResult
     *            the {@link ElectionResult} associated with this {@link ElectionArea}
     */
    public ElectionArea(String name, ElectionResult electionResult) {
        this.name = name;
        this.electionResult = electionResult;
    }
    
    /**
     * Returns the name of the ElectionArea.
     * 
     * @return the name
     */
    public String getName() {
        return this.name;
    }
    
    /**
     * Set the name of this ElectionArea.
     * 
     * @param name
     *            the name
     */
    public void setName(String name) {
        this.name = name;
        this.notifyListeners();
    }
    
    /** @return the ElectionResult */
    public ElectionResult getElectionResult() {
        return this.electionResult;
    }
    
    /**
     * sets the {@link #electionResult} recursively for this area and all children.
     * 
     * @param electionResult
     *            the ElectionResult to set
     */
    void setElectionResult(ElectionResult electionResult) {
        this.electionResult = electionResult;
        this.children.forEach(c -> c.setElectionResult(electionResult));
        this.notifyListeners();
    }
    
    public boolean isConstituency() {
        return false;
    }
    
    public int getLeaveCount() {
        return this.getChildrenSum(C::getLeaveCount);
    }
    
    /**
     * private helper method to calculate the sum of a certain child integer value
     * 
     * @param mapper
     *            a mapper which maps the child to the requested integer value.
     * @return the sum of the requested integer value of all children
     */
    private int getChildrenSum(ToIntFunction<? super C> mapper) {
        return this.children.stream().mapToInt(mapper).sum();
    }
    
    /** @return amount of citizens */
    public int getCitizens() {
        return this.getChildrenSum(C::getCitizens);
    }
    
    /** @return amount of eligible voters */
    public int getEligibleVoters() {
        return this.getChildrenSum(C::getEligibleVoters);
    }
    
    /** @return amount of cast first votes (valid and invalid) */
    public int getNumberOfVotes() {
        return this.getChildrenSum(C::getNumberOfVotes);
    }
    
    /** @return amount of valid first votes */
    public int getValidFirstVotes() {
        return this.getChildrenSum(C::getValidFirstVotes);
    }
    
    /** @return amount of valid second votes */
    public int getValidSecondVotes() {
        return this.getChildrenSum(C::getValidSecondVotes);
    }
    
    /** @return amount of invalid first votes */
    public int getInvalidFirstVotes() {
        return this.getChildrenSum(C::getInvalidFirstVotes);
    }
    
    /** @return amount of invalid second votes */
    public int getInvalidSecondVotes() {
        return this.getChildrenSum(C::getInvalidSecondVotes);
    }
    
    /** @return percentage of voters (valid and invalid) in relation to all eligible voters */
    public double getVoterTurnout() {
        return (double) this.getNumberOfVotes() / this.getEligibleVoters();
    }
    
    /** @return percentage of valid first votes */
    public double getPercentageValidFirstVotes() {
        return this.getValidFirstVotes() / this.getNumberOfVotes();
    }
    
    /** @return percentage of valid second votes in relation to all second votes */
    public double getPercentageValidSecondVotes() {
        return this.getValidSecondVotes() / this.getNumberOfVotes();
    }
    
    /** @return percentage of invalid first votes in relation to all first votes */
    public double getPercentageInvalidFirstVotes() {
        return this.getInvalidFirstVotes() / this.getNumberOfVotes();
    }
    
    /** @return percentage of invalid second votes in relation to all first votes */
    public double getPercentageInvalidSecondVotes() {
        return this.getInvalidSecondVotes() / this.getNumberOfVotes();
    }
    
    /**
     * Get the amount of first votes for a specific party in this election area.
     * 
     * @param party
     *            the party
     * @return amount of first votes; 0, if the party is not present in this area
     */
    public int getFirstVotes(Party party) {
        return this.getChildrenSum(c -> c.getFirstVotes(party));
    }
    
    /**
     * Get the amount of second votes for a specific party in this election area.
     * 
     * @param party
     *            the party
     * @return amount of second votes; 0, if the party is not present in this area
     */
    public int getSecondVotes(Party party) {
        return this.getChildrenSum(c -> c.getSecondVotes(party));
    }
    
    /**
     * Get the amount of won direct mandates for the party in this election area.
     * 
     * @param party
     *            the party
     * @return amount of won direct mandates; 0, if the party is not present in this area
     */
    public int getWonDirectMandates(Party party) {
        return this.getChildrenSum(c -> c.getWonDirectMandates(party));
    }
    
    /** @return a set of all parties */
    public Set<Party> getParties() {
        Set<Party> parties = new HashSet<>();
        this.children.forEach(c -> parties.addAll(c.getParties()));
        return parties;
    }
    
    /**
     * Change the amount of eligible voters with the set propagation method. The changed votes will only affect the amount of non votes. If the eligible voters
     * are diminished and no non votes are left, the votes will be taken from the cast votes. If possible, only invalid votes will be decreased. Only a valid
     * operation, if the new amount of eligible voters isn't greater than the amount of citizens.
     * 
     * @param amount
     *            the new amount
     * @return true, if the change could be executed; false, if an error occurred (nothing will be changed in this case)
     */
    public boolean changeEligibleVoters(int amount) {
        this.propagationActive = true;
        boolean r = this.getElectionResult().getPropagation().propagateEligibleVoters(this, amount);
        this.propagationActive = false;
        if(r) {
            this.notifyListeners();
        }
        return r;
    }
    
    /**
     * Change the amount citizens with the set propagation method over all constituencies under this election area. If the new amount is lower than the amount
     * of eligible voters or the number of votes, they will be set to this amount.
     * 
     * @param amount
     *            the new amount
     * @return true, if the change could be executed; false, if an error occurred (nothing will be changed in this case)
     */
    public boolean changeCitizens(int amount) {
        this.propagationActive = true;
        boolean r = this.getElectionResult().getPropagation().propagateCitizens(this, amount);
        this.propagationActive = false;
        if(r) {
            this.notifyListeners();
        }
        return r;
    }
    
    /**
     * Change the amount of votes with the set propagation method. The changed votes will only affect the amount of invalid votes. If the votes are decreased,
     * the votes will first be taken from the invalid votes, and, if the votes still have to be decreased, from all unlocked parties. Only a valid operation, if
     * the new amount of first votes isn't higher than the amount of eligible voters.
     * 
     * @param amount
     *            the new amount
     * @return true, if the change could be executed; false, if an error occurred (nothing will be changed in this case)
     */
    public boolean changeNumberOfVotes(int amount) {
        this.propagationActive = true;
        boolean r = this.getElectionResult().getPropagation().propagateNumberOfVotes(this, amount);
        this.propagationActive = false;
        if(r) {
            this.notifyListeners();
        }
        return r;
    }
    
    /**
     * Change the amount of valid first votes in this area and propagate the changes with the set propagation method over all unlocked parties. Not valid, if
     * the amount of votes isn't greater than the amount to set.
     * 
     * @param amount
     *            the new amount
     * @return true, if the change could be executed; false, if an error occurred (nothing will be changed in this case)
     */
    public boolean changeValidFirstVotes(int amount) {
        this.propagationActive = true;
        boolean r = this.getElectionResult().getPropagation().propagateValidFirstVotes(this, amount);
        this.propagationActive = false;
        if(r) {
            this.notifyListeners();
        }
        return r;
    }
    
    /**
     * Change the amount of valid second votes in this area and propagate the changes with the set propagation method over all unlocked parties. Not valid, if
     * the amount of votes isn't greater than the amount to set.
     * 
     * @param amount
     *            the new amount
     * @return true, if the change could be executed; false, if an error occurred (nothing will be changed in this case)
     */
    public boolean changeValidSecondVotes(int amount) {
        this.propagationActive = true;
        boolean r = this.getElectionResult().getPropagation().propagateValidSecondVotes(this, amount);
        this.propagationActive = false;
        if(r) {
            this.notifyListeners();
        }
        return r;
    }
    
    /**
     * Change the amount of invalid first votes in this area and propagate the changes with the set propagation method over all unlocked parties. Not valid, if
     * the amount of votes isn't greater than the amount to set.
     * 
     * @param amount
     *            the new amount
     * @return true, if the change could be executed; false, if an error occurred (nothing will be changed in this case)
     */
    public boolean changeInvalidFirstVotes(int amount) {
        this.propagationActive = true;
        boolean r = this.getElectionResult().getPropagation().propagateInvalidFirstVotes(this, amount);
        this.propagationActive = false;
        if(r) {
            this.notifyListeners();
        }
        return r;
    }
    
    /**
     * Change the amount of invalid second votes in this area and propagate the changes with the set propagation method over all unlocked parties. Not valid, if
     * the amount of votes isn't greater than the amount to set.
     * 
     * @param amount
     *            the new amount
     * @return true, if the change could be executed; false, if an error occurred (nothing will be changed in this case)
     */
    public boolean changeInvalidSecondVotes(int amount) {
        this.propagationActive = true;
        boolean r = this.getElectionResult().getPropagation().propagateInvalidSecondVotes(this, amount);
        this.propagationActive = false;
        if(r) {
            this.notifyListeners();
        }
        return r;
    }
    
    /**
     * Change the amount of first votes for a party in this area and propagate the changes with the set propagation method to this party in all children where
     * this party isn't locked. Not valid, if the new amount of valid first votes is greater than the amount of first votes.
     * 
     * @param amount
     *            the new amount
     * @param party
     *            the party
     * @return true, if the change could be executed; false, if the party wasn't added or an error occurred (nothing will be changed in this case)
     */
    public boolean changeFirstVotes(Party party, int amount) {
        this.propagationActive = true;
        boolean r = this.getElectionResult().getPropagation().propagateFirstVotes(this, party, amount);
        this.propagationActive = false;
        if(r) {
            this.notifyListeners();
        }
        return r;
    }
    
    /**
     * Change the amount of second votes for a party in this area and propagate the changes with the set propagation method to this party in all children where
     * this party isn't locked. Not valid, if the new amount of valid second votes is greater than the amount of second votes.
     * 
     * @param amount
     *            the new amount
     * @param party
     *            the party
     * @return true, if the change could be executed; false, if the party wasn't added or an error occurred (nothing will be changed in this case)
     */
    public boolean changeSecondVotes(Party party, int amount) {
        this.propagationActive = true;
        boolean r = this.getElectionResult().getPropagation().propagateSecondVotes(this, party, amount);
        this.propagationActive = false;
        if(r) {
            this.notifyListeners();
        }
        return r;
    }
    
    /**
     * executes the given action for each child and suppresses all notifications from the children to this class.
     * 
     * @param action
     *            the action to execute
     */
    private void forEachChild(Consumer<? super C> action) {
        this.propagationActive = true; // this is not a propagation, but also a action on all children that causes a lot of notifications.
        this.children.forEach(action);
        this.propagationActive = false;
    }
    
    /** Protect the area against changes. */
    public void lock() {
        this.isLocked = true;
        this.forEachChild(c -> c.lock());
        this.notifyListeners();
    }
    
    /**
     * Protect a party in the area against changes.
     * 
     * @param party
     *            the party to protect
     */
    public void lock(Party party) {
        this.forEachChild(c -> c.lock(party));
        this.notifyListeners();
    }
    
    /** Admit changes to the area. */
    public void unlock() {
        this.isLocked = false;
        this.forEachChild(c -> c.unlock());
        this.notifyListeners();
    }
    
    /**
     * Admit changes to a party in the area.
     * 
     * @param party
     *            the party
     */
    public void unlock(Party party) {
        this.forEachChild(c -> c.unlock(party));
        this.notifyListeners();
    }
    
    /**
     * Tells, whether an area is locked.
     * 
     * @return true, if the area is locked; false otherwise
     */
    public boolean isLocked() {
        return this.isLocked;
    }
    
    /**
     * Tells, whether a party of an area is locked.
     * 
     * @return true, if the party is locked; false otherwise
     */
    public boolean isLocked(Party party) {
        if(party == null) {
            return true;
        }
        for(ElectionArea<?> child : this.children) {
            if(!child.isLocked(party)) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Tells, whether all parties of an area are locked.
     * 
     * @return true, if so; false otherwise
     */
    public boolean allPartiesLocked() {
        for(ElectionArea<?> child : this.children) {
            if(!child.allPartiesLocked()) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * Add a child (election area) to this area.
     * 
     * @param c
     *            the child to add
     * @return true, if the operation was successful; false if the child already exists (nothing will change in this case)
     */
    boolean addChild(C c) {
        c.addChangeListener(this);
        boolean r = this.children.add(c);
        if(r) {
            this.notifyListeners();
        }
        return r;
    }
    
    /**
     * Remove a child (election area) from this area.
     * 
     * @param c
     *            the child to remove
     * @return true, if the operation was successful; false if the area to remove wasn't a child (nothing will change in this case)
     */
    boolean removeChild(C c) {
        c.removeChangeListener(this);
        boolean r = this.children.remove(c);
        if(r) {
            this.notifyListeners();
        }
        return r;
    }
    
    /** @return a set of all children */
    public ArrayList<C> getChildren() {
        return this.children;
    }
    
    /**
     * Add a party to this area. The party will be present on all lower levels. Nothing will happen if the party is already known.
     * 
     * @param party
     *            the party to add
     */
    public void addParty(Party party) {
        this.forEachChild(c -> c.addParty(party));
        this.notifyListeners();
    }
    
    /**
     * Removes a party on this and all lower levels. Removes nothing, if the party isn't known in an area.
     * 
     * @param party
     *            the party to remove
     */
    public void removeParty(Party party) {
        this.forEachChild(c -> c.removeParty(party));
        this.notifyListeners();
    }
    
    /**
     * Checks whether a party is present in this area (therefore it has to be present in at least one child).
     * 
     * @param party
     *            the party to check
     */
    public boolean isPartyPresent(Party party) {
        return this.getChildren().stream().filter(area -> area.isPartyPresent(party)).count() > 0;
    }
    
    public void addChangeListener(ChangeListener changeListener) {
        this.changeListeners.add(changeListener);
    }
    
    public boolean removeChangeListener(ChangeListener changeListener) {
        return this.changeListeners.remove(changeListener);
    }
    
    @Override
    public void onChange() {
        if(!this.propagationActive) {
            this.notifyListeners();
        }
    }
    
    protected void notifyListeners() {
        this.changeListeners.forEach(ChangeListener::onChange);
    }
    
    protected List<Party> copyParties() {
        List<Party> copy = new LinkedList<>();
        for(Party party : this.getParties()) {
            Party partyCopy = party.deepClone();
            copy.add(partyCopy);
        }
        return copy;
    }
    
    @Override
    @SuppressWarnings("rawtypes")
    public boolean equals(Object obj) {
        if(!(obj instanceof ElectionArea)) {
            return false;
        }
        ElectionArea comp = (ElectionArea) obj;
        return comp.isLocked == this.isLocked && comp.name.equals(this.name) && comp.children.equals(this.children);
    }
    
    @Override
    public int hashCode() {
        int lockedHash;
        if(this.isLocked) {
            lockedHash = 1;
        }
        else {
            lockedHash = 2;
        }
        return this.name.hashCode() + this.children.hashCode() + lockedHash;
    }
    
}
