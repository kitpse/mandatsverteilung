/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.data.model;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * A constituency stores how many votes which party got. Changes will not be propagated on a "lower level" but some changes are propagated to the contained
 * parties.
 * 
 * @author Nils Wilka
 * @version 1.0
 */
public class Constituency extends ElectionArea<Constituency> {
    
    /**
     * Number of citizens.
     */
    private int citizens;
    
    /**
     * Number of eligible voters.
     */
    private int eligibleVoters;
    
    /**
     * Number of voters.
     */
    private int numberOfVotes;
    
    /**
     * Maps each party competed for votes in this constituency to its (first and second) votes.
     */
    private Map<Party, Vote> partyToVote = new HashMap<>();
    
    /**
     * Create a new constituency without storing any data.
     * 
     * @param name
     *            The name of this {@link ElectionArea}.
     * @param electionResult
     *            The {@link ElectionResult} associated with this {@link ElectionArea}.
     */
    public Constituency(String name, ElectionResult electionResult) {
        super(name, electionResult);
    }
    
    @Override
    public int getLeaveCount() {
        return 1;
    }
    
    @Override
    public boolean isConstituency() {
        return true;
    }
    
    @Override
    boolean addChild(Constituency constituency) {
        return false;
    }
    
    @Override
    boolean removeChild(Constituency constituency) {
        return false;
    }
    
    @Override
    public int getEligibleVoters() {
        return this.eligibleVoters;
    }
    
    @Override
    public int getCitizens() {
        return this.citizens;
    }
    
    @Override
    public int getNumberOfVotes() {
        return this.numberOfVotes;
    }
    
    @Override
    public int getValidFirstVotes() {
        return this.getParties().stream().mapToInt(this::getFirstVotes).sum();
    }
    
    @Override
    public int getValidSecondVotes() {
        return this.getParties().stream().mapToInt(this::getSecondVotes).sum();
    }
    
    @Override
    public int getInvalidFirstVotes() {
        return this.getNumberOfVotes() - this.getValidFirstVotes();
    }
    
    @Override
    public int getInvalidSecondVotes() {
        return this.getNumberOfVotes() - this.getValidSecondVotes();
    }
    
    @Override
    public int getFirstVotes(Party party) {
        return Optional.ofNullable(this.partyToVote.get(party)).map(Vote::getFirstVotes).orElse(0);
    }
    
    @Override
    public int getSecondVotes(Party party) {
        return Optional.ofNullable(this.partyToVote.get(party)).map(Vote::getSecondVotes).orElse(0);
    }
    
    @Override
    public int getWonDirectMandates(Party party) {
        return party.equals(this.getDirectMandateParty()) ? 1 : 0;
    }
    
    /**
     * Gets the {@link Party} with most first votes. If two parties have the same number of first votes, the most second votes decide.
     * 
     * @return The {@link Party} with most first votes. If no parties exist in this constituency null is returned.
     */
    private Party getDirectMandateParty() {
        Party toReturn = null;
        
        if(this.getParties().size() > 0) {
            int max = -1;
            List<Party> maxParties = new LinkedList<>();
            for(Party party : this.getParties()) {
                int firstVotes = this.partyToVote.get(party).getFirstVotes();
                if(firstVotes > max) {
                    max = firstVotes;
                    maxParties = new LinkedList<>();
                    maxParties.add(party);
                }
                else if(firstVotes == max) {
                    maxParties.add(party);
                }
            }
            if(maxParties.size() > 1) {
                max = -1;
                for(Party party : maxParties) {
                    int secondVotes = this.partyToVote.get(party).getSecondVotes();
                    if(secondVotes > max) {
                        max = secondVotes;
                        toReturn = party;
                    }
                }
            }
            else {
                toReturn = maxParties.get(0);
            }
        }
        return toReturn;
    }
    
    @Override
    public Set<Party> getParties() {
        return this.partyToVote.keySet();
    }
    
    /**
     * Sets the amount of eligible voters in this constituency. This operation can destroy the consistency of the stored votes.
     * 
     * @param amount
     *            The new amount of eligible voters, must be greater than or equal to zero.
     * @return True on success (amount >= 0), false if amount < 0.
     */
    public boolean setEligibleVoters(int amount) {
        boolean exec = false;
        
        if(amount >= 0) {
            this.eligibleVoters = amount;
            this.notifyListeners();
            exec = true;
        }
        return exec;
    }
    
    @Override
    public boolean changeEligibleVoters(int amount) {
        boolean r = this.getElectionResult().getPropagation().propagateEligibleVotersConstituency(this, amount);
        if(r) {
            this.notifyListeners();
        }
        return r;
    }
    
    /**
     * Sets the amount of citizens in this constituency. This operation can destroy the consistency of the stored votes.
     * 
     * @param amount
     *            The new amount of citizens, must be greater than or equal to zero.
     * @return True on success (amount >= 0), false if amount < 0.
     */
    public boolean setCitizens(int amount) {
        boolean exec = false;
        
        if(amount >= 0) {
            this.citizens = amount;
            this.notifyListeners();
            return true;
        }
        return exec;
    }
    
    @Override
    public boolean changeCitizens(int amount) {
        boolean r = this.getElectionResult().getPropagation().propagateCitizensConstituency(this, amount);
        if(r) {
            this.notifyListeners();
        }
        return r;
    }
    
    /**
     * Sets the number of votes in this constituency. This operation can destroy the consistency of the stored votes.
     * 
     * @param amount
     *            The new number of votes, must be greater than or equal to zero.
     * @return True on success (amount >= 0), false if amount < 0.
     */
    public boolean setNumberOfVotes(int amount) {
        boolean exec = false;
        
        if(amount >= 0) {
            this.numberOfVotes = amount;
            this.notifyListeners();
            return true;
        }
        return exec;
    }
    
    @Override
    public boolean changeNumberOfVotes(int amount) {
        boolean r = this.getElectionResult().getPropagation().propagateNumberOfVotesConstituency(this, amount);
        if(r) {
            this.notifyListeners();
        }
        return r;
    }
    
    @Override
    public boolean changeValidFirstVotes(int amount) {
        boolean r = this.getElectionResult().getPropagation().propagateValidFirstVotesConstituency(this, amount);
        if(r) {
            this.notifyListeners();
        }
        return r;
    }
    
    @Override
    public boolean changeValidSecondVotes(int amount) {
        boolean r = this.getElectionResult().getPropagation().propagateValidSecondVotesConstituency(this, amount);
        if(r) {
            this.notifyListeners();
        }
        return r;
    }
    
    @Override
    public boolean changeInvalidFirstVotes(int amount) {
        boolean r = this.getElectionResult().getPropagation().propagateInvalidFirstVotesConstituency(this, amount);
        if(r) {
            this.notifyListeners();
        }
        return r;
    }
    
    @Override
    public boolean changeInvalidSecondVotes(int amount) {
        boolean r = this.getElectionResult().getPropagation().propagateInvalidSecondVotesConstituency(this, amount);
        if(r) {
            this.notifyListeners();
        }
        return r;
    }
    
    @Override
    public boolean changeFirstVotes(Party party, int amount) {
        boolean exec = false;
        
        if(amount >= 0) {
            Optional<Vote> optVote = Optional.ofNullable(this.partyToVote.get(party));
            if(optVote.isPresent() && this.getElectionResult().getPropagation().propagateFirstVotesConstituency(this, party, amount)) {
                this.notifyListeners();
                exec = true;
            }
        }
        return exec;
    }
    
    /**
     * Sets the amount of first votes in this constituency. This operation can destroy the consistency of the stored votes.
     * 
     * @param amount
     *            The new first votes, must be greater than or equal to zero.
     * @return True on success (amount >= 0), false if amount < 0.
     */
    public boolean setFirstVotes(Party party, int amount) {
        boolean exec = false;
        
        if(amount >= 0) {
            Optional<Vote> optVote = Optional.ofNullable(this.partyToVote.get(party));
            if(optVote.isPresent()) {
                optVote.get().setFirstVotes(amount);
                this.notifyListeners();
                exec = true;
            }
        }
        return exec;
    }
    
    @Override
    public boolean changeSecondVotes(Party party, int amount) {
        boolean exec = false;
        
        if(amount >= 0) {
            Optional<Vote> optVote = Optional.ofNullable(this.partyToVote.get(party));
            if(optVote.isPresent() && this.getElectionResult().getPropagation().propagateSecondVotesConstituency(this, party, amount)) {
                this.notifyListeners();
                exec = true;
            }
        }
        return exec;
    }
    
    /**
     * Sets the amount of second votes for a {@link Party}. This operation can destroy the consistency of the stored votes.
     * 
     * @param party
     *            The {@link Party} to set the amount of second votes for.
     * @param amount
     *            The amount of second votes to set.
     * @return True if setting of the second votes could be executed.
     */
    public boolean setSecondVotes(Party party, int amount) {
        boolean exec = false;
        
        if(amount >= 0) {
            Optional<Vote> optVote = Optional.ofNullable(this.partyToVote.get(party));
            if(optVote.isPresent()) {
                optVote.get().setSecondVotes(amount);
                this.notifyListeners();
                exec = true;
            }
        }
        return exec;
    }
    
    @Override
    public void lock(Party party) {
        Optional.ofNullable(this.partyToVote.get(party)).ifPresent(Vote::lock);
        this.notifyListeners();
    }
    
    @Override
    public void unlock(Party party) {
        Optional.ofNullable(this.partyToVote.get(party)).ifPresent(Vote::unlock);
        this.notifyListeners();
    }
    
    /**
     * Checks if the {@link Party} is locked.
     * 
     * @param party
     *            The {@link Party} to check for
     * @return True if the {@link Party} is locked or doesn't exist, else false.
     */
    @Override
    public boolean isLocked(Party party) {
        return Optional.ofNullable(this.partyToVote.get(party)).map(Vote::isLocked).orElse(true);
    }
    
    @Override
    public boolean allPartiesLocked() {
        for(Party party : this.partyToVote.keySet()) {
            if(!this.partyToVote.get(party).isLocked()) {
                return false;
            }
        }
        return true;
    }
    
    @Override
    public void addParty(Party party) {
        if(!this.partyToVote.containsKey(party)) {
            this.partyToVote.put(party, new Vote());
            this.notifyListeners();
        }
    }
    
    @Override
    public void removeParty(Party party) {
        Vote vote = this.partyToVote.remove(party);
        if(vote != null) {
            this.notifyListeners();
        }
    }
    
    @Override
    public boolean isPartyPresent(Party party) {
        return this.partyToVote.containsKey(party);
    }
    
    @Override
    public Constituency deepClone() {
        Constituency copy = new Constituency(this.getName(), this.getElectionResult());
        List<Party> copiedParties = this.copyParties();
        copy.setCitizens(this.getCitizens());
        copy.setEligibleVoters(this.getEligibleVoters());
        copy.setNumberOfVotes(this.getNumberOfVotes());
        
        for(Party party : this.getParties()) {
            Party searchedParty = null;
            for(Party partyCopy : copiedParties) {
                if(party.equals(partyCopy)) {
                    searchedParty = partyCopy;
                    break;
                }
            }
            copy.addParty(searchedParty);
            copy.changeFirstVotes(searchedParty, this.getFirstVotes(party));
            copy.changeSecondVotes(searchedParty, this.getSecondVotes(party));
            if(this.isLocked(party)) {
                copy.lock(searchedParty);
            }
        }
        if(this.isLocked()) {
            copy.lock();
        }
        return copy;
    }
    
    @Override
    public boolean equals(Object obj) {
        if(!super.equals(obj)) {
            return false;
        }
        if(!(obj instanceof Constituency)) {
            return false;
        }
        Constituency comp = (Constituency) obj;
        return comp.citizens == this.citizens && comp.eligibleVoters == this.eligibleVoters && comp.numberOfVotes == this.numberOfVotes
               && comp.partyToVote.equals(this.partyToVote);
    }
    
    @Override
    public int hashCode() {
        return super.hashCode() + this.citizens + this.eligibleVoters + this.numberOfVotes + this.partyToVote.hashCode();
    }
}
