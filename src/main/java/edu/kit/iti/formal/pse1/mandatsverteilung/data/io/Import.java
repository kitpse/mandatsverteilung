/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.data.io;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.management.ElectionComparison;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.management.Session;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Election;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Settings;
import edu.kit.iti.formal.pse1.mandatsverteilung.gui.controller.Mandatsverteilung;
import edu.kit.iti.formal.pse1.mandatsverteilung.propagation.NormalDistribution;
import edu.kit.iti.formal.pse1.mandatsverteilung.propagation.NormalDistribution.NDParameter;
import edu.kit.iti.formal.pse1.mandatsverteilung.propagation.PropagationMath;
import edu.kit.iti.formal.pse1.mandatsverteilung.propagation.ProportionalDistribution;
import edu.kit.iti.formal.pse1.mandatsverteilung.propagation.UniformDistribution;

/**
 * Class for parsing and importing {@link Election}s and the {@link Session} from .csv files. The {@link Election}s can either be in the format of the
 * Bundeswahlleiter or in the format specified by the {@link Export}.
 * 
 * @author Nils Wilka
 * @version 1.2
 */
public class Import {
    
    /**
     * Used separation symbol.
     */
    static final String SEPARATOR = ";";
    
    /**
     * Private to avoid instantiation.
     */
    private Import() {
    }
    
    /**
     * Imports a {@link Session} .csv file. The singleton is accessed directly and its data set. The {@link Session} has to be in the format specified by the
     * {@link Export}. or else a FormatException is thrown. {@link Election}s and {@link ElectionComparison}s in the {@link Session} will be removed. The
     * {@link Election}s and {@link ElectionComparison}s will be parsed and added to the Session until an error occurs. Remaining sections will then be ignored.
     * 
     * @param location
     *            The path of the file containing the session.
     * @return Returns only true if data has been imported and parsed correctly.
     * @throws FileNotFoundException
     *             Thrown if file to parse the session from was not found.
     * @throws IOException
     *             Thrown if an error occurred while reading the file specified by {@code location}.
     * @throws FormatException
     *             Thrown if session could not be parsed, because it has a wrong format. This is the case if it does not match the Export format.
     */
    public static boolean importSession(final String location) throws FileNotFoundException, IOException, FormatException {
        String data = MemoryAccess.loadExternalFile(location);
        new ArrayList<>(Session.getSession().getElections()).forEach(Session.getSession()::removeElection);
        new ArrayList<>(Session.getSession().getElectionComparisons()).forEach(Session.getSession()::removeElectionComparison);
        Import.parseSession(data);
        return true;
    }
    
    /**
     * Parses the {@link Session}, which means it sets the data of the {@link Session} (currently only the auto save interval) and loads contained
     * {@link Election}s and {@link ElectionComparison}s. This is done by splitting the data at empty lines, identifying what the section is and then parsing
     * it. The {@link Election}s and {@link ElectionComparison}s will be parsed and added to the Session until an error occurs. Remaining sections will then be
     * ignored. This method does not remove in the {@link Session} contained {@link Election}s and {@link ElectionComparison}s before adding new.
     * 
     * @param data
     *            The data (csv's) to be parsed.
     * @throws NullPointerException
     *             If data is null.
     * @throws FormatException
     *             Thrown if format of data is wrong. This is the case if it does not match the Export.
     */
    static void parseSession(final String data) throws NullPointerException, FormatException {
        String autoSaveIntervalString = Import.parseFirstLine(data, Export.Format.SESSION_FIRSTLINE_REGEX);
        int autoSaveIntervalNumber = Integer.parseInt(autoSaveIntervalString);
        if(Session.getSession().checkAutoSaveInterval(autoSaveIntervalNumber)) {
            Session.getSession().setAutoSaveInterval(autoSaveIntervalNumber);
        }
        else {
            throw new FormatException("Expected \"number >= 100000\" (10 seconds) as auto save interval, but got \"" + autoSaveIntervalNumber + "\".");
        }
        List<Integer> indices = Import.getBracketIndices(data);
        Import.parseSections(data, indices);
    }
    
    /**
     * Parses the first line of the given data. Checks if all values match the values given by the specified regex in sequential order and returns the second
     * value.
     * 
     * @param data
     *            The data (csv's) to parse the first line from.
     * @param regex
     *            The first line must match this regular expression.
     * @return The second value in the first line.
     * @throws NullPointerException
     *             If data is null.
     * @throws FormatException
     *             Thrown if format of first line does not match the regular expression.
     */
    private static String parseFirstLine(final String data, final String[] regex) throws NullPointerException, FormatException {
        CSVParser parser = new CSVParser(data, 0, 1); // only parse first line.
        List<String> firstLine = new ArrayList<>(2);
        Iterator<String> firstlineIterator = parser.rowIterator(0);
        for(String expected : regex) {
            if(!firstlineIterator.hasNext()) {
                throw new FormatException("Expected \"" + expected + "\" in first row, but got nothing.");
            }
            String current = firstlineIterator.next();
            firstLine.add(current);
            
            if(!current.matches(expected)) {
                throw new FormatException("Expected \"" + expected + "\" in first row, but got \"" + current + "\".");
            }
        }
        if(firstlineIterator.hasNext()) {
            throw new FormatException("Expected no additional information in first row, but got \"" + firstlineIterator.next() + "\".");
        }
        return firstLine.get(1);
    }
    
    /**
     * Checks if the data has empty lines and saves their line index in a list in sequential order.
     * 
     * @param data
     *            The data (csv's) to be parsed.
     * @return A list of line indices for the empty lines in the file.
     * @throws NullPointerException
     *             If data is null.
     * @throws FormatException
     *             Thrown if format of data is wrong. This is the case if it does not match the Export.
     */
    private static List<Integer> getBracketIndices(final String data) throws NullPointerException, FormatException {
        CSVParser parser = new CSVParser(data);
        List<Integer> indices = new LinkedList<>();
        String expected = Export.Format.SECTION_START;
        Iterator<String> columnIterator = parser.columnIterator(0);
        for(int lineIndex = 0; columnIterator.hasNext(); lineIndex++) {
            String current = columnIterator.next();
            if(current.equals(Export.Format.SECTION_START)) {
                if(!current.equals(expected)) {
                    throw new FormatException("Expected to find \"" + expected + "\", but got \"" + current + "\".");
                }
                else {
                    indices.add(lineIndex);
                    expected = Export.Format.SECTION_END;
                }
            }
            else if(current.equals(Export.Format.SECTION_END)) {
                if(!current.equals(expected)) {
                    throw new FormatException("Expected to find \"" + expected + "\", but got \"" + current + "\".");
                }
                else {
                    indices.add(lineIndex);
                    expected = Export.Format.SECTION_START;
                }
            }
        }
        if(indices.size() % 2 != 0) {
            throw new FormatException("Expected to find well defined bracket term, but got \"" + indices.size() + "\" many.");
        }
        return indices;
    }
    
    /**
     * Every section between two empty line represents an election or an election comparison. The data gets split at the empty lines, identified and then
     * parsed.
     * 
     * @param data
     *            The data to get the sections from.
     * @param indices
     *            A list of line indices for the empty lines in the file.
     * @throws NullPointerException
     *             If data is null.
     * @throws FormatException
     *             Thrown if format of data is wrong. This is the case if it does not match the Export.
     */
    private static void parseSections(final String data, final List<Integer> indices) throws NullPointerException, FormatException {
        // assert indices is in proper sequence.
        for(int i = 0; i < indices.size(); i += 2) {
            int previous = indices.get(i);
            int next = indices.get(i + 1);
            if(previous + 1 != next) {
                
                Import.parseSection(data, previous + 1, next);
            }
        }
    }
    
    /**
     * A section represents an election or an election comparison. Here it gets identified and then the parsed.
     * 
     * @param data
     *            The data to get the section from.
     * @param start
     *            The start (line) index of the section in the string.
     * @param end
     *            The end (line) index of the section in the string.
     * @throws NullPointerException
     *             If data is null.
     * @throws FormatException
     *             Thrown if format of data is wrong. This is the case if it does not match the Export.
     */
    private static void parseSection(final String data, int start, int end) throws NullPointerException, FormatException {
        CSVParser parser = new CSVParser(data, start, end);
        switch(parser.get(0, 0)) {
            case Export.Format.ELECTION_COMPARISON_ID:
                ElectionComparison comparison;
                try {
                    comparison = Import.parseElectionComparison(parser.toString());
                }
                catch(FormatException fe) {
                    throw new FormatException("For expected election comparison between lines \"" + start + "\" and \"" + end + "\".", fe);
                }
                Session.getSession().addElectionComparison(comparison);
                break;
            default:
                Election election;
                try {
                    election = Import.parseElection(parser.toString());
                }
                catch(FormatException fe) {
                    throw new FormatException("For expected election between lines \"" + start + "\" and \"" + end + "\".", fe);
                }
                Session.getSession().addElection(election);
                break;
        }
    }
    
    /**
     * Parses given data to an election comparison if format of data is right.
     * 
     * @param data
     *            The data (csv's) to be parsed.
     * @return The parsed election comparison.
     * @throws NullPointerException
     *             If data is null.
     * @throws FormatException
     *             Thrown if format of data is wrong. This is the case if it does not match the Export.
     */
    static ElectionComparison parseElectionComparison(final String data) throws NullPointerException, FormatException {
        CSVParser parser = new CSVParser(data);
        String name = Import.parseFirstLine(data, Export.Format.ELECTION_COMPARISON_FIRSTLINE_REGEX);
        
        Iterator<String> firstColumn = parser.columnIterator(0);
        int stop = -1;
        for(int row = 0; firstColumn.hasNext(); row++) {
            String current = firstColumn.next();
            if(current.equals(Export.Format.STOP)) {
                stop = row;
                break;
            }
        }
        if(stop == -1) {
            throw new FormatException("Expected to find separation symbol \"" + Export.Format.STOP + "\" between two Elections.");
        }
        CSVParser forFirst = new CSVParser(data, 1, stop);
        CSVParser forSecond = new CSVParser(data, stop + 1);
        Election first = Import.parseElection(forFirst.toString());
        Election second = Import.parseElection(forSecond.toString());
        return new ElectionComparison(first, second, name);
    }
    
    /**
     * Imports an election which has either been exported before or of the Bundeswahlleiter. The election is contained in the file specified by the given path.
     * The file has to have the correct format or else a FormatException is thrown.
     * 
     * @param location
     *            The path to the file containing the election.
     * @return An imported election with the values of the file specified by the given path.
     * @throws IOException
     *             Thrown if import went wrong.
     * @throws FileNotFoundException
     *             Thrown if file was not found.
     * @throws FormatException
     *             Thrown if file has the wrong Format. This is the case if it does not match the Export.
     * @throws NullPointerException
     *             If location is null.
     */
    public static Election importElection(final String location) throws NullPointerException, FileNotFoundException, IOException, FormatException {
        return Import.parseElection(MemoryAccess.loadExternalFile(location));
    }
    
    /**
     * Imports an election which has either been exported before or of the Bundeswahlleiter. The election is contained in the file specified by the given path.
     * The file has to have the correct format or else a FormatException is thrown.
     * 
     * @param location
     *            The path to the file containing the election.
     * @return An imported election with the values of the file specified by the given path.
     * @throws IOException
     *             Thrown if import went wrong.
     * @throws FileNotFoundException
     *             Thrown if file was not found.
     * @throws FormatException
     *             Thrown if file has the wrong Format. This is the case if it does not match the Export.
     * @throws NullPointerException
     *             If location is null.
     */
    public static Election importInternalElection(final String location) {
        try {
            return Import.parseElection(MemoryAccess.loadInternalFile(location));
        }
        catch(NullPointerException | FormatException | IOException e) {
            Mandatsverteilung.debugStackTraces(e);
            return null;
        }
    }
    
    /**
     * Parses an election which has either been exported before or of the Bundeswahlleiter. The election is contained in the given data string. The file has to
     * have the correct format or else a FormatException is thrown.
     * 
     * @param data
     *            The data (csv's) to be parsed.
     * @return The parsed election with the values of the given data.
     * @throws FormatException
     *             Thrown if file has the wrong Format. This is the case if it does not match the Export.
     * @throws NullPointerException
     *             If data is null.
     */
    static Election parseElection(final String data) throws NullPointerException, FormatException {
        Settings settings = null;
        ElectionResultParser electionResultParser = null;
        Election election = null;
        Optional<PropagationMath> math = Optional.empty();
        CSVParser parser = new CSVParser(data, 0, 1);
        
        if(parser.getColumnLength() == 1) {
            // Only the name is in 'head'.
            settings = new Settings();
            electionResultParser = new Bundeswahlleiter2013Parser(data, 2);
        }
        else {
            // More head information signals it is a re-import.
            settings = new SettingsParser(data, 1).parseSettings();
            electionResultParser = new ReimportParser(data, 2);
            math = Optional.of(Import.propagationMathFromString(parser.rowIterator(0, 1)));
        }
        election = new Election(settings, electionResultParser.parseElectionResult(), parser.get(0, 0));
        if(math.isPresent()) {
            election.getElectionResult().changePropagation(math.get());
        }
        return election;
    }
    
    /**
     * Imports an election which has either been exported before or of the Bundeswahlleiter, but only gets the values for eligible voters. The election is
     * contained in the file specified by the given path. The file has to have the correct format or else a FormatException is thrown.
     * 
     * @param location
     *            The path to the file containing the election.
     * @return An imported election with the eligible voters values of the file specified by the given path.
     * @throws IOException
     *             Thrown if import went wrong.
     * @throws FileNotFoundException
     *             Thrown if file was not found.
     * @throws FormatException
     *             Thrown if file has the wrong Format. This is the case if it does not match the Export.
     * @throws NullPointerException
     *             If location is null.
     */
    public static Election importEligibleVoters(final String location) throws NullPointerException, FileNotFoundException, IOException, FormatException {
        return Import.parseEligibleVoters(MemoryAccess.loadExternalFile(location));
    }
    
    /**
     * Parses an election which has either been exported before or of the Bundeswahlleiter, but only gets the values for eligible voters. The election is
     * contained in the given data string. The file has to have the correct format or else a FormatException is thrown.
     * 
     * @param data
     *            The data (csv's) to be parsed.
     * @return The parsed election with the eligible voters values of the given data.
     * @throws FormatException
     *             Thrown if file has the wrong Format. This is the case if it does not match the Export.
     * @throws NullPointerException
     *             If data is null.
     */
    private static Election parseEligibleVoters(final String data) throws NullPointerException, FormatException {
        CSVParser parser = new CSVParser(data, 0, 1);
        ElectionResultParser electionResultParser = Import.getElectionResultParser(data, parser);
        return new Election(new Settings(), electionResultParser.parseEligibleVoters(), parser.get(0, 0));
    }
    
    /**
     * Checks which type of election (either been exported before or of the Bundeswahlleiter) is present and initializes the {@code ElectionResultParser}. The
     * election is contained in the given data string.
     * 
     * @param data
     *            The data (csv's) to be parsed.
     * @return The parsed election with the eligible voters values of the given data.
     * @throws FormatException
     *             Thrown if file has the wrong Format. This is the case if it does not match the Export.
     */
    private static ElectionResultParser getElectionResultParser(final String data, final CSVParser parser) throws FormatException {
        ElectionResultParser electionResultParser;
        
        if(parser.getColumnLength() == 1) {
            // Only the name is in 'head'.
            electionResultParser = new Bundeswahlleiter2013Parser(data, 2);
        }
        else {
            // More head information signals it is a re-import.
            electionResultParser = new ReimportParser(data, 2);
        }
        return electionResultParser;
    }
    
    /**
     * Checks if the given name matches a known name of a {@link PropagationMath}. If this is the case initializes the corresponding class and returns it, else
     * throws a {@link FormatException}.
     * 
     * @param name
     *            The name of a valid {@link PropagationMath}.
     * @return The {@link PropagationMath} corresponding to the name.
     * @throws FormatException
     *             Thrown if name is an invalid {@link PropagationMath}.
     */
    static PropagationMath propagationMathFromString(final Iterator<String> iterator) throws FormatException {
        PropagationMath math;
        
        if(iterator.hasNext()) {
            String name = iterator.next();
            
            switch(name) {
                case "NormalDistribution":
                    NormalDistribution nd = new NormalDistribution();
                    if(iterator.hasNext()) {
                        List<NDParameter> parameters = new LinkedList<>();
                        
                        while(iterator.hasNext()) {
                            parameters.add(Import.ndParametersFromString(iterator.next(), nd));
                        }
                        nd.setParameters(parameters);
                    }
                    math = nd;
                    break;
                case "ProportionalDistribution":
                    math = new ProportionalDistribution();
                    break;
                case "UniformDistribution":
                    math = new UniformDistribution();
                    break;
                default:
                    throw new FormatException("Unknown propagation \"" + name + "\" in first line of election.");
            }
            if(iterator.hasNext()) {
                throw new FormatException("Expected no more information for propagation, but found \"" + iterator.next() + "\".");
            }
        }
        else {
            throw new FormatException("Expected to find propagation math in first row, but got nothing.");
        }
        return math;
    }
    
    /**
     * Parses given string to object of class {@link NDParameter}, as specified by its {@code toString()} method or throws a {@link FormatException}.
     * 
     * @param string
     *            The string to parse.
     * @param nd
     *            Instance of class {@link NormalDistribution}.
     * @return The parsed {@link NDParameter}.
     * @throws FormatException
     *             Thrown if {@code string} has invalid format.
     */
    static NDParameter ndParametersFromString(final String string, NormalDistribution nd) throws FormatException {
        String[] strArray = string.split(",");
        if(strArray.length != 3) {
            throw new FormatException("Expected three parameters for the normal distribution, but got \"" + strArray.length + "\".");
        }
        else {
            Double expectation, standardDeviation, dominance;
            
            try {
                expectation = Double.parseDouble(strArray[0]);
                standardDeviation = Double.parseDouble(strArray[1]);
                dominance = Double.parseDouble(strArray[2]);
            }
            catch(IllegalArgumentException iae) {
                throw new FormatException("Expected parameters to be float values, but got \"" + strArray[0] + "\" , \"" + strArray[1] + "\" and \""
                                          + strArray[2] + "\"");
            }
            return nd.new NDParameter(expectation, standardDeviation, dominance);
        }
    }
}
