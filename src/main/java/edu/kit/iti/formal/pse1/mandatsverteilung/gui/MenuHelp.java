/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.gui;

import java.awt.event.KeyEvent;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

/**
 * The classic help menu which contains the {@link JMenuItem} to show the {@link ProgramHelp}
 * 
 * @author Felix Heim
 * @version 2
 */
public class MenuHelp extends JMenu {
    private static final long serialVersionUID = 1L;
    
    MenuHelp() {
        super("Hilfe");
        JMenuItem showProgramHelp = new JMenuItem("Bedienungsanleitung anzeigen");
        showProgramHelp.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F5, 0)); // shortcut to open the help
        showProgramHelp.addActionListener(e -> ProgramHelp.showHelp()); // on click: show the help
        this.add(showProgramHelp);
    }
}
