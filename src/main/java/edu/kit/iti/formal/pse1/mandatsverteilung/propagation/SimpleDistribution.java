/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.propagation;

/**
 * This abstract class contains the common functions of AlternativeUniformDistribution and AlternativeProportionalDistribution. These classes need to implement
 * the propagate(...) method, but can therefore use regularDistribution(...) and randomDistribution(...). To configure the behavior of the regularDistribution
 * method, they must override the calculateChangePerBin(...) method.
 * 
 * @author Ben Wilhelm
 *
 */
public abstract class SimpleDistribution implements PropagationMath {
    
    protected int regularDistribution(int[] bins, int[] maxBins, int leftBins, int leftToDistribute) {
        // contains the change amount for each bin in one iteration
        int[] changePerBin = this.calculateChangePerBin(bins, maxBins, leftToDistribute, leftBins);
        
        while(Math.abs(this.arraySumExclude(changePerBin, bins, maxBins, leftToDistribute)) > 0 && leftBins > 0) {
            
            for(int i = 0; i != bins.length; i++) {
                if(bins[i] + changePerBin[i] > 0 && bins[i] + changePerBin[i] < maxBins[i]) {
                    // valid amount in bins[i] left
                    leftToDistribute -= changePerBin[i]; // reduces the abs-amount of leftToDistribute
                    bins[i] += changePerBin[i];
                }
                else if(changePerBin[i] < 0 && bins[i] != 0) {
                    // bins[i] becomes zero now and was not zero before
                    
                    // leftToDistribute is negative here, adding bins[i] reduces the abs-amount of leftToDistribute
                    leftToDistribute += bins[i];
                    bins[i] = 0;
                    leftBins--; // one more bin is zero now
                }
                else if(changePerBin[i] > 0 && bins[i] != maxBins[i]) {
                    // bins[i] becomes maximum now and was not at its maximum before
                    
                    leftToDistribute -= maxBins[i] - bins[i];
                    bins[i] = maxBins[i];
                    leftBins--; // one more bin is max value now
                }
            }
            
            changePerBin = this.calculateChangePerBin(bins, maxBins, leftToDistribute, leftBins);
            
        }
        return leftToDistribute;
    }
    
    /*
     * Calculates the sum of the array. If leftToDistribute is positive the array entrys are exclueded, that are at a index, where where the bin is at its max
     * value.
     */
    protected long arraySumExclude(int[] array, int[] bins, int[] maxBins, int leftToDistribute) {
        long sum = 0;
        for(int i = 0; i != bins.length; i++) {
            if(bins[i] < maxBins[i] || !(leftToDistribute >= 0)) {
                sum += array[i];
            }
        }
        return sum;
    }
    
    /*
     * Calculates the amounts each bin should be changed, depending on the given inputs. Does not need to check if the change is possible!
     */
    /*
     * This method essentially changes the behavior of the propagation!
     */
    //                                                                               leftBins might be zero
    abstract int[] calculateChangePerBin(int[] bins, int[] maxBins, int leftToDistribute, int leftBins);
    
    protected void randomDistribution(int[] bins, int[] maxBins, int leftToDistribute) {
        int[] randomIndices = this.randomSelectIndices(Math.abs(leftToDistribute), leftToDistribute < 0, bins, maxBins);
        
        for(int i = 0; i != randomIndices.length; i++) {
            bins[randomIndices[i]] += 1 * (int) Math.signum(leftToDistribute);
        }
    }
    
    /**
     * Selects k random indices from the array. If ignoreBins is true, only indices where the array is not zero are selected. Otherwise, only indices, where the
     * array is not max value are selected.
     * 
     * @return an array that contains the selected numbers.
     */
    private int[] randomSelectIndices(int k, boolean ignoreBins, int[] array, int[] maxValues) {
        // assert k <= number of non zero positions of the array
        // assert k <= number of non max value positions of the array
        int[] selected = new int[k];
        for(int i = 0; i != k; i++) {
            int newSelection;
            
            // select an index we did not have before and the array is not zero at.
            do {
                newSelection = (int) Math.floor(Math.random() * array.length);
            } while(this.containsToIndex(selected, newSelection, i) || array[newSelection] == 0 && ignoreBins || array[newSelection] == maxValues[newSelection]
                    && !ignoreBins);
            
            selected[i] = newSelection;
        }
        return selected;
    }
    
    /*
     * Returns true, if array[j] == x for one j in range of [0, i)
     */
    private boolean containsToIndex(int[] array, int x, int i) {
        for(int j = 0; j != i; j++) {
            if(array[j] == x) {
                return true;
            }
        }
        return false;
    }
    
    /*
     * Returns leftBins initialized with the bins to consider
     */
    protected int initializeLeftBins(int[] bins, int[] maxBins, int leftToDistribute) {
        int leftBins = bins.length;
        if(leftToDistribute > 0) {
            for(int i = 0; i != bins.length; i++) {
                if(bins[i] == maxBins[i]) {
                    leftBins--;
                }
            }
        }
        else {
            for(int i = 0; i != bins.length; i++) {
                if(bins[i] == 0) {
                    leftBins--;
                }
            }
        }
        return leftBins;
    }
    
}
