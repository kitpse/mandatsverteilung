/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.gui;

import java.awt.Color;
import java.awt.Component;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Supplier;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.ElectionArea;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Party;
import edu.kit.iti.formal.pse1.mandatsverteilung.gui.controller.Mandatsverteilung;

/**
 * The {@link ElectionData} shows all the Data associated with an Area. It shows the eligible voters, the actual voters, the valid votes, the invalid votes and
 * all the votes of all the {@link Party}s in the {@link ElectionArea}.
 * 
 * @author Felix Heim
 * @version 2
 */
public class ElectionData extends Tab {
    /** the area who's {@link Party}s should be displayed */
    private ElectionArea<?> area;
    
    /** maps all {@link Party}s displayed to their {@link PartyPanel} */
    private Map<Party, PartyPanel> partyPanelMap = new HashMap<>();
    
    /** the main content pane holding all components */
    private JPanel contentPane = new JPanel();
    
    /** the overlay which gets returned which is either the {@link #contentPane} or a {@link JScrollPane} containing the {@link #contentPane} */
    private JComponent overlay;
    
    /**
     * @param area
     *            the area who's {@link Party}s should be displayed
     * @param useScrollbar
     *            if the pane should be packed into a {@link JScrollPane}
     */
    public ElectionData(ElectionArea<?> area, boolean useScrollbar) {
        this.area = area;
        this.contentPane.setLayout(new BoxLayout(this.contentPane, BoxLayout.Y_AXIS));
        this.overlay = useScrollbar ? GuiHelper.createScrollPane(this.contentPane) : this.contentPane;
        
        // adds a citizen panel displaying the citizens
        this.contentPane.add(new CitizenPanel("Bürger", this.area::getCitizens, this.area::changeCitizens).addListenerToArea(area));
        
        Supplier<Integer> eligibleVoters = this.area::getEligibleVoters; // function to get the eligible voters
        // adds a voter panel displaying the eligible voters
        this.contentPane.add(new VoterPanel("Wahlberechtigte", eligibleVoters, this.area::getCitizens).setSetter(this.area::changeEligibleVoters)
                .addListenerToArea(area));
        
        // adds a voter panel displaying the number of votes
        this.contentPane.add(new VoterPanel("W\u00E4hler", this.area::getNumberOfVotes, eligibleVoters).setSetter(this.area::changeNumberOfVotes)
                .addListenerToArea(area));
        
        // adds a named panel displaying the valid votes
        this.contentPane.add(new NamedPanel("G\u00FCltige", this.area::getValidSecondVotes, eligibleVoters, this.area::getValidFirstVotes, eligibleVoters)
                .setSetter(this.area::changeValidSecondVotes, this.area::changeValidFirstVotes).addListenerToArea(area));
        
        // adds a named panel displaying the invalid votes
        this.contentPane
                .add(new NamedPanel("Ung\u00FCltige", this.area::getInvalidSecondVotes, eligibleVoters, this.area::getInvalidFirstVotes, eligibleVoters)
                        .setSetter(this.area::changeInvalidSecondVotes, this.area::changeInvalidFirstVotes).addListenerToArea(area));
        
        // adds all parties to the panel sorted by second votes descending
        this.area.getParties().stream().sorted((party1, party2) -> Double.compare(area.getSecondVotes(party2), area.getSecondVotes(party1)))
                .forEach(this::addNewParty);
        // register this as a listener to the election area
        this.area.addChangeListener(this::update);
        
        JPanel addPartyPanel = new JPanel(); // panel displaying the buttons to add a new party
        JButton addPartyButton = new JButton("Vorhandene Partei hinzuf\u00FCgen"); // button to add an party existing in an other area
        addPartyButton.addActionListener(e -> {
            /* get all existing parties of the federation */
            Set<Party> parties = area.getElectionResult().getFederation().getParties();
            /* filter out those already added */
            parties.removeAll(this.partyPanelMap.keySet());
            /* cast to array */
            Object[] partiesArray = parties.toArray();
            if(partiesArray.length == 0) {
                /* if all parties are added, this button doesn't do anything */
                GuiHelper.showInfo("Alle beretis existierenden Partein sind bereits hinzugef\u00FCgt", "Keine Nichtvorhandenen Partein");
            }
            else {
                /* select one of the parties not already added */
                Object obj = JOptionPane.showInputDialog(Mandatsverteilung.getMainWindow(), "Partei ausw\u00E4hlen", "Vorhandenen Partei ausw\u00E4hlen:",
                                                         JOptionPane.PLAIN_MESSAGE, null, partiesArray, partiesArray[0]);
                if(obj != null) {
                    /* if the selected value wasn't null */
                    this.area.addParty((Party) obj);
                }
            }
        });
        
        JButton addNewPartyButton = new JButton("Neue Partei hinzuf\u00FCgen");
        addNewPartyButton.addActionListener(e -> {
            /* first choose a name for the new party */
            String name = JOptionPane.showInputDialog(this.overlay, "Name der neuen Partei", "Name W\u00E4hlen", JOptionPane.PLAIN_MESSAGE);
            if(name != null) {
                Double politicalOrientation = null;
                /* then choose the political orientation */
                String input = JOptionPane.showInputDialog(Mandatsverteilung.getMainWindow(),
                                                           "Politische Einstellung von standardmä\u00DFig -100.0 (ganz links) bis 100.0 (ganz rechts)",
                                                           "W\u00E4hle die Politische Einstellung der Partei", JOptionPane.PLAIN_MESSAGE);
                try {
                    if(input != null) {
                        politicalOrientation = Double.parseDouble(input);
                        if(politicalOrientation != null) {
                            /* last choose the color of the new party */
                            Color color = JColorChooser.showDialog(Mandatsverteilung.getMainWindow(), "W\u00E4hle die Farbe der Partei", Color.BLACK);
                            if(color != null) {
                                /* finally add the new party */
                                this.area.addParty(new Party(name, color, politicalOrientation));
                            }
                        }
                    }
                }
                catch(NumberFormatException nfe) {
                }
            }
        });
        addPartyPanel.add(addPartyButton);
        addPartyPanel.add(addNewPartyButton);
        addPartyPanel.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.contentPane.add(addPartyPanel);
    }
    
    private void addNewParty(Party party) {
        // create a new Panel based on the party
        PartyPanel panel = new PartyPanel(this.area, party);
        this.partyPanelMap.put(party, panel);
        this.contentPane.add(panel);
    }
    
    private void update() {
        Set<Party> parties = this.area.getParties();
        // basically check if there are parties to be added or to be remove
        if(this.partyPanelMap.size() != parties.size()) {
            List<Party> toAdd = new LinkedList<>(); // put them into a list to avoid concurrent modification exceptions
            parties.forEach(party -> {
                PartyPanel panel = this.partyPanelMap.get(party);
                if(panel == null) { // if the party doesn't exist 
                    toAdd.add(party);
                }
            });
            toAdd.forEach(party -> { // add all parties to the election data pane
                PartyPanel panel = new PartyPanel(this.area, party);
                this.partyPanelMap.put(party, panel);
                this.contentPane.add(panel, this.contentPane.getComponentCount() - 1); // last position before the buttons to add parties
            });
        }
        if(this.partyPanelMap.size() != parties.size()) { // if the size still is different we need to remove some parties
            List<Party> toRemove = new LinkedList<>();// put them into a list to avoid concurrent modification exceptions
            for(Party party : this.partyPanelMap.keySet()) {
                if(!parties.contains(party)) {
                    toRemove.add(party);
                }
            }
            toRemove.forEach(this.partyPanelMap::remove); // remove all parties no longer used
        }
        this.partyPanelMap.values().forEach(PartyPanel::update); // normal update routine
    }
    
    @Override
    public JComponent getContentPane() {
        return this.overlay;
    }
}
