/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.gui;

import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Optional;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.WindowConstants;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.io.MemoryAccess;
import edu.kit.iti.formal.pse1.mandatsverteilung.gui.controller.Mandatsverteilung;

/**
 * The {@code ProgramHelp} is a simple Dialog which displays the program help as an html text.
 * 
 * @author Felix Heim
 * @version 1.1
 */
class ProgramHelp extends JDialog {
    private static final long serialVersionUID = 1L;
    
    private static final ProgramHelp INSTANCE;
    private static final String HELP;
    
    private ProgramHelp() {
        super(Mandatsverteilung.getMainWindow(), "Bedienungsanleitung");
        this.add(GuiHelper.createScrollPane(new JLabel("<html><div style=\"width:600px; text-align:justify\">" + ProgramHelp.HELP + "</div></html>")));
        this.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        this.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if(e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                    ProgramHelp.this.setVisible(false);
                }
            }
        });
        this.pack();
        this.setResizable(false);
        this.setLocationRelativeTo(null);
    }
    
    static void showHelp() {
        ProgramHelp.INSTANCE.setVisible(true);
    }
    
    @Override
    public Dimension getPreferredSize() {
        return new Dimension(super.getPreferredSize().width, 600); // limit the height
    }
    
    static {
        // load the help text from the internal resources 
        HELP = Optional.ofNullable(MemoryAccess.loadInternalFileOrNull("programhelp.txt")).orElse("No help found :(");
        INSTANCE = new ProgramHelp();
    }
}
