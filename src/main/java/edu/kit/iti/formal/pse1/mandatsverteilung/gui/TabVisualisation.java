/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.gui;

import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Bundestag;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Election;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Federation;

/**
 * A {@link Tab} containing some Diagrams to visualize the results of an {@link Election}.
 * 
 * @author Felix Heim
 * @version 1
 */
public class TabVisualisation extends Tab {
    private JPanel contentPane = new JPanel();
    private JComponent overlay = GuiHelper.createScrollPane(this.contentPane);
    
    /**
     * @param election
     *            the {@link Election} for which to create the {@link Diagram}s
     */
    public TabVisualisation(Election election) {
        this.contentPane.setLayout(new BoxLayout(this.contentPane, BoxLayout.Y_AXIS));
        
        Bundestag bundestag = election.getBundestag();
        // a diagram displaying the bundestag
        this.contentPane.add(new DiagramPanel(new PieChartBundestag(bundestag), "Sitzverteilung im Bundestag"));
        // a diagram displaying the seats a party has in the bundestag as percent
        this.contentPane.add(new DiagramPanel(new BundestagBarChartPercent(bundestag), "Sitzverteilung im Bundestag (in Prozent)"));
        
        Federation federation = election.getElectionResult().getFederation();
        // display the second votes as a pie chart
        this.contentPane.add(new DiagramPanel(new PieChartPercentParties(federation), "Verteilung der Zweitstimmen"));
        // display the second votes as a bar chart
        this.contentPane.add(new DiagramPanel(new SimpleBarChartPercent(federation), "Verteilung der Zweitstimmen"));
        
    }
    
    @Override
    public JComponent getContentPane() {
        return this.overlay;
    }
}
