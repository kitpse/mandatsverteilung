/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.gui;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.stream.Collectors;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Bundestag;

/**
 * This class represents a Bundestag. It basically takes a normal PieChart, only renders the upper half and renders a hole in the middle to simulate classic
 * Bundestag look.
 * 
 * @author Felix Heim
 * @version 1
 */
class PieChartBundestag extends PieChart {
    private static final long serialVersionUID = 1L;
    
    /** the formatter used to format decimal numbers */
    private static final DecimalFormat format = new DecimalFormat();
    
    /**
     * @param bundestag
     *            the {@link Bundestag} to display
     */
    PieChartBundestag(Bundestag bundestag) {
        super(true);
        bundestag.addListener(this::update);
        this.update(bundestag);
    }
    
    /** updates this Diagram with the data from the new {@link Bundestag} */
    private void update(Bundestag newBundestag) {
        this.setValues(Arrays
        // create a stream of all Parties
                .stream(newBundestag.getParties())
                // filter out Parties with zero seats
                .filter(party -> newBundestag.getSeats(party) > 0)
                // sort the parties by their political orientation
                .sorted((party1, party2) -> Double.compare(party1.getPoliticalOrientation(), party2.getPoliticalOrientation()))
                // maps the party to a triple, containing a description (the name of the party + the seats), a double representing the amount of seats and the color of the party
                .map(party -> new DisplayableValues(new StringBuilder().append(party.getName()).append(" (")
                             .append(PieChartBundestag.format.format(newBundestag.getSeats(party))).append(")   ").toString(), newBundestag.getSeats(party),
                                                    party.getColor()))
                // collect all elements to a list
                .collect(Collectors.toList()));
    }
}
