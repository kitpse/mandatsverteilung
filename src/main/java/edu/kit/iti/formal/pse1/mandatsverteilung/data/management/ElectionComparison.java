/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.data.management;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Bundestag;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.ChangeListener;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Election;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Party;

/**
 * The class ElectionComparison provides functionality to compare two Elections. Because the party Objects in the two Elections are not the same, they must be
 * mapped to each other to define which party is compared to which. By default the {@link Party}s are mapped by their equals method (that compares names). If
 * the user of this ElectionComparison notices that the mapping does not fit or is not complete, the mapping of the {@link Party}s can be changed.
 * 
 * @author Ben Wilhelm
 * @author Felix Heim
 * @version 1.0
 */
// FA280
public class ElectionComparison implements ChangeListener {
    private static final Party[] DUMMY = {};
    
    private Election election1;
    private Election election2;
    private String name;
    
    /** Maps parties of the ElectionResult of election1 to parties of the ElectionResult of election2 to compare them to each other. */
    private HashMap<Party, Party> party1ToParty2Map = new HashMap<>();
    
    /** Maps partys of the ElectionResult of election2 to parties of the ElectionResult of election1 to compare them to each other. */
    private HashMap<Party, Party> party2ToParty1Map = new HashMap<>();
    
    private List<ChangeListener> listeners = new LinkedList<>();
    
    /**
     * Creates a new ElectionComparison. Parties from election1 are default mapped for comparison by the name to parties from election2.
     * 
     * @param election1
     * @param election2
     * @param name
     */
    public ElectionComparison(Election election1, Election election2, String name) {
        this.election1 = election1;
        this.election2 = election2;
        this.name = name;
        
        Party[] partys1 = this.election1.getElectionResult().getFederation().getParties().toArray(ElectionComparison.DUMMY);
        Party[] partys2 = this.election2.getElectionResult().getFederation().getParties().toArray(ElectionComparison.DUMMY);
        this.calculatePartyMapping(partys1, partys2, this.party1ToParty2Map, false);
        this.calculatePartyMapping(partys2, partys1, this.party2ToParty1Map, false);
    }
    
    private void calculatePartyMapping(Party[] parties1, Party[] parties2, HashMap<Party, Party> partyMap, boolean updateOnly) {
        for(Party party1 : parties1) {
            for(Party party2 : parties2) {
                if(party1.equals(party2) && (!updateOnly || partyMap.get(party1) == null)) {
                    partyMap.put(party1, party2);
                }
            }
        }
    }
    
    public Election getFirstElection() {
        return this.election1;
    }
    
    public Election getSecondElection() {
        return this.election2;
    }
    
    public Set<Party> getParties() {
        Set<Party> combinedParties = this.election1.getElectionResult().getFederation().getParties();
        this.election2.getElectionResult().getFederation().getParties().forEach(party -> {
            Party mapped = this.party2ToParty1Map.get(party);
            combinedParties.add(mapped == null ? party : mapped);
        });
        return combinedParties;
    }
    
    /**
     * Returns the parties from election1.bundestag
     * 
     * @return the parties from election1.bundestag
     */
    public Party[] getBundestag1Partys() {
        return this.election1.getBundestag().getParties();
    }
    
    /**
     * Returns the parties from election2.bundestag
     * 
     * @return the parties from election2.bundestag
     */
    public Party[] getBundestag2Partys() {
        return this.election2.getBundestag().getParties();
    }
    
    /**
     * Returns the parties from election1.electionResult
     * 
     * @return the parties from election1.electionResult
     */
    public Party[] getElectionResult1Partys() {
        return this.election1.getElectionResult().getFederation().getParties().toArray(ElectionComparison.DUMMY);
    }
    
    /**
     * Returns the parties from election2.electionResult
     * 
     * @return the parties from election2.electionResult
     */
    public Party[] getElectionResult2Partys() {
        return this.election2.getElectionResult().getFederation().getParties().toArray(ElectionComparison.DUMMY);
    }
    
    /**
     * Maps party1 for comparison to party2.
     * 
     * @param party1
     *            must be a Party from election1.electionResult.
     * @param party2
     *            must be a Party from election2.electionResult.
     */
    public void mapPartysForComparsion(Party party1, Party party2) {
        this.party1ToParty2Map.remove(party2);
        this.party2ToParty1Map.remove(party1);
        this.party1ToParty2Map.put(party1, party2);
        this.party2ToParty1Map.put(party2, party1);
        this.notifyListeners();
    }
    
    /**
     * Returns the difference of the rounded BundestagSeats for the given {@link Party}.<br>
     * The {@link Party} might me either from the first or the second election, but the comparisons always goes from the first to the second election.
     * 
     * @param party
     *            the party to get differences for
     * @return the difference of the rounded BundestagSeats of the given party to the party it is compared with.
     */
    public double differenceBundestagSeats(Party party) {
        Tuple<Party> t = this.getTupleForParty(party);
        int seats1 = this.election1.getBundestag().getSeatsRounded(t.t1);
        int seats2 = this.election2.getBundestag().getSeatsRounded(t.t2);
        return seats2 - seats1;
    }
    
    /**
     * Compares the percentage of the given party on its {@link Bundestag} to the percentage of the party it is mapped to on the corresponding {@link Bundestag}
     * .<br>
     * The {@link Party} might me either from the first or the second election, but the comparisons always goes from the first to the second election.
     * 
     * @param party
     *            the party to get differences for
     * @return the difference of percentages (in percent from 0 to 1).
     */
    public double differenceBundestagSeatsPercent(Party party) {
        Tuple<Party> t = this.getTupleForParty(party);
        double seats1 = this.election1.getBundestag().getSeatsRounded(t.t1);
        double seats2 = this.election2.getBundestag().getSeatsRounded(t.t2);
        double seats1Percent = seats1 / this.election1.getBundestag().getSize();
        double seats2Percent = seats2 / this.election2.getBundestag().getSize();
        return seats2Percent - seats1Percent;
    }
    
    /**
     * Returns the difference of the second votes for the given {@link Party}.<br>
     * The {@link Party} might me either from the first or the second election, but the comparisons always goes from the first to the second election.
     * 
     * @param party
     *            the party to get differences for
     * @return the difference of the second votes of the given party to the party it is compared with.
     */
    public double differenceSecondVotes(Party party) {
        Tuple<Party> t = this.getTupleForParty(party);
        int secondVotes1 = this.election1.getElectionResult().getFederation().getSecondVotes(t.t1);
        int secondVotes2 = this.election2.getElectionResult().getFederation().getSecondVotes(t.t2);
        return secondVotes2 - secondVotes1;
    }
    
    /**
     * Compares the percentage of the second Votes of the given party to the percentage of the party it is mapped to.<br>
     * The {@link Party} might me either from the first or the second election, but the comparisons always goes from the first to the second election.
     * 
     * @param party
     *            the party to get differences for
     * @return the difference of percentages (in percent from 0 to 1).
     */
    public double differenceSecondVotesPercent(Party party) {
        Tuple<Party> t = this.getTupleForParty(party);
        double secondVotes1 = this.election1.getElectionResult().getFederation().getSecondVotes(t.t1);
        double secondVotes2 = this.election2.getElectionResult().getFederation().getSecondVotes(t.t2);
        double secondVotes1Percent = secondVotes1 / this.election1.getElectionResult().getFederation().getValidSecondVotes();
        double secondVotes2Percent = secondVotes2 / this.election2.getElectionResult().getFederation().getValidSecondVotes();
        return secondVotes2Percent - secondVotes1Percent;
    }
    
    @Override
    // update the mappings with new values
    public void onChange() {
        Party[] parties1 = this.election1.getElectionResult().getFederation().getParties().toArray(ElectionComparison.DUMMY);
        Party[] parties2 = this.election2.getElectionResult().getFederation().getParties().toArray(ElectionComparison.DUMMY);
        this.calculatePartyMapping(parties1, parties2, this.party1ToParty2Map, true);
        this.calculatePartyMapping(parties2, parties1, this.party2ToParty1Map, true);
        
        this.notifyListeners();
    }
    
    /**
     * Returns the name of this Comparison.
     * 
     * @return the name of this Comparison.
     */
    public String getName() {
        return this.name;
    }
    
    /**
     * @param name
     *            the new name of this {@code ElectionComparison} to set
     */
    public void setName(String name) {
        this.name = name;
    }
    
    public void addListener(ChangeListener listener) {
        this.listeners.add(listener);
    }
    
    public void removeListener(ChangeListener listener) {
        this.listeners.remove(listener);
    }
    
    private void notifyListeners() {
        this.listeners.forEach(ChangeListener::onChange);
    }
    
    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof ElectionComparison)) {
            return false;
        }
        ElectionComparison other = (ElectionComparison) obj;
        return this.election1.equals(other.election1) && this.election2.equals(other.election2);
    }
    
    private Tuple<Party> getTupleForParty(Party party) {
        Party other = this.party1ToParty2Map.get(party);
        if(other != null) {
            return new Tuple<>(party, other);
        }
        other = this.party2ToParty1Map.get(party);
        if(other != null) {
            return new Tuple<>(other, party);
        }
        return new Tuple<>(party, party);
    }
    
    private class Tuple<T> {
        private T t1;
        private T t2;
        
        private Tuple(T t1, T t2) {
            this.t1 = t1;
            this.t2 = t2;
        }
    }
}
