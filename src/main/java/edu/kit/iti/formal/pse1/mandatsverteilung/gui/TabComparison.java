/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.Arrays;
import java.util.Comparator;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.management.ElectionComparison;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Election;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Party;
import edu.kit.iti.formal.pse1.mandatsverteilung.gui.controller.Mandatsverteilung;

/**
 * A tab for displaying {@link Diagram}s showing the differences between the two {@link Election}s in a {@link ElectionComparison}.
 * 
 * @author Felix Heim
 * @version 1
 */
public class TabComparison extends JPanel {
    private static final long serialVersionUID = 1L;
    
    /** the {@link ElectionComparison} for which to create the tab */
    private ElectionComparison comparison;
    /** the tab name */
    private String name;
    
    private JPanel contentPane = new JPanel();
    
    /**
     * @param comparison
     *            the {@link ElectionComparison} for which to create the tab
     */
    public TabComparison(ElectionComparison comparison) {
        super(new BorderLayout());
        this.comparison = comparison;
        this.name = comparison.getName();
        
        JPanel panelAddMapping = new JPanel();
        JButton addMapping = new JButton("Partei Mapping hinzufügen");
        addMapping.addActionListener(e -> {
            new MappingDialog().showDialog();
        });
        
        panelAddMapping.add(addMapping);
        panelAddMapping.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.contentPane.add(panelAddMapping);
        
        this.contentPane.setLayout(new BoxLayout(this.contentPane, BoxLayout.Y_AXIS));
        
        // the bundestag of the first Election
        this.contentPane.add(new DiagramPanel(new PieChartBundestag(comparison.getFirstElection().getBundestag()), "Bundestag von "
                                                                                                                   + comparison.getFirstElection().getName()));
        
        // the bundestag of the second Election
        this.contentPane
                .add(new DiagramPanel(new PieChartBundestag(comparison.getSecondElection().getBundestag()), "Bundestag von "
                                                                                                            + comparison.getSecondElection().getName()));
        
        // the difference of the second votes
        this.contentPane.add(new DiagramPanel(new ComparisonBarChartPercent(comparison, comparison::differenceSecondVotes, false),
                                              "Unterschied der Zweistimmen"));
        
        // the difference in percent of the second votes
        this.contentPane.add(new DiagramPanel(new ComparisonBarChartPercent(comparison, comparison::differenceSecondVotesPercent, true),
                                              "Unterschied der Zweistimmen (in Prozent)"));
        
        // the difference in percent of the bundestag seats
        this.contentPane.add(new DiagramPanel(new ComparisonBarChartPercent(comparison, comparison::differenceBundestagSeats, false),
                                              "Unterschied der Bundestagsitze"));
        
        // the difference in percent of the bundestag seats
        this.contentPane.add(new DiagramPanel(new ComparisonBarChartPercent(comparison, comparison::differenceBundestagSeatsPercent, true),
                                              "Unterschied der Bundestagsitze (in Prozent)"));
        
        this.add(GuiHelper.createScrollPane(this.contentPane), BorderLayout.CENTER);
    }
    
    @Override
    public String getName() {
        return this.name;
    }
    
    public ElectionComparison getElectionComparison() {
        return this.comparison;
    }
    
    private class MappingDialog extends JDialog {
        private static final long serialVersionUID = 1L;
        private final Comparator<Party> comparator = (p1, p2) -> p1.getName().compareTo(p2.getName());
        
        private GridBagConstraints c = new GridBagConstraints();
        
        private MappingDialog() {
            super(Mandatsverteilung.getMainWindow());
            this.setTitle("Mapping für Vergleich hinzufügen");
            this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            this.setLayout(new GridBagLayout());
            this.c.anchor = GridBagConstraints.WEST;
            this.c.weighty = 0.01;
            this.c.gridy = 0;
            this.c.gridx = 0;
            this.add(new JLabel("Partei aus Wahl 1:"), this.c);
            this.c.gridx = 1;
            this.add(new JLabel("Partei aus Wahl 2:"), this.c);
            
            this.c.weighty = 100;
            this.c.gridy++;
            Party[] parties1 = TabComparison.this.comparison.getElectionResult1Partys();
            Arrays.sort(parties1, this.comparator);
            Party[] parties2 = TabComparison.this.comparison.getElectionResult2Partys();
            Arrays.sort(parties2, this.comparator);
            this.c.gridx = 0;
            JList<Party> p1List = new JList<>(parties1);
            this.add(p1List, this.c);
            this.c.gridx = 1;
            JList<Party> p2List = new JList<>(parties2);
            this.add(p2List, this.c);
            
            this.c.weighty = 0.01;
            this.c.gridy++;
            this.c.anchor = GridBagConstraints.CENTER;
            this.c.gridx = 0;
            JButton cancel = new JButton("Abbruch");
            cancel.addActionListener(e -> this.dispose());
            this.add(cancel, this.c);
            this.c.gridx = 1;
            JButton map = new JButton("Mappen");
            map.addActionListener(e -> {
                Party party1 = p1List.getSelectedValue();
                Party party2 = p2List.getSelectedValue();
                if(party1 == null || party2 == null) {
                    GuiHelper.showError("Keine Gültigen Einträge ausgewählt");
                }
                else {
                    TabComparison.this.comparison.mapPartysForComparsion(party1, party2);
                    this.dispose();
                }
            });
            this.add(map, this.c);
        }
        
        private void showDialog() {
            this.pack();
            this.setLocationRelativeTo(Mandatsverteilung.getMainWindow());
            this.setVisible(true);
        }
    }
}
