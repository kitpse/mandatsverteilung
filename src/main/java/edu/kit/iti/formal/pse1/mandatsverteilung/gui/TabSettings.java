/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.List;
import java.util.Optional;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import edu.kit.iti.formal.pse1.mandatsverteilung.calculation.CalculationMethod;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Propagation;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Settings;
import edu.kit.iti.formal.pse1.mandatsverteilung.propagation.NDParametersDialog;
import edu.kit.iti.formal.pse1.mandatsverteilung.propagation.NormalDistribution;
import edu.kit.iti.formal.pse1.mandatsverteilung.propagation.PropagationMath;

/**
 * The {@link TabSettings} is a {@link Tab} containing all the settings hold in {@link Settings} and the possibility to change the {@link PropagationMath}.
 * 
 * @author Felix Heim
 * @version 3
 */
public class TabSettings extends Tab {
    /** the {@link Settings} which are displayed an can be changed */
    private Settings settings;
    
    /** the a link to the {@link Propagation} to change the {@link PropagationMath} */
    private Propagation propagation;
    
    private GridBagConstraints c = new GridBagConstraints();
    private JPanel contentPane = new JPanel();
    private JComponent overlay = GuiHelper.createScrollPane(this.contentPane);
    
    /**
     * @param settings
     *            the {@link Settings}s associated with this tab
     * @param propagation
     *            the {@link Propagation} holding the {@link PropagationMath} to change
     */
    public TabSettings(Settings settings, Propagation propagation) {
        this.settings = settings;
        this.propagation = propagation;
        this.contentPane.setLayout(new GridBagLayout());
        
        this.c.weighty = 1;
        this.addLinePercentageHurdle();
        this.addLineBundestagSize();
        this.addLineCalculationMethod();
        this.addLinePropagationMath();
    }
    
    private void addLinePercentageHurdle() {
        this.c.gridy = 1;
        this.c.gridx = 0;
        this.c.weightx = 0.1;
        this.c.anchor = GridBagConstraints.WEST;
        this.c.fill = GridBagConstraints.BOTH;
        this.contentPane.add(new JLabel("Prozenth\u00FCrde:"), this.c);
        
        this.c.gridx = 1;
        this.c.weightx = 1;
        this.c.anchor = GridBagConstraints.EAST;
        this.c.fill = GridBagConstraints.HORIZONTAL;
        JTextField percentageHurdle = new JTextField(4);
        percentageHurdle.setHorizontalAlignment(SwingConstants.HORIZONTAL);
        percentageHurdle.setText(GuiHelper.doubleToPercentString(this.settings.getPercentHurdle()) + " %");
        GuiHelper.addValidator(this.overlay, percentageHurdle, (textField) -> {
            /* get the the percent value */
            Optional<Double> newValue = GuiHelper.percentStringToDouble(textField.getText());
            /* check if it's a valid percent value */
            if(GuiHelper.validatePercent(newValue)) {
                /* true -> set it */
                this.settings.setPercentHurdle(newValue.get());
            }
            else {
                /* false -> show error */
                GuiHelper.showError("Wert im Textfeld ist kein g\u00FCltiger Prozentsatz");
                textField.requestFocus();
            }
        });
        this.contentPane.add(percentageHurdle, this.c);
    }
    
    private void addLineBundestagSize() {
        this.c.gridy = 2;
        this.c.gridx = 0;
        this.c.weightx = 0.1;
        this.c.anchor = GridBagConstraints.WEST;
        this.c.fill = GridBagConstraints.BOTH;
        this.contentPane.add(new JLabel("Nominelle Bundestagsgr\u00F6\u00DFe:"), this.c);
        
        this.c.gridx = 1;
        this.c.weightx = 1;
        this.c.anchor = GridBagConstraints.EAST;
        this.c.fill = GridBagConstraints.HORIZONTAL;
        JTextField parliamentSize = new JTextField(4);
        parliamentSize.setHorizontalAlignment(SwingConstants.HORIZONTAL);
        parliamentSize.setText(Integer.toString(this.settings.getParliamentSize(), 10));
        GuiHelper.addValidator(this.overlay, parliamentSize, (textField) -> {
            try {
                /* get the new value */
                int newValue = Integer.parseInt(textField.getText());
                if(newValue < 1) {
                    /* if it's too low */
                    throw new NumberFormatException("Number ist to small");
                }
                /* if valid, then set it */
                this.settings.setParliamentSize(newValue);
            }
            catch(NumberFormatException e) {
                /* if it's invalid show an error */
                GuiHelper.showError("Kein G\u00FCltiger Wert eingegeben!");
                textField.requestFocus();
            }
        });
        this.contentPane.add(parliamentSize, this.c);
    }
    
    private void addLineCalculationMethod() {
        this.c.gridy = 3;
        this.c.gridx = 0;
        this.c.weightx = 0.1;
        this.c.anchor = GridBagConstraints.WEST;
        this.c.fill = GridBagConstraints.BOTH;
        this.contentPane.add(new JLabel("Berechnungsverfahren:"), this.c);
        
        this.c.gridx = 1;
        this.c.weightx = 1;
        this.c.anchor = GridBagConstraints.EAST;
        this.c.fill = GridBagConstraints.HORIZONTAL;
        List<CalculationMethod> methods = Settings.getMethods(); // a list of all valid calculation methods
        JComboBox<String> calculationMethod = new JComboBox<>(methods.stream().map(method -> method.getDescription()).toArray(String[]::new)); // show the descriptions of the elements in a list
        calculationMethod.setSelectedIndex(methods.indexOf(this.settings.getCalculationMethod()));
        calculationMethod.addActionListener(e -> this.settings.setCalculationMethod(methods.get(calculationMethod.getSelectedIndex()))); // on change, change in settings
        this.contentPane.add(calculationMethod, this.c);
    }
    
    private void addLinePropagationMath() {
        this.c.gridy = 4;
        this.c.gridx = 0;
        this.c.weightx = 0.1;
        this.c.anchor = GridBagConstraints.WEST;
        this.c.fill = GridBagConstraints.BOTH;
        this.contentPane.add(new JLabel("Propagierungsmethode:"), this.c);
        
        this.c.gridx = 1;
        this.c.weightx = 1;
        this.c.anchor = GridBagConstraints.EAST;
        this.c.fill = GridBagConstraints.HORIZONTAL;
        List<PropagationMath> methods = Propagation.getMethods(); // a list of all valid propagation methods
        JComboBox<String> propagationMethod = new JComboBox<>(methods.stream().map(method -> method.getDescription()).toArray(String[]::new)); // show the descriptions of the elements in a list
        propagationMethod.setSelectedIndex(methods.indexOf(this.propagation.getPropagation()));
        propagationMethod.addActionListener(e -> {
            /* on change, change in propagation */
            PropagationMath propagation = methods.get(propagationMethod.getSelectedIndex());
            if(propagation instanceof NormalDistribution) {
                /* if the new propagation is a NormalDistribution then we need to add some configuration */
                new NDParametersDialog((NormalDistribution) propagation);
            }
            /* finally set the new method */
            this.propagation.setPropagation(propagation);
        });
        this.contentPane.add(propagationMethod, this.c);
        
    }
    
    @Override
    public JComponent getContentPane() {
        return this.overlay;
    }
}
