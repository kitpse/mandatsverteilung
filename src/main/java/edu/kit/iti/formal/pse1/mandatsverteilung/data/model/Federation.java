/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.data.model;

/**
 * A federation stores how many votes which party got. Changes will be propagated to the federal states, which are the children.
 * 
 * @author unknown, Nils Wilka
 * @version 1.0
 */
public class Federation extends ElectionArea<FederalState> {
    
    /**
     * Creates a new Federation.
     * 
     * @param name
     *            The name of the federation.
     * @param electionResult
     *            The {@code ElectionResult} this federation belongs to.
     */
    public Federation(String name, ElectionResult electionResult) {
        super(name, electionResult);
    }
    
    @Override
    public Federation deepClone() {
        Federation copy = new Federation(this.getName(), this.getElectionResult());
        for(FederalState state : this.getChildren()) {
            copy.addChild(state.deepClone());
        }
        if(this.isLocked()) {
            copy.lock();
        }
        return copy;
    }
}
