/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.gui;

import java.util.stream.Collectors;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Federation;

/**
 * This class represents the second votes in a {@link Federation}. It uses an ordinary BarChart but only renders party where the votes are displayable (means at
 * least one pixel bar height).
 * 
 * @author Felix Heim
 * @version 1
 */
class SimpleBarChartPercent extends SimpleBarChart {
    private static final long serialVersionUID = 1L;
    
    /** the {@link Federation} the second votes are from */
    private Federation federation;
    
    /**
     * @param federation
     *            the {@link Federation} the second votes are from
     */
    SimpleBarChartPercent(Federation federation) {
        this.federation = federation;
        federation.addChangeListener(this::update);
        this.update();
    }
    
    private void update() {
        this.setValues(this.federation.getParties()
        // create a stream of all Parties
                .stream()
                // maps the party to a triple, containing a description (the name of the party + the seats), a double representing the amount of seats and the color of the party
                .map(party -> new DisplayableValues(party.getName(), this.federation.getSecondVotes(party), party.getColor()))
                // we only want non-zero values, so we run a filter
                .filter(values -> values.value > 0)
                // sort the values with a descending order, so biggest gets displayed first
                .sorted((values1, values2) -> Double.compare(values2.value, values1.value))
                // collect all elements to a list
                .collect(Collectors.toList()));
    }
}
