/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.data.model;

/**
 * Stores the number of (first and second) votes.
 * 
 * @author unknown
 * @version 1.0
 */
public class Vote {
    
    /**
     * The first votes from the @{link Party} this votes belong to.
     */
    private int firstVotes;
    
    /**
     * The second votes from the @{link Party} this votes belong to.
     */
    private int secondVotes;
    
    /**
     * Describes, if these votes are affected by changes.
     */
    private boolean isLocked;
    
    /**
     * Gets the first votes.
     * 
     * @return the firstVotes
     */
    public int getFirstVotes() {
        return this.firstVotes;
    }
    
    /**
     * Sets the first votes with given integer.
     * 
     * @param firstVotes
     *            The firstVotes to set
     */
    public void setFirstVotes(int firstVotes) {
        assert !this.isLocked;
        this.firstVotes = firstVotes;
    }
    
    /**
     * Gets the second votes.
     * 
     * @return The secondVotes.
     */
    public int getSecondVotes() {
        return this.secondVotes;
    }
    
    /**
     * Sets the second votes with given integer.
     * 
     * @param secondVotes
     *            the second votes to set
     */
    public void setSecondVotes(int secondVotes) {
        assert !this.isLocked;
        this.secondVotes = secondVotes;
    }
    
    /**
     * Locks the @{link Party}.
     */
    public void lock() {
        this.isLocked = true;
    }
    
    /**
     * Unlocks the @{link Party}.
     */
    public void unlock() {
        this.isLocked = false;
    }
    
    /**
     * Tells if the @{link Party} is locked.
     * 
     * @return True if locked, else false.
     */
    public boolean isLocked() {
        return this.isLocked;
    }
    
    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof Vote)) {
            return false;
        }
        Vote comp = (Vote) obj;
        return comp.isLocked == this.isLocked && comp.firstVotes == this.firstVotes && comp.secondVotes == this.secondVotes;
    }
    
    @Override
    public int hashCode() {
        int isLockedHash;
        if(this.isLocked) {
            isLockedHash = 1;
        }
        else {
            isLockedHash = 2;
        }
        return this.firstVotes + this.secondVotes + isLockedHash;
    }
    
}
