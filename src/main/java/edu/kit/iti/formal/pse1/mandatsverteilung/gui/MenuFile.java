/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.gui;

import java.awt.Event;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;
import javax.swing.filechooser.FileFilter;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.io.Import;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.management.Session;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Election;
import edu.kit.iti.formal.pse1.mandatsverteilung.gui.controller.Mandatsverteilung;

/**
 * The {@link MenuFile} contains {@link JMenuItem}s to load and save {@link Election}s and {@link Session}s. It also has an option to change the autosave timer
 * for the {@link Session} and an {@link JMenuItem} to close the program.
 * 
 * @author Felix Heim
 * @version 4
 */
class MenuFile extends JMenu {
    private static final long serialVersionUID = 1L;
    
    /** an array holding all the internal elections */
    private static final String[][] INTERNAL_ELECTIONS = {{"Bundestagswahl 2005", "bundestagswahl2005.csv"}, {"Bundestagswahl 2009", "bundestagswahl2009.csv"},
                                                          {"Bundestagswahl 2013", "bundestagswahl2013.csv"},
                                                          {"Bundestagswahl 2013 - Neg.Sgw Bayern SPD", "bundestagswahl2013-NegSgw-Bayern-SPD.csv"}};
    
    private MainWindow mainWindow;
    
    /** a {@link JFileChooser} only accepting csv files */
    private JFileChooser fileChooserCsv = MenuFile.getFileChooser(MenuFile.getFileFilter("csv"));
    
    /** a {@link JFileChooser} accepting every file */
    private JFileChooser fileChooser = MenuFile.getFileChooser(null);
    
    MenuFile(MainWindow mainWindow) {
        super("Datei");
        this.mainWindow = mainWindow;
        this.addOpenFile();
        this.addSaveElection();
        this.addCloseTab();
        this.add(new JSeparator());
        this.addSetAutoSaveTimer();
        this.add(new JSeparator());
        this.addSaveSession();
        this.addOpenSession();
        this.add(new JSeparator());
        this.addOpenInternalElection();
        this.addCloseProgram();
    }
    
    private void addOpenFile() {
        JMenuItem openFile = new JMenuItem("Datei \u00F6ffnen...");
        openFile.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask())); // add shortcut
        openFile.addActionListener(e -> {
            if(this.fileChooserCsv.showOpenDialog(this.mainWindow) == JFileChooser.APPROVE_OPTION) { // try to select a file in the file chooser
                String path = this.fileChooserCsv.getSelectedFile().getAbsolutePath(); // get the absolute path to that file
                try {
                    Election election = Session.getSession().loadElection(path);
                    Mandatsverteilung.log("Successfully loaded election \"" + election.getName() + "\" from " + path); // success -> log to the console
                }
                catch(Exception ex) {
                    Mandatsverteilung.debugStackTraces(ex);
                    GuiHelper.showError("Problem beim laden der Wahl von " + path + ":\n" + Mandatsverteilung.getNestedErrorMessages(ex));
                }
            }
        });
        this.add(openFile);
        
    }
    
    private void addSaveElection() {
        JMenuItem saveElection = new JMenuItem("Wahl speichern");
        saveElection.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask())); // add shortcut
        saveElection.addActionListener(e -> this.mainWindow.getSelectedTabElection().ifPresent(tabElection -> { // if an elections is selected
            if(this.fileChooser.showSaveDialog(this.mainWindow) == JFileChooser.APPROVE_OPTION) { // try to select a file in the file chooser
                String path = this.fileChooser.getSelectedFile().getAbsolutePath(); // get the absolute path to that file
                try {
                    Session.getSession().saveElection(tabElection.getElection(), path); // try to save it
                    Mandatsverteilung.log("Successfully saved Election \"" + tabElection.getElection().getName() + "\" at " + path); // success -> log to the console
                }
                catch(IOException ioe) {
                    Mandatsverteilung.debugStackTraces(ioe);
                    GuiHelper.showError("Fehler beim Speichern der Wahl: " + Mandatsverteilung.getNestedErrorMessages(ioe)); // failed -> error
                }
            }
        }));
        this.add(saveElection);
    }
    
    private void addCloseTab() {
        JMenuItem closeTab = new JMenuItem("Tab schlie\u00DFen");
        closeTab.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask())); // add shortcut
        closeTab.addActionListener(e -> this.mainWindow.closeSelectedTab());
        this.add(closeTab);
    }
    
    private void addSetAutoSaveTimer() {
        JMenuItem setAutoSaveTime = new JMenuItem("Setze Autospeicherung");
        setAutoSaveTime.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask() | Event.ALT_MASK)); // add shortcut
        setAutoSaveTime.addActionListener(e -> {
            try {
                String input = JOptionPane.showInputDialog(this.mainWindow, "Zeitintervall der Autospeicherung (in Sekunden)", // input of the new time as String
                                                           Session.getSession().getAutoSaveInterval() / 1000);
                if(input != null) { // if it wasn't aborted
                    Session.getSession().setAutoSaveInterval(Integer.parseInt(input) * 1000); //  convert it to millis and set it
                }
            }
            catch(NumberFormatException nfe) {
                GuiHelper.showError("Fehler bei der Eingabe des Zeitintervalls: " + nfe.getMessage()); // if an error occurs, then warn the user
            }
        });
        this.add(setAutoSaveTime);
    }
    
    private void addSaveSession() {
        JMenuItem saveSession = new JMenuItem("Sitzung speichern");
        saveSession.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask() | Event.ALT_MASK)); // add shortcut
        saveSession.addActionListener(e -> {
            if(this.fileChooser.showSaveDialog(this.mainWindow) == JFileChooser.APPROVE_OPTION) { // try to select a file in the file chooser
                    String path = this.fileChooser.getSelectedFile().getAbsolutePath(); // get the absolute path to that file
                    try {
                        Session.getSession().saveSession(path); // try to save it
                        GuiHelper.showInfo("Die Sitzung wurde erfolgreich gespeichert", "Sitzung gespeichert!"); // success -> log to the console
                    }
                    catch(IOException ioe) {
                        Mandatsverteilung.debugStackTraces(ioe);
                        GuiHelper.showError("Fehler beim Speichern der Sitzung!\n" + Mandatsverteilung.getNestedErrorMessages(ioe)); // failed -> error
                    }
                }
            });
        this.add(saveSession);
        
    }
    
    private void addOpenSession() {
        JMenuItem openSession = new JMenuItem("Sitzung laden");
        openSession.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, Toolkit.getDefaultToolkit().getMenuShortcutKeyMask() | Event.ALT_MASK)); // add shortcut
        openSession.addActionListener(e -> {
            if(this.fileChooser.showOpenDialog(this.mainWindow) == JFileChooser.APPROVE_OPTION) { // try to select a file in the file chooser
                    String path = this.fileChooser.getSelectedFile().getAbsolutePath(); // get the absolute path to that file
                    try {
                        if(Session.getSession().loadSession(path)) { // try to load it
                            Mandatsverteilung.log("Successfully loaded Session from " + path); // success -> log to the console
                        }
                        else {
                            throw new IOException("Unkown error loading the Session.");
                        }
                    }
                    catch(Exception ex) {
                        Mandatsverteilung.debugStackTraces(ex);
                        GuiHelper.showError("Fehler beim Laden der Sitzung von " + path + ":\n" + Mandatsverteilung.getNestedErrorMessages(ex)); // failed -> error
                    }
                }
            });
        this.add(openSession);
        
    }
    
    private void addOpenInternalElection() {
        JMenu openInternalFile = new JMenu("Mitgelieferte Ergebnisse");
        for(int i = 0; i < MenuFile.INTERNAL_ELECTIONS.length; i++) {
            JMenuItem openElection = new JMenuItem(MenuFile.INTERNAL_ELECTIONS[i][0]);
            final int j = i;
            openElection.addActionListener(e -> Session.getSession().addElection(Import.importInternalElection(MenuFile.INTERNAL_ELECTIONS[j][1])));
            openInternalFile.add(openElection);
        }
        this.add(openInternalFile);
    }
    
    private void addCloseProgram() {
        if(!Mandatsverteilung.isOnMac()) {
            this.add(new JSeparator());
            JMenuItem closeProgram = new JMenuItem("Programm Beenden");
            closeProgram.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4, Event.ALT_MASK)); // add shortcut
            closeProgram.addActionListener(e -> System.exit(0));
            this.add(closeProgram);
        }
    }
    
    /**
     * Creates a {@link JFileChooser} with the given {@link FileFilter}.<br>
     * In the {@link JFileChooser} only one File can be selected and hidden files are shown.<br>
     * (This is a helper method copied from one of my private projects)
     * 
     * @param fileFilter
     *            the {@link FileFilter} to use in the {@link JFileChooser}
     * @return the newly created {@link JFileChooser}
     */
    private static JFileChooser getFileChooser(FileFilter fileFilter) {
        JFileChooser fileChooser = new JFileChooser(System.getProperty("user.dir"));
        fileChooser.setMultiSelectionEnabled(false);
        fileChooser.setAcceptAllFileFilterUsed(false);
        fileChooser.setFileHidingEnabled(true);
        fileChooser.setFileFilter(fileFilter);
        return fileChooser;
    }
    
    /**
     * Creates a {@link FileFilter} only accepting the endings passed.<br>
     * (This is a helper method copied from one of my private projects)
     * 
     * @param firstAcceptedEnding
     *            the first ending (we need at least one)
     * @param othterAcceptedEndings
     *            some potential more accepted endings
     * @return the newly created {@link FileFilter}
     */
    private static FileFilter getFileFilter(String firstAcceptedEnding, String... othterAcceptedEndings) {
        StringBuilder descriptionBuilder = new StringBuilder(2 + othterAcceptedEndings.length * 2);
        descriptionBuilder.append("*.").append(firstAcceptedEnding);
        for(String acceptedEnding : othterAcceptedEndings) {
            descriptionBuilder.append(", *.").append(acceptedEnding);
        }
        String descrition = descriptionBuilder.toString();
        
        return new FileFilter() {
            @Override
            public String getDescription() {
                return descrition;
            }
            
            @Override
            public boolean accept(File f) {
                if(f.isDirectory()) {
                    return true;
                }
                String path = f.getPath().toLowerCase();
                for(String ending : othterAcceptedEndings) {
                    if(path.endsWith("." + ending)) {
                        return true;
                    }
                }
                return path.endsWith("." + firstAcceptedEnding);
            }
        };
    }
}
