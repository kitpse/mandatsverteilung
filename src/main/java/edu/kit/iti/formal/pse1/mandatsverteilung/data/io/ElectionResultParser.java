/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.data.io;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.ElectionResult;

/**
 * Abstract class for parsing a string according to specified format into a {@code ElectionResult}. Implementing classes define this format and throw a
 * FormatException if the given string does not fulfill it.
 * 
 * @author Nils Wilka
 * @version 2.3
 */
abstract class ElectionResultParser {
    
    /**
     * The line containing the information about the meaning of every column.
     */
    protected static final int POS_HEAD_INFOLINE = 0;
    
    /**
     * Additional information that has to be in the correct format.
     */
    protected static final int POS_HEAD_SUBLINE = 1;
    
    /**
     * Marks the end of the head.
     */
    protected static final int POS_HEAD_END = 2;
    
    /**
     * Marks the start of the body.
     */
    protected static final int POS_BODY_START = 3;
    
    /**
     * The regular expression the IDs of the elections areas need to match.
     */
    protected static final String ID_REGEX = "[0-9]+";
    
    /**
     * The Parser that has the lines.
     */
    protected final CSVParser parser;
    
    /**
     * Creates a new ElectionResultParser with given string array lines to parse.
     * 
     * @param lines
     *            The data to be parsed.
     * @throws NullPointerException
     *             Thrown if lines is null.
     * @throws FormatException
     *             Thrown if format of given string array is wrong. In this case if there are not enough lines.
     */
    public ElectionResultParser(final String[] lines) throws NullPointerException, FormatException {
        this(lines, 0);
    }
    
    /**
     * Creates a new ElectionResultParser with given string array lines to parse.
     * 
     * @param lines
     *            The data to be parsed.
     * @param start
     *            The number of lines that will be skipped at the start.
     * @throws NullPointerException
     *             Thrown if lines is null.
     * @throws FormatException
     *             Thrown if format of given string array is wrong. In this case if there are not enough lines.
     */
    public ElectionResultParser(final String[] lines, int start) throws NullPointerException, FormatException {
        if(lines.length - start < this.minNumberOfRows()) {
            throw new FormatException("Expected a minimum of " + this.minNumberOfRows() + ", but got " + lines.length + ".");
        }
        this.parser = new CSVParser(lines, start);
        if(lines.length < this.minNumberOfColumns()) {
            throw new FormatException("Expected a minimum of " + this.minNumberOfColumns() + ", but got " + lines.length + ".");
        }
    }
    
    /**
     * Creates a new ElectionResultParser with given string data to parse.
     * 
     * @param data
     *            The data to be parsed. Will be split at line break.
     * @param start
     *            The number of lines that will be skipped at the start.
     * @throws NullPointerException
     *             Thrown if data is null.
     * @throws FormatException
     *             Thrown if format of given string array is wrong. In this case if there are not enough lines.
     */
    public ElectionResultParser(final String data, int start) throws NullPointerException, FormatException {
        this(data.split("\n"), start);
    }
    
    /**
     * Creates a new ElectionResultParser with given string data to parse.
     * 
     * @param data
     *            The data to be parsed. Will be split at line break.
     * @throws NullPointerException
     *             Thrown if data is null.
     * @throws FormatException
     *             Thrown if format of given string array is wrong. In this case if there are not enough lines.
     */
    public ElectionResultParser(final String data) throws NullPointerException, FormatException {
        this(data, 0);
    }
    
    /**
     * Parses the given String and creates a {@code ElectionResult} with contained values. Does only set eligible voters values.
     * 
     * @param data
     *            The String containing the data to create {@code ElectionResult} from.
     * @return A new ElectionResult with values contained in {@code data},
     * @throws FormatException
     *             Thrown if given String does not fulfill the specified format.
     */
    public abstract ElectionResult parseEligibleVoters() throws FormatException;
    
    /**
     * Parses the given String and creates a {@code ElectionResult} with contained values.
     * 
     * @param data
     *            The String containing the data to create {@code ElectionResult} from.
     * @return A new ElectionResult with values contained in {@code data},
     * @throws FormatException
     *             Thrown if given String does not fulfill the specified format.
     */
    public abstract ElectionResult parseElectionResult() throws FormatException;
    
    /**
     * Gets the minimum number of lines this parser needs. This method should be overridden by subclasses if format does not need at least 38 lines.
     * 
     * @return The minimum number of lines this parser needs.
     */
    protected int minNumberOfRows() {
        // 5 lines head, 1 federation, 16 federal states with 1 constituency each.
        return 38;
    }
    
    /**
     * Gets the minimum number of columns this parser needs. This method should be overridden by subclasses if format does not need at least 10 columns.
     * 
     * @return The minimum number of columns this parser needs.
     */
    protected int minNumberOfColumns() {
        // {"Nr", "Gebiet", "gehoert zu", "Buerger", "Waehler", "Wahlberechtigte", "Gueltige", "", "Ungueltige", ""}
        return 10;
    }
}
