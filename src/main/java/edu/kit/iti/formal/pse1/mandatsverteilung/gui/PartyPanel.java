/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.util.Optional;
import java.util.function.Consumer;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.ElectionArea;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Party;
import edu.kit.iti.formal.pse1.mandatsverteilung.gui.controller.Mandatsverteilung;

/**
 * A {@link PartyPanel} displays the information like a {@link NamedPanel} for a specific {@link Party} with an extra row for the direct mandates.
 * 
 * @author Felix Heim
 * @version 1
 */
class PartyPanel extends NamedPanel {
    private static final long serialVersionUID = 1L;
    
    /** the field for the amount of won direct mandates */
    private JTextField directMandatesNumber = new JTextField(7);
    /** the field for the percent of won direct mandates */
    private JTextField directMandatesPercent = new JTextField(4);
    /** the slider for the amount of won direct mandates */
    private JSlider directMandatesSlider = new JSlider(SwingConstants.HORIZONTAL);
    
    /** the area where the values to display are from */
    private final ElectionArea<?> area;
    /** the party for which the values should be displayed */
    private final Party party;
    
    /** the {@link JLabel} which shows the {@link Color} of the {@link #party} */
    private final JLabel colorLabel = new JLabel();
    /** the function used to update the color and then notify the {@link #area} */
    private Consumer<Color> colorUpdater;
    /** the {@link JLabel} which shows the political orientation of the {@link #party} */
    private final JLabel politicalOrientationLabel = new JLabel();
    
    /** whether the initialization has been finished */
    private boolean initFinished = false;
    
    /**
     * @param area
     *            the area where the values to display are from
     * @param party
     *            the party for which the values should be displayed
     */
    PartyPanel(ElectionArea<?> area, Party party) {
        super(party.getName(), null, null, null, null);
        this.area = area;
        this.party = party;
        
        Consumer<Color> colorUpdaterPart = this.party::setColor;
        this.colorUpdater = colorUpdaterPart.andThen(color -> area.onChange());
        
        // sets the functions used to get the values
        this.setGetter(() -> area.getSecondVotes(party), area::getValidSecondVotes, () -> area.getFirstVotes(party), area::getValidFirstVotes);
        // sets the functions used to set the values in area when they get changed in the GUI
        this.setSetter(amount -> area.changeSecondVotes(party, amount), amount -> area.changeFirstVotes(party, amount));
        
        this.addDirectMandates("Direktmandate");
        this.addColorLabel();
        this.c.gridy--;
        this.addPoliticalOrientation();
        this.initFinished = true;
        this.update();
    }
    
    @Override
    void update() {
        if(this.initFinished) {
            super.update();
            this.isUpdating = true;
            this.colorLabel.setBackground(this.party.getColor());
            this.politicalOrientationLabel.setText(" Pol. Ausrichtung: " + this.party.getPoliticalOrientation());
            this.updateDirectMandates();
            this.isUpdating = false;
        }
    }
    
    private void addDirectMandates(String labelName) {
        // first add the label
        this.c.gridx = 0;
        this.c.gridy = 2;
        this.c.weighty = 1;
        this.bigCard.add(new JLabel(labelName), this.c);
        
        // next the actual number
        this.c.gridx = 1;
        this.directMandatesNumber.setEditable(false);
        this.bigCard.add(this.directMandatesNumber, this.c);
        
        // then the percentage
        this.c.gridx = 2;
        this.directMandatesPercent.setEditable(false);
        this.bigCard.add(this.directMandatesPercent, this.c);
        
        // the button to reduce the votes (has only visual functionality)
        this.c.gridx = 3;
        JButton minus = new JButton("-");
        minus.setEnabled(false);
        this.bigCard.add(minus, this.c);
        
        // in the middle the slider
        this.directMandatesSlider.setMinimum(0);
        this.c.gridx = 4;
        this.c.weightx = 1000;
        this.c.fill = GridBagConstraints.BOTH;
        this.directMandatesSlider.setEnabled(false);
        this.bigCard.add(this.directMandatesSlider, this.c);
        this.c.weightx = 0.1;
        this.c.fill = GridBagConstraints.NONE;
        
        // the button to increase the votes (has only visual functionality)
        this.c.gridx = 5;
        JButton plus = new JButton("+");
        plus.setEnabled(false);
        this.bigCard.add(plus, this.c);
    }
    
    private void addColorLabel() {
        JLabel colorLabelText = new JLabel("  Farbe:");
        double buttonWidth = NamedPanel.getButtonDimension().width;
        Dimension labelDim = colorLabelText.getPreferredSize();
        this.c.gridx = 6;
        // calculate the weight so the resizing isn't awkward
        this.c.weightx = labelDim.width / (labelDim.width + buttonWidth) * 0.01;
        this.bigCard.add(colorLabelText, this.c);
        this.c.weightx = 0.01 - this.c.weightx;
        this.c.gridx = 7;
        // limit the size of the label so it looks better
        this.colorLabel.setPreferredSize(new Dimension((int) ((buttonWidth - labelDim.width) * 0.8), labelDim.height));
        // make it opaque so the color actually gets rendered
        this.colorLabel.setOpaque(true);
        // add a listener to change the color when the color is clicked 
        GuiHelper.addMouseClickedListener(this.colorLabel, e -> Optional.ofNullable(
        /* show a color chooser dialog with the color set to the old color of the party */
        JColorChooser.showDialog(this, "W\u00E4hle neue Farbe f\u00FCr die Partei \"" + this.party.getName() + "\"", this.party.getColor()))
        /* if the selected value isn't null, then update it using the update function */
        .ifPresent(this.colorUpdater));
        this.bigCard.add(this.colorLabel, this.c);
        this.c.weightx = 0.01;
    }
    
    private void addPoliticalOrientation() {
        this.c.gridx = 6;
        this.c.gridwidth = 2;
        this.c.anchor = GridBagConstraints.WEST;
        GuiHelper
                .addMouseClickedListener(this.politicalOrientationLabel,
                                         e -> {
                                             String input = JOptionPane.showInputDialog(Mandatsverteilung.getMainWindow(),
                                                                                        "Politische Einstellung von standardmä\u00DFig -100.0 (ganz links) bis 100.0 (ganz rechts)",
                                                                                        "W\u00E4hle die Politische Einstellung der Partei",
                                                                                        JOptionPane.PLAIN_MESSAGE);
                                             if(input != null) {
                                                 try {
                                                     this.party.setPoliticalOrientation(Double.parseDouble(input));
                                                     this.area.onChange();
                                                 }
                                                 catch(NumberFormatException nfe) {
                                                     GuiHelper.showError("Fehler bei der Eingabe der neuen Politischen Ausrichtung: " + nfe.getMessage());
                                                 }
                                             }
                                         });
        this.bigCard.add(this.politicalOrientationLabel, this.c);
    }
    
    private void updateDirectMandates() {
        int directMandates = this.area.getWonDirectMandates(this.party);
        int totalDirectMandates = this.area.getLeaveCount();
        this.directMandatesNumber.setText(Integer.toString(directMandates));
        this.directMandatesPercent.setText(GuiHelper.doubleToPercentString((double) directMandates / totalDirectMandates) + " %");
        this.directMandatesSlider.setMaximum(totalDirectMandates);
        this.directMandatesSlider.setValue(directMandates);
    }
}
