/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; ; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.propagation;

import java.util.List;
import java.util.Optional;

/**
 * Interface for classes that provide the math for propagating changes (an integer value) to a set of bins (int[]).
 * 
 * @author Axel Trefzer, Ben Wilhelm
 * @version 2.0
 */
public interface PropagationMath {
    /**
     * Distributes the elements to given bins.
     * 
     * @param bins
     *            the bins with their actual level. The values of the array will be changed by distributing the elements to the bins.
     * @param elements
     *            the number of "elements" that should be propagated to the bins.
     */
    public void propagate(int[] bins, int[] maxBins, int elements);
    
    /**
     * Returns a description of the applied method.
     * 
     * @return a short description text.
     */
    String getDescription();
    
    /**
     * Gets the (optional) list of parameters.
     * 
     * @return Optional.empty() if not specified or the list of parameters specified by the implementing class.
     */
    default Optional<List<? extends Object>> getParameters() {
        return Optional.empty();
    }
}
