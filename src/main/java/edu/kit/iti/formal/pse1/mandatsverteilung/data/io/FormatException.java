/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.data.io;

/**
 * This checked exception is used if a parser finds an unexpected value is for its type of format or more in general the format is wrong.
 * 
 * @author Nils Wilka
 * @version 1.2
 */
public class FormatException extends Exception {
    
    /**
     * Auto generated serial ID.
     */
    private static final long serialVersionUID = -7872034608316976536L;
    
    /**
     * The error message to thrown exception.
     */
    private final String message;
    
    /**
     * Default constructor with default error message.
     */
    public FormatException() {
        this.message = "Format of data is wrong.";
    }
    
    /**
     * Constructor using given string as error message.
     * 
     * @param message
     *            The error message that is given if {@code getMessage()} is called.
     */
    public FormatException(final String message) {
        this.message = message;
    }
    
    /**
     * Constructor using the given column and row indices as error message.
     * 
     * @param row
     *            Part of the error message that is given if {@code getMessage()} is called.
     * @param column
     *            Part of the error message that is given if {@code getMessage()} is called.
     */
    public FormatException(final int row, int column) {
        this.message = "Format Exception at position (" + row + "," + column + ")";
    }
    
    /**
     * Constructor using the given column and row indices as error message.
     * 
     * @param row
     *            Part of the error message that is given if {@code getMessage()} is called.
     * @param column
     *            Part of the error message that is given if {@code getMessage()} is called.
     * @param cause
     *            The exception that caused this exception to be thrown.
     */
    public FormatException(final int row, int column, final Throwable cause) {
        this.message = "Format Exception at position (" + row + "," + column + ")";
    }
    
    /**
     * Constructor using given string, column and row indices as error message.
     * 
     * @param message
     *            Part of the error message that is given if {@code getMessage()} is called.
     * @param row
     *            Part of the error message that is given if {@code getMessage()} is called.
     * @param column
     *            Part of the error message that is given if {@code getMessage()} is called.
     */
    public FormatException(final String message, final int row, int column) {
        this.message = message + " at position (" + row + "," + column + ")";
    }
    
    /**
     * Constructor using given string, column and row indices as error message.
     * 
     * @param message
     *            Part of the error message that is given if {@code getMessage()} is called.
     * @param row
     *            Part of the error message that is given if {@code getMessage()} is called.
     * @param column
     *            Part of the error message that is given if {@code getMessage()} is called.
     * @param cause
     *            The exception that caused this exception to be thrown.
     */
    public FormatException(final String message, final int row, int column, final Throwable cause) {
        this.message = message + " at position (" + row + "," + column + ")";
    }
    
    /**
     * Constructor using given string as error message. The cause parameter is used for exception chaining.
     * 
     * @param message
     *            The error message that is given if {@code getMessage()} is called.
     * @param cause
     *            The exception that caused this exception to be thrown.
     */
    public FormatException(final String message, final Throwable cause) {
        this.message = message;
        this.initCause(cause);
    }
    
    @Override
    public String getMessage() {
        return this.message;
    }
}
