/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.gui;

import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.LayoutManager;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.IntPredicate;
import java.util.function.Supplier;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.ElectionArea;

/**
 * The {@link NamedPanel} is designed to display rows of double values which are limited from 0 to a defined limit. The can be modified by entering the absolute
 * number, the percent of the total, by modifying the slider or by pressing the minus or plus button.
 * 
 * @author Felix Heim
 * @version 3
 */
class NamedPanel extends JPanel {
    private static final long serialVersionUID = 1L;
    /** the predefined {@link Dimension} for all {@link JButton}s */
    private static final Dimension BUTTON_DIMENSION = new JButton("Details verbergen").getPreferredSize();
    /** the predefined {@link Dimension} for all {@link JLabel}s */
    private static final Dimension LABEL_SIZE = new JLabel("Direktmandate").getPreferredSize();
    
    /** whether the panel is updating its values */
    protected boolean isUpdating;
    
    /** the getter for the actual number of second votes */
    private Supplier<Integer> secondVotesPartGetter;
    /** the getter for the total number of second votes */
    private Supplier<Integer> secondVotesTotalGetter;
    /** the getter for the actual number of first votes */
    private Supplier<Integer> firstVotesPartGetter;
    /** the getter for the total number of first votes */
    private Supplier<Integer> firstVotesTotalGetter;
    
    /** the setter to set the actual number of second votes */
    private IntPredicate secondVotesSetter;
    /** the setter to set the actual number of first votes */
    private IntPredicate firstVotesSetter;
    
    /** the {@link LayoutManager} used by the panel */
    private CardLayout layout = new CardLayout();
    
    /** the small card which is shown before the panel gets expanded */
    protected JPanel smallCard = new JPanel();
    /** the big card which is shown when the panel gets expanded */
    protected JPanel bigCard = new JPanel();
    
    protected GridBagConstraints c = new GridBagConstraints();
    
    /** the field for the amount of second votes on the small card */
    private JTextField secondVotesNumberS = new JTextField(7);
    /** the field for the percent of second votes on the small card */
    private JTextField secondVotesPercentS = new JTextField(4);
    /** the slider for the amount of second votes on the small card */
    private JSlider secondVotesSliderS = new JSlider(SwingConstants.HORIZONTAL);
    
    /** the field for the amount of second votes on the big card */
    private JTextField secondVotesNumberB = new JTextField(7);
    /** the field for the percent of second votes on the big card */
    private JTextField secondVotesPercentB = new JTextField(4);
    /** the slider for the amount of second votes on the big card */
    private JSlider secondVotesSliderB = new JSlider(SwingConstants.HORIZONTAL);
    
    /** the field for the amount of first votes */
    private JTextField firstVotesNumber = new JTextField(7);
    /** the field for the percent of first votes */
    private JTextField firstVotesPercent = new JTextField(4);
    /** the slider for the amount of first votes */
    private JSlider firstVotesSlider = new JSlider(SwingConstants.HORIZONTAL);
    
    /** the button to show the expanded pane */
    private JButton buttonShowDetails = new JButton("Details");
    /** the button to hide the expanded pane */
    private JButton buttonHideDetails = new JButton("Details verbergen");
    
    /** the {@link Dimension} of the pane when it's not expanded */
    protected Dimension smallSize;
    
    /**
     * @param name
     *            the name to be displayed in the {@link TitledBorder}
     * @param secondVotesPart
     *            the getter for the actual number of second votes
     * @param secondVotesTotal
     *            the getter for the total number of second votes
     * @param firstVotesPart
     *            the getter for the actual number of first votes
     * @param firstVotesTotal
     *            the getter for the total number of first votes
     */
    NamedPanel(String name, Supplier<Integer> secondVotesPart, Supplier<Integer> secondVotesTotal, Supplier<Integer> firstVotesPart,
               Supplier<Integer> firstVotesTotal) {
        this.secondVotesPartGetter = secondVotesPart;
        this.secondVotesTotalGetter = secondVotesTotal;
        this.firstVotesPartGetter = firstVotesPart;
        this.firstVotesTotalGetter = firstVotesTotal;
        this.setLayout(this.layout);
        this.setBorder(BorderFactory.createTitledBorder(name));
        
        this.smallCard.setLayout(new GridBagLayout());
        this.bigCard.setLayout(new GridBagLayout());
        
        this.c.weightx = 0.01;
        this.addSecondVotes("Zweitstimmen");
        this.addButtonDetails();
        this.addFirstVotes("Erststimmen");
        
        this.update();
        
        this.add(this.smallCard, "small");
        this.smallSize = this.getPreferredSize();
        this.add(this.bigCard, "big");
        
        this.layout.show(this, "small");
        this.setPreferredSize(this.smallSize);
        
        this.setAlignmentX(Component.CENTER_ALIGNMENT);
    }
    
    /**
     * Sets the getters which are used to fill the pane with values.
     * 
     * @param secondVotesPart
     *            the getter for the actual number of second votes
     * @param secondVotesTotal
     *            the getter for the total number of second votes
     * @param firstVotesPart
     *            the getter for the actual number of first votes
     * @param firstVotesTotal
     *            the getter for the total number of first votes
     */
    void setGetter(Supplier<Integer> secondVotesPart, Supplier<Integer> secondVotesTotal, Supplier<Integer> firstVotesPart, Supplier<Integer> firstVotesTotal) {
        this.secondVotesPartGetter = secondVotesPart;
        this.secondVotesTotalGetter = secondVotesTotal;
        this.firstVotesPartGetter = firstVotesPart;
        this.firstVotesTotalGetter = firstVotesTotal;
    }
    
    /**
     * Sets the setters which are used to set the values in the area when they get changed in the GUI.
     * 
     * @param secondVotes
     *            the setter to set the actual number of second votes
     * @param firstVotes
     *            the setter to set the actual number of first votes
     * @return the same panel
     */
    NamedPanel setSetter(IntPredicate secondVotes, IntPredicate firstVotes) {
        this.secondVotesSetter = secondVotes;
        this.firstVotesSetter = firstVotes;
        return this;
    }
    
    /**
     * registers a listener updating this panel at the specified {@link ElectionArea}
     * 
     * @param area
     *            the {@link ElectionArea} where to register the listener
     * @return the same instance
     */
    NamedPanel addListenerToArea(ElectionArea<?> area) {
        area.addChangeListener(this::update);
        return this;
    }
    
    void update() {
        this.isUpdating = true;
        this.updateSecondVotes();
        this.updateFirstVotes();
        this.isUpdating = false;
    }
    
    void addSecondVotes(String labelName) {
        // in the line: first add the label ... 
        this.c.gridx = 0;
        this.c.gridy = 0;
        this.c.weighty = 1;
        JLabel labelSecondVotesS = new JLabel(labelName);
        labelSecondVotesS.setPreferredSize(NamedPanel.LABEL_SIZE);
        this.smallCard.add(labelSecondVotesS, this.c);
        JLabel labelSecondVotesB = new JLabel(labelName);
        labelSecondVotesB.setPreferredSize(NamedPanel.LABEL_SIZE);
        this.bigCard.add(labelSecondVotesB, this.c);
        
        // ... next the actual field with the number ...
        this.c.gridx = 1;
        Consumer<JTextField> secondVotesValidator = (textField) -> {
            try {
                int number = Integer.parseInt(textField.getText().replace("\\s", ""));
                if(number < 0 || !this.secondVotesSetter.test(number)) {
                    textField.setText(Integer.toString(this.secondVotesPartGetter.get()));
                }
            }
            catch(NumberFormatException e) {
                textField.setText(Integer.toString(this.secondVotesPartGetter.get()));
            }
        };
        // add validator functions to validate the input in the fields
        GuiHelper.addValidator(this, this.secondVotesNumberS, secondVotesValidator);
        this.smallCard.add(this.secondVotesNumberS, this.c);
        GuiHelper.addValidator(this, this.secondVotesNumberB, secondVotesValidator);
        this.bigCard.add(this.secondVotesNumberB, this.c);
        
        // ... then the percent ...
        this.c.gridx = 2;
        Consumer<JTextField> secondVotesPercentValidator = (textField) -> {
            Optional<Double> percent = GuiHelper.percentStringToDouble(textField.getText());
            if(!GuiHelper.validatePercent(percent) || !this.secondVotesSetter.test((int) (percent.get() * this.secondVotesTotalGetter.get()))) {
                textField.setText(GuiHelper.doubleToPercentString((double) this.secondVotesPartGetter.get() / this.secondVotesTotalGetter.get()));
            }
        };
        // again: when entered a new percent, the field needs to be validated
        GuiHelper.addValidator(this, this.secondVotesPercentS, secondVotesPercentValidator);
        this.smallCard.add(this.secondVotesPercentS, this.c);
        GuiHelper.addValidator(this, this.secondVotesPercentB, secondVotesPercentValidator);
        this.bigCard.add(this.secondVotesPercentB, this.c);
        
        // ... then comes the button to subtract votes ...
        this.c.gridx = 3;
        JButton minusS = new JButton("-");
        // reduce by 10% each time pressed
        minusS.addActionListener(e -> SwingUtilities.invokeLater(() -> this.secondVotesSliderS.setValue(Math.max(this.secondVotesSliderS.getValue()
                                                                                                                 - this.secondVotesTotalGetter.get() / 10, 0))));
        this.smallCard.add(minusS, this.c);
        JButton minusB = new JButton("-");
        // reduce by 10% each time pressed
        minusB.addActionListener(e -> SwingUtilities.invokeLater(() -> this.secondVotesSliderB.setValue(Math.max(this.secondVotesSliderB.getValue()
                                                                                                                 - this.secondVotesTotalGetter.get() / 10, 0))));
        this.bigCard.add(minusB, this.c);
        
        // ... in the middle the actual slider ...
        this.c.gridx = 4;
        this.c.weightx = 1000;
        this.c.fill = GridBagConstraints.BOTH;
        this.secondVotesSliderS.setMinimum(0);
        this.secondVotesSliderS.addChangeListener(e -> {
            /* only fire an update, if we aren't currently updating the values and if the slider isn't in movement anymore */
            if(!this.secondVotesSliderS.getValueIsAdjusting() && !this.isUpdating && this.secondVotesSliderS.getValue() != this.secondVotesPartGetter.get()) {
                if(!this.secondVotesSetter.test(this.secondVotesSliderS.getValue())) {
                    SwingUtilities.invokeLater(() -> {
                        this.secondVotesSliderS.setValue(this.secondVotesPartGetter.get());
                    });
                }
            }
        });
        this.smallCard.add(this.secondVotesSliderS, this.c);
        this.secondVotesSliderB.setMinimum(0);
        this.secondVotesSliderB.addChangeListener(e -> {
            /* only fire an update, if we aren't currently updating the values and if the slider isn't in movement anymore */
            if(!this.secondVotesSliderB.getValueIsAdjusting() && !this.isUpdating && this.secondVotesSliderB.getValue() != this.secondVotesPartGetter.get()) {
                if(!this.secondVotesSetter.test(this.secondVotesSliderB.getValue())) {
                    SwingUtilities.invokeLater(() -> this.secondVotesSliderB.setValue(this.secondVotesPartGetter.get()));
                }
            }
        });
        this.bigCard.add(this.secondVotesSliderB, this.c);
        this.c.weightx = 0.1;
        this.c.fill = GridBagConstraints.NONE;
        
        // ... the plus button to the right of the slider ...
        this.c.gridx = 5;
        JButton plusS = new JButton("+");
        // increase by 10% each time pressed
        plusS.addActionListener(e -> SwingUtilities.invokeLater(() -> {
            int secondVotesTotal = this.secondVotesTotalGetter.get();
            this.secondVotesSliderS.setValue(Math.min(this.secondVotesSliderS.getValue() + secondVotesTotal / 10, secondVotesTotal));
        }));
        this.smallCard.add(plusS, this.c);
        JButton plusB = new JButton("+");
        // increase by 10% each time pressed
        plusB.addActionListener(e -> SwingUtilities.invokeLater(() -> {
            int secondVotesTotal = this.secondVotesTotalGetter.get();
            this.secondVotesSliderB.setValue(Math.min(this.secondVotesSliderB.getValue() + secondVotesTotal / 10, secondVotesTotal));
        }));
        this.bigCard.add(plusB, this.c);
    }
    
    /** updates the second votes */
    void updateSecondVotes() {
        int secondVotesTotal = this.secondVotesTotalGetter.get();
        int secondVotesPart = this.secondVotesPartGetter.get();
        
        String secondVotesString = Integer.toString(secondVotesPart);
        this.secondVotesNumberS.setText(secondVotesString);
        this.secondVotesNumberB.setText(secondVotesString);
        
        String secondVotesPercentString = GuiHelper.doubleToPercentString((double) secondVotesPart / secondVotesTotal);
        this.secondVotesPercentS.setText(secondVotesPercentString);
        this.secondVotesPercentB.setText(secondVotesPercentString);
        
        this.secondVotesSliderS.setMaximum(secondVotesTotal);
        this.secondVotesSliderB.setMaximum(secondVotesTotal);
        
        this.secondVotesSliderS.setValue(secondVotesPart);
        this.secondVotesSliderB.setValue(secondVotesPart);
    }
    
    void addButtonDetails() {
        this.c.gridwidth = 2;
        this.c.gridx = 6;
        this.buttonShowDetails.setPreferredSize(NamedPanel.BUTTON_DIMENSION);
        // show the expanded version
        this.buttonShowDetails.addActionListener(e -> {
            this.layout.show(this, "big");
            this.setPreferredSize(null);
        });
        this.smallCard.add(this.buttonShowDetails, this.c);
        
        // show the small version
        this.buttonHideDetails.addActionListener(e -> {
            this.layout.show(this, "small");
            this.setPreferredSize(this.smallSize);
        });
        this.bigCard.add(this.buttonHideDetails, this.c);
        this.c.gridwidth = 1;
    }
    
    void addFirstVotes(String labelName) {
        // in the line: first add the label ... 
        this.c.gridx = 0;
        this.c.gridy = 1;
        this.c.weighty = 1;
        this.bigCard.add(new JLabel(labelName), this.c);
        
        // ... next the actual field with the number ...
        this.c.gridx = 1;
        // add validator functions to validate the input in the fields
        GuiHelper.addValidator(this, this.firstVotesNumber, (textField) -> {
            try {
                int number = Integer.parseInt(textField.getText().replaceAll("\\s", ""));
                if(number < 0 || !this.firstVotesSetter.test(number)) {
                    textField.setText(Integer.toString(this.firstVotesPartGetter.get()));
                }
            }
            catch(NumberFormatException e) {
                textField.setText(Integer.toString(this.firstVotesPartGetter.get()));
            }
        });
        this.bigCard.add(this.firstVotesNumber, this.c);
        
        // ... then the percent ...
        this.c.gridx = 2;
        GuiHelper.addValidator(this, this.firstVotesPercent, (textField) -> {
            Optional<Double> percent = GuiHelper.percentStringToDouble(textField.getText());
            if(!GuiHelper.validatePercent(percent) || !this.firstVotesSetter.test((int) (percent.get() * this.firstVotesTotalGetter.get()))) {
                textField.setText(GuiHelper.doubleToPercentString((double) this.firstVotesPartGetter.get() / this.firstVotesTotalGetter.get()));
            }
        });
        this.bigCard.add(this.firstVotesPercent, this.c);
        
        // ... then comes the button to subtract votes ...
        this.c.gridx = 3;
        JButton minus = new JButton("-");
        // reduce by 10% each time pressed
        minus.addActionListener(e -> this.firstVotesSlider.setValue(Math.max(this.firstVotesSlider.getValue() - this.firstVotesTotalGetter.get() / 10, 0)));
        this.bigCard.add(minus, this.c);
        
        // ... in the middle the actual slider ...
        this.c.gridx = 4;
        this.c.weightx = 1000;
        this.c.fill = GridBagConstraints.BOTH;
        this.firstVotesSlider.setMinimum(0);
        this.firstVotesSlider.addChangeListener(e -> {
            /* only fire an update, if we aren't currently updating the values and if the slider isn't in movement anymore */
            if(!this.firstVotesSlider.getValueIsAdjusting() && !this.isUpdating && this.firstVotesSlider.getValue() != this.firstVotesPartGetter.get()) {
                if(!this.firstVotesSetter.test(this.firstVotesSlider.getValue())) {
                    SwingUtilities.invokeLater(() -> this.firstVotesSlider.setValue(this.firstVotesPartGetter.get()));
                }
            }
        });
        this.bigCard.add(this.firstVotesSlider, this.c);
        this.c.weightx = 0.1;
        this.c.fill = GridBagConstraints.NONE;
        
        // ... the plus button to the right of the slider ...
        this.c.gridx = 5;
        JButton plus = new JButton("+");
        // increase by 10% each time pressed
        plus.addActionListener(e -> {
            int firstVotesTotal = this.firstVotesTotalGetter.get();
            this.firstVotesSlider.setValue(Math.min(this.firstVotesSlider.getValue() + firstVotesTotal / 10, firstVotesTotal));
        });
        this.bigCard.add(plus, this.c);
    }
    
    /** updates the first votes */
    void updateFirstVotes() {
        int firstVotesTotal = this.firstVotesTotalGetter.get();
        int firstVotesPart = this.firstVotesPartGetter.get();
        this.firstVotesNumber.setText(Integer.toString(firstVotesPart));
        this.firstVotesPercent.setText(GuiHelper.doubleToPercentString((double) firstVotesPart / firstVotesTotal));
        this.firstVotesSlider.setMaximum(firstVotesTotal);
        this.firstVotesSlider.setValue(firstVotesPart);
    }
    
    /** disables the show details button, so that there is no expanded version */
    void disableButtonShowDetails() {
        this.buttonShowDetails.setEnabled(false);
    }
    
    static Dimension getLabelSize() {
        return NamedPanel.LABEL_SIZE;
    }
    
    /** @return the predefined {@link Dimension} for all {@link JButton}s */
    protected static Dimension getButtonDimension() {
        return NamedPanel.BUTTON_DIMENSION;
    }
}