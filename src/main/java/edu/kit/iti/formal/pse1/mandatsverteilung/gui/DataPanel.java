/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.gui;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.LayoutManager;
import java.util.function.Function;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.ElectionArea;

/**
 * A {@link DataPanel} contains a label showing the sum of eligible voters and actual voters in it's collapsed state.<br>
 * In its extended state it shows the election data created by the {@link #electionDataCreator}. This will be either another {@link EncapsulatedElectionData} or
 * an normal {@link ElectionData}.
 * 
 * @author Felix Heim
 * @version 1
 */
class DataPanel extends JPanel {
    private static final long serialVersionUID = 1L;
    private static final Dimension BUTTON_DIMENSION = new JButton("Details verbergen").getPreferredSize();
    
    /** the {@link LayoutManager} used by the panel */
    private CardLayout layout = new CardLayout();
    
    /** the area to display the values from */
    private ElectionArea<?> area;
    
    /** the function to create the sub tab contained in this panel */
    private Function<ElectionArea<?>, Tab> electionDataCreator;
    
    /** the small card only showing few information */
    private JPanel smallCard = new JPanel();
    /** the big card (extended version) showing everything */
    private JPanel bigCard;
    
    /** the amount of voters */
    private JLabel votersS = new JLabel();
    /** the amount of eligible voters */
    private JLabel votersEligibleS = new JLabel();
    
    private boolean isAreaLocked;
    
    private JButton lockUnlockS = new JButton();
    private JButton lockUnlockB = new JButton();
    
    /** the amount of voters */
    private JLabel votersB = new JLabel();
    /** the amount of eligible voters */
    private JLabel votersEligibleB = new JLabel();
    
    /** the {@link Dimension} of the {@link #smallCard} */
    private Dimension smallSize;
    
    /**
     * @param area
     *            the area to display the values from
     * @param electionDataCreator
     *            the function to create the sub tab contained in this panel
     */
    DataPanel(ElectionArea<?> area, Function<ElectionArea<?>, Tab> electionDataCreator) {
        this.area = area;
        this.electionDataCreator = electionDataCreator;
        this.setLayout(this.layout);
        this.setBorder(BorderFactory.createTitledBorder(area.getName()));
        this.smallCard.setLayout(new BorderLayout());
        this.addVoters();
        this.updateVoters();
        this.updateDimensions();
        area.addChangeListener(this::updateVoters);
        this.layout.show(this, "small");
        this.smallSize.width = this.getPreferredSize().width;
        this.setPreferredSize(this.smallSize);
    }
    
    /** adds the {@link JLabel}s to the preview in the collapsed version */
    private void addVoters() {
        JPanel panelVoters = new JPanel(new FlowLayout());
        panelVoters.add(new JLabel("Wahlberechtigte: "));
        panelVoters.add(this.votersEligibleS);
        panelVoters.add(new JLabel("W\u00E4hler: "));
        panelVoters.add(this.votersS);
        this.lockUnlockS.setText((this.isAreaLocked = this.area.isLocked()) ? "Entsperren" : "Sperren");
        this.lockUnlockS.setPreferredSize(DataPanel.BUTTON_DIMENSION);
        this.lockUnlockS.addActionListener(e -> {
            if(this.isAreaLocked) {
                this.area.unlock();
                this.isAreaLocked = false;
                this.lockUnlockS.setText("Sperren");
                this.lockUnlockB.setText("Sperren");
            }
            else {
                this.area.lock();
                this.isAreaLocked = true;
                this.lockUnlockS.setText("Entsperren");
                this.lockUnlockB.setText("Entsperren");
            }
        });
        panelVoters.add(this.lockUnlockS);
        JButton buttonShowDetails = new JButton("Details");
        buttonShowDetails.setPreferredSize(DataPanel.BUTTON_DIMENSION);
        buttonShowDetails.addActionListener(e -> {
            if(this.bigCard == null) {
                this.createBigCard();
            }
            this.layout.show(this, "big");
            this.setPreferredSize(null);
        });
        panelVoters.add(buttonShowDetails);
        
        this.smallCard.add(panelVoters, BorderLayout.PAGE_START);
        
        this.add(this.smallCard, "small");
        this.smallSize = this.getPreferredSize();
    }
    
    /** creates the big card when the panel is the expanded for the first time */
    private void createBigCard() {
        this.bigCard = new JPanel();
        this.bigCard.setLayout(new BorderLayout());
        // create and add the election data to the big card
        this.bigCard.add(this.electionDataCreator.apply(this.area).getContentPane(), BorderLayout.PAGE_END);
        JPanel panelVoters = new JPanel(new FlowLayout());
        panelVoters.add(new JLabel("Wahlberechtigte: "));
        panelVoters.add(this.votersEligibleB);
        panelVoters.add(new JLabel("W\u00E4hler: "));
        panelVoters.add(this.votersB);
        this.lockUnlockB.setText(this.isAreaLocked ? "Entsperren" : "Sperren");
        this.lockUnlockB.setPreferredSize(DataPanel.BUTTON_DIMENSION);
        this.lockUnlockB.addActionListener(e -> {
            if(this.isAreaLocked) {
                this.area.unlock();
                this.isAreaLocked = false;
                this.lockUnlockS.setText("Sperren");
                this.lockUnlockB.setText("Sperren");
            }
            else {
                this.area.lock();
                this.isAreaLocked = true;
                this.lockUnlockS.setText("Entsperren");
                this.lockUnlockB.setText("Entsperren");
            }
        });
        panelVoters.add(this.lockUnlockB);
        JButton buttonHideDetails = new JButton("Details verbergen");
        buttonHideDetails.addActionListener(e -> {
            this.layout.show(this, "small");
            /* important to reset the size */
            this.setPreferredSize(this.smallSize);
            
        });
        panelVoters.add(buttonHideDetails);
        this.bigCard.add(panelVoters, BorderLayout.PAGE_START);
        this.add(this.bigCard, "big");
        
    }
    
    /** sets the {@link Dimension}s of the {@link JLabel}s */
    private void updateDimensions() {
        int height = this.votersS.getPreferredSize().height;
        
        this.votersS.setPreferredSize(new Dimension(150, height));
        this.votersB.setPreferredSize(new Dimension(150, height));
        this.votersEligibleS.setPreferredSize(new Dimension(150, height));
        this.votersEligibleB.setPreferredSize(new Dimension(150, height));
    }
    
    /** update the labels showing the information of the voters and the eligible voters */
    private void updateVoters() {
        this.votersEligibleS.setText(this.area.getEligibleVoters() + " (\u2259"
                                     + GuiHelper.doubleToPercentString((double) this.area.getEligibleVoters() / this.area.getCitizens()) + " %)");
        this.votersEligibleB.setText(this.votersEligibleS.getText());
        this.votersS.setText(this.area.getNumberOfVotes() + " (\u2259" + GuiHelper.doubleToPercentString(this.area.getVoterTurnout()) + " %)");
        this.votersB.setText(this.votersS.getText());
        this.isAreaLocked = this.area.isLocked();
        if(this.isAreaLocked) {
            this.lockUnlockS.setText("Entsperren");
            this.lockUnlockB.setText("Entsperren");
        }
        else {
            this.lockUnlockS.setText("Sperren");
            this.lockUnlockB.setText("Sperren");
        }
    }
}
