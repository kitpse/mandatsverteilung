/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.gui;

import java.awt.Component;
import java.awt.Dimension;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JTabbedPane;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.management.Session;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Election;

/**
 * The main {@link JFrame} used to display everything. It holds the MenuBar and all the Tabs.
 * 
 * @author Felix Heim
 * @version 5
 */
public class MainWindow extends JFrame {
    private static final long serialVersionUID = 1L;
    
    /** the edit menu, reference needed to update the list of opened elections */
    private MenuEdit menuEdit = new MenuEdit(this);
    
    /** the main content pane */
    private JTabbedPane mainPane = new JTabbedPane();
    
    /** a list of all {@link TabElection}s currently opened */
    private List<TabElection> elections = new LinkedList<>();
    
    /** a list of all {@link TabComparison}s currently opened */
    private List<TabComparison> comparisons = new LinkedList<>();
    
    public MainWindow() {
        super("Mandatsverteilung f\u00FCr den Deutschen Bundestag");
        this.initMenus();
        this.initMainTabPane();
        this.finishJFrameSetUp();
    }
    
    // adds the three menus to menu bar and the menu bar to the frame 
    private void initMenus() {
        JMenuBar menuBar = new JMenuBar();
        this.setJMenuBar(menuBar);
        menuBar.add(new MenuFile(this));
        menuBar.add(this.menuEdit);
        menuBar.add(new MenuHelp());
    }
    
    // initializes the main content pane of the frame
    private void initMainTabPane() {
        this.mainPane.addChangeListener(e -> this.requestFocusInWindow());
        this.setContentPane(this.mainPane);
    }
    
    // finished the frame set up by setting some properties of the gui
    private void finishJFrameSetUp() {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.getContentPane().setMinimumSize(new Dimension(900, 500));
        this.getContentPane().setMaximumSize(new Dimension(1280, 768));
        this.pack();
        this.setResizable(true);
        this.setLocationRelativeTo(null);
        this.setVisible(true);
    }
    
    @Override
    public Dimension getPreferredSize() { // calculates the preferred size based on the minimum and maximum size
        Dimension preferredSize = super.getPreferredSize();
        Dimension minimumSize = super.getMinimumSize();
        Dimension maximumSize = super.getMaximumSize();
        return new Dimension(Math.min(maximumSize.width, Math.max(preferredSize.width, minimumSize.width)), Math.min(maximumSize.height, Math
                .max(preferredSize.height, minimumSize.height)));
    }
    
    /**
     * adds a new {@link TabElection} to the content pane
     * 
     * @param tabElection
     *            the {@link TabElection} to add
     */
    public void addTabElection(TabElection tabElection) {
        if(!this.elections.contains(tabElection)) {
            this.elections.add(tabElection);
            this.menuEdit.addElection(tabElection.getElection());
            this.mainPane.addTab(tabElection.getName(), tabElection);
        }
    }
    
    /**
     * removes the given {@link TabElection} from the content pane
     * 
     * @param tabElection
     *            the {@link TabElection} to remove
     */
    public void removeTabElection(TabElection tabElection) {
        if(this.elections.contains(tabElection)) {
            this.mainPane.remove(tabElection);
            this.menuEdit.removeElection(tabElection.getElection());
            this.elections.remove(tabElection);
        }
    }
    
    /**
     * adds a new {@link TabComparison} to the content pane
     * 
     * @param tabComparison
     *            the {@link TabComparison} to add
     */
    public void addTabComparison(TabComparison tabComparison) {
        if(!this.comparisons.contains(tabComparison)) {
            this.comparisons.add(tabComparison);
            this.mainPane.addTab(tabComparison.getName(), tabComparison);
        }
    }
    
    /**
     * removes the given {@link TabComparison} from the content pane
     * 
     * @param tabComparison
     *            the {@link TabComparison} to add
     */
    public void removeTabComparison(TabComparison tabComparison) {
        if(this.comparisons.contains(tabComparison)) {
            this.mainPane.remove(tabComparison);
            this.comparisons.remove(tabComparison);
        }
    }
    
    /**
     * adds a new {@link TabNegRelVotingWeightResult} to the content pane
     * 
     * @param tab
     *            the {@link TabNegRelVotingWeightResult} to add
     */
    public void addTabAlgorithmResult(TabNegRelVotingWeightResult tab) {
        this.mainPane.addTab(tab.getName(), tab.getContentPane());
    }
    
    /** shifts the selected tab one space to the right if possible */
    public void shiftTabRight() {
        int index = this.mainPane.getSelectedIndex();
        int count = this.mainPane.getComponentCount();
        if(index != -1 && count > 1 && index < count - 1) {
            this.shiftTab(index, 1);
        }
    }
    
    /** shifts the selected tab one space to the left if possible */
    public void shiftTabLeft() {
        int index = this.mainPane.getSelectedIndex();
        if(index > 0) {
            this.shiftTab(index, -1);
        }
    }
    
    // shifts the tab at the given index the amount given in count (negative menans left, positive means right)
    private void shiftTab(int index, int count) {
        Component c = this.mainPane.getComponentAt(index);
        String title = this.mainPane.getTitleAt(index);
        this.mainPane.removeTabAt(index);
        index += count;
        this.mainPane.insertTab(title, null, c, null, index);
        this.mainPane.setSelectedIndex(index);
    }
    
    /** @return the name of the currently selected tab */
    public String getSelectedTabName() {
        return this.mainPane.getTitleAt(this.mainPane.getSelectedIndex());
    }
    
    /**
     * sets the name of the currently selected tab
     * 
     * @param newTitle
     *            the new name to set
     */
    public void setSelectedTabName(String newTitle) {
        int index = this.mainPane.getSelectedIndex();
        if(index != -1) {
            Component c = this.mainPane.getComponentAt(index);
            this.mainPane.removeTabAt(index);
            this.mainPane.insertTab(newTitle, null, c, null, index);
            this.mainPane.setSelectedIndex(index);
            if(c instanceof TabElection) {
                Election election = ((TabElection) c).getElection();
                election.setName(newTitle);
                this.menuEdit.removeElection(election);
                this.menuEdit.addElection(election);
            }
            else if(c instanceof TabComparison) {
                ((TabComparison) c).getElectionComparison().setName(newTitle);
            }
        }
    }
    
    /** removes the selected Tab from the content pane */
    public void closeSelectedTab() {
        Component c = this.mainPane.getSelectedComponent();
        if(this.elections.contains(c)) {
            Session.getSession().removeElection(((TabElection) c).getElection());
        }
        else if(this.comparisons.contains(c)) {
            Session.getSession().removeElectionComparison(((TabComparison) c).getElectionComparison());
        }
        else {
            this.mainPane.remove(c);
        }
    }
    
    /** @return the {@link Optional} containing the selected {@link TabElection} or null if the selected Tab isn't a {@link TabElection} */
    public Optional<TabElection> getSelectedTabElection() {
        return Optional.of(this.mainPane.getSelectedComponent()).flatMap(c -> c instanceof TabElection ? Optional.of((TabElection) c) : Optional.empty());
    }
}
