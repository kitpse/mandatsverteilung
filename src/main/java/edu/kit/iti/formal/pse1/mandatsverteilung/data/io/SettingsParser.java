/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.data.io;

import java.util.Iterator;

import edu.kit.iti.formal.pse1.mandatsverteilung.calculation.AlternativeDistribution;
import edu.kit.iti.formal.pse1.mandatsverteilung.calculation.CalculationMethod;
import edu.kit.iti.formal.pse1.mandatsverteilung.calculation.Distribution2013;
import edu.kit.iti.formal.pse1.mandatsverteilung.calculation.SimpleDistribution;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Settings;

/**
 * Class for parsing and setting the values for the settings of an election.
 * 
 * @author Nils Wilka
 * @version 1.0
 */
public class SettingsParser {
    
    /**
     * The Parser that has the lines.
     */
    private final CSVParser parser;
    
    /**
     * Creates a new SettingsParser which parses the first line of the given array for settings.
     * 
     * @param lines
     *            The lines to read the first line from.
     * @throws NullPointerException
     *             Thrown if lines are null.
     * @throws FormatException
     *             Thrown if lines are empty.
     */
    public SettingsParser(final String[] lines) throws NullPointerException, FormatException {
        this(lines, 0);
    }
    
    /**
     * Creates a new SettingsParser which parses the specified line of the given array for settings.
     * 
     * @param lines
     *            The lines to read the specified line from.
     * @param line
     *            The row index of the line that has the settings.
     * @throws NullPointerException
     *             Thrown if lines are null.
     * @throws FormatException
     *             Thrown if lines do not have specified line.
     */
    public SettingsParser(final String[] lines, int line) throws NullPointerException, FormatException {
        this.parser = new CSVParser(lines, line, line + 1);
    }
    
    /**
     * Creates a new SettingsParser which parses the first line of the given data string for settings.
     * 
     * @param data
     *            The data to split at line breaks and read the first line from.
     * @throws NullPointerException
     *             Thrown if data is null.
     * @throws FormatException
     *             In this case not thrown.
     */
    public SettingsParser(final String data) throws NullPointerException, FormatException {
        this(data, 0);
    }
    
    /**
     * Creates a new SettingsParser which parses the specified line of the given data string for settings.
     * 
     * @param data
     *            The data to split at line breaks and read the first line from.
     * @throws NullPointerException
     *             Thrown if data is null.
     * @throws FormatException
     *             Thrown if lines do not have specified line.
     */
    public SettingsParser(final String data, int line) throws NullPointerException, FormatException {
        this(data.split("\n"), line);
    }
    
    /**
     * Parses the data in the SettingsParser and creates the {@code Settings} with contained values.
     * 
     * @return {@code Settings} with contained values of the SettingsParser.
     * @throws FormatException
     *             Thrown if format of data at instantiation is wrong.
     */
    public Settings parseSettings() throws FormatException {
        this.checkSettings();
        return this.getSettings();
    }
    
    /**
     * Checks if the data of this object matches the expected data (specified by {@code Export}).
     * 
     * @throws FormatException
     *             Thrown if the data of this object does not matche the expected data (specified by {@code Export}).
     */
    private void checkSettings() throws FormatException {
        Iterator<String> rowIterator = this.parser.rowIterator(0);
        
        for(String expected : Export.Format.ELECTION_SETTINGS_REGEX) {
            if(rowIterator.hasNext()) {
                String current = rowIterator.next();
                if(!current.matches(expected)) {
                    throw new FormatException("Expected content of format \"" + expected + "\", but got \"" + current + "\".");
                }
            }
            else {
                throw new FormatException("Expected more settings.");
            }
        }
        if(rowIterator.hasNext()) {
            throw new FormatException("Expected settings to end, but got \"" + rowIterator.next() + "\".");
        }
    }
    
    /**
     * Parses the settings line and creates new {@code Settings} with values of the line.
     * 
     * @return New {@code Settings} with values of the line.
     * @throws FormatException
     *             Thrown if format of data at instantiation is wrong.
     */
    private Settings getSettings() throws FormatException {
        Iterator<String> rowIterator = this.parser.rowIterator(0);
        rowIterator.next();
        rowIterator.next();
        String size = rowIterator.next();
        rowIterator.next();
        String hurdle = rowIterator.next();
        rowIterator.next();
        String calculationMethod = rowIterator.next();
        CalculationMethod calc = SettingsParser.calculationMethodFromString(calculationMethod);
        rowIterator.next();
        String nonVoterParty = rowIterator.next();
        double hurdleDouble = this.checkDouble(hurdle);
        int sizeInt = this.checkInteger(size);
        
        if(!Settings.checkPercentageHurdle(hurdleDouble)) {
            throw new FormatException(Settings.getRangeHurdleDescription());
        }
        if(!Settings.checkSizeBundestag(sizeInt)) {
            throw new FormatException(Settings.getRangeSizeDescription());
        }
        return new Settings(Boolean.valueOf(nonVoterParty), hurdleDouble, sizeInt, calc);
    }
    
    /**
     * Checks if a given string matches "[-]{0,1}[0-9]+" and parses it.
     * 
     * @param current
     *            The string to check for.
     * @return The string converted to integer.
     * @throws FormatException
     *             Thrown if string does not match "[-]{0,1}[0-9]+".
     */
    private int checkInteger(String current) throws FormatException {
        if(!current.matches("[-]{0,1}[0-9]+")) {
            throw new FormatException("Expected integer, but got \"" + current + "\".");
        }
        else {
            return Integer.parseInt(current);
        }
    }
    
    /**
     * Checks if a given string matches "[-]{0,1}(\\d+\\.\\d+)" and parses it.
     * 
     * @param current
     *            The string to check for.
     * @return The string converted to double.
     * @throws FormatException
     *             Thrown if string does not match "[-]{0,1}(\\d+\\.\\d+)".
     */
    private double checkDouble(String current) throws FormatException {
        if(!current.matches("[-]{0,1}(\\d+\\.\\d+)")) {
            throw new FormatException("Expected float, but got \"" + current + "\".");
        }
        else {
            return Double.parseDouble(current);
        }
    }
    
    /**
     * Checks if the given name matches a known name for a calculation method strategy. If this is the case initializes the corresponding class and returns it.
     * 
     * @param name
     *            The name of a valid calculation method strategy.
     * @return The calculation method corresponding to the name.
     * @throws FormatException
     *             Thrown if name is an invalid calculation method.
     */
    private static CalculationMethod calculationMethodFromString(final String name) throws FormatException {
        CalculationMethod calc;
        
        switch(name) {
            case "AlternativeDistribution":
                calc = AlternativeDistribution.getMethod();
                break;
            case "Distribution2013":
                calc = Distribution2013.getMethod();
                break;
            case "SimpleDistribution":
                calc = SimpleDistribution.getMethod();
                break;
            default:
                throw new FormatException("Unknown calculation of seats method.");
        }
        return calc;
    }
}
