/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.data.io;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Constituency;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Election;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.ElectionArea;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.ElectionResult;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.FederalState;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Federation;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Party;

/**
 * Class for importing {@link ElectionResult}s from .csv files with the format used by the Bundewahlleiter in the years 2013, 2009 and 2005. The format is
 * specified as described in the following lines:
 * 
 * line 0: name of the election
 * 
 * line 1 is not important and lines 2 to 4 specify how the following lines (starting with line 5) are interpreted:
 * 
 * line 2: number of the election area; election area name; election area parent number; eligible voters;;;;voters;;;;invalid votes;;;; valid votes;;;; first
 * party;;;; second party ...
 * 
 * line 3: ;;not important;first votes;;second votes;; (repeat previous 4)
 * 
 * line 4: ;;;votes of this election; votes of previous election; (repeat previous 4)
 * 
 * lines 5 to (x - 1) contain the constituency data. The x-th line contains state data (sum of constituency data above). line (x + 1) is empty, then this is
 * repeated for next federal state ((x + 2) to y) and overall 16 states. Next up is an empty line and the Federation data (sum of federal state data) in one
 * line.
 * 
 * If a string is given with the wrong format a FormatException is thrown.
 * 
 * @author Nils Wilka
 * @version 1.0
 */
class Bundeswahlleiter2013Parser extends ElectionResultParser {
    
    /**
     * The column containing the ID for the {@link ElectionArea}.
     */
    private static final int POS_ID = 0;
    
    /**
     * The column containing the name for the {@link ElectionArea}.
     */
    private static final int POS_NAME = 1;
    
    /**
     * The column containing the "belongs to"-ID for the {@link ElectionArea}.
     */
    private static final int POS_BELONGSTO = 2;
    
    /**
     * The column containing the eligible voters for the {@link ElectionArea}.
     */
    private static final int POS_ELIGIBLE_VOTERS = 3;
    
    /**
     * The column containing the number of voters for the {@link ElectionArea}.
     */
    private static final int POS_VOTERS = 7;
    
    /**
     * The column containing the first {@link Party} for the {@link ElectionArea}.
     */
    private static final int POS_FIRST_PARTY = 19;
    
    /**
     * The expected head of an {@link ElectionResult}.
     */
    private static final String HEAD = "Nr;Gebiet;gehört;Wahlberechtigte;;;;Wähler;;;;Ungültige;;;;Gültige;;;;\n"
                                       + ";;zu;Erststimmen;;Zweitstimmen;;Erststimmen;;Zweitstimmen;;Erststimmen;;Zweitstimmen;;Erststimmen;;Zweitstimmen;;\n"
                                       + ";;;Endgültig;Vorperiode;Endgültig;Vorperiode;Endgültig;Vorperiode;Endgültig;Vorperiode;Endgültig;Vorperiode;Endgültig;Vorperiode;Endgültig;Vorperiode;Endgültig;Vorperiode;";
    
    /**
     * The expected head for the parties ({@link Party}).
     */
    private static final String PARTY_HEAD_REGEX = ".+;;;;\n" + "Erststimmen;;Zweitstimmen;;\n" + "Endgültig;Vorperiode;Endgültig;Vorperiode;";
    
    /**
     * Used to parse {@code HEAD}.
     */
    private static CSVParser HEAD_FORMAT;
    
    /**
     * Used to parse {@code PARTY_HEAD_REGEX}.
     */
    private static CSVParser PARTY_HEAD_FORMAT;
    
    /**
     * The number of columns to next party.
     */
    private static final int STEP = 4; // final first votes; previous first votes; final second votes; previous second votes; 
    
    /**
     * The number of columns from first votes to second votes.
     */
    private static final int STEP_TO_SECOND_VOTES = 2;
    
    /**
     * The {@link ElectionResult} to be set and returned.
     */
    private ElectionResult result; // Instantiation creates data structure of federation, federal states and constituencies
    
    /**
     * The parties ({@link Party}) for the {@link ElectionResult}.
     */
    private List<Party> parties;
    
    /**
     * The line index of the federation.
     */
    private Optional<Integer> federation;
    
    /**
     * The line indices of the constituencies.
     */
    private List<Integer> constituencyList;
    
    /**
     * The line indices of the federal states.
     */
    private List<Integer> stateList;
    
    static {
        try {
            Bundeswahlleiter2013Parser.HEAD_FORMAT = new CSVParser(Bundeswahlleiter2013Parser.HEAD);
            Bundeswahlleiter2013Parser.PARTY_HEAD_FORMAT = new CSVParser(Bundeswahlleiter2013Parser.PARTY_HEAD_REGEX);
        }
        catch(FormatException e) {
            // never happens. 
            e.printStackTrace();
        }
    }
    
    /**
     * Creates a new Bundeswahlleiter2013Parser with given string array lines to parse.
     * 
     * @param lines
     *            The data to be parsed.
     * @param start
     *            The number of lines that will be skipped at the start.
     * @throws NullPointerException
     *             Thrown if lines is null.
     * @throws FormatException
     *             Thrown if format of given string array is wrong. In this case if there are not enough lines.
     */
    public Bundeswahlleiter2013Parser(final String[] lines, int start) throws NullPointerException, FormatException {
        super(lines, start);
    }
    
    /**
     * Creates a new Bundeswahlleiter2013Parser with given string array lines to parse.
     * 
     * @param lines
     *            The data to be parsed.
     * @throws NullPointerException
     *             Thrown if lines is null.
     * @throws FormatException
     *             Thrown if format of given string array is wrong. In this case if there are not enough lines.
     */
    public Bundeswahlleiter2013Parser(final String[] lines) throws NullPointerException, FormatException {
        super(lines);
    }
    
    /**
     * Creates a new Bundeswahlleiter2013Parser with given string data to parse.
     * 
     * @param data
     *            The data to be parsed. Will be split at line break.
     * @param start
     *            The number of lines that will be skipped at the start.
     * @throws NullPointerException
     *             Thrown if data is null.
     * @throws FormatException
     *             Thrown if format of given string array is wrong. In this case if there are not enough lines.
     */
    public Bundeswahlleiter2013Parser(final String data, int start) throws NullPointerException, FormatException {
        super(data, start);
    }
    
    /**
     * Creates a new Bundeswahlleiter2013Parser with given string data to parse.
     * 
     * @param data
     *            The data to be parsed. Will be split at line break.
     * @throws NullPointerException
     *             Thrown if data is null.
     * @throws FormatException
     *             Thrown if format of given string array is wrong. In this case if there are not enough lines.
     */
    public Bundeswahlleiter2013Parser(final String data) throws NullPointerException, FormatException {
        super(data);
    }
    
    @Override
    public ElectionResult parseElectionResult() throws FormatException {
        this.initialize();
        this.parseHeadColumns();
        this.parsePartyHead();
        this.parseParties();
        this.selectLines();
        this.parseFederation();
        this.parseFederalStates();
        this.parseConstituencies(false);
        return this.result;
    }
    
    /**
     * Sets all data structures.
     */
    private void initialize() {
        this.result = new ElectionResult();
        this.federation = Optional.empty();
        this.constituencyList = new ArrayList<>(299);
        this.stateList = new ArrayList<>(16);
        this.parties = new ArrayList<>((this.parser.getColumnLength() - Bundeswahlleiter2013Parser.POS_FIRST_PARTY) % Bundeswahlleiter2013Parser.STEP);
        this.removeConstituencies();
        assert this.result.getFederation().getChildren().size() == Election.NUMBER_STATES;
    }
    
    /**
     * Parses the head of the {@link ElectionResult}.
     * 
     * @throws FormatException
     *             Thrown if format of data is unexpected.
     */
    private void parseHeadColumns() throws FormatException {
        for(int i = 0; i < Bundeswahlleiter2013Parser.HEAD_FORMAT.getRowLength(); i++) {
            
            for(int j = 0; j < Bundeswahlleiter2013Parser.HEAD_FORMAT.getColumnLength(); j++) {
                String expected = Bundeswahlleiter2013Parser.HEAD_FORMAT.get(i, j);
                String current = this.parser.get(i, j);
                if(!current.matches(expected)) {
                    throw new FormatException("Expected to find \"" + expected + "\", but got \"" + current + "\".", i, j);
                }
            }
        }
    }
    
    /**
     * Parses the head for each {@link Party} of the {@link ElectionResult}.
     * 
     * @throws FormatException
     *             Thrown if format of data is unexpected.
     */
    private void parsePartyHead() throws FormatException {
        if(this.parser.getColumnLength() > this.minNumberOfColumns()) {
            
            if((this.parser.getColumnLength() - this.minNumberOfColumns()) % Bundeswahlleiter2013Parser.STEP == 0) {
                
                // for all parties
                for(int k = Bundeswahlleiter2013Parser.POS_FIRST_PARTY; k < this.parser.getColumnLength(); k += Bundeswahlleiter2013Parser.STEP) {
                    
                    // parse head "matrix"
                    for(int i = 0; i < Bundeswahlleiter2013Parser.PARTY_HEAD_FORMAT.getRowLength(); i++) {
                        
                        for(int j = 0; j < Bundeswahlleiter2013Parser.PARTY_HEAD_FORMAT.getColumnLength(); j++) {
                            String expected = Bundeswahlleiter2013Parser.PARTY_HEAD_FORMAT.get(i, j);
                            String current = this.parser.get(i, j + k);
                            if(!current.matches(expected)) {
                                throw new FormatException("Expected to find \"" + expected + "\", but got \"" + current + "\"", i, j + k);
                            }
                        }
                    }
                }
            }
            else {
                throw new FormatException("Expected \"" + Bundeswahlleiter2013Parser.STEP + "\" columns per Party, but got \""
                                          + (this.parser.getColumnLength() - this.minNumberOfColumns()) + "\" lines. In row \""
                                          + ElectionResultParser.POS_HEAD_INFOLINE + "\".");
            }
        }
    }
    
    /**
     * Parses the parties ({@link Party}) for the {@link ElectionResult}.
     * 
     * @throws FormatException
     *             Thrown if format of data is unexpected.
     */
    private void parseParties() throws FormatException {
        Iterator<String> iterator = this.parser.rowIterator(ElectionResultParser.POS_HEAD_INFOLINE, Bundeswahlleiter2013Parser.POS_FIRST_PARTY);
        for(int i = Bundeswahlleiter2013Parser.POS_FIRST_PARTY; iterator.hasNext(); i++) {
            String cell = iterator.next();
            
            if((i - Bundeswahlleiter2013Parser.POS_FIRST_PARTY) % Bundeswahlleiter2013Parser.STEP == 0) {
                if(cell.matches(".+")) {
                    this.parties.add(new Party(cell));
                }
                else {
                    throw new FormatException("Expected a name, but got empty cell.");
                }
            }
            else if(!cell.equals("")) {
                throw new FormatException("Expected empty cell, but got \"" + cell + "\".");
            }
        }
    }
    
    /**
     * Selects (with ID in first column) in which line which {@link ElectionArea} has its data.
     * 
     * @throws FormatException
     *             Thrown if format of data is unexpected.
     */
    private void selectLines() throws FormatException {
        for(int lineIndex = ElectionResultParser.POS_BODY_START; lineIndex < this.parser.getRowLength(); lineIndex++) {
            Iterator<String> iterator = this.parser.rowIterator(lineIndex, Bundeswahlleiter2013Parser.POS_ID);
            
            if(!this.isEmpty(lineIndex)) {
                String stringID = iterator.next();
                try {
                    this.checkValidID(stringID);
                }
                catch(NumberFormatException nfe) {
                    throw new FormatException("Invalid ID.");
                }
                int numberID = Integer.parseInt(stringID);
                iterator.next(); // the name.
                String belongsTo = iterator.next();
                
                if(numberID == 99 && belongsTo.equals("")) {
                    if(!this.federation.isPresent()) {
                        this.federation = Optional.of(lineIndex);
                    }
                    else {
                        throw new FormatException("Expected to find only one federation, but got two.");
                    }
                }
                else {
                    int numberBelongsTo = this.checkNumber(belongsTo);
                    
                    if(numberID >= 1 && numberID <= 16 && numberBelongsTo == 99) {
                        this.stateList.add(lineIndex);
                    }
                    else if(numberID >= 1 && numberID <= 299 && numberBelongsTo >= 1 && numberBelongsTo <= 16) {
                        this.constituencyList.add(lineIndex);
                    }
                    else {
                        throw new FormatException("Unknown combination of ID " + numberID + " and \"belongs To\"-ID" + belongsTo + ".");
                    }
                }
            }
        }
    }
    
    /**
     * Checks if a row is empty
     * 
     * @param row
     *            The row index.
     * @return True if line is empty ({@code string.equals("")} for each cell)
     */
    private boolean isEmpty(int row) {
        Iterator<String> iterator = this.parser.rowIterator(row, Bundeswahlleiter2013Parser.POS_ID);
        boolean empty = true;
        
        while(iterator.hasNext()) {
            if(!iterator.next().equals("")) {
                empty = false;
                break;
            }
        }
        return empty;
    }
    
    /**
     * Checks if a given ID matches {@link ElectionResultParser.ID_REGEX}.
     * 
     * @param id
     *            The ID to check for.
     * @throws FormatException
     *             Thrown if ID does not match {@link ElectionResultParser.ID_REGEX}.
     */
    private void checkValidID(String id) throws FormatException {
        if(!id.matches(ElectionResultParser.ID_REGEX)) {
            throw new FormatException("Expected ID of format " + ElectionResultParser.ID_REGEX + ", but got " + id + ".");
        }
    }
    
    /**
     * Checks if a given string matches "[-]{0,1}[0-9]+" and parses it.
     * 
     * @param current
     *            The string to check for.
     * @return The string converted to integer.
     * @throws FormatException
     *             Thrown if string does not match "[-]{0,1}[0-9]+".
     */
    private int checkNumber(String current) throws FormatException {
        if(!current.matches("[-]{0,1}[0-9]+")) {
            throw new FormatException("Expected integer, but got " + current + ".");
        }
        else {
            return Integer.parseInt(current);
        }
    }
    
    /**
     * Checks if a given string matches "[-]{0,1}[0-9]+" and parses it.
     * 
     * @param current
     *            The string to check and parse.
     * @return {@code current} parsed to integer. If {@code current.equals("")} returns null.
     * @throws FormatException
     *             Thrown if string does not match "[-]{0,1}[0-9]+".
     */
    private Integer checkNumberEmptyNull(String current) throws FormatException {
        if(current.equals("")) {
            return null;
        }
        else if(!current.matches("[-]{0,1}[0-9]+")) {
            throw new FormatException("Expected integer, but got " + current + ".");
        }
        else {
            return Integer.parseInt(current);
        }
    }
    
    /**
     * Parses the {@link Federation}.
     */
    private void parseFederation() {
        String name = this.parser.get(this.federation.get(), Bundeswahlleiter2013Parser.POS_NAME);
        this.result.getFederation().setName(name);
    }
    
    /**
     * Parses the {@link FederalStates}.
     * 
     * @throws FormatException
     *             Thrown if format is wrong.
     */
    private void parseFederalStates() throws FormatException {
        if(this.stateList.size() > Election.NUMBER_STATES) {
            throw new FormatException("Too many federal states in file.");
        }
        else if(this.stateList.size() < Election.NUMBER_STATES) {
            throw new FormatException("Not enough federal states in file.");
        }
        
        List<FederalState> states = this.result.getFederation().getChildren();
        Set<Integer> ids = new HashSet<>();
        for(int i = 0; i < Election.NUMBER_STATES; i++) {
            String stringID = this.parser.get(this.stateList.get(i), Bundeswahlleiter2013Parser.POS_ID);
            int numberID = Integer.parseInt(stringID);
            if(ids.contains(numberID)) {
                throw new FormatException("Expected ids of states to be unique, but got \"" + numberID + "\" twice.");
            }
            String name = this.parser.get(this.stateList.get(i), Bundeswahlleiter2013Parser.POS_NAME).replaceAll("\"", "");
            states.get(numberID - 1).setName(name);
        }
    }
    
    /**
     * Removes the constituencies ({@link Constituency}) of every {@link FederalState}.
     */
    private void removeConstituencies() {
        this.result.getFederation().getChildren().forEach(state -> new ArrayList<>(state.getChildren()).forEach(state::removeChild));
    }
    
    /**
     * Parses the constituencies ({@link Constituency}).
     * 
     * @param eligibleVotersOnly
     *            Determines if only the eligible voters shall be parsed.
     * @throws FormatException
     *             Thrown if format is wrong.
     */
    private void parseConstituencies(final boolean eligibleVotersOnly) throws FormatException {
        if(this.constituencyList.size() != Election.NUMBER_CONSTITUENCIES) {
            throw new FormatException("Expected \"" + Election.NUMBER_CONSTITUENCIES + "\" constituencies, but got \"" + this.constituencyList.size() + "\".");
        }
        for(int j = 0; j < Election.NUMBER_CONSTITUENCIES; j++) {
            int row = this.constituencyList.get(j);
            Constituency constituency = this.setConstituency(row, eligibleVotersOnly);
            if(!eligibleVotersOnly) {
                this.parsePartiesForConstituency(row, constituency);
            }
        }
    }
    
    /**
     * Creates the constituencies ({@link Constituency}) and sets their values. Adds them to their belonging {@link FederalState}
     * 
     * @param row
     *            The row of the current {@link Constituency}
     * @param eligibleVotersOnly
     *            Determines if only the eligible voters shall be parsed.
     * @return
     * @throws FormatException
     *             Thrown if format is wrong.
     */
    private Constituency setConstituency(int row, final boolean eligibleVotersOnly) throws FormatException {
        String name = this.parser.get(row, Bundeswahlleiter2013Parser.POS_NAME).replaceAll("\"", "");
        Constituency constituency = new Constituency(name, this.result);
        
        String stringBelongsTo = this.parser.get(row, Bundeswahlleiter2013Parser.POS_BELONGSTO);
        int numberBelongsTo = Integer.parseInt(stringBelongsTo);
        FederalState state = this.result.getFederation().getChildren().get(numberBelongsTo - 1);
        state.addChild(constituency);
        
        this.setConstituencyValues(constituency, row, eligibleVotersOnly);
        return constituency;
    }
    
    /**
     * Sets the values of the {@link Constituency}.
     * 
     * @param constituency
     *            The {@link Constituency} to set the values for.
     * @param row
     *            The row of the {@code constituency}.
     * @param eligibleVotersOnly
     *            Determines if only the eligible voters shall be parsed.
     * @throws FormatException
     *             Thrown if format is wrong.
     */
    private void setConstituencyValues(final Constituency constituency, int row, final boolean eligibleVotersOnly) throws FormatException {
        int number = this.checkNumber(this.parser.get(row, Bundeswahlleiter2013Parser.POS_ELIGIBLE_VOTERS));
        if(!constituency.setEligibleVoters(number)) {
            throw new FormatException("Expected valid number for eligible voters, but got \"" + number + "\".");
        }
        
        if(!eligibleVotersOnly) {
            number = this.checkNumber(this.parser.get(row, Bundeswahlleiter2013Parser.POS_VOTERS));
            if(!constituency.setNumberOfVotes(number)) {
                throw new FormatException("Expected valid number for voters, but got \"" + number + "\".");
            }
            constituency.setCitizens(constituency.getEligibleVoters()); // works if setEligibleVoters worked.
        }
    }
    
    /**
     * Parses the parties for the {@link Constituency}.
     * 
     * @param row
     *            The row of the {@code constituency}.
     * @param constituency
     *            The {@link Constituency} to parse the parties for.
     * @throws FormatException
     *             Thrown if format is wrong.
     */
    private void parsePartiesForConstituency(int row, Constituency constituency) throws FormatException {
        int index = 0;
        for(int k = Bundeswahlleiter2013Parser.POS_FIRST_PARTY; k < this.parser.getColumnLength(); k += Bundeswahlleiter2013Parser.STEP) {
            this.setConstituencyVotes(constituency, this.parties.get(index++), row, k);
        }
    }
    
    /**
     * Sets the {@link Constituency} votes for given {@link Party}.
     * 
     * @param constituency
     *            The {@link Constituency} to set the votes for.
     * @param party
     *            The {@link Party} to set the votes for.
     * @param row
     *            The row of the {@code constituency}.
     * @param column
     *            The column of the {@code constituency}.
     * @throws FormatException
     *             Thrown if format is wrong.
     */
    private void setConstituencyVotes(final Constituency constituency, final Party party, int row, int column) throws FormatException {
        // assert 'column' is correct column number to 'party'
        Optional<Integer> firstVote = Optional.empty();
        Optional<Integer> secondVote = Optional.empty();
        
        try {
            firstVote = Optional.ofNullable(this.checkNumberEmptyNull(this.parser.get(row, column)));
            secondVote = Optional.ofNullable(this.checkNumberEmptyNull(this.parser.get(row, column + Bundeswahlleiter2013Parser.STEP_TO_SECOND_VOTES)));
        }
        catch(FormatException fe) {
            throw new FormatException(row, column, fe);
        }
        
        if(firstVote.isPresent() || secondVote.isPresent()) {
            constituency.addParty(party);
            if(firstVote.isPresent()) {
                if(!constituency.setFirstVotes(party, firstVote.get())) {
                    throw new FormatException("Expected valid first votes for party \"" + party.getName() + "\", but got \"" + firstVote.get() + "\".");
                }
            }
            if(secondVote.isPresent()) {
                if(!constituency.setSecondVotes(party, secondVote.get())) {
                    throw new FormatException("Expected valid second votes for party \"" + party.getName() + "\", but got \"" + secondVote.get() + "\".");
                }
            }
        }
    }
    
    @Override
    public ElectionResult parseEligibleVoters() throws FormatException {
        this.initialize();
        this.parseHeadColumns();
        this.selectLines();
        this.parseFederation();
        this.parseFederalStates();
        this.parseConstituencies(true);
        return this.result;
    }
    
    @Override
    protected int minNumberOfColumns() {
        // {"Nr", "Gebiet", "gehört", "Wahlberechtigte", "", "", "", "Wähler", "", "", "", "Gültige", "", "", "",  "Ungueltige", "", "", ""}
        return 19;
    }
}
