/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.gui;

import java.awt.Color;
import java.awt.LayoutManager;
import java.util.List;

import javax.swing.JPanel;

/**
 * {@code Diagram} is a super class to indicate, that the JPanel extending this one can be displayed as a Diagram in a visualisation.
 * 
 * @author Felix Heim
 * @version 1
 */
abstract class Diagram extends JPanel {
    private static final long serialVersionUID = 1L;
    
    /** the values to display */
    protected List<DisplayableValues> values;
    
    Diagram() {
    }
    
    Diagram(LayoutManager layoutManager) {
        super(layoutManager);
    }
    
    /**
     * This sets the values to be displayed and calculates everything, which is useful to display the values (e.g. the sum of all values).<br>
     * 
     * @param values
     *            the new values to be displayed
     */
    protected abstract void setValues(List<Diagram.DisplayableValues> values);
    
    /**
     * This class represents a Triple containing a {@link String} description, a {@link Double} value and a {@link Color} in which the other two are displayed
     * 
     * @author Felix Heim
     * @version 1
     */
    protected class DisplayableValues {
        protected String description;
        protected double value;
        protected Color color;
        
        protected DisplayableValues(String description, double value, Color color) {
            this.description = description;
            this.value = value;
            this.color = color;
        }
    }
}
