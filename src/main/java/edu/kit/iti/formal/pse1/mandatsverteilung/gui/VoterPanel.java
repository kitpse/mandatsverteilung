/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.gui;

import java.util.function.IntPredicate;
import java.util.function.Supplier;

/**
 * A {@link VoterPanel} basically displays the Information a {@link NamedPanel} can display, but only the first line and not for second and first votes, but for
 * other values (as defined by the getters in the constructor).
 * 
 * @author Felix Heim
 * @version 1
 */
class VoterPanel extends NamedPanel {
    private static final long serialVersionUID = 1L;
    
    /**
     * @param name
     *            the name of the panel
     * @param votesPart
     *            the getter for the value to display
     * @param votesTotal
     *            the getter for the maximum of the value to display
     */
    VoterPanel(String name, Supplier<Integer> votesPart, Supplier<Integer> votesTotal) {
        super(name, votesPart, votesTotal, null, null);
    }
    
    /**
     * Sets the setters which are used to set the values in the area when they get changed in the GUI.
     * 
     * @param votesSetter
     *            the setter to set the actual number of votes
     * @return the same panel
     */
    NamedPanel setSetter(IntPredicate votesSetter) {
        return super.setSetter(votesSetter, null);
    }
    
    @Override
    void addSecondVotes(String labelName) {
        // normally add it but with a different name ...
        super.addSecondVotes("Anzahl");
        // ... and without the option to expand
        this.disableButtonShowDetails();
    }
    
    @Override
    void addFirstVotes(String labelName) {
        // empty because we don't need a second line
    }
    
    @Override
    void updateFirstVotes() {
        // no second line -> no need to update it
    }
}
