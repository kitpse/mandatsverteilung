/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.gui;

import java.util.function.ToDoubleFunction;
import java.util.stream.Collectors;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.management.ElectionComparison;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Election;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Federation;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Party;

/**
 * This class represents the difference between two {@link Federation}s (of some kind of value of an Party, e.g. second votes). It uses an ordinary BarChart but
 * only renders party where the votes are displayable (which means at least one pixel bar height). The values are shown as percent.
 * 
 * @author Felix Heim
 * @version 1
 */
public class ComparisonBarChartPercent extends ComparisonBarChart {
    private static final long serialVersionUID = 1L;
    
    /** the {@link ElectionComparison} the values are from */
    private ElectionComparison comparison;
    
    /** the function used to calculate the difference between the two {@link Election}s for the specific {@link Party} */
    private ToDoubleFunction<Party> differenceFunction;
    
    /**
     * @param comparison
     *            the {@link ElectionComparison} the values are from
     * @param differenceFunction
     *            the function used to calculate the difference between the two {@link Election}s for the specific {@link Party}
     * @param usePercent
     *            whether percent values should be used rather than absolute values
     */
    ComparisonBarChartPercent(ElectionComparison comparison, ToDoubleFunction<Party> differenceFunction, boolean usePercent) {
        super(usePercent);
        this.comparison = comparison;
        this.differenceFunction = differenceFunction;
        comparison.addListener(this::update);
        this.update();
    }
    
    private void update() {
        int hurdle = (int) (this.comparison.getFirstElection().getElectionResult().getFederation().getNumberOfVotes() * 0.005); // defines the hurdle as 0.5%
        this.setValues(this.comparison
                .getParties()
                // create a stream of all Parties
                .stream()
                // we only want numbers which aren't to low, so filter out every Party where the second votes are below the hurdle
                .filter(party -> this.comparison.getFirstElection().getElectionResult().getFederation().getSecondVotes(party) > hurdle
                                 || this.comparison.getSecondElection().getElectionResult().getFederation().getSecondVotes(party) > hurdle)
                // sort the values with a descending order, so the parties with most of the seats in the first election get displayed first
                .sorted((party1, party2) -> Double.compare(this.comparison.getFirstElection().getBundestag().getSeats(party2), this.comparison
                                .getFirstElection().getBundestag().getSeats(party1)))
                // maps the party to a triple, containing a description (the name of the party + the seats), a double representing the amount of seats and the color of the party
                .map(party -> new DisplayableValues(party.getName(), this.differenceFunction.applyAsDouble(party), party.getColor()))
                // collect all elements to a list
                .collect(Collectors.toList()));
    }
}
