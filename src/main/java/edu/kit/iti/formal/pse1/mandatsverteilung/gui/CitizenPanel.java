/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.Optional;
import java.util.function.IntPredicate;
import java.util.function.Supplier;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.border.TitledBorder;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.ElectionArea;

class CitizenPanel extends JPanel {
    private static final long serialVersionUID = 1L;
    private Supplier<Integer> getter;
    private IntPredicate setter;
    
    private int max;
    
    private GridBagConstraints c = new GridBagConstraints();
    private boolean isUpdating;
    
    private JTextField numberField;
    private JTextField numberFieldPercent;
    private JSlider slider;
    
    /**
     * 
     * @param name
     *            the name to be displayed in the {@link TitledBorder}
     * @param getter
     *            the getter for the actual number of citizens
     * @param setter
     *            the setter for the actual number of citizens
     */
    CitizenPanel(String name, Supplier<Integer> getter, IntPredicate setter) {
        this.getter = getter;
        this.setter = setter;
        this.max = getter.get();
        this.setLayout(new GridBagLayout());
        this.setBorder(BorderFactory.createTitledBorder(name));
        
        this.c.weightx = 0.01;
        
        // in the line: first add the label ... 
        this.c.gridx = 0;
        this.c.gridy = 0;
        this.c.weighty = 1;
        JLabel label = new JLabel("Anzahl");
        label.setPreferredSize(NamedPanel.getLabelSize());
        this.add(label, this.c);
        
        // ... next the actual field with the number ...
        this.c.gridx = 1;
        this.numberField = new JTextField(7);
        // add validator functions to validate the input in the fields
        GuiHelper.addValidator(this, this.numberField, (textField) -> {
            try {
                int number = Integer.parseInt(textField.getText().replaceAll("\\s", ""));
                if(number < 0 || !this.setter.test(number)) {
                    textField.setText(Integer.toString(this.getter.get()));
                }
            }
            catch(NumberFormatException e) {
                textField.setText(Integer.toString(this.getter.get()));
            }
        });
        this.add(this.numberField, this.c);
        
        // ... then the percent ...
        this.c.gridx = 2;
        this.numberFieldPercent = new JTextField(4);
        // again: when entered a new percent, the field needs to be validated
        GuiHelper.addValidator(this, this.numberFieldPercent, (textField) -> {
            Optional<Double> percent = GuiHelper.percentStringToDouble(textField.getText());
            if(!percent.isPresent() || !(percent.get() < 0) || !this.setter.test((int) (percent.get() * this.getter.get()))) {
                textField.setText(GuiHelper.doubleToPercentString((double) this.getter.get() / this.max));
            }
        });
        this.add(this.numberFieldPercent, this.c);
        
        // ... then comes the button to subtract votes ...
        this.c.gridx = 3;
        this.slider = new JSlider(SwingConstants.HORIZONTAL);
        JButton minus = new JButton("-");
        // reduce by 10% each time pressed
        minus.addActionListener(e -> this.slider.setValue(Math.max(this.slider.getValue() - this.max / 10, 0)));
        this.add(minus, this.c);
        
        // ... in the middle the actual slider ...
        this.c.gridx = 4;
        this.c.weightx = 1000;
        this.c.fill = GridBagConstraints.BOTH;
        this.slider.setMinimum(0);
        this.slider.setMaximum(this.max);
        this.slider.addChangeListener(e -> {
            /* only fire an update, if we aren't currently updating the values and if the slider isn't in movement anymore */
            if(!this.slider.getValueIsAdjusting() && !this.isUpdating && this.slider.getValue() != getter.get()) {
                if(!this.setter.test(this.slider.getValue())) {
                    SwingUtilities.invokeLater(() -> this.slider.setValue(this.getter.get()));
                }
            }
        });
        this.add(this.slider, this.c);
        
        this.c.weightx = 0.1;
        this.c.fill = GridBagConstraints.NONE;
        
        // ... the plus button to the right of the slider ...
        this.c.gridx = 5;
        JButton plus = new JButton("+");
        // increase by 10% each time pressed
        plus.addActionListener(e -> {
            int sliderValue = this.slider.getValue();
            if(sliderValue == this.max) {
                /* if we are at the maximum, then increment in steps of 5% of the original maximum */
                sliderValue += this.max / 20;
                this.slider.setMaximum(this.max = sliderValue);
                this.slider.setValue(sliderValue);
            }
            else {
                /* otherwise try 10% steps of the original max till the max */
                this.slider.setValue(Math.min(sliderValue + this.max / 10, this.max));
            }
        });
        this.add(plus, this.c);
        
        this.c.gridwidth = 2;
        this.c.gridx = 6;
        JButton dummyButton = new JButton("Details");
        dummyButton.setPreferredSize(NamedPanel.getButtonDimension());
        dummyButton.setEnabled(false);
        this.add(dummyButton, this.c);
        
        this.update();
    }
    
    CitizenPanel addListenerToArea(ElectionArea<?> area) {
        area.addChangeListener(this::update);
        return this;
    }
    
    /**
     * registers a listener updating this panel at the specified {@link ElectionArea}
     * 
     * @param area
     *            the {@link ElectionArea} where to register the listener
     * @return the same instance
     */
    void update() {
        this.isUpdating = true;
        
        int part = this.getter.get();
        this.numberField.setText(Integer.toString(part));
        this.numberFieldPercent.setText(GuiHelper.doubleToPercentString((double) part / this.max));
        this.slider.setMaximum(this.max = Math.max(part, this.slider.getMaximum()));
        this.slider.setValue(part);
        
        this.isUpdating = false;
    }
}
