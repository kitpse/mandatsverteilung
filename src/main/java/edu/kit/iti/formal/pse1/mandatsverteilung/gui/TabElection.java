/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.gui;

import java.util.function.Function;

import javax.swing.JTabbedPane;

import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Election;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.ElectionArea;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.ElectionResult;
import edu.kit.iti.formal.pse1.mandatsverteilung.data.model.Federation;

/**
 * A Tab containing all the {@link Tab}s to display the election data from an {@link ElectionResult}
 * 
 * @author Felix Heim
 * @version 1
 */
public class TabElection extends JTabbedPane {
    private static final long serialVersionUID = 1L;
    
    /** the {@link Election} for which to create the tab */
    private Election election;
    
    /**
     * @param election
     *            the {@link Election} for which to create the tab
     */
    public TabElection(Election election) {
        this.election = election;
        this.setName(election.getName());
        this.populateWithTabs();
    }
    
    private void populateWithTabs() {
        Federation federation = this.election.getElectionResult().getFederation();
        
        // the tab containing the election data for the federation
        this.addTab("Bund", new ElectionData(federation, true).getContentPane());
        
        // a function to create an election data panel containing all the party panels
        Function<ElectionArea<?>, Tab> electionDataCreator = area -> new ElectionData(area, false);
        // the tab containing the election data for the federal states
        this.addTab("L\u00E4nder", new EncapsulatedElectionData(federation, true, electionDataCreator).getContentPane());
        
        // a function to create an encapsulated election data panel containing an election data panel containing all the party panels
        Function<ElectionArea<?>, Tab> encapuslatedElectionDataCreator = area -> new EncapsulatedElectionData(area, false, electionDataCreator);
        // the tab containing the election data for the constituencies
        this.addTab("Wahlkreise", new EncapsulatedElectionData(federation, true, encapuslatedElectionDataCreator).getContentPane());
        
        // the tab containing all the settings
        this.addTab("Einstellungen", new TabSettings(this.election.getSettings(), this.election.getElectionResult().getPropagation()).getContentPane());
        // the tab containing all the diagrams
        this.addTab("Visualisierung", new TabVisualisation(this.election).getContentPane());
    }
    
    public Election getElection() {
        return this.election;
    }
}
