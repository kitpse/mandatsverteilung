/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.gui;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.font.FontRenderContext;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This class is a Diagram which renders a List of {@link DisplayableValues} as a BarChart with its description to the top and the percent it is representing
 * below. It is specialized to display both, positive and negative values.<br>
 * 
 * @author Felix Heim
 * @version 2
 */
public abstract class ComparisonBarChart extends Diagram {
    private static final long serialVersionUID = 1L;
    
    /** the minimum width of a bar */
    private static final int MINIMUM_WIDTH = 25;
    
    /** the height if the BARS */
    private static final int BAR_HEIGHT = 200;
    
    /** whether percent values should be used rather than absolute values */
    private boolean usePercent;
    
    /** the amount of values to display */
    private int valueCount;
    
    /** the minimum ideal width of the chart */
    private int requiredWidth;
    
    /** the amount of spaces (bars + spaces inbetween) required */
    private int requiredSpaces;
    
    /** the highest value */
    private double maximumValue;
    
    /** the lowest value */
    private double minimumValue;
    
    /** the scale to multiply the value with, so the are displayed with the right height */
    private double scale;
    
    /** the height representing zero percent */
    private int xAxis;
    
    /**
     * @param usePercent
     *            whether percent values should be used rather than absolute values
     */
    ComparisonBarChart(boolean usePercent) {
        this.usePercent = usePercent;
    }
    
    @Override
    protected void setValues(List<DisplayableValues> values) {
        // first determine the highest and the lowest value
        this.maximumValue = Integer.MIN_VALUE;
        this.minimumValue = Integer.MAX_VALUE;
        for(DisplayableValues vals : values) {
            if(vals.value > this.maximumValue) {
                this.maximumValue = vals.value;
            }
            if(vals.value < this.minimumValue) {
                this.minimumValue = vals.value;
            }
        }
        
        // next determine the divisor used to calculate the scale and the zeroHeight. it depends on if we do only have positive values, negative values or both
        double divisor;
        if(this.maximumValue > 0 && this.minimumValue >= 0) {
            divisor = this.maximumValue;
        }
        else if(this.maximumValue > 0 && this.minimumValue < 0) {
            divisor = this.maximumValue - this.minimumValue;
        }
        else {
            divisor = -this.minimumValue;
        }
        this.scale = ComparisonBarChart.BAR_HEIGHT / divisor; // calculate the scale used to scale the values to the right height
        this.values = values.stream()
        // filter out values where the bar would be smaller than one pixel
                .filter(vals -> Math.abs(vals.value * this.scale) >= 1).collect(Collectors.toList());
        
        this.valueCount = this.values.size();
        this.requiredSpaces = 2 * this.valueCount - 1;
        this.requiredWidth = this.requiredSpaces * ComparisonBarChart.MINIMUM_WIDTH;
        this.xAxis = (int) (this.maximumValue / divisor * ComparisonBarChart.BAR_HEIGHT + 15); // calculate the height of zero percent (x-axis)
    }
    
    @Override
    public Dimension getPreferredSize() {
        return new Dimension(Math.max(775, super.getPreferredSize().width), ComparisonBarChart.BAR_HEIGHT + 40); // define a minimum width and set a fix height
    }
    
    @Override
    protected void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON); // use antialiasing
        // helper to calculate the width of a String to center it
        FontRenderContext frc = g2d.getFontRenderContext();
        Font font = g2d.getFont();
        
        int totalWidth = this.getSize().width;
        int barWidth;
        int spaceWidth;
        if(totalWidth < this.requiredWidth) { // make sure the bar width doesn't undergoes a minimum width
            barWidth = ComparisonBarChart.MINIMUM_WIDTH;
            spaceWidth = Math.max(0, (totalWidth - barWidth * this.valueCount) / (this.valueCount - 1));
        }
        else {
            barWidth = spaceWidth = totalWidth / this.requiredSpaces;
        }
        
        double barWidthHalf = barWidth / 2.0;
        int x = 0;
        int i = 0;
        for(DisplayableValues values : this.values) {
            i++;
            int value = (int) (values.value * this.scale);
            g.setColor(values.color);
            
            double descriptionWidth = font.getStringBounds(values.description, frc).getWidth(); // the width of the description
            String valueString = this.usePercent ? GuiHelper.doubleToPercentString(values.value) + "%" : Integer.toString((int) values.value); // the value to be displayed above the Bar
            double valueWidth = font.getStringBounds(valueString, frc).getWidth(); // the width of the String representing the value
            
            // calculate the x position of the description
            int xDescription = (int) (x == 0 ? x // the first element gets left aligned
                    : i == this.values.size() ? x - descriptionWidth + barWidth // the last element gets right aligned 
                    : x + barWidthHalf - descriptionWidth / 2); // every other item gets centered
            
            // calculate the x position of the percent strings
            int xValue = (int) (x == 0 ? x // the first element gets left aligned
                    : i == this.values.size() ? x - valueWidth + barWidth // the last element gets right aligned
                    : x + barWidthHalf - valueWidth / 2); // every other item gets centered
            
            if(value > 0) { // different rendering based on if we have a positive or negative value
                g.fillRect(x, this.xAxis - value, barWidth, value); // render the bar
                g.drawString(values.description, xDescription, this.xAxis + 12); // render the description
                g.drawString(valueString, xValue, this.xAxis - value - 2); // render the percent string
            }
            else {
                g.fillRect(x, this.xAxis, barWidth, -value); // render the bar
                g.drawString(values.description, xDescription, this.xAxis - 2); // render the description
                g.drawString(valueString, xValue, this.xAxis - value + 12); // render the percent string
            }
            x += barWidth + spaceWidth; // move forward
        }
        g.dispose();
    }
}
