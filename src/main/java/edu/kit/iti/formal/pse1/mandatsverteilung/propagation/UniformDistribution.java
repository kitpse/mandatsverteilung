/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.propagation;

/**
 * PropagationMath that use a uniform distribution to propagate.
 * 
 * @author Ben Wilhelm
 *
 */
public class UniformDistribution extends SimpleDistribution implements PropagationMath {
    
    @Override
    public void propagate(int[] bins, int[] maxBins, int elements) {
        assert bins.length != 0;
        assert bins.length == maxBins.length;
        // maxBins should be greater than bins in every entry
        // assert: all array elements must be non negative
        // assert: if elements is negative, the sum of all array elements must be greater or equal -elements
        // assert: if elements is positive, the sum of all maxBins - sum of array elements must be >= elements
        
        // contains the value that is left to distribute
        int leftToDistribute = elements;
        
        // number of bins to consider (if leftToDistribute is positive, number of bins that are not at its max value,
        // else number of non zero bins)
        int leftBins = this.initializeLeftBins(bins, maxBins, leftToDistribute);
        
        leftToDistribute = this.regularDistribution(bins, maxBins, leftBins, leftToDistribute);
        
        this.randomDistribution(bins, maxBins, leftToDistribute);
    }
    
    @Override
    /*
     * Calculates the amounts each bin should be changed, depending on the given inputs. Does not need to check if the change is possible!
     */
    /*
     * This method essentially changes the behavior of the propagation!
     */
    //                                                                               leftBins might be zero
    int[] calculateChangePerBin(int[] bins, int[] maxBins, int leftToDistribute, int leftBins) {
        int[] changePerBin = new int[bins.length];
        
        for(int i = 0; i != bins.length; i++) {
            if(leftBins == 0) {
                changePerBin[i] = 0;
            }
            else {
                changePerBin[i] = leftToDistribute / leftBins;
            }
        }
        
        return changePerBin;
    }
    
    @Override
    public String getDescription() {
        return "Gleichverteilte Propagierung";
    }
    
    /* only check if it's the same class */
    @Override
    public boolean equals(Object obj) {
        return obj.getClass().equals(this.getClass());
    }
    
}
