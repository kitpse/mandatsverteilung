/*-
 * Copyright (C) 2015 Alexander Grünen, Felix Heim, Axel Trefzer, Ben Wilhelm and Nils Wilka. 
 * 
 * This file is part of "ManData Simulation". 
 *
 * "ManData Simulation" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "ManData Simulation" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "MandataSimulation"; see the file COPYING.TXT.
 * If not, see <http://www.gnu.org/licenses/>.
 */

package edu.kit.iti.formal.pse1.mandatsverteilung.propagation;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

import edu.kit.iti.formal.pse1.mandatsverteilung.calculation.CalculationMethod;
import edu.kit.iti.formal.pse1.mandatsverteilung.gui.controller.Mandatsverteilung;

/**
 * PropagationMath that uses normal distributions to propagate.
 * 
 * @author Alexander Grünen
 * @version 1.0
 */
public class NormalDistribution implements PropagationMath {
    private List<NDParameter> parameters;
    
    /* locally used */
    private ProportionalDistribution proportionalDistribution = new ProportionalDistribution();
    
    public NormalDistribution() {
        this.parameters = new ArrayList<NDParameter>(1);
        this.parameters.add(new NDParameter(1, 0.1, 1));
    }
    
    @Override
    public void propagate(int[] bins, int[] maxBins, int elements) {
        int binCount = bins.length;
        int[] indices = new int[binCount];
        int orgSum = 0;
        int newSum = 0;
        Random r = new Random();
        double g;
        double[] partition = new double[this.parameters.size()];
        double[] doms = new double[this.parameters.size()];
        double expectationUD; // expected value of a uniform distribution
        
        // calculate original number of elements
        for(int j = 0; j < binCount; j++) {
            orgSum = orgSum + bins[j];
        }
        
        // fill array of indices
        for(int j = 0; j < binCount; j++) {
            indices[j] = j;
        }
        
        // shuffle the array of indices using Fisher–Yates algorithm
        for(int j = binCount - 1; j > 0; j--) {
            int a = (int) (Math.random() * (j + 1));
            int tmp = indices[a];
            indices[a] = indices[j];
            indices[j] = tmp;
        }
        
        // fill array of dominance values
        for(int j = 0; j < this.parameters.size(); j++) {
            doms[j] = this.parameters.get(j).dominance;
        }
        
        // fill partition array
        if(Mandatsverteilung.debug) {
            System.out.println("before SLS");
        }
        CalculationMethod.sls(doms, partition, binCount);
        if(Mandatsverteilung.debug) {
            System.out.println("after SLS");
        }
        
        expectationUD = elements / binCount;
        for(int j = 0; j < this.parameters.size(); j++) {
            for(int k = 0; k < binCount; k++) {
                do {
                    g = r.nextGaussian();
                } while(g < -1 && g > 1); // discard extremes
                bins[indices[k]] += (int) Math
                        .round(elements
                               / partition[j]
                               * (this.parameters.get(j).expectation * expectationUD + g
                                                                                       * (this.parameters.get(j).standardDeviation
                                                                                          * this.parameters.get(j).expectation * expectationUD)));
            }
        }
        
        for(int j = 0; j < binCount; j++) {
            if(bins[j] < 0) {
                bins[j] = 0;
            }
            else if(bins[j] > maxBins[j]) {
                bins[j] = maxBins[j];
            }
        }
        
        for(int j = 0; j < binCount; j++) {
            newSum = newSum + bins[j];
        }
        
        if(Mandatsverteilung.debug) {
            System.out.println("bins=" + bins.length);
        }
        this.proportionalDistribution.propagate(bins, maxBins, orgSum - newSum + elements);
        
        newSum = 0;
        for(int j = 0; j < binCount; j++) {
            newSum = newSum + bins[j];
        }
        for(int j = 0; j < binCount; j++) {
            if(bins[j] < 0) {
                throw new IllegalStateException("Bin has negative amount of elements after normal distribution!");
            }
        }
        if(newSum - orgSum != elements && newSum != 0) {
            throw new IllegalStateException("NormalDistribution did not propagate the correct amount, though possible!");
        }
        
    }
    
    /**
     * @param parameters
     *            the parameters to set
     */
    public void setParameters(List<NDParameter> parameters) {
        this.parameters = parameters;
    }
    
    @Override
    public String getDescription() {
        return "Normalverteilte Propagierung";
    }
    
    @Override
    public Optional<List<? extends Object>> getParameters() {
        assert this.parameters != null;
        return Optional.of(this.parameters);
    }
    
    /* only check if it's the same class */
    @Override
    public boolean equals(Object obj) {
        return obj.getClass().equals(this.getClass());
    }
    
    /**
     * This class saves and checks the parameters of a normal distribution.
     * 
     * @author Alexander Grünen
     * @version 1.0
     */
    public class NDParameter {
        private double expectation;
        private double standardDeviation;
        private double dominance;
        
        public NDParameter(double expectation, double standardDeviation, double dominance) throws IllegalArgumentException {
            if(expectation <= 0 || expectation > 2) {
                throw new IllegalArgumentException("Erwartungswerst ist nicht im g\u00FCltigen Bereich von (0,2]");
            }
            this.expectation = expectation;
            
            if(standardDeviation <= 0) {
                throw new IllegalArgumentException("Standardabweichung ist nicht im g\u00FCltigen Bereich von (0,\u221E)");
            }
            this.standardDeviation = standardDeviation;
            
            if(dominance <= 0) {
                throw new IllegalArgumentException("Dominanz ist nicht im g\u00FCltigen Bereich von (0,\u221E)");
            }
            this.dominance = dominance;
        }
        
        @Override
        public String toString() {
            return this.expectation + "," + this.standardDeviation + "," + this.dominance;
        }
    }
}
